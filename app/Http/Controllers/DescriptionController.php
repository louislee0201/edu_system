<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Model\Description;
use App\Model\DescriptionInfo;
use App\Model\DescriptionInfoSub;

use App\Tools\GlobalList;
use App\Components\Notification;

use DB;
use Mail;

class DescriptionController
{
    public function test(Request $request)
    {
        return view('emails.2021',[
            'subject'=> 'Title Subject',
            'content' => 'testing some message'
        ]);
        Notification::sendMail();
    }

    public function getDescriptionType(Request $request)
    {
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'descriptionTypeList'=> Description::DESCRIPTION_TYPE
            ]
        ]);
    }

    public function getDescriptionList(Request $request)
    {
        $classMode = $request->get('classMode');
        $related_id = $request->get('related_id');

        if(isset($classMode,$related_id))
        {
            if($classMode == 'course')
            {
                $model = Description::where('school_course_id',$related_id)->orderBy('range_key')->get();
            }
            else
            {
                $model = Description::where('school_id',$related_id)->orderBy('range_key')->get();
            }

            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                'error'=>'',
                'data'=>[
                    'descriptionList'=> $model
                ]
            ]);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']); 
    }   

    public function getDescriptionData(Request $request)
    {
        $related_id = $request->get('related_id');
        $mode = $request->get('mode');

        if(isset($related_id,$mode))
        {
            $newDsInfoSubModel = [];
            $newDsInfoModel = [];
            if($mode == 'info')
            {
                $dsInfoModel = DescriptionInfo::where('description_id',$related_id)->orderBy('range_key')->get();
                
                foreach ($dsInfoModel as $modelKey => $model) 
                {
                    if($model->picture !== null)
                    {
                        $pictureList = json_decode($model->picture);
                        $pictureBinary = [];
                        foreach ($pictureList as $picKey => $pic) 
                        {
                            $explode = explode('.', $pic);
                            $name = $explode[0];
                            $fileType = $explode[1];
                            $date = date("Y-m-d", $name);
                            $des = storage_path('app/'.Description::BASE_DESCRIPTION_IMAGE_URL.'/'.$date.'/'.$pic);

                            $fileContent = '';
                            if(file_exists($des))
                            {
                                $content = file_get_contents($des);
                                $base64 = base64_encode($content);
                                $fileContent = 'data:image/' . $fileType . ';base64,' . $base64;
                            }
                            
                            $pictureBinary[] = $fileContent;
                        }

                        $model->picture = json_encode($pictureBinary);
                    }

                    $newDsInfoModel[] = $model;
                }
            }
            else
            {
                $dsInfoSubModel = DescriptionInfoSub::where('description_info_id',$related_id)->orderBy('range_key')->get();

                foreach ($dsInfoSubModel as $modelKey => $model) 
                {
                    if($model->picture !== null)
                    {
                        $pictureList = json_decode($model->picture);
                        $pictureBinary = [];
                        foreach ($pictureList as $picKey => $pic) 
                        {
                            $explode = explode('.', $pic);
                            $name = $explode[0];
                            $fileType = $explode[1];
                            $date = date("Y-m-d", $name);
                            $des = storage_path('app/'.Description::BASE_DESCRIPTION_IMAGE_URL.'/'.$date.'/'.$pic);
                            
                            $fileContent = '';
                            if(file_exists($des))
                            {
                                $content = file_get_contents($des);
                                $base64 = base64_encode($content);
                                $fileContent = 'data:image/' . $fileType . ';base64,' . $base64;
                            }
                            
                            $pictureBinary[] = $fileContent;
                        }

                        $model->picture = json_encode($pictureBinary);
                    }

                    $newDsInfoSubModel[] = $model;
                }
            }

            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                'error'=>'',
                'data'=>[
                    'descriptionInfoList'=> $newDsInfoModel,
                    'descriptionInfoSubList'=> $newDsInfoSubModel
                ]
            ]);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']); 
    }
}
