<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Student;
use App\Model\Config;

use App\Tools\Tools;
use App\Tools\GlobalList;

use DB;
use Mail;

class ConfigController
{
	public function getHomePageData(Request $request)
    {   
        $banner = Config::where('type',Config::TYPE['BANNER'])->first();
    	return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
            	'banner'=>json_decode($banner->content)
            ]
        ]);
    }

    public function submidSeminarsForm(Request $request)
    {   
        $name = $request->get('name');
        $email = $request->get('email');
        $contact = $request->get('contact');
        $proficiency_level = $request->get('proficiency_level');
        $nationality = $request->get('nationality');
        $japan_school = $request->get('japan_school');

        if(isset(
            $name,
            $email,
            $contact,
            $proficiency_level,
            $nationality
        ))
        {
            Mail::raw('
                Name: '.$name.'
                Email: '.$email.'
                Contact: '.$contact.'
                Proficiency Level: '.$proficiency_level.'
                Nationality: '.$nationality.'
                Japanese School: '.$japan_school.'
            ',function($message){
                $message ->to(Config::EMAIL)->subject('EDUDOTCOM student submit seminars 2021/09/29 form');
            });
//             <div style="text-align:center;width:100%;"> 
//     <img style="width:100%" src="https://www.edudotcom.my/auth/image/description/1632926199.png" />
// </div>
            Mail::raw('
本中心确认了您的报名，欢迎参加该次连同立命馆亚洲太平洋大学所举办的线上说明会。
 
日期：06-10-2021（星期三）
时间：15:00~14:00 （日本、韩国时间）/ 14:00~15:00 （中国、马来西亚、新加坡时间）
 
该项说明会将利用Zoom平台进行。您可以在活动开始前30分钟内点击以下链接进入：
 
https://us02web.zoom.us/j/89694174443?pwd=WDVFT3NnanVxUWFYRUtvL09JZ0F6QT09
 
（房号）Meeting ID: 896 9417 4443
（密码）Passcode: edudotcom
 
相关活动或留学疑问，请联络：
Whatsapp: +6016-7023776
微信号: edudotcom
 
=============================================================================
 
We confirmed your registration for Ritsumeikan Asia Pacific University’s seminar!
 
Date: 06-10-2021 (Wed)
Time: 15:00~14:00 （Japan, Korea @GMT+9）/ 14:00~15:00 （China, Malaysia, Singapore @GMT+8）
 
This seminar will conduct with Zoom. You may login to the seminar room via below link before 30 minutes of the event.
 
https://us02web.zoom.us/j/89694174443?pwd=WDVFT3NnanVxUWFYRUtvL09JZ0F6QT09
 
Meeting ID: 896 9417 4443
Passcode: edudotcom
 
For more enquiry, please do not feel hesitate to contact our counsellor.
Whatsapp: +6016-7023776
Wechat ID: edudotcom
            ',function($message) use ($email){
                $message->to($email)->subject('EDUDOTCOM student submit seminars 2021/09/29 form');
            });

            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                'error'=>'',
                'data'=> true
            ]);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function submidApplyForm(Request $request)
    {   
        $name = $request->get('name');
        $email = $request->get('email');
        $contact = $request->get('contact');
        $remark = $request->get('remark');
        $form = $request->get('form');
        $link = $request->get('link');
        
        if(isset(
            $form,
            $name,
            $contact,
            $email,
            $link
        ))
        {
            Mail::raw('
                Form type: '.$form.'
                Name: '.$name.'
                Email: '.$email.'
                Contact: '.$contact.'
                Remark: '.$remark.'
                Link: '.$link.'
            ',function($message){
                $message ->to(Config::EMAIL)->subject('EDUDOTCOM student submit form');
            });
            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                'error'=>'',
                'data'=> true
            ]);
        }
    }

    public function guessRegister(Request $request)
    {
        $model = new Student;
        $model->guess_token = encrypt(Tools::getToken(Student::max('id')+1));
        $model->name = 'Guess'.date("Ymd");
        $model->save();
        $model->refresh();
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'user_token'=>$model->guess_token,
                'account_level'=>$model->account_level
            ]
        ]);
    }

    public function logout(Request $request)
    {
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>''
        ]);
    }

    public function login(Request $request)
    {
        $userModel = $request->get('userModel');
        $email = $request->get('email');
        $password = $request->get('password');
        if(isset($email,$password))
        {
            $sModel = Student::where('email',$email)
            ->where('password',md5($password))
            ->first();

            if(empty($sModel))
            {
                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'message'=>'Invalid Student',
                        'login'=>false
                    ]
                ]);
            }

            if(!$sModel->active)
            {
                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'message'=>'Account was freeze, please contact principal',
                        'login'=>false
                    ]
                ]);
            }

            if($userModel->id != $sModel->id)
            {
                $userModel->delete();
            }

            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                'error'=>'',
                'data'=>[
                    'user_token'=>$sModel->user_token,
                    'account_level'=>$sModel->account_level,
                    'email'=>$sModel->email,
                    'tel'=>$sModel->tel,
                    'name'=>$sModel->name,
                    'ad'=> (!empty($sModel->school_id)?true:false),
                    'login'=>true
                ]
            ]);
        }
        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function checkLogin(Request $request)
    {
        $userModel = $request->get('userModel');
        
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                //'user_token'=>$sModel->user_token?$sModel->user_token:$sModel->guess_token,
                'account_level'=>$userModel->account_level,
                'email'=>$userModel->email,
                'tel'=>$userModel->tel,
                'name'=>$userModel->name
            ]
        ]);
        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function userRegister(Request $request)
    {
        $userModel = $request->get('userModel');
        $email = $request->get('email');
        $password = $request->get('password');
        if(isset($email,$password))
        {
            if(!empty(Student::where('email',$email)->first()))
            {
                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'message'=>'Repeat Email',
                        'register'=>false
                    ]
                ]);
            }

            $userModel->password = md5($password);
            $userModel->email = $email;
            $userModel->tel = $request->get('tel');
            $userModel->name = explode('@', $email)[0];
            $userModel->user_token = encrypt(Tools::getToken($userModel->id));
            $userModel->account_level = Student::ACCOUNT_LEVEL_USER;
            $userModel->save();

            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                'error'=>'',
                'data'=>[
                    'user_token'=>$userModel->user_token,
                    'account_level'=>$userModel->account_level,
                    'email'=>$userModel->email,
                    'tel'=>$userModel->tel,
                    'name'=>$userModel->name,
                    'register'=>true
                ]
            ]);
        }
        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }
}
