<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\SchoolCourse;
use App\Model\SchoolMajor;
use App\Model\Description;
use App\Model\SchoolCourseLevel;
use App\Model\SchoolCourseRequirement;
use App\Model\Qualification;
use App\Model\QualificationSubject;
use App\Model\QualificationGrade;
use App\Model\Region;
use App\Model\ProficiencyCountry;
use App\Model\Proficiency;
use App\Model\School;
use App\Model\MainTag;
use App\Model\SubTag;
use App\Model\Student;
use App\Model\ExchangeRate;

use App\Tools\Tools;
use App\Tools\GlobalList;

use DB;

class SchoolCourseController
{
    public function getSelection(Request $request)
    {
        $userModel = $request->get('userModel');
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'schoolTypeList'=>School::SCHOOL_TYPE,
                'courseLevelList'=>SchoolCourseLevel::where('active',true)->orderBy('range_key')->get(),
                'studyFieldList'=>MainTag::where('active',true)->orderBy('main_tag')->get(),
                'majorList'=>SubTag::where('active',true)->orderBy('sub_tag')->get(),
                'countryList'=>ProficiencyCountry::where('active',true)->orderBy('country')->get(),
                'regionList'=>Region::select('region.*')->leftJoin('proficiency_country as a', 'a.id', '=', 'region.country_id')->where('region.active',true)->where('a.active',true)->orderBy('region')->get(),
                'qualificationList'=>Qualification::where('active',true)->get(),
                'qualificationSubjectList'=>QualificationSubject::where('active',true)->orderBy('subject_name', 'ASC')->get(),
                'prificiencyList'=>Proficiency::where('active',true)->get(),
                'qualificationGradeList'=>QualificationGrade::all(),
                'studentAcademicResult'=>$userModel->student_academic_result->groupBy('qualification_id'),
                'currencyTypeList'=>ExchangeRate::CURRENCY_TYPE_LIST
            ]
        ]);
    }

	public function getCourse(Request $request)
    {   
        $isList = $request->get('isList');
        
        if(isset($isList))
        {
            if($isList)
            {
                return SchoolCourse::frontendGetCourseList($request);
            }

            $course_id = $request->get('course_id');

            if(isset($course_id))
            {
                $model = SchoolCourse::where('id',$course_id)->first();
                if(!empty($model))
                {
                    $picture = '';
                    if(!empty($model->picture))
                    {
                        if(strpos($_SERVER['HTTP_HOST'], '192.168') === false)
                        {
                            $picture = 'http://'.str_replace("www","api",$_SERVER['HTTP_HOST']).'/auth'.SchoolCourse::BASE_SCHOOL_COURSE_IMAGE_URL.$model->picture;
                        }
                        else
                        {
                            $picture = 'http://'.$_SERVER['HTTP_HOST'].'/auth'.SchoolCourse::BASE_SCHOOL_COURSE_IMAGE_URL.$model->picture;
                            //$picture = 'http://www.louis-blog.com/auth'.SchoolCourse::BASE_SCHOOL_COURSE_IMAGE_URL.$model->picture;
                        }
                    }

                    $logo = $model->school->logo;
                    if(!empty($logo))
                    {
                        if(strpos($_SERVER['HTTP_HOST'], '192.168') === false)
                        {
                            $logo = 'http://'.str_replace("www","api",$_SERVER['HTTP_HOST']).'/auth'.School::BASE_SCHOOL_IMAGE_URL.$logo;
                        }
                        else
                        {
                            $logo = 'http://'.$_SERVER['HTTP_HOST'].'/auth'.School::BASE_SCHOOL_IMAGE_URL.$logo;
                            //$logo = 'http://api.edudotcom.my/auth'.School::BASE_SCHOOL_IMAGE_URL.$logo;
                        }
                    }
                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'school_id'=>$model->school->id,
                            'duration'=>$model->duration,
                            'course_id'=>$model->id,
                            'course_name'=>$model->course_name,
                            'school_name'=>$model->school->school_name,
                            'course_level'=>$model->school_course_level_id,
                            'logo'=>$logo,
                            'picture'=>$picture,
                            'country'=>ProficiencyCountry::where('id',$model->school->country)->first()->country,
                            'region'=>Region::where('id',$model->school->region)->where('country_id',$model->school->country)->first()->region,
                            'interestList'=>SchoolCourse::getInterestSchoolCourseList($model,5),
                            'school_contact_me'=>$model->school->contact_me,
                            'contact_me'=>$model->contact_me,
                            'email'=>$model->school->user->username
                            //'descriptionList'=> Description::where('school_course_id',$course_id)->get(),
                            //'courseLevelList'=>SchoolCourseLevel::all(),
                            //'requirementList'=>$model->school_course_requirement->groupBy(['qualification_id','qualification_subject_id','requirement_type']),
                            //'requirementTypeList'=> SchoolCourseRequirement::REQUIREMENT_TYPE_LIST,
                            //'gradeTypeList'=>SchoolCourseRequirement::GRADE_TYPE,
                        ]
                    ]);
                }
                throw new \Exception('Invalid Course',GlobalList::API_RESPONSE_CODE['601']);
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
        
    }
}
