<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Model\Student;
use App\Model\StudentAcademicResult;

use App\Tools\GlobalList;

use DB;
use Mail;

class StudentAcademicResultController
{
    public function updateStudentAcademicResult(Request $request)
    {
        $user_token = $request->get('user_token');
        $studentQualificationSubjectList = $request->get('studentQualificationSubjectList');
        $qualification_id = $request->get('qualification_id');

        if(isset($user_token))
        {
            $userModel = Student::where('user_token',$user_token)->orWhere('guess_token',$user_token)->first();
            if(!empty($userModel) && isset($qualification_id,$studentQualificationSubjectList))
            {
                DB::delete('DELETE FROM tbl_student_academic_result WHERE student_id = '.$userModel->id.' AND  qualification_id = '.$qualification_id);
                foreach ($studentQualificationSubjectList as $subject_id => $grade_id) 
                {
                    $model = new StudentAcademicResult;
                    $model->qualification_id = $qualification_id;
                    $model->student_id = $userModel->id;
                    $model->qualification_grade_id = $grade_id;
                    $model->qualification_subject_id = $subject_id;
                    $model->save();
                }

                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'studentAcademicResult'=>$userModel->student_academic_result->groupBy('qualification_id')
                    ]
                ]);
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
        
    }
}
