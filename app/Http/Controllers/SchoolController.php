<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\School;
use App\Model\User;
use App\Model\ProficiencyCountry;
use App\Model\Description;
use App\Model\SchoolCourse;
use App\Model\Region;
use App\Model\Config;
use App\Model\SchoolCourseArrangement;

use App\Tools\Tools;
use App\Tools\GlobalList;

use DB;

class SchoolController
{
	public function getSchool(Request $request)
    {   
        $isList = $request->get('isList');
        if(isset($isList))
        {
            if($isList)
            {
                return School::frontendGetSchoolList($request);
            }

            $school_id = $request->get('school_id');

            if(isset($school_id))
            {
                $schoolModel = School::where('id',$school_id)->first();
                if(!empty($schoolModel))
                {
                    $picture = '';
                    if(!empty($schoolModel->picture))
                    {
                        if(strpos($_SERVER['HTTP_HOST'], '192.168') === false)
                        {
                            $picture = 'http://'.str_replace("www","api",$_SERVER['HTTP_HOST']).'/auth'.School::BASE_SCHOOL_IMAGE_URL.$schoolModel->picture;
                        }
                        else
                        {
                            $picture = 'http://'.$_SERVER['HTTP_HOST'].'/auth'.School::BASE_SCHOOL_IMAGE_URL.$schoolModel->picture;
                        }
                    }
                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'school_tel'=> $schoolModel->school_tel ,
                            'admin_tel'=>Config::TEL,
                            'admin_email'=>Config::EMAIL,
                            'school_id'=>$schoolModel->id,
                            'school_name'=>$schoolModel->school_name,
                            'school_name_cn'=>$schoolModel->school_name_cn,
                            'country'=>$schoolModel->country,
                            'address'=>$schoolModel->address,
                            'picture'=>$picture,
                            'official_website'=>$schoolModel->official_website,
                            'contact_me'=>$schoolModel->contact_me,
                            'email'=>$schoolModel->user->username,
                            'countryList'=>ProficiencyCountry::all(),
                            'descriptionList'=> Description::where('school_id',$school_id)->get(),
                            //'courseList'=> SchoolCourse::where('school_id',$school_id)->where('active',true)->get(),
                            'region'=>Region::where('country_id',$schoolModel->country)->where('id',$schoolModel->region)->first()->region,
                            'interestList'=>School::getInterestSchoolList($schoolModel,5),
                            'schoolArrangementList'=>SchoolCourseArrangement::where('school_id',$schoolModel->id)->orderBy('range_key','ASC')->get(),
                            'schoolCourseArrangementList'=>SchoolCourse::where('school_id',$schoolModel->id)->orderBy('range_key','ASC')->get()->groupBy('school_course_arrangement_id')
                        ]
                    ]);
                }
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
        
    }
}
