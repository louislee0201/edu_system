<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Hash;

use App\Tools\GlobalList;

use App\Model\Student;

use App\Tools\Tools;

class ApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $user_token = $request->get('user_token');

        
        if(isset($user_token) && $user_token)
        {
            $userModel = Student::where('user_token', $user_token)
                         ->orWhere('guess_token', $user_token)
                         ->first();

            if(empty($userModel))
            {
                throw new \Exception('Unauthorized',GlobalList::API_RESPONSE_CODE['602']);
            }

            if(!$userModel->active)
            {
                throw new \Exception('Account was freeze, please contact principal',GlobalList::API_RESPONSE_CODE['602']);
            }

            $request->attributes->add(['userModel' => $userModel]);
        }
        else
        {
            if( explode('/', $request->route()->uri)[1] != 'guessRegister')
            {
                throw new \Exception('Unauthorized',GlobalList::API_RESPONSE_CODE['602']);
            }
        }

        return $next($request);
    }
}
