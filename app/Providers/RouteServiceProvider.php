<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapFrontendApiRoutes();

        $this->mapCusotomerAuthRoutes();

        $this->mapAdminApiRoutes();

        $this->mapAdminAuthRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */

    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace('App\FrontendApi\Controllers')
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('ApiMiddleware')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    protected function mapFrontendApiRoutes()
    {
        Route::prefix('frontend_api')
             ->namespace('App\FrontendApi\Controllers')
             ->group(base_path('routes/frontend_api.php'));
    }

    protected function mapCusotomerAuthRoutes()
    {
        Route::prefix('customer_auth')
             ->middleware('CustomerAuthMiddleware')
             ->namespace('App\FrontendApi\Controllers')
             ->group(base_path('routes/customer_auth.php'));
    }

    protected function mapAdminApiRoutes()
    {
        Route::prefix('admin_api')
             ->middleware('AdminApiMiddleware')
             //->middleware('CorsMiddleware')
             ->namespace('App\AdminApi\Controllers')
             ->group(base_path('routes/admin_api.php'));
    }

    protected function mapAdminAuthRoutes()
    {
        Route::prefix('auth')
             ->namespace('App\AdminApi\Controllers')
             ->group(base_path('routes/auth.php'));
    }
}
