<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class SchoolCourseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schoolCourse {function?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $functionName = $this->argument("function");
        $this->$functionName();
    }

    public function refreshShuffleId()
    {
        DB::update('UPDATE tbl_school_course SET shuffle_id = FLOOR(RAND()*('.explode(" ", microtime())[1].')+1)');
        $check = false;
        while (!$check)
        {
            $datas = DB::select('SELECT shuffle_id FROM tbl_school_course GROUP BY shuffle_id HAVING COUNT(*) > 1');
            if(count($datas) > 0)
            {
                foreach ($datas as $key => $data) 
                {
                    DB::update('UPDATE tbl_school_course SET shuffle_id = shuffle_id+id where shuffle_id = '.$data->shuffle_id);
                }
            }
            else
            {
                $check = true;
            }
        }
        /*$data = DB::select('SELECT shuffle_id, COUNT(*) FROM tbl_school_course GROUP BY shuffle_id HAVING COUNT(*) > 1');
        echo count($data);*/
    }
}
