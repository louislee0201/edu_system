<?php

namespace App\Console\Commands;

use App\Model\ExchangeRate;

use Illuminate\Console\Command;
use DB;

class ExchangeRateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exchangeRate {function?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $functionName = $this->argument("function");
        $this->$functionName();
    }

    public function refreshExchangeRate()
    {
        $combinedToken = [];
        foreach (ExchangeRate::CURRENCY_TYPE_LIST as $key => $country_token) 
        {
            $rmodel = ExchangeRate::where('from_currency_type',$key)->where('to_currency_type',$key)->first();
            if(empty($rmodel))
            {
                $rmodel = new ExchangeRate;
                $rmodel->from_currency_type = $key;
                $rmodel->to_currency_type = $key;
                $rmodel->rate = 1;
                $rmodel->save();
            }

            foreach (ExchangeRate::CURRENCY_TYPE_LIST as $key2 => $country_token2) 
            {
                if($key2 > $key)
                {
                    $combinedToken[] = $country_token2;
                }
            }
            $json = file_get_contents('https://api.frankfurter.app/latest?amount=1&from='.$country_token.'&to='.join(',',$combinedToken));
            $result = json_decode($json, true);
            if(isset($result['rates'],$result['base']))
            {
                foreach ($result['rates'] as $type => $rate) 
                {
                    $model = ExchangeRate::where('from_currency_type',$key)->where('to_currency_type',ExchangeRate::CURRENCY_TYPE_LIST_REVERSE[$type])->first();
                    if(!empty($model))
                    {
                        $model->rate = $rate;
                    }
                    else
                    {
                        $model = new ExchangeRate;
                        $model->from_currency_type = $key;
                        $model->to_currency_type = ExchangeRate::CURRENCY_TYPE_LIST_REVERSE[$type];
                        $model->rate = $rate;
                    }
                       
                    $model->save();

                    $xmodel = ExchangeRate::where('from_currency_type',ExchangeRate::CURRENCY_TYPE_LIST_REVERSE[$type])->where('to_currency_type',$key)->first();
                    if(!empty($xmodel))
                    {
                        $xmodel->rate = 1 / $rate;
                    }
                    else
                    {
                        $xmodel = new ExchangeRate;
                        $xmodel->to_currency_type = $key;
                        $xmodel->from_currency_type = ExchangeRate::CURRENCY_TYPE_LIST_REVERSE[$type];
                        $xmodel->rate = 1 / $rate;
                    }
                       
                    $xmodel->save();
                }
            }
        }
    }
}
