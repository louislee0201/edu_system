<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;

use App\Model\Qualification;
use App\Model\QualificationSubject;
use App\Model\ProficiencyLanguage;


use App\Tools\Tools;
use App\Tools\GlobalList;

use DB;

class ProficiencyLanguageController
{
    public function editLanguage(Request $request)
    {   
        $action = $request->get('action');
        if(isset($action))
        {
            if($action=='add')
            {
                $language = $request->get('language');
                if(isset($language))
                {
                    $model = new ProficiencyLanguage;
                    $model->language = $language;
                    $model->save();

                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'languageList'=>ProficiencyLanguage::all()
                        ]
                    ]);
                }
            }
            else
            {
                $language_id = $request->get('language_id');
                $language = $request->get('language');
                if(isset($language_id))
                {
                    $model = ProficiencyLanguage::where('id',$language_id)->first();
                    if(!empty($model))
                    {
                        if($action=='activation')
                        {
                            $model->active = ($model->active?0:1);
                            $model->save();

                            return response()->json([
                                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                                'error'=>'',
                                'data'=>[
                                    'languageList'=>ProficiencyLanguage::all()
                                ]
                            ]);
                        }
                        elseif($action=='edit' && isset($language))
                        {
                            $model->language = $language;
                            $model->save();

                            return response()->json([
                                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                                'error'=>'',
                                'data'=>[
                                    'languageList'=>ProficiencyLanguage::all()
                                ]
                            ]);
                        }
                        elseif($action=='delete')
                        {
                            $model->delete();

                            return response()->json([
                                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                                'error'=>'',
                                'data'=>[
                                    'languageList'=>ProficiencyLanguage::all()
                                ]
                            ]);
                        }
                    }
                }
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }
}
