<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;

use App\Model\School;
use App\Model\User;
use App\Model\ProficiencyCountry;
use App\Model\Region;
use App\Model\SchoolCourseArrangement;
use App\Model\Student;
use App\Model\SchoolCourse;

use App\Tools\Tools;
use App\Tools\GlobalList;

use DB;

class SchoolController
{
	public function getSchool(Request $request)
    {   
        $isList = $request->get('isList');
        if(isset($isList))
        {
            if($isList)
            {
                return School::getSchoolList($request);
            }

            $school_id = $request->get('school_id');

            if(isset($school_id))
            {
                $schoolModel = School::where('id',$school_id)->first();
                if(!empty($schoolModel))
                {
                    $picture = '';
                    if(!empty($schoolModel->picture))
                    {
                        if(strpos($_SERVER['HTTP_HOST'], '192.168') === false)
                        {
                            $picture = 'http://'.str_replace("www","api",$_SERVER['HTTP_HOST']).'/auth'.School::BASE_SCHOOL_IMAGE_URL.$schoolModel->picture;
                        }
                        else
                        {
                            $picture = 'http://'.$_SERVER['HTTP_HOST'].'/auth'.School::BASE_SCHOOL_IMAGE_URL.$schoolModel->picture;
                        }
                    }
                    $logo = '';
                    if(!empty($schoolModel->logo))
                    {
                        if(strpos($_SERVER['HTTP_HOST'], '192.168') === false)
                        {
                            $logo = 'http://'.str_replace("www","api",$_SERVER['HTTP_HOST']).'/auth'.School::BASE_SCHOOL_IMAGE_URL.$schoolModel->logo;
                        }
                        else
                        {
                            $logo = 'http://'.$_SERVER['HTTP_HOST'].'/auth'.School::BASE_SCHOOL_IMAGE_URL.$schoolModel->logo;
                        }
                    }
                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'school_type'=>$schoolModel->school_type,
                            'school_tel'=>$schoolModel->school_tel,
                            'school_id'=>$schoolModel->id,
                            'school_name'=>$schoolModel->school_name,
                            'school_name_cn'=>$schoolModel->school_name_cn,
                            'country'=>$schoolModel->country,
                            'region'=>$schoolModel->region,
                            'address'=>$schoolModel->address,
                            'picture'=>$picture,
                            'logo'=>$logo,
                            'official_website'=>$schoolModel->official_website,
                            'email'=>$schoolModel->user->username,
                            'contact_me'=>$schoolModel->contact_me,
                            'regionList'=>Region::where('country_id',$schoolModel->country)->get(),
                            'countryList'=>ProficiencyCountry::all(),
                            'schoolTypeList'=>School::SCHOOL_TYPE,
                            'schoolArrangementList'=>SchoolCourseArrangement::where('school_id',$schoolModel->id)->orderBy('range_key','ASC')->get(),
                            'schoolCourseArrangementList'=>SchoolCourse::where('school_id',$schoolModel->id)->orderBy('range_key','ASC')->get()->groupBy('school_course_arrangement_id')
                            //'schoolCourseArrangementList'=>$schoolModel->school_course->orderBy('range_key','ASC')->get()->groupBy('school_course_arrangement_id')
                        ]
                    ]);
                }
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
        
    }

    public function deleteSchool(Request $request)
    {
        $school_id = $request->get('school_id');
        $type = $request->get('type');
        if(isset($school_id,$type))
        {
            $schoolModel = School::where('id',$school_id)->first();
            if(!empty($schoolModel))
            {
                if($type == 'user activation')
                {
                    $status = $schoolModel->user->active;
                    $schoolModel->user->active = ($status?0:1);
                    $schoolModel->user->save();
                }
                elseif($type == 'school activation')
                {
                    $schoolModel->active = ($schoolModel->active?0:1);
                    $schoolModel->save();
                }
                else
                {
                    throw new \Exception('Invalid Type',GlobalList::API_RESPONSE_CODE['500']);
                }
                /*if(!empty($schoolModel->picture))
                {
                    if(!School::deleteImage($schoolModel->picture))
                    {
                        throw new \Exception('Image Cannot be Delete',GlobalList::API_RESPONSE_CODE['500']);
                    }
                }
                $schoolModel->delete();
                $schoolModel->user->delete();*/

                return $this->getSchool($request);
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    /*public function addNewSchool(Request $request)
    {
        $userModel = new User;
        $userModel->username = $request->get('email');
        $userModel->password = encrypt($request->get('password'));
        $userModel->role = 'school';
        $userModel->save();

        $schoolModel = new School;
        $schoolModel->address = $request->get('address');
        $schoolModel->region = $request->get('region');
        $schoolModel->country = $request->get('country');
        $schoolModel->school_name = $request->get('school_name');
        $schoolModel->school_name_cn = $request->get('school_name_cn');
        $schoolModel = (!empty($request->get('picture')))?School::saveImage($request->get('picture'),$schoolModel):$schoolModel;
        $schoolModel = (!empty($request->get('logo')))?School::saveImage($request->get('logo','logo'),$schoolModel):$schoolModel;
        $schoolModel->official_website = $request->get('official_website');
        $schoolModel->user_id = $userModel->id;
        $schoolModel->save();

        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>$schoolModel->id
        ]);
    }*/

    public function editSchool(Request $request)
    {
        $school_name = $request->get('school_name');
        $school_name_cn = $request->get('school_name_cn');
        $school_id = $request->get('school_id');

        if(isset($school_name,$school_name_cn,$school_id))
        {
            $schoolModel = School::where('id',$school_id)->first();
            if(!empty($schoolModel))
            {
                $schoolModel->school_name = $school_name;
                $schoolModel->school_name_cn = $school_name_cn;
                $schoolModel->address = $request->get('address');
                $schoolModel->region = $request->get('region');
                $schoolModel->country = $request->get('country');
                $schoolModel->official_website = $request->get('official_website');
                $schoolModel = (strpos($request->get('picture'), 'base') !== false)?School::saveImage($request->get('picture'),$schoolModel):$schoolModel;
                $schoolModel = (strpos($request->get('logo'), 'base') !== false)?School::saveImage($request->get('logo'),$schoolModel,'logo'):$schoolModel;

                $schoolModel->save();

                return $this->getSchool($request);
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function updateSchool(Request $request)
    {
        $action = $request->get('action');

        if(isset($action))
        {
            $school_id = $request->get('school_id');
            if(isset($school_id))
            {
                $schoolModel = School::where('id',$school_id)->first();
                if(!empty($schoolModel))
                {
                    if($action == 'delete')
                    {
                        if(!empty($schoolModel->picture))
                        {
                            if(!School::deleteImage($schoolModel->picture))
                            {
                                throw new \Exception('Image Cannot be Delete',GlobalList::API_RESPONSE_CODE['601']);
                            }
                        }

                        $schoolModel->delete();
                        $schoolModel->user->delete();

                        return $this->getSchool($request);
                    }

                    if($action == 'edit')
                    {
                        $school_name = $request->get('school_name');
                        $school_name_cn = $request->get('school_name_cn');
                        $school_type = $request->get('school_type');

                        if(isset($school_name,$school_name_cn,$school_type))
                        {
                            $schoolModel->school_type = $school_type;
                            $schoolModel->school_name = $school_name;
                            $schoolModel->school_name_cn = $school_name_cn;
                            $schoolModel->address = $request->get('address');
                            $schoolModel->region = $request->get('region');
                            $schoolModel->country = $request->get('country');
                            $schoolModel->school_tel = $request->get('school_tel');
                            $schoolModel->official_website = $request->get('official_website');
                            $schoolModel->contact_me = $request->get('contact_me');
                            $schoolModel = (strpos($request->get('picture'), 'base') !== false)?School::saveImage($request->get('picture'),$schoolModel):$schoolModel;
                            $schoolModel = (strpos($request->get('logo'), 'base') !== false)?School::saveImage($request->get('logo'),$schoolModel,'logo'):$schoolModel;
                            $schoolModel->save();

                            return $this->getSchool($request);
                        }
                    }

                    if($action == 'activation')
                    {
                        $type = $request->get('type');

                        if(isset($type))
                        {
                            if($type == 'user')
                            {
                                $status = $schoolModel->user->active;
                                $schoolModel->user->active = ($status?0:1);
                                $schoolModel->user->save();

                                return $this->getSchool($request);
                            }
                            elseif($type == 'school')
                            {
                                $schoolModel->active = ($schoolModel->active?0:1);
                                $schoolModel->save();

                                return $this->getSchool($request);
                            }
                        }
                    }

                    if($action == 'generateAccount')
                    {
                        $userInfo = $schoolModel->user;
                        $sModel = Student::where('email',$userInfo->username)->first();
                        if(empty($sModel))
                        {
                            $sModel = new Student;
                        }

                        $sModel->email = $userInfo->username;
                        $sModel->name = explode('@',$userInfo->username)[0];
                        $sModel->password = md5(decrypt($userInfo->password));
                        $sModel->tel = $schoolModel->school_tel;
                        $sModel->account_level = Student::ACCOUNT_LEVEL_USER;
                        $sModel->school_id = $schoolModel->id;
                        $sModel->user_token = encrypt(Tools::getToken($userInfo->id));
                        $sModel->save();
                        return $this->getSchool($request);
                    }
                }
            }

            if($action == 'add')
            {
                $school_name = $request->get('school_name');
                $school_name_cn = $request->get('school_name_cn');
                $password = $request->get('password');
                $username = $request->get('email');
                $school_type = $request->get('school_type');

                if(isset(
                    $school_name,
                    $school_name_cn,
                    $password,
                    $username,
                    $school_type
                ))
                {
                    $userModel = new User;
                    $userModel->username = $username;
                    $userModel->password = encrypt($password);
                    $userModel->role = 'school';
                    $userModel->save();

                    $schoolModel = new School;
                    $schoolModel->school_type = $school_type;
                    $schoolModel->address = $request->get('address');
                    $schoolModel->region = $request->get('region');
                    $schoolModel->country = $request->get('country');
                    $schoolModel->school_tel = $request->get('school_tel');
                    $schoolModel->school_name = $school_name;
                    $schoolModel->school_name_cn = $school_name_cn;
                    $schoolModel = (!empty($request->get('picture')))?School::saveImage($request->get('picture'),$schoolModel):$schoolModel;
                    $schoolModel = (!empty($request->get('logo')))?School::saveImage($request->get('logo'),$schoolModel,'logo'):$schoolModel;
                    $schoolModel->official_website = $request->get('official_website');
                    $schoolModel->user_id = $userModel->id;
                    $schoolModel->save();

                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>$schoolModel->id
                    ]);
                }

                
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }
}
