<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Model\Description;
use App\Model\DescriptionInfo;
use App\Model\DescriptionInfoSub;

use App\Tools\GlobalList;

use DB;

class DescriptionController
{
    public function getDescriptionType(Request $request)
    {
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'descriptionTypeList'=> Description::DESCRIPTION_TYPE
            ]
        ]);
    }

    public function updateDescription(Request $request)
    {
        $action = $request->get('action');
        $descriptionList = $request->get('descriptionList');
        $title = $request->get('title');
        $classMode = $request->get('classMode');
        $related_id = $request->get('related_id');
        $description_id = $request->get('description_id');
        
        if(isset($action))
        {
            if($action == 'add')
            {
                if(isset(
                    $descriptionList,
                    $title,
                    $classMode,
                    $related_id
                ))
                {
                    if($classMode == 'course')
                    {
                        $classMode = 'school_course_id';
                    }
                    else
                    {
                        $classMode = 'school_id';
                    }
                    $dsModel = new Description;
                    $dsModel->title = $title;
                    $dsModel[$classMode] = $related_id;
                    $content = [];
                    foreach ($descriptionList as $key => $value) 
                    {
                        $content[] = Description::checkIssetPic($value);
                    }
                    //$content = Description::checkIssetPic($descriptionList);
                    $dsModel->content = json_encode($content);

                    $checkModel = Description::where($classMode,$related_id)->get()->max('range_key');
                    $dsModel->range_key = ($checkModel !== null ? $checkModel + 1 : 0);
                    $dsModel->save();
                    return $this->getDescriptionList($request);
                }
            }
            else if($action == 'edit')
            {
                $change_key = $request->get('change_key');

                if(isset(
                    $change_key,
                    $description_id,
                    $related_id,
                    $classMode
                ))
                { 
                    if($classMode == 'course')
                    {
                        $classMode = 'school_course_id';
                    }
                    else
                    {
                        $classMode = 'school_id';
                    }
                    $models = Description::where('id','!=',$description_id)
                                    ->where($classMode,$related_id)
                                    ->orderBy('range_key', 'asc')
                                    ->get();
                    $wModel = Description::where('id',$description_id)
                                    ->where($classMode,$related_id)
                                    ->first();
                    $count = 0;
                    foreach ($models as $modelKey => $model) 
                    {
                        if($model->range_key == $change_key)
                        {
                            if($modelKey < $change_key)
                            {
                                $model->range_key = $count;
                                $model->save();
                                $count++;

                                $wModel->range_key = $count;
                                $wModel->save();
                                $count++;
                            }
                            else
                            {
                                $wModel->range_key = $count;
                                $wModel->save();
                                $count++;

                                $model->range_key = $count;
                                $model->save();
                                $count++;
                            }
                        }
                        else
                        {
                            $model->range_key = $count;
                            $model->save();
                            $count++;
                        }
                        
                    }

                    return $this->getDescriptionList($request);
                }
                else if(isset(
                    $descriptionList,
                    $title,
                    $classMode,
                    $related_id,
                    $description_id
                ))
                {
                    if($classMode == 'course')
                    {
                        $classMode = 'school_course_id';
                    }
                    else
                    {
                        $classMode = 'school_id';
                    }
                    
                    $dsModel = Description::where('id',$description_id)->where($classMode,$related_id)->first();
                    if(!empty($dsModel))
                    {
                        $content = [];
                        foreach ($descriptionList as $key => $value) 
                        {
                            $content[] = Description::checkIssetPic($value);
                        }
                        //$content = Description::checkIssetPic($descriptionList);
                        $dsModel->title = $title;
                        $dsModel->content = json_encode($content);
                        $dsModel->save();
                        return $this->getDescriptionList($request);
                    }
                }
            }
            else if($action == 'delete')
            {
                if(isset($description_id))
                {
                    $model = Description::where('id',$description_id)->first();
                    if(!empty($model))
                    {
                        $model->deletes();
                        return $this->getDescriptionList($request);
                    }
                }
            }
            
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']); 
    }

    public function getDescriptionList(Request $request)
    {
        $classMode = $request->get('classMode');
        $related_id = $request->get('related_id');

        if(isset($classMode,$related_id))
        {
            if($classMode == 'course')
            {
                $model = Description::where('school_course_id',$related_id)->orderBy('range_key')->get();
            }
            else
            {
                $model = Description::where('school_id',$related_id)->orderBy('range_key')->get();
            }

            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                'error'=>'',
                'data'=>[
                    'descriptionList'=> $model
                ]
            ]);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']); 
    }   

    public function getDescriptionData(Request $request)
    {
        $related_id = $request->get('related_id');
        $mode = $request->get('mode');

        if(isset($related_id,$mode))
        {
            $newDsInfoSubModel = [];
            $newDsInfoModel = [];
            if($mode == 'info')
            {
                $dsInfoModel = DescriptionInfo::where('description_id',$related_id)->orderBy('range_key')->get();
                
                foreach ($dsInfoModel as $modelKey => $model) 
                {
                    if($model->picture !== null)
                    {
                        $pictureList = json_decode($model->picture);
                        $pictureBinary = [];
                        foreach ($pictureList as $picKey => $pic) 
                        {
                            $explode = explode('.', $pic);
                            $name = $explode[0];
                            $fileType = $explode[1];
                            $date = date("Y-m-d", $name);
                            $des = storage_path('app/'.Description::BASE_DESCRIPTION_IMAGE_URL.'/'.$date.'/'.$pic);
                            $fileContent = '';
                            if(file_exists($des))
                            {
                                $content = file_get_contents($des);
                                $base64 = base64_encode($content);
                                $fileContent = 'data:image/' . $fileType . ';base64,' . $base64;
                            }
                            $pictureBinary[] = $fileContent;
                        }

                        $model->picture = json_encode($pictureBinary);
                    }

                    $newDsInfoModel[] = $model;
                }
            }
            else
            {
                $dsInfoSubModel = DescriptionInfoSub::where('description_info_id',$related_id)->orderBy('range_key')->get();

                foreach ($dsInfoSubModel as $modelKey => $model) 
                {
                    if($model->picture !== null)
                    {
                        $pictureList = json_decode($model->picture);
                        $pictureBinary = [];
                        foreach ($pictureList as $picKey => $pic) 
                        {
                            $explode = explode('.', $pic);
                            $name = $explode[0];
                            $fileType = $explode[1];
                            $date = date("Y-m-d", $name);
                            $des = storage_path('app/'.Description::BASE_DESCRIPTION_IMAGE_URL.'/'.$date.'/'.$pic);
                            $fileContent = '';
                            if(file_exists($des))
                            {
                                $content = file_get_contents($des);
                                $base64 = base64_encode($content);
                                $fileContent = 'data:image/' . $fileType . ';base64,' . $base64;
                            }
                            $pictureBinary[] = $fileContent;
                        }

                        $model->picture = json_encode($pictureBinary);
                    }

                    $newDsInfoSubModel[] = $model;
                }
            }

            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                'error'=>'',
                'data'=>[
                    'descriptionInfoList'=> $newDsInfoModel,
                    'descriptionInfoSubList'=> $newDsInfoSubModel
                ]
            ]);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']); 
    }
}
