<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;

use App\Model\User;

use App\Tools\Tools;
use App\Tools\GlobalList;

class AuthController
{
    public function loginStatus(Request $request)
    {
        $api_token = $request->get('api_token');
        
        if(isset($api_token))
        {
            if(!$api_token)
            {
                throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['602']);
            }
            $userModel = User::where('api_token', $api_token)->first();

            if(empty($userModel))
            {
                throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['602']);
            }

            if(!$userModel->active)
            {
                throw new \Exception('Account was freeze, please contact principal',GlobalList::API_RESPONSE_CODE['602']);
            }

            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                'error'=>''
            ]);
        }
    }

    public function checkUser($request)
    {
        $username = $request->get('username');
        $password = $request->get('password');
        $api_token = $request->get('api_token');

        if(isset($username) && isset($password))
        {
            $userModel = User::where('username', $username)->first();
            if(!empty($userModel))
            {
                if(decrypt($userModel->password) == $password)
                {
                    if(!$userModel->active)
                    {
                        throw new \Exception('Account was freeze, please contact principal',GlobalList::API_RESPONSE_CODE['601']);
                    }
                    return $userModel;
                }
            }
            return null;
        }

        if(isset($api_token))
        {
            return User::where('api_token', $api_token)->first();
        }

        return null;
    }

    public function login(Request $request)
    {
        $userModel = $this->checkUser($request);

        if(empty($userModel))
        {
            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['601'],
                'error'=>'username or password wrong'
            ]);
        }

        $userModel->api_token = encrypt(Tools::getToken($userModel->id));
        $userModel->save();

        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'access_token'=>$userModel->api_token,
                'role'=>$userModel->role
            ]
        ]);

    }

    public function logout(Request $request)
    {
        $userModel = $this->checkUser($request);

        if(empty($userModel))
        {
            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['601'],
                'error'=>''
            ]);
        }
        
        $userModel->api_token = null;
        $userModel->save();

        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>''
        ]);
    }
}