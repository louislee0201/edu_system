<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;

use App\Model\SchoolCourse;
use App\Model\School;
use App\Model\SchoolCourseLevel;
use App\Model\SchoolCourseRequirement;
use App\Model\Qualification;
use App\Model\QualificationSubject;
use App\Model\QualificationGrade;
use App\Model\MainTag;
use App\Model\SubTag;
use App\Model\SchoolMajor;
use App\Model\ExchangeRate;

use App\Tools\Tools;
use App\Tools\GlobalList;

use DB;
use Artisan;

class SchoolCourseController
{
    public function editSubTagList(Request $request)
    {
        $course_id = $request->get('course_id');
        $action = $request->get('action');
        $sub_tag_id = $request->get('sub_tag_id');
        $school_major_id = $request->get('school_major_id');

        if(isset($action) && isset($course_id) && isset($sub_tag_id))
        {   
            $model = SchoolCourse::where('id',$course_id)->first();
            if(!empty($model))
            {
                if($action == 'add')
                {
                    $stModel = SubTag::where('id',$sub_tag_id)->first();
                    if(!empty($stModel))
                    {
                        $smModel = new SchoolMajor;
                        $smModel->school_course_id = $model->id;
                        $smModel->main_tag_id = $stModel->main_tag->id;
                        $smModel->sub_tag_id = $stModel->id;
                        $smModel->save();
                    }
                    
                }
                elseif($action == 'delete' && isset($school_major_id))
                {
                    $smModel = SchoolMajor::where('id',$school_major_id)->where('school_course_id',$model->id)->first();
                    if(!empty($smModel))
                    {
                        $smModel->delete();
                    }
                }

                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'subTagList'=>SchoolMajor::where('school_course_id',$model->id)->get()
                    ]
                ]);
            }
            
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

	public function getCourse(Request $request)
    {   
        $isList = $request->get('isList');
        if(isset($isList))
        {
            if($isList)
            {
                return SchoolCourse::getCourseList($request);
            }

            $course_id = $request->get('course_id');

            if(isset($course_id))
            {
                $userModel = $request->get('userModel');
                $model = SchoolCourse::where('id',$course_id);

                if(Tools::checkPermissionLevel($userModel->role,GlobalList::ADMIN_PERMISSION))
                {
                    $model = $model->where('school_id', $userModel->school->id);
                }

                $model = $model->first();
                if(!empty($model))
                {
                    $picture = '';
                    if(!empty($model->picture))
                    {
                        if(strpos($_SERVER['HTTP_HOST'], '192.168') === false)
                        {
                            $picture = 'http://'.str_replace("www","api",$_SERVER['HTTP_HOST']).'/auth'.SchoolCourse::BASE_SCHOOL_COURSE_IMAGE_URL.$model->picture;
                        }
                        else
                        {
                            $picture = 'http://'.$_SERVER['HTTP_HOST'].'/auth'.SchoolCourse::BASE_SCHOOL_COURSE_IMAGE_URL.$model->picture;
                        }
                    }
                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'subTagsList'=>SubTag::orderBy('sub_tag')->get(),
                            'mainTagsList'=>MainTag::orderBy('main_tag')->get(),
                            'subTagList'=>SchoolMajor::where('school_course_id',$model->id)->get(),
                            'course_id'=>$model->id,
                            'course_name'=>$model->course_name,
                            'school_name'=>$model->school->school_name,
                            'course_level'=>$model->school_course_level_id,
                            'secondary_complete'=>$model->secondary_complete,
                            'picture'=>$picture,
                            'courseLevelList'=>SchoolCourseLevel::all(),
                            'requirementTypeList'=> SchoolCourseRequirement::REQUIREMENT_TYPE_LIST,
                            'gradeTypeList'=>SchoolCourseRequirement::GRADE_TYPE,
                            'duration'=>$model->duration,
                            'tuition_fee'=>$model->tuition_fee,
                            'currency_type'=>$model->currency_type,
                            'currencyTypeList'=>ExchangeRate::CURRENCY_TYPE_LIST,
                            'contact_me'=>$model->contact_me
                        ]
                    ]);
                }
                throw new \Exception('Invalid Course',GlobalList::API_RESPONSE_CODE['601']);
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function deleteCourse(Request $request)
    {
        $course_id = $request->get('course_id');
        $type = $request->get('type');
                        
        if(isset($course_id,$type))
        {
            $userModel = $request->get('userModel');
            $courseModel = SchoolCourse::where('id',$course_id);

            if(Tools::checkPermissionLevel($userModel->role,GlobalList::ADMIN_PERMISSION))
            {
                $courseModel = $courseModel->where('school_id', $userModel->school->id);
            }

            $courseModel = $courseModel->first();

            if(!empty($courseModel))
            {
                if($type == 'activation')
                {
                    $courseModel->active = ($courseModel->active?0:1);
                    $courseModel->save();
                }
                elseif($type == 'delete')
                {
                    $courseModel->delete();
                }
                else
                {
                    throw new \Exception('Invalid type',GlobalList::API_RESPONSE_CODE['601']);
                }
                return $this->getCourse($request);
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function addNewCourse(Request $request)
    {
        $scModel = new SchoolCourse;
        $scModel->course_name = $request->get('course_name');
        $scModel->school_course_level_id = $request->get('course_level');
        $scModel->school_id = $request->get('school_name');
        $scModel->save();

        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>$scModel->id
        ]);
    }

    public function editCourse(Request $request)
    {
        $course_id = $request->get('course_id');
        $course_name = $request->get('course_name');
        $course_level = $request->get('course_level');
        $action = $request->get('action');
        $school_id = $request->get('school_id');
        $secondary_complete = $request->get('secondary_complete');
        $duration = $request->get('duration');
        $currency_type = $request->get('currency_type');
        $tuition_fee = $request->get('tuition_fee');
        $contact_me = $request->get('contact_me');

        if(isset($action))
        {
            if(isset($course_id))
            {
                $courseModel = SchoolCourse::where('id',$course_id)->first();
                if(!empty($courseModel))
                {
                    if($action == 'delete')
                    {
                        $courseModel->delete();

                        return $this->getCourse($request);
                    }

                    if($action == 'edit' && isset($course_name,$course_level,$secondary_complete,$duration,$currency_type,$tuition_fee))
                    {
                        $courseModel->course_name = $course_name;
                        $courseModel->school_course_level_id = $course_level;
                        $courseModel->secondary_complete = $secondary_complete;
                        $courseModel->duration = $duration;
                        $courseModel->currency_type = $currency_type;
                        $courseModel->tuition_fee = $tuition_fee;
                        $courseModel->contact_me = $contact_me;

                        $userModel = $request->get('userModel');
                        if(Tools::checkPermissionLevel($userModel->role,GlobalList::ADMIN_PERMISSION))
                        {
                            $courseModel = $courseModel->where('school_id', $userModel->school->id);
                        }

                        $courseModel = (!empty($request->get('picture')))?SchoolCourse::saveImage($request->get('picture'),$courseModel):$courseModel;

                        $courseModel->save();

                        return $this->getCourse($request);
                    }

                    if($action == 'activation')
                    {
                        $courseModel = SchoolCourse::where('id',$course_id)->first();
                        $courseModel->active = ($courseModel->active?0:1);
                        $courseModel->save();

                        return $this->getCourse($request);
                    }
                }
            }

            if($action == 'add')
            {
                if(isset(
                    $course_level,
                    $course_name,
                    $school_id,
                    $secondary_complete/*,
                    $duration,
                    $currency_type,
                    $tuition_fee*/
                ))
                {
                    $schoolModel = School::where('id',$school_id)->first();
                    if(!empty($schoolModel))
                    {
                        $scModel = new SchoolCourse;
                        $scModel->course_name = $course_name;
                        $scModel->duration = $duration;
                        $scModel->currency_type = $currency_type;
                        $scModel->tuition_fee = $tuition_fee;
                        $scModel->school_course_level_id = $course_level;
                        $scModel->school_id = $school_id;
                        $scModel->secondary_complete = $secondary_complete;
                        $scModel->contact_me = $schoolModel->contact_me;

                        $scModel->range_key = SchoolCourse::max('id');

                        $scModel = (!empty($request->get('picture')))?SchoolCourse::saveImage($request->get('picture'),$scModel):$scModel;
                        $scModel->save();

                        Artisan::call("schoolCourse refreshShuffleId");

                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>'',
                            'data'=>$scModel->id
                        ]);
                    }
                    else
                    {
                        throw new \Exception('please select school',GlobalList::API_RESPONSE_CODE['601']);
                    }
                }

                
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function getSchoolList(Request $request)
    {
        $searchContent = $request->get('searchContent');
        $schoolModels = [];

        if (preg_match("/[\x7f-\xff]/", $searchContent)) 
        {
            $schoolModels = School::where('school_name_cn','like','%'.$searchContent.'%')->get();
        } 
        else 
        {
            $schoolModels = School::where('school_name','like','%'.$searchContent.'%')->get();
            
        }

        /*if(!empty($searchContent))
        {
            $schoolModels = School::where('school_name','like','%'.$searchContent.'%')->orwhere('school_name_cn','like','%'.$searchContent.'%')->get();
            
        }*/
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'searchResult'=>$schoolModels
            ]
        ]);
    }

    public function getCourseLevelList(Request $request)
    {
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'courseLevelList'=>SchoolCourseLevel::all(),
                'currencyTypeList'=>ExchangeRate::CURRENCY_TYPE_LIST
            ]
        ]);
    }

    public function editCourseLevel(Request $request)
    {
        $action = $request->get('action');
        if(isset($action))
        {
            if($action=='add')
            {
                $level = $request->get('level');
                if(isset($level))
                {
                    $model = new SchoolCourseLevel;
                    $model->level_name = $level;
                    $model->save();

                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'courseLevelList'=>SchoolCourseLevel::all()
                        ]
                    ]);
                }
            }
            else
            {
                $level_id = $request->get('level_id');
                $level = $request->get('level');
                if(isset($level_id))
                {
                    if($action=='activation')
                    {
                        $model = SchoolCourseLevel::where('id',$level_id)->first();
                        $model->active = ($model->active?0:1);
                        $model->save();

                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>'',
                            'data'=>[
                                'courseLevelList'=>SchoolCourseLevel::all()
                            ]
                        ]);
                    }
                    elseif($action=='edit' && isset($level))
                    {
                        $model = SchoolCourseLevel::where('id',$level_id)->first();
                        if(!empty($model))
                        {
                            $model->level_name = $level;
                            $model->save();

                            return response()->json([
                                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                                'error'=>'',
                                'data'=>[
                                    'courseLevelList'=>SchoolCourseLevel::all()
                                ]
                            ]);
                        }
                    }
                    elseif($action=='delete')
                    {
                        $model = SchoolCourseLevel::where('id',$level_id)->first();
                        if(!empty($model))
                        {
                            $model->delete();

                            return response()->json([
                                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                                'error'=>'',
                                'data'=>[
                                    'courseLevelList'=>SchoolCourseLevel::all()
                                ]
                            ]);
                        }
                    }
                }
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']); 
    }
}
