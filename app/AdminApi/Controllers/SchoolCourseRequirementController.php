<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;

use App\Model\SchoolCourseRequirement;
use App\Model\QualificationGrade;
use App\Model\QualificationSubject;
use App\Model\SchoolCourseRequirementIncludedInstead;
use App\Model\SchoolCourseRequirementIncluded;
use App\Model\SchoolCourseRequirementGrade;
use App\Model\Qualification;
use App\Model\SchoolCourse;
use App\Model\Proficiency;
use App\Model\ProficiencyGrade;
use App\Model\ProficiencyInfo;

use App\Tools\Tools;
use App\Tools\GlobalList;

use DB;

class SchoolCourseRequirementController
{
    public function getRequirementAttr(Request $request)
    {
        $course_id = $request->get('course_id');
        if(isset($course_id))
        {
            $model = SchoolCourse::where('id',$course_id)->first();
            if(!empty($model))
            {
                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'requirementConditions'=>SchoolCourse::getCourseRequirementConditions($model->id),
                        'insteadGradeTypeList'=>SchoolCourseRequirementIncludedInstead::GRADE_TYPE,
                        'gradeTypeList'=>SchoolCourseRequirement::GRADE_TYPE,
                        'qualificationList'=>Qualification::all(),
                        'qualificationSubjectList'=>QualificationSubject::all(),
                        'qualificationGradeList'=>QualificationGrade::all(),
                        //'proficiencyList'=>Proficiency::all(),
                        //'proficiencyInfo'=>$piModel,
                        'proficiencyScoreTypeList'=> SchoolCourseRequirement::PROFICIENCY_SCORE_TYPE
                    ]
                ]);
            }
        }
        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

	public function submitRequirement(Request $request)
    {   
        $course_id = $request->get('course_id');
        $qualification_id = $request->get('qualification_id');
        $gradeTypeId = $request->get('gradeTypeId');
        $no_specific_requirement = $request->get('no_specific_requirement');

        $submitedIncludedAndInstead = $request->get('submitedIncludedAndInstead');
        $submitedIncludedAndInsteadExtra = $request->get('submitedIncludedAndInsteadExtra');

        $submitedGrade = $request->get('submitedGrade');
        $submitedWithoutSubject = $request->get('submitedWithoutSubject');

        $action = $request->get('action');
        $editKey = $request->get('editKey');

        if(isset(
            $course_id,
            $qualification_id,

            $gradeTypeId,
            $no_specific_requirement,

            $submitedIncludedAndInstead,
            $submitedIncludedAndInsteadExtra,
            $submitedGrade,
            $submitedWithoutSubject,

            $action,
            $editKey
        ))
        {
            $qModel = Qualification::where('id',$qualification_id)->first();
            $cModel = SchoolCourse::where('id',$course_id)->first();
            $rModel = SchoolCourseRequirement::where('id',$editKey)->first();
            if(empty($rModel))
            {
                if($action == 'edit')
                {
                    throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
                }
                $rModel = new SchoolCourseRequirement;
            }
            if(!empty($cModel) && !empty($qModel))
            {
                $rModel->no_specific_requirement = $no_specific_requirement;
                $rModel->qualification_id = $qualification_id;
                $rModel->school_course_id = $course_id;
                $rModel->grade_type = $gradeTypeId;
                $rModel->save();
                $rModel->refresh();
                if($action == 'edit')
                {
                    DB::delete('DELETE FROM tbl_school_course_requirement_grade WHERE school_course_requirement_id = '.$rModel->id);
                    DB::delete('
                        DELETE FROM tbl_school_course_requirement_included_instead
                        WHERE school_course_requirement_included_id IN (
                            SELECT id FROM tbl_school_course_requirement_included WHERE school_course_requirement_id = '.$rModel->id.'
                        )
                    ');
                    DB::delete('DELETE FROM tbl_school_course_requirement_included WHERE school_course_requirement_id = '.$rModel->id);
                }

                foreach ($submitedIncludedAndInstead as $sisKey => $sis) 
                {
                    $sis = (Object)$sis;
                    $riModel = new SchoolCourseRequirementIncluded;
                    $riModel->school_course_requirement_id = $rModel->id;
                    $riModel->qualification_subject_id = $sis->includedSubjectId;
                    $riModel->qualification_grade_id = $sis->includedGradeId;
                    $riModel->instead_quantity = $sis->insteadQuantity;
                    if(!$sis->insteadFirstQualificatinId && !$sis->insteadSecondQualificatinId)
                    {
                        $riModel->case_handling = SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED;
                        $riModel->save();
                    }
                    else
                    {
                        $riModel->case_handling = SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD;
                        if($riModel->save())
                        {
                            $riModel->refresh();
                            $riiModel = new SchoolCourseRequirementIncludedInstead;
                            $riiModel->school_course_requirement_included_id = $riModel->id;
                            if($sis->insteadFirstQualificatinId)
                            {
                                $riiModel->instead_qualification_id = $sis->insteadFirstQualificatinId;
                                $riiModel->instead_qualification_subject_id = $sis->insteadFirstSubjectId;
                                $riiModel->instead_qualification_grade_id = $sis->insteadFirstGradeId;
                                $riiModel->instead_grade_type = $sis->insteadFirstGradeTypeId;
                            } 

                            if($sis->insteadSecondQualificatinId)
                            {
                                
                                $riiModel->second_instead_qualification_id = $sis->insteadSecondQualificatinId;
                                $riiModel->second_instead_qualification_subject_id = $sis->insteadSecondSubjectId;
                                $riiModel->second_instead_qualification_grade_id = $sis->insteadSecondGradeId;
                                $riiModel->second_instead_grade_type = $sis->insteadSecondGradeTypeId;
                            }

                            $riiModel->save();
                        }   
                    }
                }

                foreach ($submitedIncludedAndInsteadExtra as $sisKey => $sis) 
                {
                    $sis = (Object)$sis;
                    $riModel = new SchoolCourseRequirementIncluded;
                    $riModel->school_course_requirement_id = $rModel->id;
                    $riModel->qualification_subject_id = $sis->includedSubjectId;
                    $riModel->qualification_grade_id = $sis->includedGradeId;
                    $riModel->instead_quantity = $sis->insteadQuantity;
                    $riModel->case_handling = SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED;
                    if($riModel->save())
                    {
                        $riModel->refresh();
                        $riiModel = new SchoolCourseRequirementIncludedInstead;
                        $riiModel->school_course_requirement_included_id = $riModel->id;
                        if($sis->insteadFirstQualificatinId)
                        {
                            $riiModel->instead_qualification_id = $sis->insteadFirstQualificatinId;
                            $riiModel->instead_qualification_subject_id = $sis->insteadFirstSubjectId;
                            $riiModel->instead_qualification_grade_id = $sis->insteadFirstGradeId;
                            $riiModel->instead_grade_type = $sis->insteadFirstGradeTypeId;
                        } 
                        if($sis->insteadSecondQualificatinId)
                        {
                            $riiModel->second_instead_qualification_id = $sis->insteadSecondQualificatinId;
                            $riiModel->second_instead_qualification_subject_id = $sis->insteadSecondSubjectId;
                            $riiModel->second_instead_qualification_grade_id = $sis->insteadSecondGradeId;
                            $riiModel->second_instead_grade_type = $sis->insteadSecondGradeTypeId;
                        }

                        $riiModel->save();
                    }   
                }

                foreach ($submitedWithoutSubject as $swsKey => $sws) 
                {
                    $riModel = new SchoolCourseRequirementIncluded;
                    $riModel->school_course_requirement_id = $rModel->id;
                    $riModel->qualification_subject_id = $sws;
                    $riModel->instead_quantity = 1;
                    $riModel->case_handling = SchoolCourseRequirementIncluded::CASE_HANDLING_NOT_INCLUDED;
                    $riModel->save();
                }

                $total_score = 0;
                $total_cgpa = 0;
                foreach ($submitedGrade as $grade_id => $count) 
                {
                    if($count > 0)
                    {
                        $rgModel = new SchoolCourseRequirementGrade;
                        $rgModel->school_course_requirement_id = $rModel->id;
                        $rgModel->qualification_grade_quantity = $count;
                        $rgModel->qualification_grade_id = $grade_id;
                        $rgModel->save();

                        $gModel = QualificationGrade::where('id',$grade_id)->first();
                        $total_score += $gModel->score * $count;
                        $total_cgpa += $gModel->cgpa * $count;
                    }
                }

                if(
                    $gradeTypeId == SchoolCourseRequirement::GRADE_SCORE_MORE_THAN ||
                    $gradeTypeId == SchoolCourseRequirement::GRADE_SCORE_LESS_THAN ||
                    $gradeTypeId == SchoolCourseRequirement::SCORE_MORE_THAN ||
                    $gradeTypeId == SchoolCourseRequirement::SCORE_LESS_THAN
                )
                {
                    $rModel->total_score = $total_score;
                }
                else
                {
                    $rModel->total_cgpa = $total_cgpa;
                }
                $rModel->save();
                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'requirementConditions'=>SchoolCourse::getCourseRequirementConditions($course_id)
                    ]
                ]);
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function updateRequirement(Request $request)
    {
        $qualification_id = $request->get('qualification_id');
        $course_id = $request->get('course_id');
        $action = $request->get('action');

        if(isset($action,$qualification_id,$course_id))
        {
            $model = SchoolCourseRequirement::where('qualification_id',$qualification_id)->where('school_course_id',$course_id)->first();
            if(!empty($model))
            {   
                if($action == 'delete')
                {
                    DB::delete('DELETE FROM tbl_school_course_requirement_grade WHERE school_course_requirement_id = '.$model->id);
                    DB::delete('
                        DELETE FROM tbl_school_course_requirement_included_instead
                        WHERE school_course_requirement_included_id IN (
                            SELECT id FROM tbl_school_course_requirement_included WHERE school_course_requirement_id = '.$model->id.'
                        )
                    ');
                    DB::delete('DELETE FROM tbl_school_course_requirement_included WHERE school_course_requirement_id = '.$model->id);
                    $model->delete();
                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'requirementConditions'=>SchoolCourse::getCourseRequirementConditions($course_id),
                        ]
                    ]);
                }
                elseif($action == 'activation')
                {   
                    $model->active = $model->active?0:1;
                    $model->save();

                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'requirementConditions'=>SchoolCourse::getCourseRequirementConditions($course_id),
                        ]
                    ]);
                }
            }
        }
        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }
}
