<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;

use App\Model\Qualification;
use App\Model\QualificationSubject;
use App\Model\QualificationGrade;


use App\Tools\Tools;
use App\Tools\GlobalList;

use DB;

class QualificationGradeController
{
	public function deleteGrade(Request $request)
    {   
        $id = $request->get('grade_id');
        $qualification_id = $request->get('qualification_id');
        $type = $request->get('type');

        if(isset($id,$qualification_id))
        {
            $model = QualificationGrade::where('id',$id)->where('qualification_id',$qualification_id)->first();
            if(!empty($model))
            {
                if($type == 'delete')
                {
                    $model->delete();   
                }
                elseif($type == 'activation')
                {
                    if($model->active == 0)
                    {
                        $model->active = 1;
                    }
                    else
                    {
                        $model->active = 0;
                    }
                    $model->save();
                }
                else
                {
                    throw new \Exception('Invalid Type',GlobalList::API_RESPONSE_CODE['601']);
                }

                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'qualification_grade'=>QualificationGrade::where('qualification_id',$qualification_id)->orderBy('score','asc')->get()
                    ]
                ]);
            }

            throw new \Exception('Invalid Grade',GlobalList::API_RESPONSE_CODE['601']);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function editGrade(Request $request)
    {   
        $id = $request->get('grade_id');
        $grade = $request->get('grade');
        $score = $request->get('score');
        $qualification_id = $request->get('qualification_id');
        $cgpa = $request->get('cgpa');
        $fromScore = $request->get('fromScore');
        $toScore = $request->get('toScore');
        $scoreType = $request->get('scoreType');

        $action = $request->get('action');
        if(isset($action,$qualification_id))
        {
            if($action=='add')
            {
                if(isset($scoreType,$grade))
                {
                    $model = new QualificationGrade;
                    if($scoreType == 1 && isset($fromScore,$toScore))
                    {
                        $model->from_score = $fromScore;
                        $model->to_score = $toScore;
                    }
                    elseif($scoreType == 2)
                    {
                        $model->cgpa = ($cgpa > 0 && isset($cgpa) ? $cgpa : null);
                        $model->score = ($score > 0 && isset($score) ? $score : null);
                    }
                    else
                    {
                        throw new \Exception('Invalid Qualification',GlobalList::API_RESPONSE_CODE['601']);
                    }
                    $model->grade = $grade;
                    $model->type = $scoreType;
                    $model->qualification_id = $qualification_id;
                    $model->save();

                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'dataList'=>QualificationGrade::where('qualification_id',$qualification_id)->orderBy('score','asc')->get()
                        ]
                    ]);
                }
            }
            else
            {
                if(isset($id))
                {
                    $model = QualificationGrade::where('id',$id)
                                                ->where('qualification_id',$qualification_id)
                                                ->first();
                    if(!empty($model))
                    {
                        if($action=='activation')
                        {
                            $model->active = ($model->active?0:1);
                            $model->save();

                            return response()->json([
                                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                                'error'=>'',
                                'data'=>[
                                    'dataList'=>QualificationGrade::where('qualification_id',$qualification_id)->orderBy('score','asc')->get()
                                ]
                            ]);
                        }
                        elseif($action=='edit')
                        {
                            if(isset(
                                $grade,
                                $scoreType
                            ))
                            {
                                if($scoreType == 1 && isset($fromScore,$toScore))
                                {
                                    $model->from_score = $fromScore;
                                    $model->to_score = $toScore;
                                }
                                elseif($scoreType == 2)
                                {
                                    $model->cgpa = ($cgpa > 0 && isset($cgpa) ? $cgpa : null);
                                    $model->score = ($score > 0 && isset($score) ? $score : null);
                                }
                                else
                                {
                                    throw new \Exception('Invalid Qualification',GlobalList::API_RESPONSE_CODE['601']);
                                }
                                $model->grade = $grade;
                                $model->type = $scoreType;
                                $model->save();

                                return response()->json([
                                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                                    'error'=>'',
                                    'data'=>[
                                        'dataList'=>QualificationGrade::where('qualification_id',$qualification_id)->orderBy('score','asc')->get()
                                    ]
                                ]);
                            }
                        }
                        elseif($action=='delete')
                        {
                            $model->delete();

                            return response()->json([
                                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                                'error'=>'',
                                'data'=>[
                                    'dataList'=>QualificationGrade::where('qualification_id',$qualification_id)->orderBy('score','asc')->get()
                                ]
                            ]);
                        }
                    }
                }
            }
        }

        

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function addGrade(Request $request)
    {   
        $id = $request->get('grade_id');
        $grade = $request->get('grade');
        $score = $request->get('score');
        $qualification_id = $request->get('qualification_id');
        $cgpa = $request->get('cgpa');
        $fromScore = $request->get('fromScore');
        $toScore = $request->get('toScore');
        $scoreType = $request->get('scoreType');
        
        if(isset(
            $grade,
            $score,
            $qualification_id,
            $cgpa,
            $fromScore,
            $toScore,
            $scoreType
        ))
        {
            $qModel = Qualification::where('id',$qualification_id)->first();
            if(!empty($qModel))
            {
                $model = new QualificationGrade;
                if($scoreType == 1)
                {
                    $model->from_score = $fromScore;
                    $model->to_score = $toScore;
                }
                elseif($scoreType == 2)
                {
                    $model->cgpa = $cgpa;
                    $model->score = $score;
                }
                else
                {
                    throw new \Exception('Invalid Qualification',GlobalList::API_RESPONSE_CODE['601']);
                }
                $model->grade = $grade;
                $model->qualification_id = $qualification_id;
                $model->type = $scoreType;
                $model->save();

                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'qualification_grade'=>$qModel->qualification_grade
                    ]
                ]);
            }
            throw new \Exception('Invalid Qualification',GlobalList::API_RESPONSE_CODE['601']);
        }
    }
}
