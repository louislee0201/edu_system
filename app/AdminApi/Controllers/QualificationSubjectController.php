<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;

use App\Model\Qualification;
use App\Model\QualificationSubject;

use App\Tools\Tools;
use App\Tools\GlobalList;

use DB;

class QualificationSubjectController
{
    public function getQualificationSubject(Request $request)
    {
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>QualificationSubject::getQualificationSubjectList($request)
        ]);
    }

	public function deleteQualificationSubject(Request $request)
    {   
        $id = $request->get('qualification_subject_id');
        $type = $request->get('type');
        if(isset($id))
        {
            $model = QualificationSubject::where('id',$id)->first();
            if(!empty($model))
            {
                $qualification_id = $model->qualification_id;
                if($type == 'delete')
                {
                    $model->delete();   
                }
                elseif($type == 'activation')
                {
                    if($model->active == 0)
                    {
                        $model->active = 1;
                    }
                    else
                    {
                        $model->active = 0;
                    }
                    $model->save();
                }
                else
                {
                    throw new \Exception('Invalid Type',GlobalList::API_RESPONSE_CODE['601']);
                }
                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'qualification_subject'=>QualificationSubject::where('qualification_id',$qualification_id)->orderBy('id','desc')->get()
                    ]
                ]);
            }

            throw new \Exception('Invalid Qualification',GlobalList::API_RESPONSE_CODE['601']);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function editQualificationSubject(Request $request)
    {   
        $qualification_id = $request->get('qualification_id');
        $is_proficiency = $request->get('is_proficiency');
        $subject_name = $request->get('qualification_subject');
        $language = $request->get('language');
        $action = $request->get('action');
        if(isset($action))
        {
            if($action=='add')
            {
                if(isset($qualification_id,$subject_name,$is_proficiency,$language))
                {
                    $model = new QualificationSubject;
                    $model->qualification_id = $qualification_id;
                    $model->is_proficiency = $is_proficiency;
                    $model->code = $request->get('code');
                    $model->subject_name = $subject_name;
                    $model->language = $language;
                    $model->save();

                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'dataList'=>QualificationSubject::where('qualification_id',$model->qualification_id)->orderBy('id','desc')->get()
                        ]
                    ]);
                }
            }
            else
            {
                $id = $request->get('qualification_subject_id');
                if(isset($id))
                {
                    $model = QualificationSubject::where('id',$id)->first();
                    if(!empty($model))
                    {
                        if($action=='activation')
                        {
                            $model->active = ($model->active?0:1);
                            $model->save();

                            return response()->json([
                                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                                'error'=>'',
                                'data'=>[
                                    'dataList'=>QualificationSubject::where('qualification_id',$model->qualification_id)->orderBy('id','desc')->get()
                                ]
                            ]);
                        }
                        elseif($action=='edit' && isset($is_proficiency,$subject_name))
                        {
                            $model->subject_name = $subject_name;
                            $model->code = $request->get('code');
                            $model->is_proficiency = $is_proficiency;

                            if($is_proficiency == 1 && isset($language))
                            {
                                if($language > 0)
                                {
                                    $model->language = $language;
                                }
                                else
                                {
                                    throw new \Exception('Invalid Qualification',GlobalList::API_RESPONSE_CODE['601']);
                                }
                            }
                            else
                            {
                                $model->language = null;
                            }
                            $model->save();

                            return response()->json([
                                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                                'error'=>'',
                                'data'=>[
                                    'dataList'=>QualificationSubject::where('qualification_id',$model->qualification_id)->orderBy('id','desc')->get()
                                ]
                            ]);
                        }
                        elseif($action=='delete')
                        {
                            $model->delete();
                            return response()->json([
                                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                                'error'=>'',
                                'data'=>[
                                    'dataList'=>QualificationSubject::where('qualification_id',$model->qualification_id)->orderBy('id','desc')->get()
                                ]
                            ]);
                        }
                    }
                }
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function addQualificationSubject(Request $request)
    {   
        $qualification_id = $request->get('qualification_id');
        $subject_name = $request->get('subject_name');
        $is_proficiency = $request->get('is_proficiency');
        $language = $request->get('language');

        if(isset(
            $qualification_id,
            $subject_name,
            $is_proficiency
        ))
        {
            $qModel = Qualification::where('id',$qualification_id)->first();
            if(!empty($qModel))
            {
                $model = new QualificationSubject;
                $model->qualification_id = $qualification_id;
                $model->subject_name = $subject_name;
                $model->code = $request->get('code');
                $model->is_proficiency = $is_proficiency;
                if($is_proficiency == 1)
                {
                    if(isset($language) && $language > 0)
                    {
                        $model->language = $language;
                    }
                    else
                    {
                        throw new \Exception('Invalid Qualification',GlobalList::API_RESPONSE_CODE['601']);
                    }
                }
                $model->save();

                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'qualification_subject'=>$qModel->qualification_subject
                    ]
                ]);
            }
            throw new \Exception('Invalid Qualification',GlobalList::API_RESPONSE_CODE['601']);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }
}
