<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;

use App\Model\MainTag;
use App\Model\SubTag;
use App\Model\ProficiencyCountry;

use App\Tools\Tools;
use App\Tools\GlobalList;

class MainTagController
{
    public function getTag(Request $request)
    {
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
            	'mainTagList'=>MainTag::orderBy('main_tag')->get(),
            	'subTagList'=>SubTag::orderBy('sub_tag')->get()
            ]
        ]);
    }

    public function updateSubTagList(Request $request)
    {
        $action = $request->get('action');

    	if(isset($action))
    	{
    		$sub_tag = $request->get('sub_tag');
    		$sub_tag_id = $request->get('sub_tag_id');
    		$main_tag_id = $request->get('main_tag_id');
            $sub_country_id = $request->get('sub_country_id');
    		
    		$stModel = '';
    		if($action == 'add' && isset($sub_tag) && isset($main_tag_id) && isset($sub_country_id))
    		{
    			if(!empty(MainTag::where('id',$main_tag_id)->first()))
    			{
    				$stModel = new SubTag;
	    			$stModel->sub_tag = $sub_tag;
	    			$stModel->main_tag_id = $main_tag_id;
                    $stModel->country_id = $sub_country_id;
	    			$stModel->save();
    			}
    			
    		}
    		elseif($action == 'edit' && isset($sub_tag_id,$sub_tag,$sub_country_id))
    		{
    			$stModel = SubTag::where('id',$sub_tag_id)->first();
    			$stModel->sub_tag = $sub_tag;
                $stModel->country_id = $sub_country_id;
    			$stModel->save();
    		}
    		elseif($action == 'delete' && isset($sub_tag_id))
    		{

    			$stModel = SubTag::where('id',$sub_tag_id)->first();
    			$stModel->delete();  
    		}
    		elseif($action == 'activation' && isset($sub_tag_id))
    		{
    			$stModel = SubTag::where('id',$sub_tag_id)->first();
    			$stModel->active = $stModel->active ? false : true;
    			$stModel->save();
    		}
    		if($stModel != '')
    		{
    			$mtModel = MainTag::where('id',$stModel->main_tag_id)->first();
    			return response()->json([
		            'code'=>GlobalList::API_RESPONSE_CODE['200'],
		            'error'=>'',
		            'data'=>[
		            	'main_tag'=>$mtModel->main_tag,
		            	'subTagList'=>$mtModel->sub_tag
		            ]
		        ]);
    		}
    	}

    	throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }
    

    public function updateMainTagList(Request $request)
    {
    	$action = $request->get('action');

    	if(isset($action))
    	{
    		$main_tag = $request->get('main_tag');
    		$main_tag_id = $request->get('main_tag_id');
            $country_id = $request->get('country_id');
            $id = $request->get('id');

    		if($action == 'add' && isset($main_tag))
    		{
    			$mtModel = new MainTag;
    			$mtModel->main_tag = $main_tag;
    			$mtModel->save();
    		}
    		elseif($action == 'edit' && isset($main_tag_id,$main_tag,$country_id))
    		{
    			$mtModel = MainTag::where('id',$main_tag_id)->first();
    			$mtModel->main_tag = $main_tag;
                $mtModel->country_id = $country_id;
    			$mtModel->save();
                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'main_tag'=>$mtModel->main_tag,
                        'country_id'=>$mtModel->country_id,
                        'countryList'=>ProficiencyCountry::all(),
                        'subTagList'=>$mtModel->sub_tag
                    ]
                ]);
    		}
    		elseif($action == 'delete' && isset($id))
    		{
    			$mtModel = MainTag::where('id',$id)->first();
    			$mtModel->delete();  
    		}
    		elseif($action == 'activation' && isset($id))
    		{
    			$mtModel = MainTag::where('id',$id)->first();
    			$mtModel->active = $mtModel->active ? false : true;
    			$mtModel->save();
    		}

    		return $this->getTag($request);
    	}

    	$isList = $request->get('isList');
    	if(isset($isList))
    	{	
    		$main_tag_id = $request->get('main_tag_id');
    		if(!$isList && isset($main_tag_id))
    		{
    			$mtModel = MainTag::where('id',$main_tag_id)->first();
    			return response()->json([
		            'code'=>GlobalList::API_RESPONSE_CODE['200'],
		            'error'=>'',
		            'data'=>[
		            	'main_tag'=>$mtModel->main_tag,
                        'country_id'=>$mtModel->country_id,
                        'countryList'=>ProficiencyCountry::all(),
		            	'subTagList'=>$mtModel->sub_tag
		            ]
		        ]);
    		}
    	}

    	throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }
    
}