<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;

use App\Model\Proficiency;
use App\Model\ProficiencyCountry;
use App\Model\ProficiencyLanguage;
use App\Model\ProficiencyGrade;
use App\Model\ProficiencyInfo;
use App\Model\School;

use App\Tools\Tools;
use App\Tools\GlobalList;


use DB;

class ProficiencyController
{
    public function editCountry(Request $request)
    {
        $action = $request->get('action');
        if(isset($action))
        {
            if($action=='add')
            {
                $country = $request->get('country');
                if(isset($country))
                {
                    $model = new ProficiencyCountry;
                    $model->country = $country;
                    $model->save();

                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'countryList'=>ProficiencyCountry::all()
                        ]
                    ]);
                }
            }
            else
            {
                $country_id = $request->get('country_id');
                $country = $request->get('country');
                if(isset($country_id))
                {
                    $model = ProficiencyCountry::where('id',$country_id)->first();
                    if($action=='activation' && !empty($model))
                    {
                        $model->active = ($model->active ? false : true);
                        $model->save();

                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>'',
                            'data'=>[
                                'countryList'=>ProficiencyCountry::all()
                            ]
                        ]);
                    }
                    elseif($action=='edit' && isset($country) && !empty($model))
                    {
                        $model->country = $country;
                        $model->save();

                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>'',
                            'data'=>[
                                'countryList'=>ProficiencyCountry::all()
                            ]
                        ]);
                    }
                    elseif($action=='delete' && !empty($model))
                    {
                        $model->delete();

                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>'',
                            'data'=>[
                                'countryList'=>ProficiencyCountry::all()
                            ]
                        ]);
                    }
                }
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']); 
    }

    public function getCountry(Request $request)
    {   
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'countryList'=>ProficiencyCountry::all(),
                'languageList'=>ProficiencyLanguage::all(),
                'schoolTypeList'=>School::SCHOOL_TYPE,
            ]
        ]);
    }

	public function getProficiency(Request $request)
    {   
        $isList = $request->get('isList');
        if(isset($isList))
        {
            if($isList)
            {
                return Proficiency::getProficiencyList($request);
            }

            $id = $request->get('proficiency_id');

            if(isset($id))
            {
                $model = Proficiency::where('id',$id)->first();
                if(!empty($model))
                {
                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'proficiency_id'=>$model->id,
                            'proficiency_name'=>$model->proficiency_name,
                            'proficiency_language'=>$model->proficiency_language,
                            'language'=>$model->language,
                            'country'=>$model->proficiency_country_id,
                            'proficiencyGradeList'=>$model->proficiency_grade,
                            'scoreTypeList'=>ProficiencyGrade::TYPE,
                            'languageList'=>ProficiencyLanguage::all(),
                            'countryList'=>ProficiencyCountry::all(),
                            'infoList'=>$model->proficiency_info
                        ]
                    ]);
                }
            }

            throw new \Exception('Invalid Proficiency',GlobalList::API_RESPONSE_CODE['601']);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
        
    }

    public function deleteProficiency(Request $request)
    {
        $proficiency_id = $request->get('proficiency_id');  
                        
        if(isset($proficiency_id))
        {
            $model = Proficiency::where('id',$proficiency_id)->first();
            if(!empty($model))
            {
                $model->active = ($model->active?0:1);
                $model->save();
                return $this->getProficiency($request);
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function addProfiency(Request $request)
    {
        $language = $request->get('language');
        $proficiency_country_id = $request->get('country');
        $proficiency_name = $request->get('proficiency_subject');

        $reading_score_start = $request->get('reading_start'); 
        $reading_score_end = $request->get('reading_end'); 

        $speaking_score_start = $request->get('speaking_start'); 
        $speaking_score_end = $request->get('speaking_end'); 

        $writing_score_start = $request->get('writing_start'); 
        $writing_score_end = $request->get('writing_end'); 

        $listening_score_start = $request->get('listening_start'); 
        $listening_score_end = $request->get('listening_end'); 
                        
        if(isset($language,$proficiency_country_id,$proficiency_name))
        {
            $model = New Proficiency;
            $model->language = $language;
            $model->proficiency_name = $proficiency_name;
            $model->proficiency_country_id = $proficiency_country_id;
            $model->save();

            $modelInfo = new ProficiencyInfo;
            $modelInfo->proficiency_id = $model->id;

            if(isset($reading_score_start,$reading_score_end))
            {
                $modelInfo->reading_score_start = $reading_score_start;
                $modelInfo->reading_score_end = $reading_score_end;
            }

            if(isset($speaking_score_start,$speaking_score_end))
            {
                $modelInfo->speaking_score_start = $speaking_score_start;
                $modelInfo->speaking_score_end = $speaking_score_end;
            }

            if(isset($writing_score_start,$writing_score_end))
            {
                $modelInfo->writing_score_start = $writing_score_start;
                $modelInfo->writing_score_end = $writing_score_end;
            }

            if(isset($listening_score_start,$listening_score_end))
            {
                $modelInfo->listening_score_start = $listening_score_start;
                $modelInfo->listening_score_end = $listening_score_end;
            }

            $modelInfo->save();

            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                'error'=>'',
                'data'=>$model->id
            ]);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function editProficiency(Request $request)
    {
        $language = $request->get('language');
        $country = $request->get('country');
        $id = $request->get('proficiency_id');
        $proficiency_name = $request->get('proficiency_name');
        $infoList = $request->get('infoList');
        $reading_score_start = $request->get('reading_start'); 
        $reading_score_end = $request->get('reading_end'); 

        $speaking_score_start = $request->get('speaking_start'); 
        $speaking_score_end = $request->get('speaking_end'); 

        $writing_score_start = $request->get('writing_start'); 
        $writing_score_end = $request->get('writing_end'); 

        $listening_score_start = $request->get('listening_start'); 
        $listening_score_end = $request->get('listening_end'); 
        $action = $request->get('action');

        if(isset($action))
        {
            if(isset($id))
            {
                $model = Proficiency::where('id',$id)->first();
                if(!empty($model))
                {
                    if($action == 'delete')
                    {
                        $model->proficiency_info->delete();
                        $model->delete();
                        
                        return $this->getProficiency($request);
                    }
                    elseif($action == 'edit' && isset($infoList,$language,$country,$proficiency_name))
                    {
                        $reading_score_start = $infoList['reading_score_start'];
                        $reading_score_end = $infoList['reading_score_end'];

                        $speaking_score_start = $infoList['speaking_score_start'];
                        $speaking_score_end = $infoList['speaking_score_end'];

                        $writing_score_start = $infoList['writing_score_start'];
                        $writing_score_end = $infoList['writing_score_end'];

                        $listening_score_start = $infoList['listening_score_start'];
                        $listening_score_end = $infoList['listening_score_end'];

                        $model->language = $language;
                        $model->proficiency_name = $proficiency_name;
                        $model->proficiency_country_id = $country;
                        $model->save();

                        $modelInfo = $model->proficiency_info;

                        if(isset($reading_score_start,$reading_score_end))
                        {
                            $modelInfo->reading_score_start = $reading_score_start;
                            $modelInfo->reading_score_end = $reading_score_end;
                        }

                        if(isset($speaking_score_start,$speaking_score_end))
                        {
                            $modelInfo->speaking_score_start = $speaking_score_start;
                            $modelInfo->speaking_score_end = $speaking_score_end;
                        }

                        if(isset($writing_score_start,$writing_score_end))
                        {
                            $modelInfo->writing_score_start = $writing_score_start;
                            $modelInfo->writing_score_end = $writing_score_end;
                        }

                        if(isset($listening_score_start,$listening_score_end))
                        {
                            $modelInfo->listening_score_start = $listening_score_start;
                            $modelInfo->listening_score_end = $listening_score_end;
                        }

                        $modelInfo->save();

                        return $this->getProficiency($request);
                    }
                    elseif($action == 'activation')
                    {
                        $model->active = ($model->active?0:1);
                        $model->save();

                        return $this->getProficiency($request);
                    }  
                }
            }

            if($action == 'add' && isset($language,$country,$proficiency_name))
            {
                $model = New Proficiency;
                $model->language = $language;
                $model->proficiency_name = $proficiency_name;
                $model->proficiency_country_id = $country;
                $model->save();

                $modelInfo = new ProficiencyInfo;
                $modelInfo->proficiency_id = $model->id;

                if(isset($reading_score_start,$reading_score_end))
                {
                    $modelInfo->reading_score_start = $reading_score_start;
                    $modelInfo->reading_score_end = $reading_score_end;
                }

                if(isset($speaking_score_start,$speaking_score_end))
                {
                    $modelInfo->speaking_score_start = $speaking_score_start;
                    $modelInfo->speaking_score_end = $speaking_score_end;
                }

                if(isset($writing_score_start,$writing_score_end))
                {
                    $modelInfo->writing_score_start = $writing_score_start;
                    $modelInfo->writing_score_end = $writing_score_end;
                }

                if(isset($listening_score_start,$listening_score_end))
                {
                    $modelInfo->listening_score_start = $listening_score_start;
                    $modelInfo->listening_score_end = $listening_score_end;
                }

                $modelInfo->save();

                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>$model->id
                ]);
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function getLanguage(Request $request)
    {
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'languageList'=>ProficiencyLanguage::all()
            ]
        ]);
    }
}
