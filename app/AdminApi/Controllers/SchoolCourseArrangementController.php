<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;

use App\Model\School;
use App\Model\SchoolCourseArrangement;
use App\Model\SchoolCourse;

use App\Tools\Tools;
use App\Tools\GlobalList;

use DB;
class SchoolCourseArrangementController
{
	public function editSchoolCourseArrangementTitle(Request $request)
	{
		$school_id = $request->get('school_id');
		$title = $request->get('title');
		$changeTo = $request->get('changeTo');
		$course_id = $request->get('course_id');
		$action = $request->get('action');
		$editedRangeKeyCourse = $request->get('editedRangeKeyCourse');
		$editTitleKey = $request->get('editTitleKey');
		$editedRangeKeyTitle = $request->get('editedRangeKeyTitle');
		$arrangement_id = $request->get('arrangement_id');

		if(isset($action,$school_id))
		{
			$sModel = School::where('id',$school_id)->first();
			if(!empty($sModel))
			{
				if($action == 'add' && isset($title))
				{
					$model = new SchoolCourseArrangement;
					$model->title = $title;
					$model->school_id = $school_id;
					$checkModel = SchoolCourseArrangement::where('school_id',$school_id)->get()->max('range_key');
					$model->range_key = ($checkModel !== null ? $checkModel + 1 : 0);
					$model->save();
					
					return response()->json([
	                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
	                    'error'=>'',
	                    'data'=>[
	                        'schoolArrangementList'=>SchoolCourseArrangement::where('school_id',$school_id)->orderBy('range_key','ASC')->get()
	                    ]
	                ]);
				}
				elseif($action == 'edit' && isset($title,$editTitleKey))
				{
					$model = SchoolCourseArrangement::where('id',$editTitleKey)->first();
					if(!empty($model))
					{
						$model->title = $title;
						$model->save();

						return response()->json([
		                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
		                    'error'=>'',
		                    'data'=>[
		                        'schoolArrangementList'=>SchoolCourseArrangement::where('school_id',$school_id)->orderBy('range_key','ASC')->get()
		                    ]
		                ]);
					}
				}
				elseif($action == 'editTitleRangeKey' && isset($editedRangeKeyTitle))
				{
					foreach ($editedRangeKeyTitle as $arrangement_id => $range_key) 
					{
						$cModel = SchoolCourseArrangement::where('id',$arrangement_id)->first();
						if(empty($cModel))
						{
							throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
						}

						$cModel->range_key = $range_key;
						$cModel->save();
					}

					return response()->json([
	                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
	                    'error'=>'',
	                    'data'=>[
	                        'schoolArrangementList'=>SchoolCourseArrangement::where('school_id',$school_id)->orderBy('range_key','ASC')->get()
	                    ]
	                ]);

				}
				elseif($action == 'deleteTitle' && isset($arrangement_id))
				{
					$cModel = SchoolCourseArrangement::where('id',$arrangement_id)->first();
					if(!empty($cModel))
					{
						DB::update('UPDATE tbl_school_course SET school_course_arrangement_id = 0 WHERE school_course_arrangement_id = '.$arrangement_id);
						$cModel->delete();
					}

					return response()->json([
	                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
	                    'error'=>'',
	                    'data'=>[
	                    	'schoolCourseArrangementList'=>SchoolCourse::where('school_id',$sModel->id)->orderBy('range_key','ASC')->get()->groupBy('school_course_arrangement_id'),
	                        'schoolArrangementList'=>SchoolCourseArrangement::where('school_id',$school_id)->orderBy('range_key','ASC')->get()
	                    ]
	                ]);

				}
				elseif($action == 'editCourse' && isset($course_id,$changeTo))
				{
					$scModel = $sModel->school_course->where('id',$course_id)->first();
					if(!empty($scModel))
					{
						$scModel->school_course_arrangement_id = $changeTo;
						$scModel->save();
						
						return response()->json([
		                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
		                    'error'=>'',
		                    'data'=>[
		                        'schoolCourseArrangementList'=>SchoolCourse::where('school_id',$sModel->id)->orderBy('range_key','ASC')->get()->groupBy('school_course_arrangement_id')
		                    ]
		                ]);
					}
				}
				elseif($action == 'editCourseRangeKey' && isset($editedRangeKeyCourse))
				{
					foreach ($editedRangeKeyCourse as $course_id => $range_key) 
					{
						$cModel = $sModel->school_course->where('id',$course_id)->first();
						if(empty($cModel))
						{
							throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
						}

						$cModel->range_key = $range_key;
						$cModel->save();
					}

					return response()->json([
	                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
	                    'error'=>'',
	                    'data'=>[
	                        'schoolCourseArrangementList'=>SchoolCourse::where('school_id',$sModel->id)->orderBy('range_key','ASC')->get()->groupBy('school_course_arrangement_id')
	                    ]
	                ]);
				}
			}
		}
		throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
	}
}