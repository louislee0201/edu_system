<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Model\Config;
use App\Model\School;
use App\Model\SchoolCourse;
use App\Model\ProficiencyLanguage;
use App\Model\ProficiencyCountry;
use App\Model\Region;

use App\Tools\GlobalList;

use DB;

class ConfigController
{
    public function pdf($direction,$fileName)
    {
        if(!empty($fileName))
        {
            $explode = explode('.', $fileName);
            $name = $explode[0];
            $date = date("Y-m-d", $name);
            $des = storage_path('app/pdf/'.$direction.'/'.$date.'/'.$fileName);

            
            if(file_exists($des))
            {
                header('Content-type: application/pdf'); 
                header('Content-Transfer-Encoding: binary'); 
                header('Accept-Ranges: bytes');
                echo file_get_contents($des);
            }
        }
    }
    public function image($direction,$fileName)
    {
        if(!empty($fileName))
        {
            $explode = explode('.', $fileName);
            $name = $explode[0];
            $date = date("Y-m-d", $name);
            $des = storage_path('app/image/'.$direction.'/'.$date.'/'.$fileName);

            
            if(file_exists($des))
            {
                header('Content-type: image/png');
                echo file_get_contents($des);
            }
        }
    }

    public function getReionList(Request $request)
    {
        $country_id = $request->get('country_id');

        if(isset($country_id))
        {
            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                'error'=>'',
                'data'=>[
                    'regionList'=>Region::where('country_id',$country_id)->get()
                ]
            ]);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }
    public function updateRegion(Request $request)
    {
        $action = $request->get('action');
        $country_id = $request->get('country_id');
        $region_id = $request->get('region_id');
        $region = $request->get('region');

        if(isset($action))
        {
            if($action=='add' && isset($country_id,$region))
            {
                $checkModel = ProficiencyCountry::where('id',$country_id)->first();
                if(!empty($checkModel))
                {
                    $model = new Region;
                    $model->country_id = $country_id;
                    $model->region = $region;
                    $model->save();

                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'regionList'=>Region::where('country_id',$country_id)->get()
                        ]
                    ]);
                }
            }
            else
            {
                if(isset($region_id,$country_id))
                {
                    $model = Region::where('country_id',$country_id)->where('id',$region_id)->first();
                    if($action=='activation' && !empty($model))
                    {
                        $model->active = ($model->active ? false : true);
                        $model->save();

                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>'',
                            'data'=>[
                                'regionList'=>Region::where('country_id',$country_id)->get()
                            ]
                        ]);
                    }
                    elseif($action=='edit' && isset($region) && !empty($model))
                    {
                        $model->region = $region;
                        $model->save();

                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>'',
                            'data'=>[
                                'regionList'=>Region::where('country_id',$country_id)->get()
                            ]
                        ]);
                    }
                    elseif($action=='delete' && !empty($model))
                    {
                        $model->delete();

                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>'',
                            'data'=>[
                                'regionList'=>Region::where('country_id',$country_id)->get()
                            ]
                        ]);
                    }
                }
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }
}
