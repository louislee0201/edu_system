<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;

use App\Model\Proficiency;
use App\Model\ProficiencyGrade;

use App\Tools\Tools;
use App\Tools\GlobalList;

use DB;

class ProficiencyGradeController
{
    public function addProfiencyGrade(Request $request)
    {   
        $id = $request->get('grade_id');
        $grade = $request->get('grade');
        $score = $request->get('score');
        $proficiency_id = $request->get('proficiency_id');
        $cgpa = $request->get('cgpa');
        $fromScore = $request->get('fromScore');
        $toScore = $request->get('toScore');
        $scoreType = $request->get('scoreType');
        
        if(isset(
            $grade,
            $score,
            $proficiency_id,
            $cgpa,
            $scoreType
        ))
        {
            $qModel = Proficiency::where('id',$proficiency_id)->first();
            if(!empty($qModel))
            {
                $model = new ProficiencyGrade;
                if($scoreType == 1 && isset($fromScore,$toScore))
                {
                    $model->from_score = $fromScore;
                    $model->to_score = $toScore;
                }
                elseif($scoreType == 2)
                {
                    $model->cgpa = $cgpa;
                    $model->score = $score;
                }
                else
                {
                    throw new \Exception('Invalid Proficiency',GlobalList::API_RESPONSE_CODE['601']);
                }
                $model->grade = $grade;
                $model->proficiency_id = $proficiency_id;
                $model->type = $scoreType;
                $model->save();

                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'proficiencyGradeList'=>$qModel->proficiency_grade
                    ]
                ]);
            }
            throw new \Exception('Invalid Proficiency',GlobalList::API_RESPONSE_CODE['601']);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function activationProficiencyGrade(Request $request)
    {   
        $id = $request->get('grade_id');
        $proficiency_id = $request->get('proficiency_id');
        $type = $request->get('type');

        if(isset($id,$proficiency_id,$type))
        {
            $model = ProficiencyGrade::where('id',$id)->where('proficiency_id',$proficiency_id)->first();
            if(!empty($model))
            {
                if($type == 'delete')
                {
                    $model->delete();   
                }
                elseif($type == 'activation')
                {
                    if($model->active == 0)
                    {
                        $model->active = 1;
                    }
                    else
                    {
                        $model->active = 0;
                    }
                    $model->save();
                }
                else
                {
                    throw new \Exception('Invalid Type',GlobalList::API_RESPONSE_CODE['601']);
                }
                
                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>[
                        'proficiencyGradeList'=>ProficiencyGrade::where('proficiency_id',$proficiency_id)->orderBy('id','desc')->get()
                    ]
                ]);
            }

            throw new \Exception('Invalid Grade',GlobalList::API_RESPONSE_CODE['601']);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function editProficiencyGrade(Request $request)
    {   
        $id = $request->get('grade_id');
        $grade = $request->get('grade');
        $score = $request->get('score');
        $proficiency_id = $request->get('proficiency_id');
        $cgpa = $request->get('cgpa');
        $fromScore = $request->get('fromScore');
        $toScore = $request->get('toScore');
        $scoreType = $request->get('scoreType');
        $action = $request->get('action');

        if(isset($action,$proficiency_id))
        {
            if(isset($id))
            {
                $model = ProficiencyGrade::where('id',$id)->where('proficiency_id',$proficiency_id)->first();
                if(!empty($model))
                {
                    if($action == 'delete')
                    {
                        $model->delete();
                        
                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>'',
                            'data'=>[
                                'proficiencyGradeList'=>ProficiencyGrade::where('proficiency_id',$proficiency_id)->orderBy('id','desc')->get()
                            ]
                        ]);
                    }
                    elseif($action == 'edit' && isset($scoreType,$grade))
                    {
                        if($scoreType == 1 && isset($fromScore,$toScore))
                        {
                            $model->from_score = $fromScore;
                            $model->to_score = $toScore;
                        }
                        elseif($scoreType == 2)
                        {
                            $model->cgpa = ($cgpa > 0 && isset($cgpa) ? $cgpa : null);
                            $model->score = ($score > 0 && isset($score) ? $score : null);
                        }
                        else
                        {
                            throw new \Exception('Invalid Proficiency',GlobalList::API_RESPONSE_CODE['601']);
                        }
                        $model->grade = strtoupper($grade);
                        $model->proficiency_id = $proficiency_id;
                        $model->type = $scoreType;
                        $model->save();
                           
                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>'',
                            'data'=>[
                                'proficiencyGradeList'=>ProficiencyGrade::where('proficiency_id',$model->proficiency_id)->orderBy('id','desc')->get()
                            ]
                        ]);
                    }
                    elseif($action == 'activation')
                    {
                        $model->active = ($model->active?0:1);
                        $model->save();

                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>'',
                            'data'=>[
                                'proficiencyGradeList'=>ProficiencyGrade::where('proficiency_id',$model->proficiency_id)->orderBy('id','desc')->get()
                            ]
                        ]);
                    }  
                }
            }

            if($action == 'add' && isset($scoreType,$grade))
            {
                $qModel = Proficiency::where('id',$proficiency_id)->first();
                $gmodel = ProficiencyGrade::where('grade',strtoupper($grade))->where('proficiency_id',$proficiency_id)->first();
                if(!empty($qModel) && empty($gmodel))
                {
                    $model = new ProficiencyGrade;
                    if($scoreType == 1 && isset($fromScore,$toScore))
                    {
                        $model->from_score = $fromScore;
                        $model->to_score = $toScore;
                    }
                    elseif($scoreType == 2 && isset($cgpa,$score))
                    {
                        $model->cgpa = $cgpa;
                        $model->score = $score;
                    }
                    else
                    {
                        throw new \Exception('Invalid Proficiency',GlobalList::API_RESPONSE_CODE['601']);
                    }
                    $model->grade = strtoupper($grade);
                    $model->proficiency_id = $proficiency_id;
                    $model->type = $scoreType;
                    $model->save();

                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'proficiencyGradeList'=>$qModel->proficiency_grade
                        ]
                    ]);
                }
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }
}

