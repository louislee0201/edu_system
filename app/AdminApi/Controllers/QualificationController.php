<?php

namespace App\AdminApi\Controllers;

use Illuminate\Http\Request;

use App\Model\Qualification;
use App\Model\QualificationSubject;
use App\Model\QualificationGrade;
use App\Model\ProficiencyCountry;
use App\Model\ProficiencyLanguage;
use App\Model\ProficiencyGrade;

use App\Tools\Tools;
use App\Tools\GlobalList;

use DB;

class QualificationController
{
	public function getQualification(Request $request)
    {   
        $isList = $request->get('isList');
        if(isset($isList))
        {
            if($isList)
            {
                return Qualification::getQualificationList($request);
            }

            $id = $request->get('qualification_id');

            if(isset($id))
            {
                $model = Qualification::where('id',$id)->first();
                if(!empty($model))
                {
                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'qualification_id'=>$model->id,
                            'qualification_name'=>$model->qualification_name,
                            'country'=>$model->country,
                            'qualification_subject'=>$model->qualification_subject,
                            'qualification_grade'=> $model->qualification_grade,
                            'countryList'=>ProficiencyCountry::all(),
                            'scoreTypeList'=>ProficiencyGrade::TYPE,
                            'languageList'=>ProficiencyLanguage::all(),
                        ]
                    ]);
                }
            }

            throw new \Exception('Invalid Qualification',GlobalList::API_RESPONSE_CODE['601']);
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
        
    }

    public function deleteQualification(Request $request)
    {
        $qualification_id = $request->get('qualification_id');  
                        
        if(isset($qualification_id))
        {
            $model = Qualification::where('id',$qualification_id)->first();
            if(!empty($model))
            {
                $model->active = ($model->active?0:1);
                $model->save();
                /*DB::delete('DELETE FROM tbl_qualification_subject WHERE qualification_id = '.$qualification_id);
                DB::delete('DELETE FROM tbl_qualification_grade WHERE qualification_id = '.$qualification_id);*/
                return $this->getQualification($request);
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function addNewQualification(Request $request)
    {
        $qualification_name = $request->get('qualification_name');
        $country = $request->get('country');
        
        if(isset(
            $qualification_name,
            $country
        ))
        {
            $model = new Qualification;
            $model->qualification_name = $qualification_name;
            $model->country = $country;
            $model->save();

            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['200'],
                'error'=>'',
                'data'=>$model->id
            ]); 
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }

    public function editQualification(Request $request)
    {
        $action = $request->get('action');
        $country = $request->get('country');
        $qualification_name = $request->get('qualification_name');
        if(isset($action))
        {
            if($action=='add' && isset($qualification_name,$country))
            {
                $model = new Qualification;
                $model->qualification_name = $qualification_name;
                $model->country = $country;
                $model->save();
                
                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                    'error'=>'',
                    'data'=>$model->id
                ]);
            }
            else
            {
                $qualification_id = $request->get('qualification_id');

                if(isset($qualification_id))
                {
                    $model = Qualification::where('id',$qualification_id)->first();
                    if(!empty($model))
                    {
                        if($action=='activation')
                        {
                            $model->active = ($model->active?0:1);
                            $model->save();

                            return $this->getQualification($request);
                        }
                        elseif($action=='edit' && isset($country,$qualification_name))
                        {
                            
                            $model->qualification_name = $qualification_name;
                            $model->country = $country;
                            $model->save();

                            return $this->getQualification($request);
                        }
                        elseif($action=='delete')
                        {
                            $model->delete();
                            return $this->getQualification($request);
                        }
                    }
                }
            }
        }

        throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']);
    }
}
