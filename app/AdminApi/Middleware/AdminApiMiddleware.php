<?php

namespace App\AdminApi\Middleware;

use Closure;
use Illuminate\Support\Facades\Hash;

use App\Tools\GlobalList;

use App\Model\User;
use App\Tools\Tools;

class AdminApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $api_token = $request->get('api_token');

        
        if(isset($api_token))
        {
            if(!$api_token)
            {
                throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['602']);
            }
            $userModel = User::where('api_token', $api_token)
                         ->first();

            if(empty($userModel))
            {
                throw new \Exception('Unauthorized',GlobalList::API_RESPONSE_CODE['602']);
            }

            $request->attributes->add(['userModel' => $userModel]);
        }
        else
        {
            throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['602']);
        }

        
        if(!Tools::checkRole($userModel->role,explode('/', $request->route()->uri)[1]))
        {
            throw new \Exception('Unauthorized',GlobalList::API_RESPONSE_CODE['601']);
        }

        return $next($request);
    }
}
