<?php

namespace App\Tools;

use App\Tools\GlobalList;

use App\Model\Customer;

class Tools 
{
	static function getToken($Uid)
	{
		$list = str_split('abcdefghijklmnopqrstuvwxyz'
                            .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                            .'1234567890');
        shuffle($list);
        $token = '';
        foreach (array_rand($list, 10) as $k) 
        {
            $token .= $list[$k];
        }

        return $Uid.$token;
	}

    static function checkRole($role, $requestPath)
    {
        foreach (GlobalList::ROLE_FUNCTION_VALIDATION as $key => $rules) 
        {
            if(( in_array($role, $rules['role']) || $rules['role'][0] == '*' ) && in_array($requestPath, $rules['path']))
            {
                return true;
            }
        }
        return false;
    }

    static function checkPermissionLevel($role,$permissionDetect)
    {
        if(GlobalList::PERMISSION_LEVEL[$role] < $permissionDetect)
        {
            return true;
        }
        return false;
    }
}
