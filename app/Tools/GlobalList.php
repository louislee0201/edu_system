<?php

namespace App\Tools;

class GlobalList 
{
	const PERMISSION_LEVEL = [
		'superb' => 10,
		'admin' => 9,
		'school' => 1
	];
	const ADMIN_PERMISSION = 9;

	const API_RESPONSE_CODE = [
		'200'=>200,//success
		'500'=>500,//internal error
		'601'=>601,//api key invalid
		'602'=>602,//token expired
	];

	const ROLE_FUNCTION_VALIDATION = [
		[
			'role'=>['*'],
			'path'=>['getLanguage','getRequirementAttr','getDescriptionType','getReionList','getSelection']
		],
		[
			'role'=>['admin','superb'],
			'path'=>[
					'getSchool','deleteSchool','addNewSchool','editSchool','getSchoolList','getQualification','addNewQualification','deleteQualification','editQualification','addQualificationSubject',
					'deleteQualificationSubject','editQualificationSubject','getProficiency','getCountry','editCountry','deleteGrade','editGrade','addGrade','addProfiency','editProficiency','deleteProficiency',
					'editLanguage','getQualificationSubject','addProfiencyGrade','editProficiencyGrade','activationProficiencyGrade','updateCountry',
				]
		],
		[
			'role'=>['admin','superb'],
			'path'=>[
					'updateSchool','updateCourse','updateCourseLevel','updateQualificationSubject','updateLanguage','updateQualificationGrade','updateProficiency','updateRegion','getTag','updateMainTagList','updateSubTagList',
					'editCourseSubTagList'
				]
		],
		[
			'role'=>['admin','superb','school'],
			'path'=>['getCourse','deleteCourse','addNewCourse','editCourse','getCourseLevelList','editCourseLevel','submitRequirement','updateRequirement','updateDescription','getDescriptionList','getDescriptionData',
				'editSchoolCourseArrangementTitle'
			]
		]
	];
}
