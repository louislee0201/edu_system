<?php

namespace App\Components;
use Mail;

class ENContent
{
	static function VERIFY_EMAIL_ACCOUNT_AUTH($data)
	{
        $subject = 'Verify email account auth';

        $content = '
            This email is to verify user permissions,
            <a href="https://www.edudotcom.my/veirfyEmailAccountAuth?vierfy_code='.$data->vierfy_code.'">Click here</a> if you are the person.
            *This verification will only be valid within 24 hours.
        ';
	}
}