<?php

namespace App\Components;
use Mail;
use App\Components\EnContent;

class Notification
{
    const VERIFY_EMAIL_ACCOUNT_AUTH = 'VERIFY_EMAIL_ACCOUNT_AUTH';

	static function sendMail($data)
	{
        $data = (Object)$data;
        
        Mail::send('emails.2021',
        	[
        		'subject'=> $data->subject,
            	'content' => $data->content
        	],function($message){
	            $message ->to($data->to)->subject($data->subject);
	        });
        //返回的一个错误数组，利用此可以判断是否发送成功
        if(count(Mail::failures())>0)
        {
        	Mail::send('Some thing error with email subject: '.$data->subject,function($message){
	            $message ->to('le3ck0201@gmail.com')->subject('Some thing error with email');
	        });
        }
	}
}