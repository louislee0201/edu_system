<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SchoolCourseRequirementIncludedInstead extends Model
{
    protected $table = 'school_course_requirement_included_instead';
    public $timestamps = false;
    protected $fillable = ['*'];

    const SCORE_MORE_THAN = 1;
    const SCORE_LESS_THAN = 2;
    const CGPA_MORE_THAN = 3;
    const CGPA_LESS_THAN = 4;

    const GRADE_TYPE = [
        1 => 'score more than',
        2 => 'score less than',
        3 => 'cgpa more than',
        4 => 'cgpa less than',
    ];
}
