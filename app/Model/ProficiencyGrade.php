<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProficiencyGrade extends Model
{
    protected $table = 'proficiency_grade';
    public $timestamps = false;
    protected $fillable = ['*'];

    const TYPE = [
    	1 => 'between score',
    	2 => 'fix score'
    ];
}
