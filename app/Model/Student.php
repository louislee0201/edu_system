<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'student';
    public $timestamps = true;
    protected $fillable = ['*'];

    const ACCOUNT_LEVEL_GUESS = 1;
    const ACCOUNT_LEVEL_USER = 2;

    public function student_academic_result()
    {
    	return $this->hasMany(StudentAcademicResult::class,'student_id','id');
    }
}
