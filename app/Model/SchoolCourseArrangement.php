<?php

namespace App\Model;

use App\Model\School;
use App\Model\SchoolCourseArrangement;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use DB;

class SchoolCourseArrangement extends Model
{
    protected $table = 'school_course_arrangement';
    public $timestamps = false;
    protected $fillable = ['*'];

    public function school_course()
    {
        return $this->hasMany(SchoolCourse::class,'school_course_arragement_id','id');
    }

    static function getCourseArragemented($school_id)
    {
    	$models = DB::select('
    		SELECT *
    		FROM tbl_school_course sc
    		LEFT JOIN tbl_school_course_arrangement sca ON sca.id = sc.school_course_arrangement_id
    		WHERE sc.school_id = '.$school_id.' AND
    		sc.school_course_arrangement_id > 0
    	');

    	return $models;
    }
}
