<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SchoolCourseRequirementGrade extends Model
{
    protected $table = 'school_course_requirement_grade';
    public $timestamps = false;
    protected $fillable = ['*'];
}
