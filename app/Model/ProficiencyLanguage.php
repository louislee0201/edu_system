<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProficiencyLanguage extends Model
{
    protected $table = 'proficiency_language';
    public $timestamps = false;
    protected $fillable = ['*'];
}
