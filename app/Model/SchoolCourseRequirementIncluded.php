<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SchoolCourseRequirementIncluded extends Model
{
    protected $table = 'school_course_requirement_included';
    public $timestamps = false;
    protected $fillable = ['*'];

    const CASE_HANDLING_NOT_INCLUDED = 0;
    const CASE_HANDLING_INCLUDED = 1;
    const CASE_HANDLING_INSTEAD = 2;
    const CASE_HANDLING_EXTRA_INCLUDED = 3;

    const CASE_HANDLING = [
    	0 => 'not included',
    	1 => 'included',
    	2 => 'instead',
    	3 => 'extra instead'
    ];
}
