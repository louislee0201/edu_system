<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DescriptionInfoSub extends Model
{
    protected $table = 'description_info_sub';
    public $timestamps = false;
    protected $fillable = ['*'];

    public function description_info_sub()
    {
        return $this->hasMany(DescriptionInfoSub::class,'description_info_id','id');
    }

    public function description_info() {
	    return $this->belongsTo(DescriptionInfo::class,'id','description_info_id');
	}
}
