<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Region extends Model
{
    protected $table = 'region';
    public $timestamps = false;
    protected $fillable = ['*'];

}
