<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use DB;

class SchoolCourseLevel extends Model
{
    protected $table = 'school_course_level';
    public $timestamps = false;
    protected $fillable = ['*'];
}
