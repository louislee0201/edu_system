<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QualificationGrade extends Model
{
    protected $table = 'qualification_grade';
    public $timestamps = false;
    protected $fillable = ['*'];
}
