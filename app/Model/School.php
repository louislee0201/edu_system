<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use DB;
use File;
use App\Model\User;
use App\Model\ProficiencyCountry;
use App\Model\Region;

use App\Tools\Tools;
use App\Tools\GlobalList;

class School extends Model
{
    protected $table = 'school';
    public $timestamps = true;
    protected $fillable = ['*'];

    const SCHOOL_TYPE = [
        1 => 'private',
        2 => 'public'
    ];

    const BASE_SCHOOL_IMAGE_URL = '/image/school/';

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function school_course_arrangement()
    {
        return $this->hasMany(SchoolCourseArrangement::class,'school_id','id');
    }

    public function school_course()
    {
        return $this->hasMany(SchoolCourse::class,'school_id','id');
    }

    static function getInterestSchoolList($scModel,$limit)
    {
        $search_level = 3;
        $model = [];
        $filterList = '';
        do{
            if($search_level == 3)
            {
                $filterList = '
                    s.region = '.$scModel->region.' AND
                    s.country = '.$scModel->country.' AND
                ';
            }
            elseif($search_level == 2)
            {
                $filterList = '
                    s.country = '.$scModel->country.' AND
                ';
            }
            elseif($search_level == 1)
            {
                $filterList = '';
            }

            $model = DB::select('
                SELECT * 
                FROM tbl_school s
                WHERE '.$filterList.' 
                s.id != '.$scModel->id.'
                Limit '.$limit.'

            ');
            $search_level--;
        } while(count($model) < $limit && $search_level > 0);
        //throw new \Exception($search_level,GlobalList::API_RESPONSE_CODE['601']);
        return $model;
    }

    static function frontendGetSchoolList($request)
    {
        $clickPage = $request->get('clickPage');
        $filterArr = $request->get('filterArr');
        $userModel = $request->get('userModel');

        $schoolModels = DB::table('school')->select('school.id as school_id', 'school.*')->inRandomOrder();

        if($userModel->school_id)
        {
            $schoolModels = $schoolModels->where('school.id', '=', $userModel->school_id);
        }
        else
        {
            $schoolModels = $schoolModels->where('school.active', '=', true);
        }
        
        if(isset($filterArr))
        {
            foreach ($filterArr as $key => $value) 
            {
                if(!empty($value))
                {
                    $schoolModels = $schoolModels->where($key, 'like', '%'.$value[0].'%');
                }
            }
        }

        $schoolModels = $schoolModels->get();

        //$dataList = $models->items;
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'totalItem'=>count($schoolModels),
                'dataList'=>$schoolModels
            ]
        ]);
    }

    static function getSchoolList($request)
    {
        $clickPage = $request->get('clickPage');
        $filterArr = $request->get('filterArr');

        $schoolModels = DB::table('school')
                          ->leftJoin('user', 'school.user_id', '=', 'user.id')
                          ->leftJoin('student as s2', 'user.username', '=', 's2.email')
                          ->leftJoin('student', 'school.id', '=', 'student.school_id')
        				  ->select('school.id as school_id', 'school.*', 'user.active as acc_status', 'user.username as email','student.school_id as generated_account', 's2.email as sEmail')
        				  ->orderBy('id', 'desc');

        if(isset($filterArr))
        {
            foreach ($filterArr as $key => $value) 
            {
                if(!empty($value))
                {
                    if($key == 'school_name_cn' || $key == 'school_name')
                    {
                        $schoolModels = $schoolModels->where('school.school_name', 'like', '%'.$value[0].'%')->orWhere('school.school_name_cn', 'like', '%'.$value[0].'%');
                    }
                    else
                    {
                       $schoolModels = $schoolModels->where($key, 'like', '%'.$value[0].'%'); 
                    }
                }
            }
        }

        $schoolModels = $schoolModels->Paginate(
                        10,
                        ['*'],
                        'page',
                        ( isset($clickPage) ? $clickPage : null )
                    );

        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'totalItem'=>$schoolModels->total(),
                'dataList'=>$schoolModels->items(),
            ]
        ]);
    }

    static function saveImage($image,$schoolModel,$editKey = 'picture')
    {
        $currentDate = date('Y-m-d');
        $destinationPath = storage_path('app/'.self::BASE_SCHOOL_IMAGE_URL.$currentDate.'/');
        if (!file_exists($destinationPath)) 
        {
            File::makeDirectory($destinationPath);
        }

        if(!empty($schoolModel[$editKey]))
        {
            self::deleteImage($schoolModel[$editKey]);
        }

        if(preg_match('/^(data:\s*image\/(\w+);base64,)/',$image,$res))
        {
            $type = $res[2];
            $image = base64_decode(str_replace($res[1],'', $image));
            $fileName = strtotime("now");
            while (file_exists(storage_path('app/'.self::BASE_SCHOOL_IMAGE_URL.$currentDate.'/'.$fileName.'.'.$type))) {
                $fileName++;
            }
            $schoolModel[$editKey] = $fileName.'.'.$type;
            Storage::disk('local')->put(self::BASE_SCHOOL_IMAGE_URL.$currentDate.'/'.$schoolModel[$editKey], $image);
        }
        return $schoolModel;
    }

    static function deleteImage($picture)
    {
        $explode = explode('.', $picture);
        $name = $explode[0];
        $date = date("Y-m-d", $name);
        $file = storage_path('app/'.self::BASE_SCHOOL_IMAGE_URL.$date.'/'.$picture);
        if(file_exists($file))
        {
            unlink($file);
            return true;
        }
        return false;
    }
}
