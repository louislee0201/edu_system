<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use DB;
use File;
use App\Model\QualificationSubject;
use App\Model\QualificationGrade;
use App\Model\ProficiencyCountry;
use App\Tools\Tools;
use App\Tools\GlobalList;

class Qualification extends Model
{
    protected $table = 'qualification';
    public $timestamps = false;
    protected $fillable = ['*'];
    
    public function qualification_subject()
    {
        return $this->hasMany(QualificationSubject::class,'qualification_id','id')->orderBy('id', 'desc');
    }

    public function qualification_grade()
    {
        return $this->hasMany(QualificationGrade::class,'qualification_id','id')->orderBy('score', 'asc');
    }

    static function getQualificationList($request)
    {
        $clickPage = $request->get('clickPage');
        $filterArr = $request->get('filterArr');

        $models = DB::table('qualification')
        				  ->select('id as qualification_id', 'qualification.*')
        				  ->orderBy('id', 'desc');

        if(isset($filterArr))
        {
            foreach ($filterArr as $key => $value) 
            {
                if(!empty($value))
                {
                    $models = $models->where($key, '=', $value);
                }
            }
        }

        $models = $models->Paginate(
                        10,
                        ['*'],
                        'page',
                        ( isset($clickPage) ? $clickPage : null )
                    );

        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'totalItem'=>$models->total(),
                'dataList'=>$models->items(),
                'countryList'=>ProficiencyCountry::all()
            ]
        ]);
    }

    static function saveImage($image,$schoolModel)
    {
        $currentDate = date('Y-m-d');
        $destinationPath = storage_path('app/'.self::BASE_SCHOOL_IMAGE_URL.$currentDate.'/');
        if (!file_exists($destinationPath)) 
        {
            File::makeDirectory($destinationPath);
        }

        if(!empty($schoolModel->picture))
        {
            self::deleteImage($schoolModel->picture);
        }

        if(preg_match('/^(data:\s*image\/(\w+);base64,)/',$image,$res))
        {
            $type = $res[2];
            $image = base64_decode(str_replace($res[1],'', $image));
            $schoolModel->picture = strtotime("now").'.'.$type;
            Storage::disk('local')->put(self::BASE_SCHOOL_IMAGE_URL.$currentDate.'/'.$schoolModel->picture, $image);
        }
        return $schoolModel;
    }

    static function deleteImage($picture)
    {
        $explode = explode('.', $picture);
        $name = $explode[0];
        $date = date("Y-m-d", $name);
        $file = storage_path('app/'.self::BASE_SCHOOL_IMAGE_URL.$date.'/'.$picture);
        if(file_exists($file))
        {
            unlink($file);
            return true;
        }
        return false;
    }
}
