<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model
{
    protected $table = 'exchange_rate';
    public $timestamps = true;
    protected $fillable = ['*'];

    const CURRENCY_TYPE_LIST = [
        1=>'USD',
        2=>'GBP',
       //3=>'TWD',
        4=>'HKD',
        5=>'CNY',
        6=>'SGD',
        7=>'MYR',
        8=>'KRW',
        9=>'JPY',
        10=>'THB',
        //11=>'EUR',
        12=>'IDR'
    ];

    const CURRENCY_TYPE_LIST_REVERSE = [
        'USD'=>1,
        'GBP'=>2,
        //'TWD'=>3,
        'HKD'=>4,
        'CNY'=>5,
        'SGD'=>6,
        'MYR'=>7,
        'KRW'=>8,
        'JPY'=>9,
        'THB'=>10,
        //'EUR'=>11,
        'IDR'=>12
    ];
}
