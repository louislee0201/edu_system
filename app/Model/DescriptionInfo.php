<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DescriptionInfo extends Model
{
    protected $table = 'description_info';
    public $timestamps = false;
    protected $fillable = ['*'];

    public function description() {
	    return $this->belongsTo(Description::class,'id','description_id');
	}
}

