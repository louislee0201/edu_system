<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\School;

class User extends Model
{
    protected $table = 'user';
    public $timestamps = false;
    protected $fillable = ['*'];

    public function school()
    {
        return $this->hasOne(School::class,'user_id','id');
    }
}
