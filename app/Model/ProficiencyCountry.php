<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProficiencyCountry extends Model
{
    protected $table = 'proficiency_country';
    public $timestamps = false;
    protected $fillable = ['*'];
}
