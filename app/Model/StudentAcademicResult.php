<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudentAcademicResult extends Model
{
    protected $table = 'student_academic_result';
    public $timestamps = false;
    protected $fillable = ['*'];
}
