<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SchoolMajor extends Model
{
    protected $table = 'school_major';
    public $timestamps = false;
    protected $fillable = ['*'];
}
