<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'config';
    public $timestamps = true;
    protected $fillable = ['*'];

    const TYPE = [
    	'BANNER'=>1
    ];

    const EMAIL = 'enquiry@edudotcom.my';
    const TEL = '+60167023776';
    const DEVELOPMENT = false;
}
