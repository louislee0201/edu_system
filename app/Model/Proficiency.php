<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use DB;
use App\Tools\Tools;
use App\Tools\GlobalList;

use App\Model\ProficiencyCountry;
use App\Model\ProficiencyGrade;
use App\Model\ProficiencyInfo;

class Proficiency extends Model
{
    protected $table = 'proficiency';
    public $timestamps = false;
    protected $fillable = ['*'];

    public function proficiency_grade()
    {
        return $this->hasMany(ProficiencyGrade::class,'proficiency_id','id')->orderBy('id', 'desc');
    }

    public function proficiency_info()
    {
        return $this->hasOne(ProficiencyInfo::class,'proficiency_id','id');
    }

    static function getProficiencyList($request)
    {
        $clickPage = $request->get('clickPage');
        $filterArr = $request->get('filterArr');

        $models = DB::table('proficiency')
                      /*->whereNull('school_course_id')*/
    				  ->select('id as proficiency_id', 'proficiency_country_id as country', 'proficiency.*')
    				  ->orderBy('id', 'desc');

        if(isset($filterArr))
        {
            foreach ($filterArr as $key => $value) 
            {
                if(!empty($value))
                {
                    $models = $models->where($key, '=', $value);
                }
            }
        }

        $models = $models->Paginate(
                        10,
                        ['*'],
                        'page',
                        ( isset($clickPage) ? $clickPage : null )
                    );

        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'totalItem'=>$models->total(),
                'dataList'=>$models->items(),
                'languageList'=>ProficiencyLanguage::all(),
                'countryList'=>ProficiencyCountry::all()
            ]
        ]);
    }
}
