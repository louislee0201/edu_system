<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubTag extends Model
{
    protected $table = 'sub_tag';
    public $timestamps = false;
    protected $fillable = ['*'];

    public function main_tag()
    {
        return $this->hasOne(MainTag::class,'id','main_tag_id');
    }
}
