<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use DB;

class SchoolCourseRequirement extends Model
{
    protected $table = 'school_course_requirement';
    public $timestamps = false;
    protected $fillable = ['*'];

    const GRADE_TYPE = [
        1 => 'grade score more than',
        2 => 'grade score less than',
        3 => 'grade cgpa more than',
        4 => 'grade cgpa less than',
        5 => 'score more than',
        6 => 'score less than',
        7 => 'cgpa more than',
        8 => 'cgpa less than',
    ];
    const GRADE_SCORE_MORE_THAN = 1;
    const GRADE_SCORE_LESS_THAN = 2;
    const GRADE_CGPA_MORE_THAN = 3;
    const GRADE_CGPA_LESS_THAN = 4;
    const SCORE_MORE_THAN = 5;
    const SCORE_LESS_THAN = 6;
    const CGPA_MORE_THAN = 7;
    const CGPA_LESS_THAN = 8;

    const GRADE_CHOOSE_SYSTEM = 1;
    const GRADE_CHOOSE_PERSONAL = 2;//school personal grade

    /*const REQUIREMENT_TYPE_NORMAL = 0;
    const REQUIREMENT_TYPE_UN_INCLUDE = 1;
    const REQUIREMENT_TYPE_INCLUDE = 2;
    const REQUIREMENT_TYPE_INSTEAD = 3;
    const REQUIREMENT_TYPE_NO_SPECIFIC_REQUIREMENT = 4;*/

    const NO_SPECIFIC_REQUIREMENT = 0;
    const SPECIFIC_REQUIREMENT = 1;

    const REQUIREMENT_TYPE_LIST = [
    	0 => 'normal',
    	1 => 'un include',
    	2 => 'include',
    	3 => 'instead',
    	4 => 'no specific requirement'
    ];

    const PROFICIENCY_SCORE_TYPE = [
    	1 => 'listeningScore',
    	2 => 'readingScore',
    	3 => 'speakingScore',
    	4 => 'writingScore'
    ];
}
