<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use DB;

class QualificationSubject extends Model
{
    protected $table = 'qualification_subject';
    public $timestamps = false;
    protected $fillable = ['*'];

    static function getQualificationSubjectList($request)
    {
    	$clickPage = $request->get('clickPage');
        $filterArr = $request->get('filterArr');

        $models = DB::table('qualification_subject')
                          ->leftJoin('qualification', 'qualification_subject.qualification_id', '=', 'qualification.id')
        				  ->select('qualification_subject.id as subject_id', 'qualification_subject.*', 'qualification.qualification_name')
        				  ->where('is_proficiency',true)
        				  ->orderBy('id', 'desc');

        if(isset($filterArr))
        {
            foreach ($filterArr as $key => $value) 
            {
                if(!empty($value))
                {
                    $models = $models->where($key, '=', $value);
                }
            }
        }

        $models = $models->Paginate(
                        100,//10
                        ['*'],
                        'page',
                        ( isset($clickPage) ? $clickPage : null )
                    );

        return [
            'totalItem'=>$models->total(),
            'dataList'=>$models->items()
        ];
    }
}
