<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Tools\GlobalList;
use App\Model\DescriptionInfoSub;
use File;

class Description extends Model
{
    protected $table = 'description';
    public $timestamps = false;
    protected $fillable = ['*'];

    const DESCRIPTION_TYPE = [
    	1 => 'only picture',
    	2 => 'only text',
    	3 => 'picture and text',
    	4 => 'new component',
        5 => 'optional',
        6 => 'video',
        7 => 'pdf'
    ];

    const BASE_DESCRIPTION_IMAGE_URL = '/image/description/';
    const BASE_DESCRIPTION_PDF_URL = '/pdf/description/';

    public function description_info()
    {
        return $this->hasMany(DescriptionInfo::class,'description_id','id');
    }

    public function description_info_sub() 
    {
	    return $this->hasManyThrough( DescriptionInfoSub::class, DescriptionInfo::class,'description_id','description_info_id','id','id');
	}

    public function deletes()
    {
    	$this->description_info_sub()->delete();
    	$this->description_info()->delete();
    	$this->delete();
    }

    static function checkIssetPic($descriptionList,$e = '')
    {
        foreach ($descriptionList as $description_type => $list) 
        {
            if(self::DESCRIPTION_TYPE[$description_type] == 'only text'){
                $datas = explode('\n',$descriptionList[$description_type]['data']);
                $content = '';
                foreach ($datas as $key => $data) 
                {
                    if($data != '')
                    {
                        if($key + 1 == count($datas))
                        {
                            $content .= $data;
                        }
                        else
                        {
                            $content .= $data."\n";
                        }
                        
                    }
                    else
                    {
                        if($key + 1 != count($datas))
                        {
                            $content .= "\n";
                        }
                    }
                    
                }
                $descriptionList[$description_type]['data'] = $content;
            }
            
            if(self::DESCRIPTION_TYPE[$description_type] == 'only picture' || self::DESCRIPTION_TYPE[$description_type] == 'picture and text'){
                $descriptionList[$description_type]['data']['img'] = self::saveImage2($list['data']['img']);
            }

            if(self::DESCRIPTION_TYPE[$description_type] == 'pdf'){
                foreach ($list['data'] as $sourceKey => $source) 
                {
                    $descriptionList[$description_type]['data'][$sourceKey]['pdf'] = self::savePDF($source['pdf']);
                    $descriptionList[$description_type]['data'][$sourceKey]['img'] = self::saveImage2($source['img']);
                }
            }

            if(self::DESCRIPTION_TYPE[$description_type] == 'new component' || self::DESCRIPTION_TYPE[$description_type] == 'optional'){
                foreach ($list['data'] as $key => $value) 
                {
                    $descriptionList[$description_type]['data'][$key] = self::checkIssetPic($value);
                }
            }
        }
        return $descriptionList;
    }

    static function savePDF($pdf)
    {
        $currentDate = date('Y-m-d');
        $destinationPath = storage_path('app/'.self::BASE_DESCRIPTION_PDF_URL.$currentDate.'/');
        if (!file_exists($destinationPath)) 
        {
            File::makeDirectory($destinationPath,0777,true);
        }
        $fileName = '';
        if(preg_match('/^(data:\s*application\/(\w+);base64,)/',$pdf,$res))
        {
            $type = $res[2];
            $time = strtotime("now");
            $fileName = ($time).'.'.$type;
            $pdf = base64_decode(str_replace($res[1],'', $pdf));
            Storage::disk('local')->put(self::BASE_DESCRIPTION_PDF_URL.$currentDate.'/'.$fileName, $pdf);
        }
        else 
        {
            $time = explode(".",$pdf)[0];
            if(is_numeric($time))
            {
                $fileName = $pdf;
            }
        }
        return $fileName;
    }

    static function saveImage2($images)
    {
        $currentDate = date('Y-m-d');
        $destinationPath = storage_path('app/'.self::BASE_DESCRIPTION_IMAGE_URL.$currentDate.'/');
        if (!file_exists($destinationPath)) 
        {
            File::makeDirectory($destinationPath,0777,true);
        }
        if(is_array($images))
        {   
            $pictureList = [];

            foreach ($images as $key => $image)
            {
                if(preg_match('/^(data:\s*image\/(\w+);base64,)/',$image,$res))
                {
                    $type = $res[2];
                    $time = strtotime("now");
                    $count = $key;
                    $fileName = ($time+$count).'.'.$type;
                    while (Storage::disk('local')->exists(self::BASE_DESCRIPTION_IMAGE_URL.$currentDate.'/'.$fileName, $image)) 
                    {
                        $count++;
                        $fileName = ($time+$count).'.'.$type;
                    }
                    $image = base64_decode(str_replace($res[1],'', $image));
                    Storage::disk('local')->put(self::BASE_DESCRIPTION_IMAGE_URL.$currentDate.'/'.$fileName, $image);
                    $pictureList[] = $fileName;
                }
                else 
                {
                    $time = explode(".",$image)[0];
                    if(is_numeric($time))
                    {
                        $pictureList[] = $image;
                    }
                }
            }
            return $pictureList;
        }
        else
        {
            $image = $images;
            $fileName = '';
            if(preg_match('/^(data:\s*image\/(\w+);base64,)/',$image,$res))
            {
                $type = $res[2];
                $time = strtotime("now");
                $count = 0;
                $fileName = ($time+$count).'.'.$type;
                while (Storage::disk('local')->exists(self::BASE_DESCRIPTION_IMAGE_URL.$currentDate.'/'.$fileName, $image)) 
                {
                    $count++;
                    $fileName = ($time+$count).'.'.$type;
                }
                $image = base64_decode(str_replace($res[1],'', $image));
                Storage::disk('local')->put(self::BASE_DESCRIPTION_IMAGE_URL.$currentDate.'/'.$fileName, $image);
            }
            else 
            {
                $time = explode(".",$image)[0];
                if(is_numeric($time))
                {
                    $fileName = $image;
                }
            }
            return $fileName;
        }
        
    }

    static function analysisDataAndSave($description_type, $model, $content)
    {
    	$model->description_type = $description_type;
        if(Description::DESCRIPTION_TYPE[$description_type] == 'only text')
        {
            $model->content = $content;
        }
        else if(Description::DESCRIPTION_TYPE[$description_type] == 'only picture')
        {
            $model = Description::saveImage($content,$model);
        }
        else if(Description::DESCRIPTION_TYPE[$description_type] == 'picture and text')
        {
            $model = Description::savePictureAndText($content,$model);
        }	
        else if(Description::DESCRIPTION_TYPE[$description_type] == 'new component')
        {
            foreach ($content as $subTitle => $subLists) 
            {
            	$model->title = $subTitle;
            	$model->save();
                foreach ($subLists as $rangeKey => $subList) 
                {
                	$subModel = new DescriptionInfoSub;
                    $subModel->description_info_id = $model->id;
                    $subModel->range_key = $rangeKey;
                    foreach ($subList as $description_type_sub => $content) 
                    {
                        self::analysisDataAndSave($description_type_sub,$subModel,$content);
                    }
                }
            }
        }
        else
        {
            throw new \Exception('Invalid Api',GlobalList::API_RESPONSE_CODE['601']); 
        }

        if(!isset($model->id))
        {
        	$model->save();
        }
    }

    static function savePictureAndText($lists,$model)
    {
    	$text = [];
    	$pictureList = [];
    	foreach ($lists as $listKey => $list) 
    	{
    		$text[] = $list['content'];
    		$pictureList[] = $list['img'];
    	}

    	$model->content = json_encode($text);
    	$model = self::saveImage($pictureList,$model);

    	return $model;
    }

    static function saveImage($images,$model)
    {
        $currentDate = date('Y-m-d');
        $destinationPath = storage_path('app/'.self::BASE_DESCRIPTION_IMAGE_URL.$currentDate.'/');
        if (!file_exists($destinationPath)) 
        {
            File::makeDirectory($destinationPath);
        }

        if(!empty($model->picture))
        {
            self::deleteImage($model->picture);
        }

        $pictureList = [];
        foreach ($images as $key => $image) 
        {
        	if(preg_match('/^(data:\s*image\/(\w+);base64,)/',$image,$res))
	        {
	        	$type = $res[2];
	        	$time = strtotime("now");
	        	$fileName = ($time+$key).'.'.$type;
	            $image = base64_decode(str_replace($res[1],'', $image));
	            $pictureList[] = $fileName;
	            Storage::disk('local')->put(self::BASE_DESCRIPTION_IMAGE_URL.$currentDate.'/'.$fileName, $image);
	        }
        }

        $model->picture = json_encode($pictureList);
        return $model;
    }

    static function deleteImage($picture)
    {
    	foreach ($picture as $key => $value) 
    	{
    		$explode = explode('.', $value);
	        $name = $explode[0];
	        $date = date("Y-m-d", $name);
	        $file = storage_path('app/'.self::BASE_DESCRIPTION_IMAGE_URL.$date.'/'.$value);
	        if(file_exists($file))
	        {
	            unlink($file);
	        }
    	}
    	return true;
    }
}
