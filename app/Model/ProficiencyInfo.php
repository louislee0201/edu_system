<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProficiencyInfo extends Model
{
    protected $table = 'proficiency_info';
    public $timestamps = false;
    protected $fillable = ['*'];
}
