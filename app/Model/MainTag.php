<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MainTag extends Model
{
    protected $table = 'main_tag';
    public $timestamps = false;
    protected $fillable = ['*'];

    public function sub_tag()
    {
        return $this->hasMany(SubTag::class,'main_tag_id','id');
    }
}
