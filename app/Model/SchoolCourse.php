<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use DB;
use File;
use App\Model\School;
use App\Model\SchoolCourse;
use App\Model\SchoolCourseRequirement;
use App\Model\QualificationGrade;
use App\Model\StudentAcademicResult;
use App\Model\SchoolCourseRequirementIncluded;
use App\Model\SchoolCourseRequirementIncludedInstead;

use App\Tools\GlobalList;
use App\Tools\Tools;

class SchoolCourse extends Model
{
    protected $table = 'school_course';
    public $timestamps = false;
    protected $fillable = ['*'];

    const BASE_SCHOOL_COURSE_IMAGE_URL = '/image/school_course/';
    
    public function school_major()
    {
        return $this->hasMany(SchoolMajor::class,'school_course_id','id');
    }

    public function school()
    {
        return $this->hasOne(School::class,'id','school_id');
    }

    public function school_course_requirement()
    {
        return $this->hasMany(SchoolCourseRequirement::class,'school_course_id','id');
    }

    public function delete()
    {
        SchoolCourseRequirement::where('school_course_id',$this->id)->delete();
        if($this->picture !== null)
        {
            self::deleteImage($this->picture);
        }
        self::where('id',$this->id)->delete();
    }

    static function getCourseRequirementConditions($course_id)
    {
        return [
            'requirementList'=> DB::select('
                                    SELECT *
                                    FROM tbl_school_course_requirement scr
                                    WHERE scr.school_course_id = '.$course_id.'
                                '),
            'requirementGradeList'=> DB::select('
                                        SELECT *
                                        FROM tbl_school_course_requirement_grade scrg
                                        WHERE scrg.school_course_requirement_id IN (
                                            SELECT id
                                            FROM tbl_school_course_requirement scr
                                            WHERE scr.school_course_id = '.$course_id.'
                                        )
                                    '),
            'requirementIncludedList'=> DB::select('
                                            SELECT *
                                            FROM tbl_school_course_requirement_included scri
                                            WHERE scri.school_course_requirement_id IN (
                                                SELECT id
                                                FROM tbl_school_course_requirement scr
                                                WHERE scr.school_course_id = '.$course_id.'
                                            )
                                        '),
            'requirementIncludedInsteadList'=>  DB::select('
                                                    SELECT *
                                                    FROM tbl_school_course_requirement_included_instead scrii
                                                    WHERE scrii.school_course_requirement_included_id IN (
                                                        SELECT id
                                                        FROM tbl_school_course_requirement_included scri
                                                        WHERE scri.school_course_requirement_id IN (
                                                            SELECT id
                                                            FROM tbl_school_course_requirement scr
                                                            WHERE scr.school_course_id = '.$course_id.'
                                                        )
                                                    )
                                                '),
        ];
    }

    static function getInterestSchoolCourseList($scModel,$limit)
    {
        $search_level = 4;
        $model = [];
        $filterList = '';
        do{
            if($search_level == 4)
            {
                $filterList = '
                    (
                        sm.sub_tag_id IN (
                            SELECT sm.sub_tag_id 
                            FROM tbl_school_major sm 
                            WHERE sm.school_course_id = '.$scModel->id.'
                        ) OR

                        sm.main_tag_id IN (
                            SELECT sm.main_tag_id 
                            FROM tbl_school_major sm 
                            WHERE sm.school_course_id = '.$scModel->id.'
                        )
                    ) AND
                    
                    sc.school_course_level_id = '.$scModel->school_course_level_id.' AND
                    s.region = '.$scModel->school->region.' AND
                    s.country = '.$scModel->school->country.'
                ';
            }
            elseif($search_level == 3)
            {
                $filterList = '
                    (
                        sm.sub_tag_id IN (
                            SELECT sm.sub_tag_id 
                            FROM tbl_school_major sm 
                            WHERE sm.school_course_id = '.$scModel->id.'
                        ) OR

                        sm.main_tag_id IN (
                            SELECT sm.main_tag_id 
                            FROM tbl_school_major sm 
                            WHERE sm.school_course_id = '.$scModel->id.'
                        )
                    ) AND
                    
                    sc.school_course_level_id = '.$scModel->school_course_level_id.' AND
                    s.country = '.$scModel->school->country.'
                ';
            }
            elseif($search_level == 2)
            {
                $filterList = '
                    (
                        sm.sub_tag_id IN (
                            SELECT sm.sub_tag_id 
                            FROM tbl_school_major sm 
                            WHERE sm.school_course_id = '.$scModel->id.'
                        ) OR

                        sm.main_tag_id IN (
                            SELECT sm.main_tag_id 
                            FROM tbl_school_major sm 
                            WHERE sm.school_course_id = '.$scModel->id.'
                        )
                    ) AND
                    
                    sc.school_course_level_id = '.$scModel->school_course_level_id.'
                ';
            }
            elseif($search_level == 1)
            {
                $filterList = '
                    sc.school_course_level_id = '.$scModel->school_course_level_id.'
                ';
            }

            $model = DB::select('
                SELECT sc.*, s.logo as logo
                FROM tbl_school_course sc
                LEFT JOIN tbl_school_major sm ON sm.school_course_id = sc.id
                LEFT JOIN tbl_school s ON s.id = sc.school_id
                WHERE '.$filterList.' AND
                sc.id != '.$scModel->id.'
                ORDER BY sc.shuffle_id ASC
                Limit '.$limit.'

            ');
            $search_level--;
        } while(count($model) < $limit && $search_level > 0);

        return $model;
    }

    static function frontendGetCourseList($request)
    {
        $clickPage = $request->get('clickPage');
        $filterArr = $request->get('filterArr');
        $userModel = $request->get('userModel');
        $qualification_id = $request->get('qualification_id');
        $currency_code = $request->get('currency_code');
        $secondary_complete = $request->get('secondary_complete');
        $userModel = $request->get('userModel');

        $pageSize = 20;
        $getNewPageData = $clickPage * $pageSize;
        $currentPage = $getNewPageData - $pageSize;
        $filterSQL = '';
        $secondFilterSQL = [];
        if(isset($secondary_complete))
        {   
            if($secondary_complete == 0)
            {
                $filterSQL .= 'school.secondary_complete = '.$secondary_complete;
            }
        }

        if(isset($filterArr))
        {
            foreach ($filterArr as $key => $value) 
            {
                if(!empty($value))
                {
                    if($key == 'course_level_id')
                    {
                        $filterSQL .= 'school_course_level_id = '.$value .' AND ';
                        $secondFilterSQL[] = 'school_course_level_id = '.$value;
                    }
                    elseif($key == 'country_id')    
                    {
                        $filterSQL .= 'school.country = '.$value .' AND ';
                        $secondFilterSQL[] = 'school.country = '.$value;
                    }
                    elseif($key == 'major_id')
                    {
                        $filterSQL .= 'school_major.sub_tag_id = '.$value .' AND ';
                        $secondFilterSQL[] = 'school_major.sub_tag_id = '.$value;
                    }
                    elseif($key == 'study_field_id')
                    {
                        $filterSQL .= 'school_major.main_tag_id = '.$value .' AND ';
                        $secondFilterSQL[] = 'school_major.main_tag_id = '.$value;
                    }
                    elseif($key == 'region_id')
                    {
                        $filterSQL .= 'school.region = '.$value .' AND ';
                        $secondFilterSQL[] = 'school.region = '.$value;
                    }
                }
            }
        }

        $studentID = $userModel->id;
        $filterQualification = '';
        if($qualification_id > 0)
        {
            $gradeScoreLessThan = '
                grade_type = '.SchoolCourseRequirement::GRADE_SCORE_LESS_THAN.' AND 
                /* =============== for case handling 1 or 2 ==================== */

                /* =============== calculate total score of course ==================== */
                (
                    total_score >= (
                        SELECT SUM(t.total)
                        FROM (
                            SELECT SUM(score) as total 
                            FROM (
                                SELECT qgrade.score as score, ROW_NUMBER() OVER(ORDER BY qgrade.score ASC) as row_num
                                FROM tbl_student_academic_result as saresult
                                LEFT JOIN tbl_qualification_grade as qgrade ON saresult.qualification_grade_id = qgrade.id 
                                WHERE saresult.student_id = '.$studentID.' AND
                                saresult.qualification_id = school_course_requirement.qualification_id AND
                                saresult.qualification_subject_id NOT IN (
                                    SELECT  (
                                        CASE
                                            WHEN qg1.score < dataA.score AND 
                                            scrii.instead_qualification_id = '.$qualification_id.' AND qg2.score >= (
                                                SELECT qg3.score
                                                FROM tbl_student_academic_result as sar
                                                LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                                WHERE sar.qualification_subject_id in ( 
                                                    SELECT scris.instead_qualification_subject_id
                                                    FROM tbl_school_course_requirement_included_instead as scris
                                                    LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.instead_qualification_grade_id
                                                    LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                    WHERE scris.school_course_requirement_included_id = scri.id
                                                ) AND
                                                sar.qualification_id = '.$qualification_id.' AND 
                                                sar.student_id = '.$studentID.' AND
                                                sar.qualification_subject_id = scrii.instead_qualification_subject_id
                                            ) THEN scrii.instead_qualification_subject_id

                                            WHEN qg1.score < dataA.score AND 
                                            scrii.second_instead_qualification_id = '.$qualification_id.' AND qg3.score >= (
                                                SELECT qg3.score
                                                FROM tbl_student_academic_result as sar
                                                LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                                WHERE sar.qualification_subject_id in ( 
                                                    SELECT scris.second_instead_qualification_subject_id
                                                    FROM tbl_school_course_requirement_included_instead as scris
                                                    LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.second_instead_qualification_grade_id
                                                    LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                    WHERE scris.school_course_requirement_included_id = scri.id
                                                ) AND
                                                sar.qualification_id = '.$qualification_id.' AND 
                                                sar.student_id = '.$studentID.' AND
                                                sar.qualification_subject_id = scriii.second_instead_qualification_subject_id
                                            ) THEN scriii.second_instead_qualification_subject_id
                                            ELSE scri.qualification_subject_id
                                        END

                                    ) as qualification_subject_id
                                    FROM tbl_school_course_requirement_included as scri
                                    LEFT JOIN tbl_qualification_grade as qg1 ON scri.qualification_grade_id = qg1.id

                                    LEFT JOIN tbl_school_course_requirement_included_instead as scrii ON scrii.school_course_requirement_included_id = scri.id
                                    LEFT JOIN tbl_qualification_grade as qg2 ON scrii.instead_qualification_grade_id = qg2.id

                                    LEFT JOIN tbl_school_course_requirement_included_instead as scriii ON scriii.school_course_requirement_included_id = scri.id
                                    LEFT JOIN tbl_qualification_grade as qg3 ON scriii.second_instead_qualification_grade_id = qg3.id

                                    RIGHT JOIN (
                                        SELECT sar.qualification_subject_id, qg3.score
                                        FROM tbl_student_academic_result as sar
                                        LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                        WHERE sar.qualification_subject_id in ( 
                                            SELECT scris.qualification_subject_id
                                            FROM tbl_school_course_requirement_included as scris
                                            LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.qualification_grade_id
                                            WHERE scris.school_course_requirement_id = school_course_requirement.id
                                        ) AND
                                        sar.qualification_id = '.$qualification_id.' AND 
                                        sar.student_id = '.$studentID.'
                                    ) dataA on dataA.qualification_subject_id = scri.qualification_subject_id

                                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND 
                                    (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.') AND
                                    (
                                        qg1.score >= dataA.score OR
                                        qg2.score >= (
                                            SELECT qg3.score
                                            FROM tbl_student_academic_result as sar
                                            LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                            WHERE sar.qualification_subject_id in ( 
                                                SELECT scris.instead_qualification_subject_id
                                                FROM tbl_school_course_requirement_included_instead as scris
                                                LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.instead_qualification_grade_id
                                                LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                WHERE scris.school_course_requirement_included_id = scri.id
                                            ) AND
                                            sar.qualification_id = '.$qualification_id.' AND 
                                            sar.student_id = '.$studentID.' AND
                                            sar.qualification_subject_id = scrii.instead_qualification_subject_id
                                        ) OR
                                        qg3.score >= (
                                            SELECT qg3.score
                                            FROM tbl_student_academic_result as sar
                                            LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                            WHERE sar.qualification_subject_id in ( 
                                                SELECT scris.second_instead_qualification_subject_id
                                                FROM tbl_school_course_requirement_included_instead as scris
                                                LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.second_instead_qualification_grade_id
                                                LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                WHERE scris.school_course_requirement_included_id = scri.id
                                            ) AND
                                            sar.qualification_id = '.$qualification_id.' AND 
                                            sar.student_id = '.$studentID.' AND
                                            sar.qualification_subject_id = scriii.second_instead_qualification_subject_id
                                        )
                                    )

                                    UNION ALL

                                    SELECT scri.qualification_subject_id 
                                    FROM tbl_school_course_requirement_included as scri
                                    WHERE scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_NOT_INCLUDED.' AND
                                    scri.school_course_requirement_id = school_course_requirement.id
                                )
                                ORDER BY qgrade.score ASC
                            ) as total_point
                            WHERE row_num <= ( SELECT sum(qualification_grade_quantity) FROM tbl_school_course_requirement_grade WHERE school_course_requirement_id = school_course_requirement.id ) - (
                                SELECT COUNT(*)
                                FROM tbl_school_course_requirement_included scri
                                LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                                WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.')
                            )

                            UNION ALL

                            SELECT SUM(qg.score) as total 
                            FROM tbl_school_course_requirement_included scri
                            LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                            WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                            (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.') AND
                            (
                                scri.instead_quantity <= (
                                    SELECT COUNT(*)
                                    FROM tbl_school_course_requirement_included_instead as scrii
                                    LEFT JOIN tbl_qualification_grade as qg1 ON scrii.instead_qualification_grade_id = qg1.id

                                    LEFT JOIN tbl_qualification_grade as qg2 ON scrii.second_instead_qualification_grade_id = qg2.id
                                    WHERE scrii.school_course_requirement_included_id = (
                                        scri.id
                                    ) AND
                                    (
                                        (
                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                                qg1.score <= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                                qg1.score >= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                                qg1.cgpa <= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                                qg1.cgpa >= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            )
                                        ) OR

                                        (
                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                                qg2.score <= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                                qg2.score >= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                                qg2.cgpa <= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                                qg2.cgpa >= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            )
                                        )
                                    )
                                ) /*OR

                                qg.score >= (
                                    SELECT qg1.score
                                    FROM tbl_student_academic_result sar
                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                    WHERE sar.qualification_subject_id = scri.qualification_subject_id AND 
                                    sar.student_id = '.$studentID.'
                                )*/

                                + (
                                    SELECT count(*)
                                    FROM tbl_student_academic_result sar
                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                    WHERE sar.qualification_subject_id = scri.qualification_subject_id AND
                                    qg.score >= qg1.score AND
                                    sar.student_id = '.$studentID.'
                                )
                            )
                        )t
                    ) 
                ) AND

                /* =============== calculate included valid course ==================== */
                (

                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.') AND
                    (
                        scri.instead_quantity <= (
                            SELECT COUNT(*)
                            FROM tbl_school_course_requirement_included_instead as scrii
                            LEFT JOIN tbl_qualification_grade as qg1 ON scrii.instead_qualification_grade_id = qg1.id

                            LEFT JOIN tbl_qualification_grade as qg2 ON scrii.second_instead_qualification_grade_id = qg2.id
                            WHERE scrii.school_course_requirement_included_id = (
                                scri.id
                            ) AND
                            (
                                (
                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg1.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg1.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg1.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg1.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                ) OR

                                (
                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg2.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg2.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg2.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg2.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                )
                            )
                        ) + (
                            SELECT count(*)
                            FROM tbl_student_academic_result sar
                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                            WHERE sar.qualification_subject_id = scri.qualification_subject_id AND
                            qg.score >= qg1.score And
                            sar.student_id = '.$studentID.'
                        )
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included as scri
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND 
                    (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.')
                ) AND

                (SELECT MAX(score) FROM tbl_qualification_grade WHERE qualification_id = '.$qualification_id.' AND id = school_course_requirement_grade.qualification_grade_id) >=
                (
                    SELECT MAX(score) FROM tbl_qualification_grade WHERE qualification_id = '.$qualification_id.' AND id IN (
                        SELECT total.id FROM (
                            SELECT saresult.qualification_grade_id as id, ROW_NUMBER() OVER(ORDER BY qgrade.score ASC) as row_num
                            FROM tbl_student_academic_result as saresult
                            LEFT JOIN tbl_qualification_grade as qgrade ON saresult.qualification_grade_id = qgrade.id 
                            WHERE saresult.student_id = '.$studentID.' AND
                            saresult.qualification_id = '.$qualification_id.'
                            ORDER BY qgrade.score ASC
                        ) as total
                        WHERE row_num <= ( SELECT sum(qualification_grade_quantity) FROM tbl_school_course_requirement_grade WHERE school_course_requirement_id = school_course_requirement.id )
                    )
                ) AND

                /* =============== for case handling 3 ==================== */

                /* =============== calculate included valid course ==================== */
                (

                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.' AND
                    (
                        scri.instead_quantity <= (
                            SELECT COUNT(*)
                            FROM tbl_school_course_requirement_included_instead as scrii
                            LEFT JOIN tbl_qualification_grade as qg1 ON scrii.instead_qualification_grade_id = qg1.id

                            LEFT JOIN tbl_qualification_grade as qg2 ON scrii.second_instead_qualification_grade_id = qg2.id
                            WHERE scrii.school_course_requirement_included_id = (
                                scri.id
                            ) AND
                            (
                                (
                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg1.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg1.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg1.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg1.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                ) OR

                                (
                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg2.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg2.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg2.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg2.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                )
                            )
                        ) + (
                            SELECT count(*)
                            FROM tbl_student_academic_result sar
                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                            WHERE sar.qualification_subject_id = scri.qualification_subject_id AND
                            qg.score >= qg1.score AND
                            sar.student_id = '.$studentID.'
                        )
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included as scri
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.'
                )           
            ';

            $gradeScoreMoreThan = '
                grade_type = '.SchoolCourseRequirement::GRADE_SCORE_MORE_THAN.' AND 
                /* =============== for case handling 1 or 2 ==================== */

                /* =============== calculate total score of course ==================== */
                (
                    total_score <= (
                        SELECT SUM(t.total)
                        FROM (
                            SELECT SUM(score) as total 
                            FROM (
                                SELECT qgrade.score as score, ROW_NUMBER() OVER(ORDER BY qgrade.score DESC) as row_num
                                FROM tbl_student_academic_result as saresult
                                LEFT JOIN tbl_qualification_grade as qgrade ON saresult.qualification_grade_id = qgrade.id 
                                WHERE saresult.student_id = '.$studentID.' AND
                                saresult.qualification_id = school_course_requirement.qualification_id AND
                                saresult.qualification_subject_id NOT IN (
                                    SELECT  (
                                        CASE
                                            WHEN qg1.score > dataA.score AND 
                                            scrii.instead_qualification_id = '.$qualification_id.' AND qg2.score <= (
                                                SELECT qg3.score
                                                FROM tbl_student_academic_result as sar
                                                LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                                WHERE sar.qualification_subject_id in ( 
                                                    SELECT scris.instead_qualification_subject_id
                                                    FROM tbl_school_course_requirement_included_instead as scris
                                                    LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.instead_qualification_grade_id
                                                    LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                    WHERE scris.school_course_requirement_included_id = scri.id
                                                ) AND
                                                sar.qualification_id = '.$qualification_id.' AND 
                                                sar.student_id = '.$studentID.' AND
                                                sar.qualification_subject_id = scrii.instead_qualification_subject_id
                                            ) THEN scrii.instead_qualification_subject_id
                                            WHEN qg1.score > dataA.score AND 
                                            scrii.second_instead_qualification_id = '.$qualification_id.' AND qg3.score <= (
                                                SELECT qg3.score
                                                FROM tbl_student_academic_result as sar
                                                LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                                WHERE sar.qualification_subject_id in ( 
                                                    SELECT scris.second_instead_qualification_subject_id
                                                    FROM tbl_school_course_requirement_included_instead as scris
                                                    LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.second_instead_qualification_grade_id
                                                    LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                    WHERE scris.school_course_requirement_included_id = scri.id
                                                ) AND
                                                sar.qualification_id = '.$qualification_id.' AND 
                                                sar.student_id = '.$studentID.' AND
                                                sar.qualification_subject_id = scriii.second_instead_qualification_subject_id
                                            ) THEN scriii.second_instead_qualification_subject_id
                                            ELSE scri.qualification_subject_id
                                        END
                                    ) as qualification_subject_id
                                    FROM tbl_school_course_requirement_included as scri
                                    LEFT JOIN tbl_qualification_grade as qg1 ON scri.qualification_grade_id = qg1.id

                                    LEFT JOIN tbl_school_course_requirement_included_instead as scrii ON scrii.school_course_requirement_included_id = scri.id
                                    LEFT JOIN tbl_qualification_grade as qg2 ON scrii.instead_qualification_grade_id = qg2.id

                                    LEFT JOIN tbl_school_course_requirement_included_instead as scriii ON scriii.school_course_requirement_included_id = scri.id
                                    LEFT JOIN tbl_qualification_grade as qg3 ON scriii.second_instead_qualification_grade_id = qg3.id

                                    RIGHT JOIN (
                                        SELECT sar.qualification_subject_id, qg3.score
                                        FROM tbl_student_academic_result as sar
                                        LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                        WHERE sar.qualification_subject_id in ( 
                                            SELECT scris.qualification_subject_id
                                            FROM tbl_school_course_requirement_included as scris
                                            LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.qualification_grade_id
                                            WHERE scris.school_course_requirement_id = school_course_requirement.id
                                        ) AND
                                        sar.qualification_id = '.$qualification_id.' AND 
                                        sar.student_id = '.$studentID.'
                                    ) dataA on dataA.qualification_subject_id = scri.qualification_subject_id

                                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND 
                                    (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.') AND
                                    (
                                        qg1.score <= dataA.score OR
                                        qg2.score <= (
                                            SELECT qg3.score
                                            FROM tbl_student_academic_result as sar
                                            LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                            WHERE sar.qualification_subject_id in ( 
                                                SELECT scris.instead_qualification_subject_id
                                                FROM tbl_school_course_requirement_included_instead as scris
                                                LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.instead_qualification_grade_id
                                                LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                WHERE scris.school_course_requirement_included_id = scri.id
                                            ) AND
                                            sar.qualification_id = '.$qualification_id.' AND 
                                            sar.student_id = '.$studentID.' AND
                                            sar.qualification_subject_id = scrii.instead_qualification_subject_id
                                        ) OR
                                        qg3.score <= (
                                            SELECT qg3.score
                                            FROM tbl_student_academic_result as sar
                                            LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                            WHERE sar.qualification_subject_id in ( 
                                                SELECT scris.second_instead_qualification_subject_id
                                                FROM tbl_school_course_requirement_included_instead as scris
                                                LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.second_instead_qualification_grade_id
                                                LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                WHERE scris.school_course_requirement_included_id = scri.id
                                            ) AND
                                            sar.qualification_id = '.$qualification_id.' AND 
                                            sar.student_id = '.$studentID.' AND
                                            sar.qualification_subject_id = scriii.second_instead_qualification_subject_id
                                        )
                                    )

                                    UNION ALL

                                    SELECT scri.qualification_subject_id 
                                    FROM tbl_school_course_requirement_included as scri
                                    WHERE scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_NOT_INCLUDED.' AND
                                    scri.school_course_requirement_id = school_course_requirement.id
                                )
                                ORDER BY qgrade.score DESC
                            ) as total_point
                            WHERE row_num <= ( SELECT sum(qualification_grade_quantity) FROM tbl_school_course_requirement_grade WHERE school_course_requirement_id = school_course_requirement.id ) - (
                                SELECT COUNT(*)
                                FROM tbl_school_course_requirement_included scri
                                LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                                WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.')
                            )

                            UNION ALL

                            SELECT SUM(qg.score) as total 
                            FROM tbl_school_course_requirement_included scri
                            LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                            WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                            (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.') AND
                            (
                                scri.instead_quantity <= (
                                    SELECT COUNT(*)
                                    FROM tbl_school_course_requirement_included_instead as scrii
                                    LEFT JOIN tbl_qualification_grade as qg1 ON scrii.instead_qualification_grade_id = qg1.id

                                    LEFT JOIN tbl_qualification_grade as qg2 ON scrii.second_instead_qualification_grade_id = qg2.id
                                    WHERE scrii.school_course_requirement_included_id = (
                                        scri.id
                                    ) AND
                                    (
                                        (
                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                                qg1.score <= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                                qg1.score >= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                                qg1.cgpa <= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                                qg1.cgpa >= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            )
                                        ) OR

                                        (
                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                                qg2.score <= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                                qg2.score >= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                                qg2.cgpa <= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                                qg2.cgpa >= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            )
                                        )
                                    )
                                ) + (
                                    SELECT count(*)
                                    FROM tbl_student_academic_result sar
                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                    WHERE sar.qualification_subject_id = scri.qualification_subject_id AND
                                    qg.score <= qg1.score AND
                                    sar.student_id = '.$studentID.'
                                )
                            )
                        )t
                    ) 
                ) AND

                /* =============== calculate included valid course ==================== */
                (

                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.') AND
                    (
                        scri.instead_quantity <= (
                            SELECT COUNT(*)
                            FROM tbl_school_course_requirement_included_instead as scrii
                            LEFT JOIN tbl_qualification_grade as qg1 ON scrii.instead_qualification_grade_id = qg1.id

                            LEFT JOIN tbl_qualification_grade as qg2 ON scrii.second_instead_qualification_grade_id = qg2.id
                            WHERE scrii.school_course_requirement_included_id = (
                                scri.id
                            ) AND
                            (
                                (
                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg1.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg1.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg1.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg1.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                ) OR

                                (
                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg2.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg2.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg2.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg2.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                )
                            )
                        ) + (
                            SELECT count(*)
                            FROM tbl_student_academic_result sar
                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                            WHERE sar.qualification_subject_id = scri.qualification_subject_id AND
                            qg.score <= qg1.score AND
                            sar.student_id = '.$studentID.'
                        )
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included as scri
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND 
                    (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.')
                ) AND

                (SELECT MIN(score) FROM tbl_qualification_grade WHERE qualification_id = '.$qualification_id.' AND id = school_course_requirement_grade.qualification_grade_id) <=
                (
                    SELECT MIN(score) FROM tbl_qualification_grade WHERE qualification_id = '.$qualification_id.' AND id IN (
                        SELECT total.id FROM (
                            SELECT saresult.qualification_grade_id as id, ROW_NUMBER() OVER(ORDER BY qgrade.score DESC) as row_num
                            FROM tbl_student_academic_result as saresult
                            LEFT JOIN tbl_qualification_grade as qgrade ON saresult.qualification_grade_id = qgrade.id 
                            WHERE saresult.student_id = '.$studentID.' AND
                            saresult.qualification_id = '.$qualification_id.'
                            ORDER BY qgrade.score DESC
                        ) as total
                        WHERE row_num <= ( SELECT sum(qualification_grade_quantity) FROM tbl_school_course_requirement_grade WHERE school_course_requirement_id = school_course_requirement.id )
                    )
                ) AND

                /* =============== for case handling 3 ==================== */

                /* =============== calculate included valid course ==================== */
                (

                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.' AND
                    (
                        scri.instead_quantity <= (
                            SELECT COUNT(*)
                            FROM tbl_school_course_requirement_included_instead as scrii
                            LEFT JOIN tbl_qualification_grade as qg1 ON scrii.instead_qualification_grade_id = qg1.id

                            LEFT JOIN tbl_qualification_grade as qg2 ON scrii.second_instead_qualification_grade_id = qg2.id
                            WHERE scrii.school_course_requirement_included_id = (
                                scri.id
                            ) AND
                            (
                                (
                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg1.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg1.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg1.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg1.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                ) OR

                                (
                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg2.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg2.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg2.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg2.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                )
                            )
                        ) + (
                            SELECT count(*)
                            FROM tbl_student_academic_result sar
                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                            WHERE sar.qualification_subject_id = scri.qualification_subject_id AND
                            qg.score <= qg1.score AND
                            sar.student_id = '.$studentID.'
                        )
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included as scri
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.'
                )
            ';

            $gradeCGPALessThan = '
                grade_type = '.SchoolCourseRequirement::GRADE_CGPA_LESS_THAN.' AND 
                /* =============== for case handling 1 or 2 ==================== */

                /* =============== calculate total cgpa of course ==================== */
                (
                    total_cgpa >= (
                        SELECT SUM(t.total)
                        FROM (
                            SELECT SUM(cgpa) as total 
                            FROM (
                                SELECT qgrade.cgpa as cgpa, ROW_NUMBER() OVER(ORDER BY qgrade.cgpa ASC) as row_num
                                FROM tbl_student_academic_result as saresult
                                LEFT JOIN tbl_qualification_grade as qgrade ON saresult.qualification_grade_id = qgrade.id 
                                WHERE saresult.student_id = '.$studentID.' AND
                                saresult.qualification_id = school_course_requirement.qualification_id AND
                                saresult.qualification_subject_id NOT IN (
                                    SELECT  (
                                        CASE
                                            WHEN qg1.cgpa < dataA.cgpa AND 
                                            scrii.instead_qualification_id = '.$qualification_id.' AND qg2.cgpa >= (
                                                SELECT qg3.cgpa
                                                FROM tbl_student_academic_result as sar
                                                LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                                WHERE sar.qualification_subject_id in ( 
                                                    SELECT scris.instead_qualification_subject_id
                                                    FROM tbl_school_course_requirement_included_instead as scris
                                                    LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.instead_qualification_grade_id
                                                    LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                    WHERE scris.school_course_requirement_included_id = scri.id
                                                ) AND
                                                sar.qualification_id = '.$qualification_id.' AND 
                                                sar.student_id = '.$studentID.' AND
                                                sar.qualification_subject_id = scrii.instead_qualification_subject_id
                                            ) THEN scrii.instead_qualification_subject_id

                                            WHEN qg1.cgpa < dataA.cgpa AND 
                                            scrii.second_instead_qualification_id = '.$qualification_id.' AND qg3.cgpa >= (
                                                SELECT qg3.cgpa
                                                FROM tbl_student_academic_result as sar
                                                LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                                WHERE sar.qualification_subject_id in ( 
                                                    SELECT scris.second_instead_qualification_subject_id
                                                    FROM tbl_school_course_requirement_included_instead as scris
                                                    LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.second_instead_qualification_grade_id
                                                    LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                    WHERE scris.school_course_requirement_included_id = scri.id
                                                ) AND
                                                sar.qualification_id = '.$qualification_id.' AND 
                                                sar.student_id = '.$studentID.' AND
                                                sar.qualification_subject_id = scriii.second_instead_qualification_subject_id
                                            ) THEN scriii.second_instead_qualification_subject_id
                                            ELSE scri.qualification_subject_id
                                        END

                                    ) as qualification_subject_id
                                    FROM tbl_school_course_requirement_included as scri
                                    LEFT JOIN tbl_qualification_grade as qg1 ON scri.qualification_grade_id = qg1.id

                                    LEFT JOIN tbl_school_course_requirement_included_instead as scrii ON scrii.school_course_requirement_included_id = scri.id
                                    LEFT JOIN tbl_qualification_grade as qg2 ON scrii.instead_qualification_grade_id = qg2.id

                                    LEFT JOIN tbl_school_course_requirement_included_instead as scriii ON scriii.school_course_requirement_included_id = scri.id
                                    LEFT JOIN tbl_qualification_grade as qg3 ON scriii.second_instead_qualification_grade_id = qg3.id

                                    RIGHT JOIN (
                                        SELECT sar.qualification_subject_id, qg3.cgpa
                                        FROM tbl_student_academic_result as sar
                                        LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                        WHERE sar.qualification_subject_id in ( 
                                            SELECT scris.qualification_subject_id
                                            FROM tbl_school_course_requirement_included as scris
                                            LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.qualification_grade_id
                                            WHERE scris.school_course_requirement_id = school_course_requirement.id
                                        ) AND
                                        sar.qualification_id = '.$qualification_id.' AND 
                                        sar.student_id = '.$studentID.'
                                    ) dataA on dataA.qualification_subject_id = scri.qualification_subject_id

                                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND 
                                    (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.') AND
                                    (
                                        qg1.cgpa >= dataA.cgpa OR
                                        qg2.cgpa >= (
                                            SELECT qg3.cgpa
                                            FROM tbl_student_academic_result as sar
                                            LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                            WHERE sar.qualification_subject_id in ( 
                                                SELECT scris.instead_qualification_subject_id
                                                FROM tbl_school_course_requirement_included_instead as scris
                                                LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.instead_qualification_grade_id
                                                LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                WHERE scris.school_course_requirement_included_id = scri.id
                                            ) AND
                                            sar.qualification_id = '.$qualification_id.' AND 
                                            sar.student_id = '.$studentID.' AND
                                            sar.qualification_subject_id = scrii.instead_qualification_subject_id
                                        ) OR
                                        qg3.cgpa >= (
                                            SELECT qg3.cgpa
                                            FROM tbl_student_academic_result as sar
                                            LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                            WHERE sar.qualification_subject_id in ( 
                                                SELECT scris.second_instead_qualification_subject_id
                                                FROM tbl_school_course_requirement_included_instead as scris
                                                LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.second_instead_qualification_grade_id
                                                LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                WHERE scris.school_course_requirement_included_id = scri.id
                                            ) AND
                                            sar.qualification_id = '.$qualification_id.' AND 
                                            sar.student_id = '.$studentID.' AND
                                            sar.qualification_subject_id = scriii.second_instead_qualification_subject_id
                                        )
                                    )

                                    UNION ALL

                                    SELECT scri.qualification_subject_id 
                                    FROM tbl_school_course_requirement_included as scri
                                    WHERE scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_NOT_INCLUDED.' AND
                                    scri.school_course_requirement_id = school_course_requirement.id
                                )
                                ORDER BY qgrade.cgpa ASC
                            ) as total_point
                            WHERE row_num <= ( SELECT sum(qualification_grade_quantity) FROM tbl_school_course_requirement_grade WHERE school_course_requirement_id = school_course_requirement.id ) - (
                                SELECT COUNT(*)
                                FROM tbl_school_course_requirement_included scri
                                LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                                WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.')
                            )

                            UNION ALL

                            SELECT SUM(qg.cgpa) as total 
                            FROM tbl_school_course_requirement_included scri
                            LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                            WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                            (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.') AND
                            (
                                scri.instead_quantity <= (
                                    SELECT COUNT(*)
                                    FROM tbl_school_course_requirement_included_instead as scrii
                                    LEFT JOIN tbl_qualification_grade as qg1 ON scrii.instead_qualification_grade_id = qg1.id

                                    LEFT JOIN tbl_qualification_grade as qg2 ON scrii.second_instead_qualification_grade_id = qg2.id
                                    WHERE scrii.school_course_requirement_included_id = (
                                        scri.id
                                    ) AND
                                    (
                                        (
                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                                qg1.score <= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                                qg1.score >= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                                qg1.cgpa <= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                                qg1.cgpa >= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            )
                                        ) OR

                                        (
                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                                qg2.score <= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                                qg2.score >= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                                qg2.cgpa <= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                                qg2.cgpa >= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            )
                                        )
                                    )
                                ) /*OR

                                qg.cgpa >= (
                                    SELECT qg1.cgpa
                                    FROM tbl_student_academic_result sar
                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                    WHERE sar.qualification_subject_id = scri.qualification_subject_id AND 
                                    sar.student_id = '.$studentID.'
                                )*/

                                + (
                                    SELECT count(*)
                                    FROM tbl_student_academic_result sar
                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                    WHERE sar.qualification_subject_id = scri.qualification_subject_id AND
                                    qg.cgpa >= qg1.cgpa AND
                                    sar.student_id = '.$studentID.'
                                )
                            )
                        )t
                    ) 
                ) AND

                /* =============== calculate included valid course ==================== */
                (

                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.') AND
                    (
                        scri.instead_quantity <= (
                            SELECT COUNT(*)
                            FROM tbl_school_course_requirement_included_instead as scrii
                            LEFT JOIN tbl_qualification_grade as qg1 ON scrii.instead_qualification_grade_id = qg1.id

                            LEFT JOIN tbl_qualification_grade as qg2 ON scrii.second_instead_qualification_grade_id = qg2.id
                            WHERE scrii.school_course_requirement_included_id = (
                                scri.id
                            ) AND
                            (
                                (
                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg1.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg1.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg1.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg1.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                ) OR

                                (
                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg2.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg2.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg2.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg2.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                )
                            )
                        ) + (
                            SELECT count(*)
                            FROM tbl_student_academic_result sar
                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                            WHERE sar.qualification_subject_id = scri.qualification_subject_id AND
                            qg.cgpa >= qg1.cgpa AND
                            sar.student_id = '.$studentID.'
                        )
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included as scri
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND 
                    (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.')
                ) AND

                (SELECT MAX(cgpa) FROM tbl_qualification_grade WHERE qualification_id = '.$qualification_id.' AND id = school_course_requirement_grade.qualification_grade_id) >=
                (
                    SELECT MAX(cgpa) FROM tbl_qualification_grade WHERE qualification_id = '.$qualification_id.' AND id IN (
                        SELECT total.id FROM (
                            SELECT saresult.qualification_grade_id as id, ROW_NUMBER() OVER(ORDER BY qgrade.cgpa ASC) as row_num
                            FROM tbl_student_academic_result as saresult
                            LEFT JOIN tbl_qualification_grade as qgrade ON saresult.qualification_grade_id = qgrade.id 
                            WHERE saresult.student_id = '.$studentID.' AND
                            saresult.qualification_id = '.$qualification_id.'
                            ORDER BY qgrade.cgpa ASC
                        ) as total
                        WHERE row_num <= ( SELECT sum(qualification_grade_quantity) FROM tbl_school_course_requirement_grade WHERE school_course_requirement_id = school_course_requirement.id )
                    )
                ) AND

                /* =============== for case handling 3 ==================== */

                /* =============== calculate included valid course ==================== */
                (

                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.' AND
                    (
                        scri.instead_quantity <= (
                            SELECT COUNT(*)
                            FROM tbl_school_course_requirement_included_instead as scrii
                            LEFT JOIN tbl_qualification_grade as qg1 ON scrii.instead_qualification_grade_id = qg1.id

                            LEFT JOIN tbl_qualification_grade as qg2 ON scrii.second_instead_qualification_grade_id = qg2.id
                            WHERE scrii.school_course_requirement_included_id = (
                                scri.id
                            ) AND
                            (
                                (
                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg1.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg1.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg1.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg1.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                ) OR

                                (
                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg2.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg2.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg2.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg2.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                )
                            )
                        ) + (
                            SELECT count(*)
                            FROM tbl_student_academic_result sar
                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                            WHERE sar.qualification_subject_id = scri.qualification_subject_id AND
                            qg.cgpa >= qg1.cgpa AND
                            sar.student_id = '.$studentID.'
                        )
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included as scri
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.'
                )           
            ';

            $gradeCGPAMoreThan = '
                grade_type = '.SchoolCourseRequirement::GRADE_CGPA_MORE_THAN.' AND 
                /* =============== for case handling 1 or 2 ==================== */

                /* =============== calculate total cgpa of course ==================== */
                (
                    total_cgpa <= (
                        SELECT SUM(t.total)
                        FROM (
                            SELECT SUM(cgpa) as total 
                            FROM (
                                SELECT qgrade.cgpa as cgpa, ROW_NUMBER() OVER(ORDER BY qgrade.cgpa DESC) as row_num
                                FROM tbl_student_academic_result as saresult
                                LEFT JOIN tbl_qualification_grade as qgrade ON saresult.qualification_grade_id = qgrade.id 
                                WHERE saresult.student_id = '.$studentID.' AND
                                saresult.qualification_id = school_course_requirement.qualification_id AND
                                saresult.qualification_subject_id NOT IN (
                                    SELECT  (
                                        CASE
                                            WHEN qg1.cgpa > dataA.cgpa AND 
                                            scrii.instead_qualification_id = '.$qualification_id.' AND qg2.cgpa <= (
                                                SELECT qg3.cgpa
                                                FROM tbl_student_academic_result as sar
                                                LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                                WHERE sar.qualification_subject_id in ( 
                                                    SELECT scris.instead_qualification_subject_id
                                                    FROM tbl_school_course_requirement_included_instead as scris
                                                    LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.instead_qualification_grade_id
                                                    LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                    WHERE scris.school_course_requirement_included_id = scri.id
                                                ) AND
                                                sar.qualification_id = '.$qualification_id.' AND 
                                                sar.student_id = '.$studentID.' AND
                                                sar.qualification_subject_id = scrii.instead_qualification_subject_id
                                            ) THEN scrii.instead_qualification_subject_id
                                            WHEN qg1.cgpa > dataA.cgpa AND 
                                            scrii.second_instead_qualification_id = '.$qualification_id.' AND qg3.cgpa <= (
                                                SELECT qg3.cgpa
                                                FROM tbl_student_academic_result as sar
                                                LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                                WHERE sar.qualification_subject_id in ( 
                                                    SELECT scris.second_instead_qualification_subject_id
                                                    FROM tbl_school_course_requirement_included_instead as scris
                                                    LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.second_instead_qualification_grade_id
                                                    LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                    WHERE scris.school_course_requirement_included_id = scri.id
                                                ) AND
                                                sar.qualification_id = '.$qualification_id.' AND 
                                                sar.student_id = '.$studentID.' AND
                                                sar.qualification_subject_id = scriii.second_instead_qualification_subject_id
                                            ) THEN scriii.second_instead_qualification_subject_id
                                            ELSE scri.qualification_subject_id
                                        END
                                    ) as qualification_subject_id
                                    FROM tbl_school_course_requirement_included as scri
                                    LEFT JOIN tbl_qualification_grade as qg1 ON scri.qualification_grade_id = qg1.id

                                    LEFT JOIN tbl_school_course_requirement_included_instead as scrii ON scrii.school_course_requirement_included_id = scri.id
                                    LEFT JOIN tbl_qualification_grade as qg2 ON scrii.instead_qualification_grade_id = qg2.id

                                    LEFT JOIN tbl_school_course_requirement_included_instead as scriii ON scriii.school_course_requirement_included_id = scri.id
                                    LEFT JOIN tbl_qualification_grade as qg3 ON scriii.second_instead_qualification_grade_id = qg3.id

                                    RIGHT JOIN (
                                        SELECT sar.qualification_subject_id, qg3.cgpa
                                        FROM tbl_student_academic_result as sar
                                        LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                        WHERE sar.qualification_subject_id in ( 
                                            SELECT scris.qualification_subject_id
                                            FROM tbl_school_course_requirement_included as scris
                                            LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.qualification_grade_id
                                            WHERE scris.school_course_requirement_id = school_course_requirement.id
                                        ) AND
                                        sar.qualification_id = '.$qualification_id.' AND 
                                        sar.student_id = '.$studentID.'
                                    ) dataA on dataA.qualification_subject_id = scri.qualification_subject_id

                                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND 
                                    (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.') AND
                                    (
                                        qg1.cgpa <= dataA.cgpa OR
                                        qg2.cgpa <= (
                                            SELECT qg3.cgpa
                                            FROM tbl_student_academic_result as sar
                                            LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                            WHERE sar.qualification_subject_id in ( 
                                                SELECT scris.instead_qualification_subject_id
                                                FROM tbl_school_course_requirement_included_instead as scris
                                                LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.instead_qualification_grade_id
                                                LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                WHERE scris.school_course_requirement_included_id = scri.id
                                            ) AND
                                            sar.qualification_id = '.$qualification_id.' AND 
                                            sar.student_id = '.$studentID.' AND
                                            sar.qualification_subject_id = scrii.instead_qualification_subject_id
                                        ) OR
                                        qg3.cgpa <= (
                                            SELECT qg3.cgpa
                                            FROM tbl_student_academic_result as sar
                                            LEFT JOIN tbl_qualification_grade as qg3 ON qg3.id = sar.qualification_grade_id
                                            WHERE sar.qualification_subject_id in ( 
                                                SELECT scris.second_instead_qualification_subject_id
                                                FROM tbl_school_course_requirement_included_instead as scris
                                                LEFT JOIN tbl_qualification_grade as qg4 ON qg4.id = scris.second_instead_qualification_grade_id
                                                LEFT JOIN tbl_school_course_requirement_included as scri ON scri.id = scris.school_course_requirement_included_id
                                                WHERE scris.school_course_requirement_included_id = scri.id
                                            ) AND
                                            sar.qualification_id = '.$qualification_id.' AND 
                                            sar.student_id = '.$studentID.' AND
                                            sar.qualification_subject_id = scriii.second_instead_qualification_subject_id
                                        )
                                    )

                                    UNION ALL

                                    SELECT scri.qualification_subject_id 
                                    FROM tbl_school_course_requirement_included as scri
                                    WHERE scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_NOT_INCLUDED.' AND
                                    scri.school_course_requirement_id = school_course_requirement.id
                                )
                                ORDER BY qgrade.cgpa DESC
                            ) as total_point
                            WHERE row_num <= ( SELECT sum(qualification_grade_quantity) FROM tbl_school_course_requirement_grade WHERE school_course_requirement_id = school_course_requirement.id ) - (
                                SELECT COUNT(*)
                                FROM tbl_school_course_requirement_included scri
                                LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                                WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.')
                            )

                            UNION ALL

                            SELECT SUM(qg.cgpa) as total 
                            FROM tbl_school_course_requirement_included scri
                            LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                            WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                            (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.') AND
                            (
                                scri.instead_quantity <= (
                                    SELECT COUNT(*)
                                    FROM tbl_school_course_requirement_included_instead as scrii
                                    LEFT JOIN tbl_qualification_grade as qg1 ON scrii.instead_qualification_grade_id = qg1.id

                                    LEFT JOIN tbl_qualification_grade as qg2 ON scrii.second_instead_qualification_grade_id = qg2.id
                                    WHERE scrii.school_course_requirement_included_id = (
                                        scri.id
                                    ) AND
                                    (
                                        (
                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                                qg1.score <= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                                qg1.score >= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                                qg1.cgpa <= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                                qg1.cgpa >= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            )
                                        ) OR

                                        (
                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                                qg2.score <= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                                qg2.score >= (
                                                    SELECT qg1.score
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                                qg2.cgpa <= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            ) OR

                                            (
                                                scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                                qg2.cgpa >= (
                                                    SELECT qg1.cgpa
                                                    FROM tbl_student_academic_result sar
                                                    LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                                    WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                                    sar.student_id = '.$studentID.'
                                                )
                                            )
                                        )
                                    )
                                ) + (
                                    qg.cgpa <= (
                                        SELECT qg1.cgpa
                                        FROM tbl_student_academic_result sar
                                        LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                        WHERE sar.qualification_subject_id = scri.qualification_subject_id AND
                                        sar.student_id = '.$studentID.'
                                    )
                                )
                            )
                        )t
                    ) 
                ) AND

                /* =============== calculate included valid course ==================== */
                (

                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.') AND
                    (
                        scri.instead_quantity <= (
                            SELECT COUNT(*)
                            FROM tbl_school_course_requirement_included_instead as scrii
                            LEFT JOIN tbl_qualification_grade as qg1 ON scrii.instead_qualification_grade_id = qg1.id

                            LEFT JOIN tbl_qualification_grade as qg2 ON scrii.second_instead_qualification_grade_id = qg2.id
                            WHERE scrii.school_course_requirement_included_id = (
                                scri.id
                            ) AND
                            (
                                (
                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg1.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg1.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg1.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg1.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                ) OR

                                (
                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg2.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg2.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg2.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg2.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                )
                            )
                        ) + (
                            qg.cgpa <= (
                                SELECT qg1.cgpa
                                FROM tbl_student_academic_result sar
                                LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                WHERE sar.qualification_subject_id = scri.qualification_subject_id AND
                                sar.student_id = '.$studentID.'
                            )
                        )
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included as scri
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND 
                    (scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' OR scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INSTEAD.')
                ) AND

                (SELECT MIN(cgpa) FROM tbl_qualification_grade WHERE qualification_id = '.$qualification_id.' AND id = school_course_requirement_grade.qualification_grade_id) <=
                (
                    SELECT MIN(cgpa) FROM tbl_qualification_grade WHERE qualification_id = '.$qualification_id.' AND id IN (
                        SELECT total.id FROM (
                            SELECT saresult.qualification_grade_id as id, ROW_NUMBER() OVER(ORDER BY qgrade.cgpa DESC) as row_num
                            FROM tbl_student_academic_result as saresult
                            LEFT JOIN tbl_qualification_grade as qgrade ON saresult.qualification_grade_id = qgrade.id 
                            WHERE saresult.student_id = '.$studentID.' AND
                            saresult.qualification_id = '.$qualification_id.'
                            ORDER BY qgrade.cgpa DESC
                        ) as total
                        WHERE row_num <= ( SELECT sum(qualification_grade_quantity) FROM tbl_school_course_requirement_grade WHERE school_course_requirement_id = school_course_requirement.id )
                    )
                ) AND

                /* =============== for case handling 3 ==================== */

                /* =============== calculate included valid course ==================== */
                (

                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade as qg ON scri.qualification_grade_id = qg.id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.' AND
                    (
                        scri.instead_quantity <= (
                            SELECT COUNT(*)
                            FROM tbl_school_course_requirement_included_instead as scrii
                            LEFT JOIN tbl_qualification_grade as qg1 ON scrii.instead_qualification_grade_id = qg1.id

                            LEFT JOIN tbl_qualification_grade as qg2 ON scrii.second_instead_qualification_grade_id = qg2.id
                            WHERE scrii.school_course_requirement_included_id = (
                                scri.id
                            ) AND
                            (
                                (
                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg1.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg1.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg1.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg1.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                ) OR

                                (
                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_MORE_THAN.' AND
                                        qg2.score <= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::SCORE_LESS_THAN.' AND
                                        qg2.score >= (
                                            SELECT qg1.score
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_MORE_THAN.' AND
                                        qg2.cgpa <= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    ) OR

                                    (
                                        scrii.second_instead_grade_type = '.SchoolCourseRequirementIncludedInstead::CGPA_LESS_THAN.' AND
                                        qg2.cgpa >= (
                                            SELECT qg1.cgpa
                                            FROM tbl_student_academic_result sar
                                            LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                            WHERE sar.qualification_subject_id = scrii.second_instead_qualification_subject_id AND
                                            sar.student_id = '.$studentID.'
                                        )
                                    )
                                )
                            )
                        ) + (
                            qg.cgpa <= (
                                SELECT qg1.cgpa
                                FROM tbl_student_academic_result sar
                                LEFT JOIN tbl_qualification_grade qg1 ON sar.qualification_grade_id = qg1.id
                                WHERE sar.qualification_subject_id = scri.qualification_subject_id AND
                                sar.student_id = '.$studentID.'
                            )
                        )
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included as scri
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.'
                )
            ';

            $scoreMoreThan = '
                grade_type = '.SchoolCourseRequirement::SCORE_MORE_THAN.' AND 
                /* =============== for case handling 1 or 2 ==================== */

                /* =============== calculate total score of course ==================== */
                (
                    total_score <= (
                        SELECT SUM(r.score)
                        FROM (
                            SELECT t.score
                            FROM (
                                SELECT qg.score, ROW_NUMBER() OVER(ORDER BY qg.score DESC) as row_num
                                FROM tbl_student_academic_result sar
                                LEFT JOIN tbl_qualification_grade qg ON qg.id = sar.qualification_grade_id
                                WHERE sar.student_id = '.$studentID.' AND
                                sar.qualification_id = '.$qualification_id.' AND
                                sar.qualification_subject_id NOT IN (
                                    SELECT scri.qualification_subject_id
                                    FROM tbl_school_course_requirement_included scri
                                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' AND
                                    qg.score <= (
                                        SELECT qg1.score
                                        FROM tbl_student_academic_result sar
                                        LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                                        WHERE sar.student_id = '.$studentID.' AND
                                        sar.qualification_id = '.$qualification_id.' AND
                                        sar.qualification_subject_id = scri.qualification_subject_id
                                    )

                                    UNION ALL

                                    SELECT scri.qualification_subject_id
                                    FROM tbl_school_course_requirement_included scri
                                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_NOT_INCLUDED.'
                                )
                            ) t
                            WHERE t.row_num <= (
                                SELECT SUM(scrg.qualification_grade_quantity)
                                FROM tbl_school_course_requirement_grade scrg
                                LEFT JOIN tbl_school_course_requirement scrt ON scrt.id = scrg.school_course_requirement_id
                                WHERE scrg.school_course_requirement_id = school_course_requirement.id AND
                                scrt.qualification_id = '.$qualification_id.'
                            )-(
                                SELECT COUNT(*)
                                FROM tbl_school_course_requirement_included scri
                                LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.'
                            )

                            UNION ALL

                            SELECT SUM(qg.score)
                            FROM tbl_student_academic_result sar
                            LEFT JOIN tbl_qualification_grade qg ON qg.id = sar.qualification_grade_id
                            WHERE sar.student_id = '.$studentID.' AND
                            sar.qualification_id = '.$qualification_id.' AND
                            sar.qualification_subject_id IN (
                                SELECT scri.qualification_subject_id
                                FROM tbl_school_course_requirement_included scri
                                LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' AND
                                qg.score <= (
                                    SELECT qg1.score
                                    FROM tbl_student_academic_result sar
                                    LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                                    WHERE sar.student_id = '.$studentID.' AND
                                    sar.qualification_id = '.$qualification_id.' AND
                                    sar.qualification_subject_id = scri.qualification_subject_id
                                )
                            )
                        ) r
                    )
                ) AND

                /* =============== calculate included valid course ==================== */
                (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' AND
                    qg.score <= (
                        SELECT qg1.score
                        FROM tbl_student_academic_result sar
                        LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                        WHERE sar.student_id = '.$studentID.' AND
                        sar.qualification_id = '.$qualification_id.' AND
                        sar.qualification_subject_id = scri.qualification_subject_id
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.'
                ) AND

                /* =============== for case handling 3 ==================== */

                /* =============== calculate included valid course ==================== */

                (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.' AND
                    qg.score <= (
                        SELECT qg1.score
                        FROM tbl_student_academic_result sar
                        LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                        WHERE sar.student_id = '.$studentID.' AND
                        sar.qualification_id = '.$qualification_id.' AND
                        sar.qualification_subject_id = scri.qualification_subject_id
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.'
                )
            ';

            $scoreLessThan = '
                grade_type = '.SchoolCourseRequirement::SCORE_LESS_THAN.' AND 
                /* =============== for case handling 1 or 2 ==================== */

                /* =============== calculate total score of course ==================== */
                (
                    total_score >= (
                        SELECT SUM(r.score)
                        FROM (
                            SELECT t.score
                            FROM (
                                SELECT qg.score, ROW_NUMBER() OVER(ORDER BY qg.score ASC) as row_num
                                FROM tbl_student_academic_result sar
                                LEFT JOIN tbl_qualification_grade qg ON qg.id = sar.qualification_grade_id
                                WHERE sar.student_id = '.$studentID.' AND
                                sar.qualification_id = '.$qualification_id.' AND
                                sar.qualification_subject_id NOT IN (
                                    SELECT scri.qualification_subject_id
                                    FROM tbl_school_course_requirement_included scri
                                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' AND
                                    qg.score >= (
                                        SELECT qg1.score
                                        FROM tbl_student_academic_result sar
                                        LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                                        WHERE sar.student_id = '.$studentID.' AND
                                        sar.qualification_id = '.$qualification_id.' AND
                                        sar.qualification_subject_id = scri.qualification_subject_id
                                    )

                                    UNION ALL

                                    SELECT scri.qualification_subject_id
                                    FROM tbl_school_course_requirement_included scri
                                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_NOT_INCLUDED.'
                                )
                            ) t
                            WHERE t.row_num <= (
                                SELECT SUM(scrg.qualification_grade_quantity)
                                FROM tbl_school_course_requirement_grade scrg
                                LEFT JOIN tbl_school_course_requirement scrt ON scrt.id = scrg.school_course_requirement_id
                                WHERE scrg.school_course_requirement_id = school_course_requirement.id AND
                                scrt.qualification_id = '.$qualification_id.'
                            )-(
                                SELECT COUNT(*)
                                FROM tbl_school_course_requirement_included scri
                                LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.'
                            )

                            UNION ALL

                            SELECT SUM(qg.score)
                            FROM tbl_student_academic_result sar
                            LEFT JOIN tbl_qualification_grade qg ON qg.id = sar.qualification_grade_id
                            WHERE sar.student_id = '.$studentID.' AND
                            sar.qualification_id = '.$qualification_id.' AND
                            sar.qualification_subject_id IN (
                                SELECT scri.qualification_subject_id
                                FROM tbl_school_course_requirement_included scri
                                LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' AND
                                qg.score >= (
                                    SELECT qg1.score
                                    FROM tbl_student_academic_result sar
                                    LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                                    WHERE sar.student_id = '.$studentID.' AND
                                    sar.qualification_id = '.$qualification_id.' AND
                                    sar.qualification_subject_id = scri.qualification_subject_id
                                )
                            )
                        ) r
                    )
                ) AND

                /* =============== calculate included valid course ==================== */
                (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' AND
                    qg.score >= (
                        SELECT qg1.score
                        FROM tbl_student_academic_result sar
                        LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                        WHERE sar.student_id = '.$studentID.' AND
                        sar.qualification_id = '.$qualification_id.' AND
                        sar.qualification_subject_id = scri.qualification_subject_id
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.'
                ) AND

                /* =============== for case handling 3 ==================== */

                /* =============== calculate included valid course ==================== */

                (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.' AND
                    qg.score >= (
                        SELECT qg1.score
                        FROM tbl_student_academic_result sar
                        LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                        WHERE sar.student_id = '.$studentID.' AND
                        sar.qualification_id = '.$qualification_id.' AND
                        sar.qualification_subject_id = scri.qualification_subject_id
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.'
                )
            ';

            $CGPAMoreThan = '
                grade_type = '.SchoolCourseRequirement::CGPA_MORE_THAN.' AND 
                /* =============== for case handling 1 or 2 ==================== */

                /* =============== calculate total cgpa of course ==================== */
                (
                    total_cgpa <= (
                        SELECT SUM(r.cgpa)
                        FROM (
                            SELECT t.cgpa
                            FROM (
                                SELECT qg.cgpa, ROW_NUMBER() OVER(ORDER BY qg.cgpa DESC) as row_num
                                FROM tbl_student_academic_result sar
                                LEFT JOIN tbl_qualification_grade qg ON qg.id = sar.qualification_grade_id
                                WHERE sar.student_id = '.$studentID.' AND
                                sar.qualification_id = '.$qualification_id.' AND
                                sar.qualification_subject_id NOT IN (
                                    SELECT scri.qualification_subject_id
                                    FROM tbl_school_course_requirement_included scri
                                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' AND
                                    qg.cgpa <= (
                                        SELECT qg1.cgpa
                                        FROM tbl_student_academic_result sar
                                        LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                                        WHERE sar.student_id = '.$studentID.' AND
                                        sar.qualification_id = '.$qualification_id.' AND
                                        sar.qualification_subject_id = scri.qualification_subject_id
                                    )

                                    UNION ALL

                                    SELECT scri.qualification_subject_id
                                    FROM tbl_school_course_requirement_included scri
                                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_NOT_INCLUDED.'
                                )
                            ) t
                            WHERE t.row_num <= (
                                SELECT SUM(scrg.qualification_grade_quantity)
                                FROM tbl_school_course_requirement_grade scrg
                                LEFT JOIN tbl_school_course_requirement scrt ON scrt.id = scrg.school_course_requirement_id
                                WHERE scrg.school_course_requirement_id = school_course_requirement.id AND
                                scrt.qualification_id = '.$qualification_id.'
                            )-(
                                SELECT COUNT(*)
                                FROM tbl_school_course_requirement_included scri
                                LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.'
                            )

                            UNION ALL

                            SELECT SUM(qg.cgpa)
                            FROM tbl_student_academic_result sar
                            LEFT JOIN tbl_qualification_grade qg ON qg.id = sar.qualification_grade_id
                            WHERE sar.student_id = '.$studentID.' AND
                            sar.qualification_id = '.$qualification_id.' AND
                            sar.qualification_subject_id IN (
                                SELECT scri.qualification_subject_id
                                FROM tbl_school_course_requirement_included scri
                                LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' AND
                                qg.cgpa <= (
                                    SELECT qg1.cgpa
                                    FROM tbl_student_academic_result sar
                                    LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                                    WHERE sar.student_id = '.$studentID.' AND
                                    sar.qualification_id = '.$qualification_id.' AND
                                    sar.qualification_subject_id = scri.qualification_subject_id
                                )
                            )
                        ) r
                    )
                ) AND

                /* =============== calculate included valid course ==================== */
                (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' AND
                    qg.cgpa <= (
                        SELECT qg1.cgpa
                        FROM tbl_student_academic_result sar
                        LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                        WHERE sar.student_id = '.$studentID.' AND
                        sar.qualification_id = '.$qualification_id.' AND
                        sar.qualification_subject_id = scri.qualification_subject_id
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.'
                ) AND

                /* =============== for case handling 3 ==================== */

                /* =============== calculate included valid course ==================== */

                (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.' AND
                    qg.cgpa <= (
                        SELECT qg1.cgpa
                        FROM tbl_student_academic_result sar
                        LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                        WHERE sar.student_id = '.$studentID.' AND
                        sar.qualification_id = '.$qualification_id.' AND
                        sar.qualification_subject_id = scri.qualification_subject_id
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.'
                )
            ';

            $CGPALessThan = '
                grade_type = '.SchoolCourseRequirement::CGPA_LESS_THAN.' AND 
                /* =============== for case handling 1 or 2 ==================== */

                /* =============== calculate total cgpa of course ==================== */
                (
                    total_cgpa >= (
                        SELECT SUM(r.cgpa)
                        FROM (
                            SELECT t.cgpa
                            FROM (
                                SELECT qg.cgpa, ROW_NUMBER() OVER(ORDER BY qg.cgpa ASC) as row_num
                                FROM tbl_student_academic_result sar
                                LEFT JOIN tbl_qualification_grade qg ON qg.id = sar.qualification_grade_id
                                WHERE sar.student_id = '.$studentID.' AND
                                sar.qualification_id = '.$qualification_id.' AND
                                sar.qualification_subject_id NOT IN (
                                    SELECT scri.qualification_subject_id
                                    FROM tbl_school_course_requirement_included scri
                                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' AND
                                    qg.cgpa >= (
                                        SELECT qg1.cgpa
                                        FROM tbl_student_academic_result sar
                                        LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                                        WHERE sar.student_id = '.$studentID.' AND
                                        sar.qualification_id = '.$qualification_id.' AND
                                        sar.qualification_subject_id = scri.qualification_subject_id
                                    )

                                    UNION ALL

                                    SELECT scri.qualification_subject_id
                                    FROM tbl_school_course_requirement_included scri
                                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_NOT_INCLUDED.'
                                )
                            ) t
                            WHERE t.row_num <= (
                                SELECT SUM(scrg.qualification_grade_quantity)
                                FROM tbl_school_course_requirement_grade scrg
                                LEFT JOIN tbl_school_course_requirement scrt ON scrt.id = scrg.school_course_requirement_id
                                WHERE scrg.school_course_requirement_id = school_course_requirement.id AND
                                scrt.qualification_id = '.$qualification_id.'
                            )-(
                                SELECT COUNT(*)
                                FROM tbl_school_course_requirement_included scri
                                LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.'
                            )

                            UNION ALL

                            SELECT SUM(qg.cgpa)
                            FROM tbl_student_academic_result sar
                            LEFT JOIN tbl_qualification_grade qg ON qg.id = sar.qualification_grade_id
                            WHERE sar.student_id = '.$studentID.' AND
                            sar.qualification_id = '.$qualification_id.' AND
                            sar.qualification_subject_id IN (
                                SELECT scri.qualification_subject_id
                                FROM tbl_school_course_requirement_included scri
                                LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                                WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                                scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' AND
                                qg.cgpa >= (
                                    SELECT qg1.cgpa
                                    FROM tbl_student_academic_result sar
                                    LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                                    WHERE sar.student_id = '.$studentID.' AND
                                    sar.qualification_id = '.$qualification_id.' AND
                                    sar.qualification_subject_id = scri.qualification_subject_id
                                )
                            )
                        ) r
                    )
                ) AND

                /* =============== calculate included valid course ==================== */
                (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.' AND
                    qg.cgpa >= (
                        SELECT qg1.cgpa
                        FROM tbl_student_academic_result sar
                        LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                        WHERE sar.student_id = '.$studentID.' AND
                        sar.qualification_id = '.$qualification_id.' AND
                        sar.qualification_subject_id = scri.qualification_subject_id
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_INCLUDED.'
                ) AND

                /* =============== for case handling 3 ==================== */

                /* =============== calculate included valid course ==================== */

                (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.' AND
                    qg.cgpa >= (
                        SELECT qg1.cgpa
                        FROM tbl_student_academic_result sar
                        LEFT JOIN tbl_qualification_grade qg1 ON qg1.id = sar.qualification_grade_id
                        WHERE sar.student_id = '.$studentID.' AND
                        sar.qualification_id = '.$qualification_id.' AND
                        sar.qualification_subject_id = scri.qualification_subject_id
                    )
                ) = (
                    SELECT COUNT(*)
                    FROM tbl_school_course_requirement_included scri
                    LEFT JOIN tbl_qualification_grade qg ON qg.id = scri.qualification_grade_id
                    WHERE scri.school_course_requirement_id = school_course_requirement.id AND
                    scri.case_handling = '.SchoolCourseRequirementIncluded::CASE_HANDLING_EXTRA_INCLUDED.'
                )
            ';

            $filterQualification = '(
                school_course_requirement.qualification_id ='.$qualification_id.' AND 

                (
                    (
                        (SELECT sum(qualification_grade_quantity) FROM tbl_school_course_requirement_grade WHERE school_course_requirement_id = school_course_requirement.id ) <= 
                        ( SELECT COUNT(*) FROM tbl_student_academic_result WHERE student_id = '.$studentID.' AND qualification_id = '.$qualification_id.' ) AND
                        (
                            (
                                '.$gradeScoreMoreThan.'
                            ) OR

                            (
                                '.$gradeScoreLessThan.'
                            ) OR

                            (
                                '.$gradeCGPALessThan.'
                            ) OR

                            (
                                '.$gradeCGPAMoreThan.'
                            ) OR

                            (
                                '.$scoreMoreThan.'
                            ) OR

                            (
                                '.$scoreLessThan.'
                            ) OR

                            (
                                '.$CGPAMoreThan.'
                            ) OR

                            (
                                '.$CGPALessThan.'
                            )
                        )
                    ) OR

                    (
                        school_course_requirement.no_specific_requirement = 1
                    )
                )
            )';
            
            $secondFilterSQL[] = $filterQualification;
            $filterQualification .= ' AND ';
        }

        if($userModel->school_id)
        {
            $filterSQL .= 'school.id = '.$userModel->school_id.' AND ';
            $secondFilterSQL[] = 'school.id = '.$userModel->school_id;
        }
        else
        {
            $filterSQL .= 'school_course.active = 1 AND school.active = 1 AND';
            $secondFilterSQL[] = 'school.active = 1';
            $secondFilterSQL[] = 'school_course.active = 1';
        }

        if(count($secondFilterSQL)>0)
        {
            $secondFilterSQL = 'WHERE '.join(" AND ",$secondFilterSQL);
        }
        else
        {
            $secondFilterSQL = '';
        }

        $models = DB::select('
            SELECT school_course.*,
            (school_course.tuition_fee * exchange.rate) as exchanged_tuition_fee,
            school.school_type, school.school_name, school.logo, region.region, country.country,
            GROUP_CONCAT( DISTINCT school_major.sub_tag_id ORDER BY school_major.id) as sub_tag_list, 
            COUNT(*) over() as totalItem
            FROM tbl_school_course as school_course
            LEFT JOIN tbl_school_course_requirement as school_course_requirement ON school_course.id = school_course_requirement.school_course_id 
            LEFT JOIN tbl_school_course_requirement_grade as school_course_requirement_grade ON school_course_requirement.id = school_course_requirement_grade.school_course_requirement_id
            LEFT JOIN tbl_school as school ON school_course.school_id = school.id
            LEFT JOIN tbl_school_major as school_major ON  school_course.id = school_major.school_course_id
            LEFT JOIN tbl_exchange_rate as exchange ON exchange.from_currency_type = school_course.currency_type
            LEFT JOIN tbl_region as region ON region.id = school.region
            LEFT JOIN tbl_proficiency_country as country ON country.id = school.country
            WHERE '.$filterSQL.' '.$filterQualification.'
            shuffle_id >= (

                SELECT school_course.shuffle_id 
                FROM tbl_school_course as school_course
                LEFT JOIN tbl_school_course_requirement as school_course_requirement ON school_course.id = school_course_requirement.school_course_id 
                LEFT JOIN tbl_school_course_requirement_grade as school_course_requirement_grade ON school_course_requirement.id = school_course_requirement_grade.school_course_requirement_id
                LEFT JOIN tbl_school as school ON school_course.school_id = school.id
                LEFT JOIN tbl_school_major as school_major ON  school_course.id = school_major.school_course_id 
                LEFT JOIN tbl_exchange_rate as exchange ON exchange.from_currency_type = school_course.currency_type
                LEFT JOIN tbl_region as region ON region.id = school.region
                LEFT JOIN tbl_proficiency_country as country ON country.id = school.country
                '.$secondFilterSQL.'
                GROUP BY school_course.id
                ORDER BY school_course.shuffle_id 
                LIMIT '.$currentPage.', 1

            ) AND
            exchange.to_currency_type = '.$currency_code .'
            GROUP BY school_course.id, exchanged_tuition_fee
            ORDER BY school_course.shuffle_id
            LIMIT '.$pageSize.'
        ');

        $selectionData = DB::select('
            SELECT school.region, school_course.school_course_level_id,
            GROUP_CONCAT( DISTINCT school_major.main_tag_id ORDER BY school_major.id) as main_tag_list, 
            GROUP_CONCAT( DISTINCT school_major.sub_tag_id ORDER BY school_major.id) as sub_tag_list
            FROM tbl_school_course as school_course
            LEFT JOIN tbl_school_course_requirement as school_course_requirement ON school_course.id = school_course_requirement.school_course_id 
            LEFT JOIN tbl_school_course_requirement_grade as school_course_requirement_grade ON school_course_requirement.id = school_course_requirement_grade.school_course_requirement_id
            LEFT JOIN tbl_school as school ON school_course.school_id = school.id
            LEFT JOIN tbl_school_major as school_major ON  school_course.id = school_major.school_course_id 
            LEFT JOIN tbl_exchange_rate as exchange ON exchange.from_currency_type = school_course.currency_type
            LEFT JOIN tbl_region as region ON region.id = school.region
            LEFT JOIN tbl_proficiency_country as country ON country.id = school.country
            '.$secondFilterSQL.'
            GROUP BY school_course.id
        ');

        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'totalItem'=>count($models)>0?$models[0]->totalItem:0,
                'dataList'=>$models,
                'pageSize'=>$pageSize,
                'selectionData'=> $selectionData
                //'courseLevelList'=>SchoolCourseLevel::all()
            ]
        ]);
    }

    static function frontendGetCourseList1($request)
    {
        $clickPage = $request->get('clickPage');
        $filterArr = $request->get('filterArr');
        $is_admin = $request->get('is_admin');

        $models = DB::table('school_course')
                          ->leftJoin('school', 'school_course.school_id', '=', 'school.id')
                          ->leftJoin('school_major','school_course.id','=','school_major.school_course_id')
                          ->distinct()
                          ->select(
                                'school_course.id as course_id',
                                'school_course.school_course_level_id as course_level', 
                                'school.school_name',
                                'school.school_name_cn',
                                'school.logo', 
                                'school.school_name_cn', 
                                'school.address', 
                                'school_course.*',
                                'school.logo as picture' //replace course picture
                            )->orderBy('shuffle_id');
        $models = $models->where('school_course.active', true);

        if(isset($filterArr))
        {
            foreach ($filterArr as $key => $value) 
            {
                if(!empty($value))
                {
                    if($key == 'course_level_id')
                    {
                        $models = $models->where('school_course_level_id', $value);
                    }
                    elseif($key == 'country_id')
                    {
                        $models = $models->where('school.country', $value);
                    }
                    elseif($key == 'major_id')
                    {
                        $models = $models->where('school_major.sub_tag_id', $value);
                    }
                    elseif($key == 'study_field_id')
                    {
                        $models = $models->where('school_major.main_tag_id', $value);
                    }
                    elseif($key == 'region_id')
                    {
                        $models = $models->where('school.region', $value);
                    }
                    
                }
            }
        }

        if(isset($is_admin))
        {
            if(!$is_admin)
            {
                $models = $models->where('school_course.school_id', '=', 36);
            }
        }

        //3 B SPM
        $conditions = [
            14=>[//qualification_id
                31 => (object)[ //grade_id
                    'quantity'=>3,
                    'cgpa'=> 0,
                    'score'=> 12
                ]
            ]
        ];

        //$gradeModel = QualificationGrade::where('qualification_id',14)->where('id',31)->first();
        
       
        //throw new \Exception($score,GlobalList::API_RESPONSE_CODE['601']);

        /*$models = $models->leftJoin('school_course_requirement_tem', 'school_course.id', '=', 'school_course_requirement_tem.school_course_id')
                         ->leftJoin('school_course_requirement_grade', 'school_course_requirement_tem.id', '=', 'school_course_requirement_grade.school_course_requirement_id')
                        select(
                            DB::raw('
                                (
                                    CASE 
                                    WHEN grade_type == "2" THEN SELECT * FROM some_table WHERE some_col = '$someVariable'
                                    END
                                )'
                            )
                        )
                        ->where(function($query) use ($conditions){
                            foreach ($conditions as $qualification_id => $condition) 
                            {
                                $cgpa = 0;
                                $score = 0;
                                foreach ($condition as $grade_id => $detail) 
                                {
                                    $cgpa += $detail->cgpa;
                                    $score += $detail->score;
                                }

                                $query->orWhere([
                                        ['school_course_requirement_tem.total_point','<=',$score],
                                        ['school_course_requirement_tem.grade_type','=', 1],
                                    ])
                                    ->orWhere([
                                        ['school_course_requirement_tem.qualification_id',$qualification_id],
                                        ['school_course_requirement_tem.total_point','>=',$score],
                                        ['school_course_requirement_tem.grade_type','=', 2],
                                        function($query) use ($qualification_id){
                                            foreach ($condition as $grade_id => $detail) 
                                            {
                                                $query->where('school_course_requirement_grade.qualification_grade_id',$grade_id)
                                                      ->where('school_course_requirement_grade.qualification_grade_quantity','>=',$detail->quantity)
                                            }
                                            $qModel = QualificationGrade::where('qualification_id',$qualification_id)->min('score');
                                            //throw new \Exception($qModel,GlobalList::API_RESPONSE_CODE['601']);
                                        }
                                    ])
                                    ->orWhere([
                                        ['school_course_requirement_tem.total_point','<=',$cgpa],
                                        ['school_course_requirement_tem.grade_type','=', 3],
                                    ])
                                    ->orWhere([
                                        ['school_course_requirement_tem.total_point','>=',$cgpa],
                                        ['school_course_requirement_tem.grade_type','=', 4],
                                    ])
                                    ->orWhere([
                                        ['school_course_requirement_tem.total_point','<=',$score],
                                        ['school_course_requirement_tem.grade_type','=', 5],
                                    ])
                                    ->orWhere([
                                        ['school_course_requirement_tem.total_point','>=',$score],
                                        ['school_course_requirement_tem.grade_type','=', 6],
                                    ])
                                    ->orWhere([
                                        ['school_course_requirement_tem.total_point','<=',$cgpa],
                                        ['school_course_requirement_tem.grade_type','=', 7],
                                    ])
                                    ->orWhere([
                                        ['school_course_requirement_tem.total_point','>=',$cgpa],
                                        ['school_course_requirement_tem.grade_type','=', 8],
                                    ]);
                            }
                        });

        $models = $models->Paginate(
                        100,
                        ['*'],
                        'page',
                        ( isset($clickPage) ? $clickPage : null )
                    );
*/
        //$dataList = $models->items;
        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'totalItem'=>$models->total(),
                'dataList'=>$models->items(),
                'courseLevelList'=>SchoolCourseLevel::all()
            ]
        ]);
    }

    static function getCourseList($request)
    {
        $clickPage = $request->get('clickPage');
        $filterArr = $request->get('filterArr');
        $userModel = $request->get('userModel');

        $models = DB::table('school_course')
                          ->leftJoin('school', 'school_course.school_id', '=', 'school.id')
        				  ->select(
                                'school_course.id as course_id',
                                'school_course.school_course_level_id as course_level', 
                                'school.school_name',
                                'school.school_name_cn',
                                'school.logo', 
                                'school.school_name_cn', 
                                'school.address', 
                                'school_course.*'
                            )
        				  ->orderBy('id', 'desc');

        if(isset($userModel))
        {
            if(Tools::checkPermissionLevel($userModel->role,GlobalList::ADMIN_PERMISSION))
            {
                $models = $models->where('school_id', $userModel->school->id);
            }
        }

        if(isset($filterArr))
        {
            foreach ($filterArr as $key => $value) 
            {
                if(!empty($value))
                {
                    if($key == 'course_level')
                    {
                        $models = $models->where('school_course_level_id', 'like', '%'.$value[0].'%');
                    }
                    elseif($key == 'school_name_cn' || $key == 'school_name')
                    {
                        $models = $models->where('school.school_name', 'like', '%'.$value[0].'%')->orWhere('school.school_name_cn', 'like', '%'.$value[0].'%');
                    }
                    else
                    {
                        $models = $models->where('school_course.'.$key, 'like', '%'.$value[0].'%');
                    }
                }
            }
        }

        $models = $models->Paginate(
                        10,
                        ['*'],
                        'page',
                        ( isset($clickPage) ? $clickPage : null )
                    );

        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'totalItem'=>$models->total(),
                'dataList'=>$models->items(),
                'courseLevelList'=>SchoolCourseLevel::all()
            ]
        ]);
    }

    static function saveImage($image,$model)
    {
        $currentDate = date('Y-m-d');
        $destinationPath = storage_path('app/'.self::BASE_SCHOOL_COURSE_IMAGE_URL.$currentDate.'/');
        if (!file_exists($destinationPath)) 
        {
            File::makeDirectory($destinationPath);
        }

        if(!empty($model->picture))
        {
            self::deleteImage($model->picture);
        }

        if(preg_match('/^(data:\s*image\/(\w+);base64,)/',$image,$res))
        {
            $type = $res[2];
            $image = base64_decode(str_replace($res[1],'', $image));
            $model->picture = strtotime("now").'.'.$type;
            Storage::disk('local')->put(self::BASE_SCHOOL_COURSE_IMAGE_URL.$currentDate.'/'.$model->picture, $image);
        }
        return $model;
    }

    static function deleteImage($picture)
    {
        $explode = explode('.', $picture);
        $name = $explode[0];
        $date = date("Y-m-d", $name);
        $file = storage_path('app/'.self::BASE_SCHOOL_COURSE_IMAGE_URL.$date.'/'.$picture);
        if(file_exists($file))
        {
            unlink($file);
            return true;
        }
        return false;
    }
}
