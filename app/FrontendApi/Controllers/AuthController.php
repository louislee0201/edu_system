<?php

namespace App\FrontendApi\Controllers;

use Illuminate\Http\Request;

use App\Model\User;

use App\Tools\Tools;
use App\Tools\GlobalList;

class AuthController
{
    public function checkUser($request)
    {
        $username = $request->get('username');
        $password = $request->get('password');
        $api_token = $request->get('api_token');

        if(isset($username) && isset($password))
        {
            return User::where('username', $username)
                         ->where('password', md5($password))
                         ->first();
        }

        if(isset($api_token))
        {
            return User::where('api_token', decrypt($api_token))
                         ->first();
        }

        return null;
    }

    public function login(Request $request)
    {
        $userModel = $this->checkUser($request);

        if(empty($userModel))
        {
            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['601'],
                'error'=>'username or password wrong'
            ]);
        }

        $userModel->api_token = Tools::getToken($userModel->id);
        $userModel->save();

        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
                'access_token'=>encrypt($userModel->api_token),
                'role'=>$userModel->role
            ]
        ]);

    }

    public function logout(Request $request)
    {
        $userModel = $this->checkUser($request);

        if(empty($userModel))
        {
            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['601'],
                'error'=>''
            ]);
        }
        
        $userModel->api_token = null;
        $userModel->save();

        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>''
        ]);
    }
}