<?php

namespace App\FrontendApi\Controllers;

use Illuminate\Http\Request;

use App\Model\User;
use App\Model\Customer;
use App\Model\CustomerInfo;
use App\Model\CustomerBillingAddress;
use App\Model\DeliveryAddress;

use App\Tools\Tools;
use App\Tools\GlobalList;

use DB;

class CustomerController
{
    public function getCustomer(Request $request)
    {
        $isList = $request->get('isList');
        $userModel = $request->get('userModel');

        if(isset($isList,$userModel))
        {
            if($isList)
            {
                return Customer::getCustomerList($request);
            }
            else
            {
                $customer_id = $request->get('customer_id');

                if(isset($customer_id))
                {
                    $cusModel = Customer::where('id', $customer_id)->first();
                    if(!empty($cusModel))
                    {
                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>'',
                            'data'=>[
                                'customer_id'=> $cusModel->id,
                                'email'=> $cusModel->email,
                                'header_code'=> $cusModel->header_code,
                                'payment_method'=> $cusModel->payment_type,
                                'container_payment'=> $cusModel->container_payment,
                                'ssm_code'=> $cusModel->customer_info->id,

                                'company_name'=> $cusModel->customer_info->company_name,
                                'company_address'=> $cusModel->customer_info->company_address,
                                'company_postcode'=> $cusModel->customer_info->company_postcode,
                                'company_state'=> $cusModel->customer_info->company_state,
                                'company_country'=> $cusModel->customer_info->company_country,

                                'billing_name'=> $cusModel->customer_billing_address->billing_name,
                                'billing_address'=> $cusModel->customer_billing_address->billing_address,
                                'billing_postcode'=> $cusModel->customer_billing_address->billing_postcode,
                                'billing_state'=> $cusModel->customer_billing_address->billing_state,
                                'billing_country'=> $cusModel->customer_billing_address->billing_country,

                                'receiver_info_list'=>$cusModel->delivery_address
                            ]
                        ]);
                    }  

                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['601'],
                        'error'=>'Invalid Customer'
                    ]); 
                }
                
                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['601'],
                    'error'=>'Invalid Api'
                ]);
            }
        }
        else
        {
            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['602'],
                'error'=>'Invalid Api'
            ]);
        }
    }

    public function deleteCustomer(Request $request)
    {
        $customer_id = $request->get('customer_id');

        $cusModel = Customer::where('id', $customer_id)->first();

        if(!empty($cusModel))
        {
            $cusModel->delete();
            return Customer::getCustomerList($request);
        }
        else
        {
            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['601'],
                'error'=>'Invalid Customer'
            ]);
        }
    }

    public function editCustomer(Request $request)
    {
        $customer_id = $request->get('customer_id');
        $action = $request->get('action');
        $receiver_id = $request->get('receiver_id');

        if(isset($customer_id,$action))
        {
            if($action == 'resetCustomerInfo')
            {
                $cusModel = Customer::where('id', $customer_id)->first();
                if(!empty($cusModel))
                {
                    $cusModel->email = $request->get('email');
                    $cusModel->header_code = $request->get('header_code');
                    $cusModel->payment_type = $request->get('payment_method');
                    $cusModel->container_payment = $request->get('container_payment');

                    $cusModel->customer_info->ssm_code = $request->get('ssm_code');
                    $cusModel->customer_info->company_name = $request->get('company_name');
                    $cusModel->customer_info->company_address = $request->get('company_address');
                    $cusModel->customer_info->company_postcode = $request->get('company_postcode');
                    $cusModel->customer_info->company_state = $request->get('company_state');
                    $cusModel->customer_info->company_country = $request->get('company_country');

                    if($cusModel->save() && $cusModel->customer_info->save())
                    {
                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>''
                        ]);
                    }
                }
            }
            elseif($action == 'resetCustomerInvoiceInfo')
            {
                $customerBillingAddress = CustomerBillingAddress::where('customer_id', $customer_id)->first();
                if(!empty($customerBillingAddress))
                {
                    $customerBillingAddress->billing_name = $request->get('billing_name');
                    $customerBillingAddress->billing_address = $request->get('billing_address');
                    $customerBillingAddress->billing_country = $request->get('billing_country');
                    $customerBillingAddress->billing_state = $request->get('billing_state');
                    $customerBillingAddress->billing_postcode = $request->get('billing_postcode');

                    if($customerBillingAddress->save())
                    {
                        return response()->json([
                            'code'=>GlobalList::API_RESPONSE_CODE['200'],
                            'error'=>''
                        ]);
                    }
                }
            }
            elseif($action == 'resetDeliveryAddress')
            {
                $update_id = $request->get('update_id');

                $DeliveryAddress = DeliveryAddress::where('customer_id', $customer_id)->where('id',$update_id)->first();
                if(!empty($DeliveryAddress))
                {
                    foreach ($request->get('receiver_info_list') as $key => $value) 
                    {
                        if($update_id==$value['id'])
                        {
                            $DeliveryAddress->receiver_name = $value['receiver_name'];
                            $DeliveryAddress->receiver_address = $value['receiver_address'];
                            $DeliveryAddress->receiver_postcode = $value['receiver_postcode'];
                            $DeliveryAddress->receiver_state = $value['receiver_state'];
                            $DeliveryAddress->receiver_country = $value['receiver_country'];
                            $DeliveryAddress->receiver_tel = $value['receiver_tel'];
                            $DeliveryAddress->from_third_party_name = $value['from_third_party_name'];

                            if($DeliveryAddress->save())
                            {
                                return response()->json([
                                    'code'=>GlobalList::API_RESPONSE_CODE['200'],
                                    'error'=>''
                                ]);
                            }
                        }
                    }
                    
                }
                
            }

            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['601'],
                'error'=>'Invalid Api'
            ]);
        }

        if(isset($receiver_id,$customer_id))
        {
            DB::update('UPDATE tbl_delivery_address SET is_default = 0 WHERE customer_id = '.$customer_id.' AND is_default = 1');

            DB::update('UPDATE tbl_delivery_address SET is_default = 1 WHERE customer_id = '.$customer_id.' AND id = '.$receiver_id);

            $request->request->add(['isList'=>false]);

            return $this->getCustomer($request);
        }

        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['601'],
            'error'=>'Invalid Api'
        ]);
    }

    public function addCustomer(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        $header_code = $request->get('header_code');
        $payment_type = $request->get('payment_type');
        $container_payment = $request->get('container_payment');
        
        $ssm_code = $request->get('ssm_code');
        $company_name = $request->get('company_name');
        $company_address = $request->get('company_address');
        $company_state = $request->get('company_state');
        $company_country = $request->get('company_country');
        $company_postcode = $request->get('company_postcode');

        $billing_name = $request->get('billing_name');
        $billing_address = $request->get('billing_address');
        $billing_postcode = $request->get('billing_postcode');
        $billing_state = $request->get('billing_state');
        $billing_country = $request->get('billing_country');

        $receiver_name = $request->get('receiver_name');
        $receiver_address = $request->get('receiver_address');
        $receiver_postcode = $request->get('receiver_postcode');
        $receiver_state = $request->get('receiver_state');
        $receiver_country = $request->get('receiver_country');
        $receiver_tel = $request->get('receiver_tel');
        $from_third_party_name = $request->get('from_third_party_name');

        if(
            isset(
                $email,
                $password,
                $header_code,
                $payment_type,
                $container_payment,
                $ssm_code,
                $company_name,
                $company_address,
                $company_state,
                $company_country,
                $company_postcode,

                $billing_name,
                $billing_address,
                $billing_postcode,
                $billing_state,
                $billing_country,

                $receiver_name,
                $receiver_address,
                $receiver_postcode,
                $receiver_state,
                $receiver_country,
                $receiver_tel
            )
        )
        {
            $checkModel = Customer::where('header_code',strtoupper($header_code))->first();
            if(empty($checkModel))
            {
                $cusModel = new Customer;
                $cusModel->email = $email;
                $cusModel->password = md5($password);
                $cusModel->header_code = strtoupper($header_code);
                $cusModel->payment_type = $payment_type;
                $cusModel->container_payment = $container_payment;

                $cusModel->save();

                $cusInfoModel = new CustomerInfo;
                $cusInfoModel->customer_id = $cusModel->id;
                $cusInfoModel->ssm_code = $ssm_code;
                $cusInfoModel->company_name = $company_name;
                $cusInfoModel->company_address = $company_address;
                $cusInfoModel->company_state = $company_state;
                $cusInfoModel->company_country = $company_country;
                $cusInfoModel->company_postcode = $company_postcode;

                $cbaModel = new CustomerBillingAddress;
                $cbaModel->customer_id = $cusModel->id;
                $cbaModel->billing_name = $billing_name;
                $cbaModel->billing_address = $billing_address;
                $cbaModel->billing_postcode = $billing_postcode;
                $cbaModel->billing_state = $billing_state;
                $cbaModel->billing_country = $billing_country;

                $daModel = new DeliveryAddress;
                $daModel->is_default = 1;
                $daModel->customer_id = $cusModel->id;
                $daModel->receiver_name = $receiver_name;
                $daModel->receiver_address = $receiver_address;
                $daModel->receiver_postcode = $receiver_postcode;
                $daModel->receiver_state = $receiver_state;
                $daModel->receiver_country = $receiver_country;
                $daModel->receiver_tel = $receiver_tel;
                $daModel->from_third_party_name = ($from_third_party_name!=''?$from_third_party_name:'');

                if($cbaModel->save() && $daModel->save() && $cusInfoModel->save())
                {
                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['200'],
                        'error'=>'',
                        'data'=>[
                            'customer_id'=>$cusModel->id
                        ]
                    ]);
                }
                else
                {
                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['500'],
                        'error'=>'Internal Error'
                    ]);
                }
            }
            else
            {
                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['601'],
                    'error'=>'Repeat Header Code'
                ]);
            }
        }

        return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['601'],
            'error'=>'Invalid Api'
        ]);
    }
}