<?php

namespace App\FrontendApi\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Model\Config;
use App\Model\School;
use App\Model\SchoolCourse;

use App\Tools\GlobalList;

use DB;

use View;

class ConfigController
{
    public function homePage()
    {
        if(Config::DEVELOPMENT && (strpos($_SERVER['SERVER_NAME'],'www.edudotcom')!==false || strpos($_SERVER['SERVER_NAME'],'edudotcom')!==false) && strpos($_SERVER['SERVER_NAME'],'api.edudotcom')===false)
        {
            return view('maintain');
        }
        else
        {
            View::addExtension('html', 'php');
            return view()->file(public_path().'/frontend/index.html');
        }
    }

    public function getBanner(Request $request)
    {
    	$banner = Config::where('type',Config::TYPE['BANNER'])->first();
    	return response()->json([
            'code'=>GlobalList::API_RESPONSE_CODE['200'],
            'error'=>'',
            'data'=>[
            	'banner'=>json_decode($banner->content)
            ]
        ]);
    }

    public function image($direction,$fileName)
    {
        if(!empty($fileName))
        {
            $des = storage_path('app/'.$direction.'/'.$fileName);
            if(file_exists($des))
            {
                header('Content-type: image/png');
                echo file_get_contents($des);
            }
        }
    }

    public function search(Request $request)
    {
    	$searchContent = $request->get('searchContent');

    	if(isset($searchContent))
    	{
    		$course = DB::table('school_course')
			            ->where('name', 'like', '%'.$searchContent.'%')
			            ->get();

			$country = DB::table('school')
			            ->where('country', 'like', '%'.$searchContent.'%')
			            ->get();

			$school = DB::table('school')
			            ->where('name', 'like', '%'.$searchContent.'%')
			            ->get();

    		return response()->json([
	            'code'=>GlobalList::API_RESPONSE_CODE['200'],
	            'error'=>'',
	            'data'=>[
	            	'school'=>$school,
	            	'course'=>$course,
	            	'country'=>$country
	            ]
	        ]);
    	}
    }
}
