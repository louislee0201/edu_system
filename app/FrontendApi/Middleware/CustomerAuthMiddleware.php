<?php

namespace App\FrontendApi\Middleware;

use Closure;
use Illuminate\Support\Facades\Hash;

use App\Tools\GlobalList;

use App\Model\User;
use App\Tools\Tools;

class CustomerAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        try 
        {
            $api_token = $request->get('api_token');
            if(isset($api_token))
            {
                $userModel = User::where('api_token', decrypt($api_token))
                             ->first();

                if(empty($userModel))
                {
                    return response()->json([
                        'code'=>GlobalList::API_RESPONSE_CODE['602'],
                        'error'=>'Unauthorized'
                    ]);
                }

                $request->attributes->add(['userModel' => $userModel]);
            }
            else
            {
                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['602'],
                    'error'=>'Invalid Api'
                ]);
            }

            
            if(!Tools::checkRole($userModel,$request->route()->uri))
            {
                return response()->json([
                    'code'=>GlobalList::API_RESPONSE_CODE['601'],
                    'error'=>'Unauthorized'
                ]);
            }
        } 
        catch (\Exception $e) 
        {
            return response()->json([
                'code'=>GlobalList::API_RESPONSE_CODE['500'],
                'error'=>'Invalid Api'
            ]);
        }

        return $next($request);
    }
}
