<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::match(['get','post'], 'eduGP', 'Auth\EduGPController@eduGP');
//
Route::match(['post'], 'getCourse', 'SchoolCourseController@getCourse');
Route::match(['post'], 'getSchool', 'SchoolController@getSchool');
Route::match(['post'], 'getSelection', 'SchoolCourseController@getSelection');

Route::match(['post'], 'getDescriptionType', 'DescriptionController@getDescriptionType');
Route::match(['post'], 'getDescriptionList', 'DescriptionController@getDescriptionList');
Route::match(['post'], 'getDescriptionData', 'DescriptionController@getDescriptionData');

Route::match(['post'], 'getHomePageData', 'ConfigController@getHomePageData');
Route::match(['post'], 'guessRegister', 'ConfigController@guessRegister');
Route::match(['post'], 'checkLogin', 'ConfigController@checkLogin');
Route::match(['post'], 'login', 'ConfigController@login');
Route::match(['post'], 'logout', 'ConfigController@logout');
Route::match(['post'], 'userRegister', 'ConfigController@userRegister');
Route::match(['post'], 'submidApplyForm', 'ConfigController@submidApplyForm');
Route::match(['post'], 'submidSeminarsForm', 'ConfigController@submidSeminarsForm');

Route::match(['get','post'], 'test', 'DescriptionController@test');

Route::match(['post'], 'updateStudentAcademicResult', 'StudentAcademicResultController@updateStudentAcademicResult');


