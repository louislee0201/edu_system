<?php
use Illuminate\Http\Request;
Route::match(['post'], 'getSchool', 'SchoolController@getSchool');
Route::match(['post'], 'updateSchool', 'SchoolController@updateSchool');
//Route::match(['post'], 'getReionList', 'SchoolController@getReionList');
/*Route::match(['post'], 'addNewSchool', 'SchoolController@addNewSchool');
Route::match(['post'], 'editSchool', 'SchoolController@editSchool');*/

Route::match(['post'], 'getCourse', 'SchoolCourseController@getCourse');
//Route::match(['post'], 'deleteCourse', 'SchoolCourseController@deleteCourse');
Route::match(['post'], 'getSchoolList', 'SchoolCourseController@getSchoolList');
//Route::match(['post'], 'addNewCourse', 'SchoolCourseController@addNewCourse');
Route::match(['post'], 'updateCourse', 'SchoolCourseController@editCourse');
Route::match(['post'], 'getCourseLevelList', 'SchoolCourseController@getCourseLevelList');
Route::match(['post'], 'updateCourseLevel', 'SchoolCourseController@editCourseLevel');
Route::match(['post'], 'editCourseSubTagList', 'SchoolCourseController@editSubTagList');

Route::match(['post'], 'getRequirementAttr', 'SchoolCourseRequirementController@getRequirementAttr');
Route::match(['post'], 'submitRequirement', 'SchoolCourseRequirementController@submitRequirement');
Route::match(['post'], 'updateRequirement', 'SchoolCourseRequirementController@updateRequirement');


Route::match(['post'], 'getQualification', 'QualificationController@getQualification');
//Route::match(['post'], 'addNewQualification', 'QualificationController@addNewQualification');
Route::match(['post'], 'deleteQualification', 'QualificationController@deleteQualification');
Route::match(['post'], 'editQualification', 'QualificationController@editQualification');

Route::match(['post'], 'deleteQualificationSubject', 'QualificationSubjectController@deleteQualificationSubject');
Route::match(['post'], 'updateQualificationSubject', 'QualificationSubjectController@editQualificationSubject');
Route::match(['post'], 'addQualificationSubject', 'QualificationSubjectController@addQualificationSubject');
Route::match(['post'], 'getQualificationSubject', 'QualificationSubjectController@getQualificationSubject');

Route::match(['post'], 'getProficiency', 'ProficiencyController@getProficiency');
Route::match(['post'], 'getCountry', 'ProficiencyController@getCountry');
Route::match(['post'], 'updateCountry', 'ProficiencyController@editCountry');
Route::match(['post'], 'addProfiency', 'ProficiencyController@addProfiency');
Route::match(['post'], 'deleteProficiency', 'ProficiencyController@deleteProficiency');
Route::match(['post'], 'updateProficiency', 'ProficiencyController@editProficiency');
Route::match(['post'], 'getLanguage', 'ProficiencyController@getLanguage');
Route::match(['post'], 'updateLanguage', 'ProficiencyLanguageController@editLanguage');

Route::match(['post'], 'deleteGrade', 'QualificationGradeController@deleteGrade');
Route::match(['post'], 'addGrade', 'QualificationGradeController@addGrade');
Route::match(['post'], 'updateQualificationGrade', 'QualificationGradeController@editGrade');

Route::match(['post'], 'addProfiencyGrade', 'ProficiencyGradeController@addProfiencyGrade');
Route::match(['post'], 'editProficiencyGrade', 'ProficiencyGradeController@editProficiencyGrade');
Route::match(['post'], 'activationProficiencyGrade', 'ProficiencyGradeController@activationProficiencyGrade');

Route::match(['post'], 'getDescriptionType', 'DescriptionController@getDescriptionType');
Route::match(['post'], 'updateDescription', 'DescriptionController@updateDescription');
Route::match(['post'], 'getDescriptionList', 'DescriptionController@getDescriptionList');
Route::match(['post'], 'getDescriptionData', 'DescriptionController@getDescriptionData');

Route::match(['post'], 'getReionList', 'ConfigController@getReionList');
Route::match(['post'], 'updateRegion', 'ConfigController@updateRegion');

Route::match(['post'], 'getTag', 'MainTagController@getTag');
Route::match(['post'], 'updateMainTagList', 'MainTagController@updateMainTagList');
Route::match(['post'], 'updateSubTagList', 'MainTagController@updateSubTagList');


Route::match(['post'], 'editSchoolCourseArrangementTitle', 'SchoolCourseArrangementController@editSchoolCourseArrangementTitle');


