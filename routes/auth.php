<?php

use Illuminate\Http\Request;

Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');
Route::post('loginStatus', 'AuthController@loginStatus');
Route::get('image/{direction}/{fileName}', 'ConfigController@image');
Route::get('pdf/{direction}/{fileName}', 'ConfigController@pdf');