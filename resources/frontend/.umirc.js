const path = require('path');
//const px2rem = require('postcss-plugin-px2rem');
//用nodejs 12v
// ref: https://umijs.org/config/
export default {
    base: '/',//admin/
    publicPath: '/frontend/',//admin/
    outputPath: '../../public/frontend',
    hash: true,
    plugins: [
        // ref: https://umijs.org/plugin/umi-plugin-react.html
        ['umi-plugin-react', {
            antd: true,
            dva: true,
            // dynamicImport: false,
            // dll: true,
            // routes: {
            //     exclude: [
            //         /model\.(j|t)sx?$/,
            //         /service\.(j|t)sx?$/,
            //         /models\//,
            //         /components\//,
            //         /services\//,
            //     ]
            // },
            //hd: true,
            hardSource: false,
            locale: {
                enable: true,
                default: 'zh-CN',
                baseNavigator: true,
            },
        }],
    ],
    // extraPostCSSPlugins: [
    //     //https://www.npmjs.com/package/postcss-plugin-px2rem
    //     px2rem({
    //         rootValue: 256,//开启hd后需要换算：rootValue=designWidth*100/750,此处设计稿为1920，所以1920*100/750=256
    //         propBlackList: ['border', 'border-top', 'border-left', 'border-right', 'border-bottom', 'border-radius'/* , 'font-size' */],//这些属性不需要转换
    //         selectorBlackList: ['t_npx']//以包含t_npx的class不需要转换
    //     })
    // ],
    alias: {
        components: path.resolve(__dirname, 'src/components'),
        utils: path.resolve(__dirname, 'src/utils'),
        services: path.resolve(__dirname, 'src/services'),
        models: path.resolve(__dirname, 'src/models'),
        // themes:path.resolve(__dirname,'src/themes'),
        images: path.resolve(__dirname, 'src/assets')
    },
    cssLoaderOptions: {
        localIdentName: '[local]'
    },
}
