import {
    getLocalStorage, removeLocalStroage, saveLocalStorage, getSessionStorage, removeSessionStroage,
    saveSessionStorage
} from '@/tools'
import { STORAGE_KEY } from '@/constants/default'
//function
function isNotExist(data) {
    return data === null || typeof (data) === 'undefined'
}

function isOutPeriod(data) {
    let readTime = Number(new Date()) / 1000

    if ((readTime - data.writeTime) > data.period) {
        return true
    }
    return false
}

export function saveUser(isLocal, { name, data, saveTime }) {
    let dataJSON = getLocalStorage(name)
    if (!isNotExist(dataJSON)) {
        removeLocalStroage(name)
    }

    dataJSON = getSessionStorage(name)
    if (!isNotExist(dataJSON)) {
        removeSessionStroage(name)
    }

    let saveData = {
        data,
        writeTime: Number(new Date()) / 1000,
        period: parseInt(saveTime) * 24 * 60 * 60
    }

    if (isLocal) {
        saveLocalStorage(name, JSON.stringify(saveData))
    }
    else {
        saveSessionStorage(name, JSON.stringify(saveData))
    }
}

export function getUser(key = '') {
    let dataJSON = getLocalStorage(STORAGE_KEY.user)
    let isLocal = true
    if (isNotExist(dataJSON)) {
        dataJSON = getSessionStorage(STORAGE_KEY.user)
        isLocal = false
        if (isNotExist(dataJSON)) {
            return null
        }
    }

    let localData = JSON.parse(dataJSON)

    if (isOutPeriod(localData)) {
        if (isLocal) {
            removeLocalStroage(STORAGE_KEY.user)
        }
        else {
            removeSessionStroage(STORAGE_KEY.user)
        }
        return null
    }
    
    return localData.data[key]

    //return data.data
}

export function removeUser() {
    removeLocalStroage(STORAGE_KEY.user)
    removeSessionStroage(STORAGE_KEY.user)
}

export function getUserData() {
    let dataJSON = getLocalStorage(STORAGE_KEY.user)
    let isLocal = true
    if (isNotExist(dataJSON)) {
        dataJSON = getSessionStorage(STORAGE_KEY.user)
        isLocal = false
        if (isNotExist(dataJSON)) {
            return null
        }
    }

    let localData = JSON.parse(dataJSON)

    if (isOutPeriod(localData)) {
        if (isLocal) {
            removeLocalStroage(STORAGE_KEY.user)
        }
        else {
            removeSessionStroage(STORAGE_KEY.user)
        }
        return null
    }

    return localData.data
}

export function getAccLevel() {
    const access_token = getUser('acc_level')
    if (access_token !== null) {
        return access_token
    }
    return false
}

export function getUserToken() {
    const access_token = getUser('user_token')
    if (access_token !== null) {
        return access_token
    }
    return false
}

export function getUserLanguage() {
    const language = getLocalStorage(STORAGE_KEY.language)
    if (isNotExist(language)) {
        return false
    }
    return language
}

export function getUserID() {
    const uid = getUser('uid')
    if (uid !== null) {
        return uid
    }
    return false
}

export function reSetUserToken(key, user_token) {
    setUser(key, user_token)
}

export function setUser(key, user_token) {
    let dataJSON = getLocalStorage(STORAGE_KEY.user)
    let isLocal = true
    if (isNotExist(dataJSON)) {
        dataJSON = getSessionStorage(STORAGE_KEY.user)
        if (isNotExist(dataJSON)) {
            return false
        }
        isLocal = false
    }

    let localData = JSON.parse(dataJSON)
    localData.data[key] = user_token

    if (isLocal) {
        saveLocalStorage(STORAGE_KEY.user, JSON.stringify(localData))
    }
    else {
        saveSessionStorage(STORAGE_KEY.user, JSON.stringify(localData))
    }
    return true
}