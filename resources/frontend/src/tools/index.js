//local storage
export function getLocalStorage(name) {
    return localStorage.getItem(name);
}

export function removeLocalStroage(name) {
    localStorage.removeItem(name)
}

export function saveLocalStorage(name, saveData) {
    localStorage.setItem(name, saveData)
}

//session storage
export function getSessionStorage(name) {
    return sessionStorage.getItem(name);
}

export function removeSessionStroage(name) {
    sessionStorage.removeItem(name)
}

export function saveSessionStorage(name, saveData) {
    sessionStorage.setItem(name, saveData)
}

export function getLanguageList() {
    return {
        'en-US': 'English',
        'zh-CN': '中文简体'
    }
}

export function systemSetLocale(name, saveData) {
    saveLocalStorage(name, saveData)
}

// export function get64baseImage(file,onLoadCallback)
// {
//     const reader = new FileReader()
//     reader.onload = onLoadCallback
//     reader.readAsDataURL(file)
//     return reader
// }

export function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}