export const baseUrl = window.location.origin + '/';
export const HOST = window.location.hostname.indexOf('louis-blog') >= 0 || window.location.hostname.indexOf('edudotcom') >= 0 ? window.location.protocol + '//' + window.location.hostname.replace('www', 'api') + '/' : "https://api.edudotcom.my/"

export const API_GUESS_REGISTER = 'api/guessRegister'
export const API_CHECK_LOGIN = 'api/checkLogin'
export const API_LOGIN = 'api/login'
export const API_LOGOUT = 'api/logout'
export const API_USER_REGISTER = 'api/userRegister'

export const API_GET_COURSE = 'api/getCourse'
export const API_GET_SCHOOL = 'api/getSchool'
export const API_GET_SELECTION = 'api/getSelection'

export const API_GET_DESCRIPTION_TYPE = 'api/getDescriptionType'
export const API_GET_DESCRIPTION_LIST = 'api/getDescriptionList'
export const API_GET_DESCRIPTION_DATA = 'api/getDescriptionData'

export const API_GET_HOME_PAGE_DATA = 'api/getHomePageData'

export const API_UPDATE_STUDENT_ACADEMIC_RESULT = 'api/updateStudentAcademicResult'

export const API_SUBMID_APPLY_FORM = 'api/submidApplyForm'
export const API_SUBMID_SEMINARS_FORM = 'api/submidSeminarsForm'