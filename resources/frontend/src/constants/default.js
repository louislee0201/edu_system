export const PRIMARY_COLOR = '#f26931'
export const SUB_PRIMARY_COLOR = '#daa520'
export const APP_NAME = 'Edu Dotcom'
export const BACKGROUND_COLOR = '#f5f5f5'
export const DEFAULT_SETTING = {
    language: 'en-US'
}

export const SYSTEM_LANGUAGE = {
    zh_CN: 'zh-CN',
    en_US: 'en-US'
}

export const STORAGE_KEY = {
    user: 'user_info',
    language: 'umi_locale'
}