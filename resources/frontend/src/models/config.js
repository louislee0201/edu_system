import {reg} from 'services/my';
import router from 'umi/router';
import {removeUser, saveUser, getUserLanguage, getUserToken} from '@/tools/user'
import {systemSetLocale} from '@/tools'
import {DEFAULT_SETTING, STORAGE_KEY} from '@/constants/default'
import {setLocale} from 'umi/locale'
import {
  API_GUESS_REGISTER,
  API_CHECK_LOGIN,
  API_LOGOUT,
  API_LOGIN,
  API_USER_REGISTER,
  API_SUBMID_SEMINARS_FORM
} from '@/constants/api'
import {request} from 'services';

export default {
  namespace: 'config',
  state: {
    language: '',
    user_token: '',
    name: '',
    email: '',
    account_level: 1,
    tel: '',

    ipCountryCode: 'USD',
    ipCountryCodeId: 1, // for USD

    loginStatusCheck: false,
    currencyStatusCheck: false,

    newsAndEvents: [
      {
        type: 'Annoucement',
        img: 'https://edudotcom.my/auth/image/description/1689908930.png',
        date: '21 July 2023',
        topic: 'Japan Study Abroad Online Seminar',
        content: <div className='desktop-homePage-news-content-father-child-card-desc'>
          Join our online seminar to learn all the essential information about studying abroad in Japan!
          <br/><br/>
          Date: 30/07/2023, Sunday<br/>
          Time: 8:00 pm - 9:30 pm<br/>
          Platform: Zoom<br/><br/>

          <a href="https://forms.gle/FZNgKb4FfrSVCMcYA" target="_blank" rel="noreferrer">Registration here</a>
        </div>,
        color: 'purple'
      },
      {
        type: 'Annoucement',
        img: 'https://edudotcom.my/auth/image/description/1687099575.jpeg',
        date: '19 June 2023',
        topic: 'Japanese Study Center\'s Online Japanese Course System: Over 100 Users in 1 Year!',
        content: <div className='desktop-homePage-news-content-father-child-card-desc'>
          We are excited to announce that our online Japanese course system at the Japanese Study Center has reached
          a <br/>
          milestone of over 100 users within just one year! <br/>
          This accomplishment is a testament to our dedication and your support. <br/> <br/>

          Our system offers convenient and effective online Japanese courses, catering to learners of all levels. <br/>
          Whether you're a beginner or have some experience, our diverse range of courses covers essential skills like
          listening, <br/>
          speaking, reading, and writing.
          <br/><br/>

          Key highlights:
          <br/>
          Diverse courses: From basic to advanced levels, our courses meet various learning needs, <br/>
          including everyday conversation and exam preparation. <br/>

          Personalized learning: Our system adjusts to your progress and preferences, <br/>
          allowing you to choose content, timing, and pace that suit you best. <br/>

          Interactive tools: Engage with interactive exercises, listening practice, <br/>
          and online assignments to enhance your language skills. <br/>

          Real-time support: Our team of professional Japanese teachers provides <br/>
          real-time assistance and feedback throughout your learning journey. <br/>

          We appreciate each student who has chosen our online Japanese course system. <br/>
          Your support and feedback are invaluable as we continuously improve and expand our offerings. <br/>

          If you haven't joined yet, now is the perfect time! <br/>
          Visit our website for more information and registration. <br/>
          <br/> <br/>
          Thank you for your support!
        </div>,
        color: 'purple'
      },
      {
        type: 'Event',
        img: 'https://edudotcom.my/auth/image/description/1632919337.jpeg',
        date: '29 September 2021',
        topic: '立命館アジア太平洋大学线上说明会',
        content: <div className='desktop-homePage-news-content-father-child-card-desc'>
          Ritsumeikan Asia Pacific University - Online Seminar <br/><br/>

          Date: 06-10-2021 (Wed)<br/>
          Time: 15:00~14:00 （Japan, Korea @GMT+9）/ 14:00~15:00 （China, Malaysia, Singapore @GMT+8）<br/><br/>

          We invite you to participate in our online seminar with Ritsumeikan Asia Pacific University. <br/>
          This seminar will conducted in Japanese via Zoom online. To participate, please
          <a onClick={() => {
            router.push('/introduction/newsAndEvents/form')
          }}> Click here </a>
          to fill in a simple form.
        </div>,
        color: 'blue'
      },
      {
        type: 'Event',
        img: 'https://www.edudotcom.my/auth/image/description/1621172705.jpeg',
        date: '5 May 2021',
        topic: 'Schedule of seminars (Japan)',
        content: <div className='desktop-homePage-news-content-father-child-card-desc'>
          This coming May and June, let’s find out the latest Japan study information with us for free! <br/><br/>

          To join our seminar, please click on the below link or direct contact to us.<br/>
          <a
            onClick={() => window.open('https://docs.google.com/forms/d/e/1FAIpQLSdXZ7MtFWRpofzjuMUFVgGfD9mGn8Rp2NJV3VCl9QJ5c-EWog/viewform', '_blank')}>shorturl.at/lvBL6</a><br/><br/>

          <div style={{color: 'red', fontWeight: 'bold'}}>【29/5/2021 Saturday】</div>
          ️”Study in Japan” General Info Session@12pm-1.30pm<br/>
          ️Kwansei Gakuin University2pm-3.30pm<br/><br/>

          <div style={{color: 'red', fontWeight: 'bold'}}>【30/5/2021 Sunday】</div>
          ️Tokyo Desiger Gakuin Collegr12pm-1.30pm<br/>
          ️Tokyo Visial Arts College@2pm-3.30pm<br/><br/>

          <div style={{color: 'red', fontWeight: 'bold'}}>【05/6/2021 Saturday】</div>
          ️Digital Hollywood University@12pm-1.30pm<br/>
          ️Meros Language School@2pm-3.30pm<br/>
          （Include Japan Arts Program)<br/><br/>

          <div style={{color: 'red', fontWeight: 'bold'}}>【06/6/2021 Sunday】</div>
          ️Akamonkai Japanese Language School@12pm-2pm<br/>
          （Include Career in Japan）<br/><br/>

          <div style={{color: 'red', fontWeight: 'bold'}}>【12/6/2021 Saturday】</div>
          ️Ritsumeikan Asia Pacific University@12pm-1.30pm<br/>
          （Japanese or English Track: Business / Social Science）<br/>
          ️Kyoto University of Advanced Science@2pm-3.30pm<br/>
          （English Track: Engineering）<br/><br/>

          <div style={{color: 'red', fontWeight: 'bold'}}>【13/6/2021 Sunday】</div>
          ️Osaka YMCA Japanese Language School@12pm-1.30pm
        </div>,
        color: 'blue'
      },
      {
        type: 'Annoucement',
        img: 'http://www.edudotcom.my/auth/image/description/1610872498.jpeg',
        date: '17 January 2021',
        topic: 'Temporary closure of official website',
        content: <div className='desktop-homePage-news-content-father-child-card-desc'>
          We are temporary closed down our official website and the brand new website will be launching on this coming
          March. Stay tune and don't forget to get the latest news from our offical facebook or instagram channel!
        </div>,
        color: 'purple'
      },
      {
        type: 'News',
        img: 'http://www.edudotcom.my/auth/image/description/1610874527.jpeg',
        date: '15 January 2021',
        topic: 'Japan is closing national border for foreign students',
        content: <div /* className='desktop-homePage-news-content-father-child-card-desc' */>
          Japan declares closing of national border due to the corovavirus-19 until further announcement.
          However, all schools in Japan will still accept application from overseas and will not delay school entrance
          date at the moments.
        </div>,
        color: 'red'
      },
      {
        type: 'Annoucement',
        img: 'http://www.edudotcom.my/auth/image/description/1610873974.jpeg',
        date: '14 January 2021',
        topic: 'IELTS Test is available as Scheduled during MCO Period',
        content: <div /* className='desktop-homePage-news-content-father-child-card-desc' */>
          This is to inform that both Computer-Delivered and Paper-based IELTS will be available during the current MCO
          period. All precautions are taken under strict SOP guidelines to ensure the safety of everyone. Please contact
          to our counsellor at +6014-2773177 if you need to apply for IELTS or any enquiry.
          Please take care to look after your health at this time by following the latest advice, available here:
          https://www.who.int/emergencies/diseases/novel-coronavirus-2019
          Thank you for your understanding.
        </div>,
        color: 'purple'
      },
      {
        type: 'News',
        img: 'http://www.edudotcom.my/auth/image/description/1610871991.jpeg',
        date: '5 January 2021',
        topic: 'EDU Entrance Scholarship for Akamonkai',
        content: <div /* className='desktop-homePage-news-content-father-child-card-desc' */>
          In order to encourage students joining for Akamonkai Japanese Language School, EDU DOTCOM now sponsoring
          JPY40,000 as New Entrance Scholarship for students. For more info and details, please refer to the relevant
          school page in our website or contact to our counsellor.
        </div>,
        color: 'red'
      },
      {
        type: 'News',
        img: 'http://www.edudotcom.my/auth/image/description/1610870950.jpeg',
        date: '1 January 2021',
        topic: 'Kaplan\'s Application Fee waiver for January 2021',
        content: <div /* className='desktop-homePage-news-content-father-child-card-desc' */>
          (1) Target Audience : Malaysian Students <br/>
          (2) Promotion : SGD$ 492.20 Application Fees Waiver<br/>
          (3) Qualifying Period : 1 Jan 2021 – 31 Jan 2021<br/>
          (4) Terms & Conditions :<br/>
          • Waiver shall only apply to the program and intake specified upon application form and/or Waiver Letter.<br/>
          • The waiver will not be indicated in the Student Contract.<br/>
          • Deferment of intake or change of program will result in forfeiting the waiver.<br/>
          • Withdrawal from program prior to commencement will result in forfeiting the waiver.<br/>
          • There shall be no duplication of waiver allowed.
        </div>,
        color: 'red'
      },
    ]
  },
  reducers: {
    r_updateState(state, {payload}) {
      return {...state, ...payload}
    }
  },
  effects: {
    * e_userRegister({payload}, {put, call}) {
      const {success, data} = yield call(request, payload, {
        method: 'post',
        constant: API_USER_REGISTER
      })
      if (success) {
        if (!data.register) {
          return data
        }
        saveUser(true, {
          name: STORAGE_KEY.user,
          data: {user_token: data.user_token},
          saveTime: 99999
        })

        yield put({
          type: 'r_updateState',
          payload: {
            ...data
          }
        });
        router.goBack()
        return data
      }
      return false
    },
    * e_login({payload}, {put, call, select}) {
      const {success, data} = yield call(request, payload, {
        method: 'post',
        constant: API_LOGIN
      })
      if (success) {
        if (!data.login) {
          return data
        }
        saveUser(true, {
          name: STORAGE_KEY.user,
          data: {user_token: data.user_token},
          saveTime: 99999
        })

        yield put({
          type: 'r_updateState',
          payload: {
            ...data
          }
        });
        router.goBack()
        if (data.ad) {
          let courseState = yield select(state => state.course)
          yield put({
            type: 'course/r_updateState',
            payload: {
              searchCourseFilter: {
                ...courseState.searchCourseFilter,
                qualificationList: [],
                dataList: []
              }
            }
          });
        }
        return data
      }
      return false
    },

    * e_logout({payload}, {put, call}) {
      const {success, data} = yield call(request, payload, {
        method: 'post',
        constant: API_LOGOUT
      })
      if (success) {
        removeUser()
        location.reload()
      }
    },

    * e_checkLogin({payload}, {put, call}) {
      const {success, data} = yield call(request, payload, {
        method: 'post',
        constant: API_CHECK_LOGIN
      })
      if (success) {
        yield put({
          type: 'r_updateState',
          payload: {
            loginStatusCheck: true,
            ...data
          }
        });
        return true
      }
      removeUser()
      location.reload()
    },

    * e_changeLanguage({payload}, {put, call}) {
      setLocale(payload, false)
      yield put({
        type: 'r_updateState',
        payload: {
          language: payload
        }
      });
    },
    * e_guessRegister({payload}, {put, call}) {
      const {success, data} = yield call(request, payload, {
        method: 'post',
        constant: API_GUESS_REGISTER
      })
      if (success) {
        saveUser(true, {
          name: STORAGE_KEY.user,
          data: {user_token: data.user_token},
          saveTime: 99999
        })

        yield put({
          type: 'r_updateState',
          payload: {
            loginStatusCheck: true,
            ...data
          }
        });

        //router.push('/home')
      }
    },
    * e_submitSeminarsForm({payload}, {put, call}) {
      const {success, data} = yield call(request, payload, {
        method: 'post',
        constant: API_SUBMID_SEMINARS_FORM
      })
      if (success) {
        return true
      }
      return false
    },
  },
  subscriptions: {
    setup({dispatch, history}) {
      fetch("https://ipinfo.io/json").then((response) => {
        response.text().then((result) => {
          /* let json = JSON.parse(result.substring(
              result.lastIndexOf("(") + 1,
              result.lastIndexOf(")")
          )); */

          result = JSON.parse(result)
          if (result.country) {
            /* let currencyTypeList = {
                USD: 1,
                GBP: 2,
                HKD: 4,
                CNY: 5,
                SGD: 6,
                MYR: 7,
                KRW: 8,
                JPY: 9,
                THB: 10,
                IDR: 12
            } */
            let currencyTypeList = {
              US: 'USD',
              GB: 'GBP',
              HK: 'HKD',
              CN: 'CNY',
              SG: 'SGD',
              MY: 'MYR',
              KR: 'KRW',
              JP: 'JPY',
              TH: 'THB',
              ID: 'IDR'
            }
            let countryTypeList = {
              US: 1,
              GB: 2,
              HK: 4,
              CN: 5,
              SG: 6,
              MY: 7,
              KR: 8,
              JP: 9,
              TH: 10,
              ID: 12
            }
            if (countryTypeList[result.country]) {
              dispatch({
                type: 'r_updateState',
                payload: {
                  ipCountryCode: currencyTypeList[result.country],
                  ipCountryCodeId: countryTypeList[result.country],
                  currencyStatusCheck: true
                }
              })
            } else {
              dispatch({
                type: 'r_updateState',
                payload: {
                  currencyStatusCheck: true
                }
              })
            }


          } else {
            dispatch({
              type: 'r_updateState',
              payload: {
                currencyStatusCheck: true
              }
            })
          }


        })
      });

      //giving default language (zh-CN)
      const language = getUserLanguage()
      if (!language) {
        systemSetLocale(STORAGE_KEY.language, DEFAULT_SETTING.language)
      } else {
        dispatch({
          type: 'r_updateState',
          payload: {
            language
          }
        })
      }

      //check user token
      const user_token = getUserToken();
      if (user_token) {
        dispatch({
          type: 'e_checkLogin',
          payload: {
            user_token
          }
        }).then((result) => {
          /*  if (result) {
               history.listen(({ pathname }) => {
                   if (pathname == '/login' || pathname == '/') {
                       //router.push('/home')
                   }
               })
           } */
        })
      } else {
        dispatch({
          type: 'e_guessRegister',
          payload: {}
        })
      }
    },
  },
};
