import { request } from 'services';
import { API_GET_DESCRIPTION_TYPE, API_GET_DESCRIPTION_LIST, API_GET_DESCRIPTION_DATA } from '@/constants/api'

export default {
    namespace: 'description',
    state: {},
    reducers: {
        r_updateState(state, { payload }) {
            return { ...state, ...payload }
        }
    },
    effects: {
        * e_getDescriptionType({ payload }, { put, call }) {
            const { success, data } = yield call(request, payload, {
                method: 'post',
                constant: API_GET_DESCRIPTION_TYPE
            })
            return success ? data : false
        },
        * e_getDescriptionList({ payload }, { put, call }) {
            const { success, data } = yield call(request, payload, {
                method: 'post',
                constant: API_GET_DESCRIPTION_LIST
            })
            return success ? data : false
        },
        * e_getDescriptionData({ payload }, { put, call }) {
            const { success, data } = yield call(request, payload, {
                method: 'post',
                constant: API_GET_DESCRIPTION_DATA
            })
            return success ? data : false
        },

    },
}
