import { request } from 'services';
import { API_GET_SCHOOL } from '@/constants/api'

export default {
    namespace: 'school',
    state: {},
    reducers: {
        r_updateState(state, { payload }) {
            return { ...state, ...payload }
        }
    },
    effects: {
        * e_getSchool({ payload }, { put, call }) {
            const { success, data } = yield call(request, payload, {
                method: 'post',
                constant: API_GET_SCHOOL
            })
            return success ? data : false
        }
    },
}
