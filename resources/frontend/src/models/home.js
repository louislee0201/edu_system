import { request } from 'services';
import { API_GET_HOME_PAGE_DATA, API_SUBMID_APPLY_FORM } from '@/constants/api'

export default {
    namespace: 'home',
    state: {},
    effects: {
        * e_getHomePageData({ payload }, { put, call }) {
            const { success, data } = yield call(request, payload, {
                method: 'post',
                constant: API_GET_HOME_PAGE_DATA
            })
            return success ? data : false
        },
        * e_submitApplyForm({ payload }, { put, call }) {
            const { success, data } = yield call(request, payload, {
                method: 'post',
                constant: API_SUBMID_APPLY_FORM
            })
            return success ? data : false
        },
    },
    reducers: {
        r_updateState(state, { payload }) {
            return { ...state, ...payload }
        }
    },
    subscriptions: {
        // setup({ dispatch, history }) {
        //     return history.listen(({ pathname, search }) => {
        //         if (pathname == '/home' || pathname == '/') {
        //             dispatch({
        //                 type: 'reg',
        //             });
        //         }
        //     });
        // },
    },
};
