import { API_GET_COURSE, API_GET_SELECTION, API_UPDATE_STUDENT_ACADEMIC_RESULT } from '@/constants/api'
import { request } from 'services';

export default {
    namespace: 'course',
    state: {
        searchCourseFilter: {
            course_level_id: 0,
            study_field_id: 0,
            major_id: 0,
            country_id: 3,
            region_id: 0,
            qualificationActiveId: 0,
            qualification_id: 0,
            secondary_complete: 1,

            dataList: [],
            totalItem: 0,
            pageSize: 0,
            currentPage: 1,
            firstLoaded: false,

            countryList: [],
            courseLevelList: [],
            currencyTypeList: {},
            majorList: [],
            prificiencyList: [],
            qualificationGradeList: [],
            qualificationList: [],
            qualificationSubjectList: [],
            regionList: [],
            schoolTypeList: {},
            studentAcademicResult: {},
            studyFieldList: [],
            ipCountryCode: 'USD',
            ipCountryCodeId: 1, // for USD
            position: 0,

            //validCountry: {},
            validLevel: {},
            validStudyField: {},
            validMajor: {},
            selectionData: [],
            validOriKey: 'country',

            remainderTotal: 0
        }
    },
    reducers: {
        r_updateState(state, { payload }) {
            return { ...state, ...payload }
        }
    },
    effects: {
        * e_getSelection({ payload }, { put, call }) {
            const { success, data } = yield call(request, payload, {
                method: 'post',
                constant: API_GET_SELECTION
            })

            // if (success) {
            //     if(data.countryList.length == 1)
            //     {
            //         data.country_id = data.countryList[0].id
            //     }
            // }
            return success ? data : false
        },
        * e_getCourse({ payload }, { put, call }) {
            const { success, data } = yield call(request, payload, {
                method: 'post',
                constant: API_GET_COURSE
            })
            return success ? data : false
        },
        * e_updateStudentAcademicResult({ payload }, { put, call }) {
            const { success, data } = yield call(request, payload, {
                method: 'post',
                constant: API_UPDATE_STUDENT_ACADEMIC_RESULT
            })
            return success ? data : false
        },
    },
}
