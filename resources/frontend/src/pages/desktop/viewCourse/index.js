import React, { useEffect, useState } from "react";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import { Toast, Menu, Badge } from 'antd-mobile';
import { Descriptions, Collapse, Divider, Card } from 'antd';
import { EnvironmentOutlined, StarTwoTone } from '@ant-design/icons';
import { HOST } from '@/constants/api'
import router from 'umi/router';
import Loading from '@/components/loading'
import DesktopLoading from '@/components/desktopLoading'
import './index.less'
import { PRIMARY_COLOR } from '@/constants/default'
import DescriptionTable from '@/components/desktop-description-table'
import DesktopApplyForm from '@/components/desktop-apply-form'
import { Helmet } from "react-helmet";
import { baseUrl } from "../../../constants/api";

export default (props) => {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        loading: true,
        interestList: [],
        applyFormVisible: false,
        course_name: "",
        related_id: 0,
    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    useEffect(() => {
        const related_id = props.location.query.course_id;
        if (related_id) {
            setState({ loading: true })
            dispatch({
                type: "course/e_getCourse",
                payload: {
                    isList: false,
                    course_id: related_id
                }
            }).then(result => {
                if (result) {
                    setState({ ...result, related_id, loading: false })
                }
            });
        }
    }, [props.location.query.course_id]);

    return <>
        <Helmet>
            <meta name="description" content={state.course_name} />
            <link rel="canonical" href={baseUrl + "viewCourse?course_id=" + state.related_id} />
        </Helmet>

        <DesktopLoading loading={state.loading}>
            <DesktopApplyForm info={{
                contact_me: state.contact_me,
                school_contact_me: state.school_contact_me,
                email: state.email,
                school_name: state.school_name
            }} visible={state.applyFormVisible} onClose={() => setState({ applyFormVisible: false })} />
            <div className='desktop-content'>
                <div className='desktop-viewCourse2'>
                    <div
                        className='desktop-viewCourse'
                    >
                        <div className='desktop-viewCourse-school_info'>
                            <div className='desktop-viewCourse-school_info-logo'>
                                <img
                                    src={/* HOST */    /* 'http://api.edudotcom.my/' + 'auth/image/school/'  +*/ state.logo}
                                    width="100%"
                                    height='100%'
                                //style={{ borderRadius: '20px' }}
                                />
                            </div>
                            <div className='desktop-viewCourse-school_info-text'>
                                <div className='desktop-viewCourse-school_info-text-apply' onClick={() => setState({ applyFormVisible: true })}>
                                    {formatMessage({ id: 'enquiry - apply' })}
                                </div>
                                <div className='desktop-viewCourse-school_info-text-course_name'>
                                    <div
                                        className='desktop-viewCourse-school_info-text-course_name-text'
                                    //onClick={() => router.push('/viewSchool?school_id=' + state.school_id)}
                                    >
                                        {state.course_name}
                                    </div>
                                    <div className='desktop-viewCourse-school_info-text-course_name-bookmark'>
                                        <StarTwoTone
                                            onClick={(e) => {
                                                e.stopPropagation()
                                            }}
                                            twoToneColor={'grey'}//#ffe00
                                            style={{ fontSize: '28px' }}
                                            className='desktop-viewCourse-school_info-text-course_name-bookmark-icon'
                                        />
                                    </div>
                                </div>

                                <div
                                    className='desktop-viewCourse-school_info-text-school_name active'
                                    onClick={() => router.push('/viewSchool?school_id=' + state.school_id)}
                                >
                                    <div
                                        className='desktop-viewCourse-school_info-text-school_name-icon'
                                    >
                                        <img src={require('@/assets/school_icon.jpeg')} style={{ width: '30px', height: '100%' }} />
                                    </div>
                                    <div className='desktop-viewCourse-school_info-text-school_name-text'>
                                        {state.school_name}
                                    </div>
                                </div>

                                <div className='desktop-viewCourse-school_info-text-school_name'>
                                    <div
                                        className='desktop-viewCourse-school_info-text-school_name-icon'
                                    >
                                        <img src={require('@/assets/location_icon.jpeg')} style={{ width: '30px', height: '100%' }} />
                                    </div>
                                    <div className='desktop-viewCourse-school_info-text-school_name-text'>
                                        {state.region != state.country && state.region + ', '}{state.country}
                                    </div>
                                </div>

                                <div className='desktop-viewCourse-school_info-text-school_name'>
                                    <div
                                        className='desktop-viewCourse-school_info-text-school_name-icon'
                                    >
                                        <img src={require('@/assets/duration_icon.jpeg')} style={{ width: '30px', height: '100%' }} />
                                    </div>
                                    <div className='desktop-viewCourse-school_info-text-school_name-text'>
                                        {
                                            state.duration > 0
                                                ? parseInt(state.duration / 12) > 0
                                                    ? parseInt(state.duration / 12).toFixed() + (parseInt(state.duration / 12).toFixed() > 1 ? formatMessage({ id: 'years' }) : formatMessage({ id: 'year' })) + ' '
                                                    : ''
                                                : formatMessage({ id: 'unknow' })
                                        }
                                        {
                                            state.duration > 0 &&
                                                state.duration % 12 > 0
                                                ? state.duration % 12 + (state.duration % 12 > 1 ? formatMessage({ id: 'month\'s' }) : formatMessage({ id: 'month' }))
                                                : ''
                                        }
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div className='desktop-viewCourse-course_overView'>
                            <div className='desktop-viewCourse-course_overView'>
                                <div className='desktop-viewCourse-course_overView-title'>{formatMessage({ id: 'course overview' })}</div>
                                <Divider /* orientation="left" */>{/* {formatMessage({ id: 'course overview' })} */}</Divider>
                                <DescriptionTable
                                    classMode='course'
                                    related_id={state.related_id}
                                    setState={setState}
                                />
                            </div>
                        </div>
                    </div>

                    <div className='desktop-viewCourse-interest'>
                        <div className='desktop-viewCourse-interest-title'>
                            {formatMessage({ id: 'Other courses you may be interested' })} :
                        </div>

                        <div className='desktop-viewCourse-interest-course_list'>
                            {
                                Object.values(state.interestList).map((course, key) => {
                                    return <div className='desktop-viewCourse-interest-course_list active'>
                                        <Card
                                            onClick={() => router.push('/viewCourse?course_id=' + course.id)}
                                            style={{
                                                width: '100%',
                                                display: 'flex',
                                                boxShadow: ' 0px 0px 5px 0px rgba(0, 0, 0, 0.2)',
                                                //marginTop: '10px',
                                                position: 'relative'
                                            }}
                                            cover={
                                                <img
                                                    alt="logo"
                                                    style={{ height: '0.6rem', width: '0.6rem' }}
                                                    src={HOST /* 'http://api.edudotcom.my/'  */ + 'auth/image/school/' + course.logo}
                                                />
                                            }
                                        >
                                            <div className='desktop-viewCourse-interest-course_list-content'>
                                                <div className='desktop-viewCourse-interest-course_list-content-title' style={{ WebkitBoxOrient: 'vertical' }}>
                                                    <Badge text="Private" /> {course.course_name}
                                                </div>


                                                <div className='desktop-viewCourse-interest-course_list-content-details'>

                                                </div>
                                                <div className='desktop-viewCourse-interest-course_list-content-location'>
                                                    <div
                                                        type="link"
                                                        className='desktop-viewCourse-interest-course_list-content-location-school'
                                                    >
                                                        {course.school_name}
                                                    </div>
                                                    <div className='desktop-viewCourse-interest-course_list-content-location-studyFields' style={{ marginBottom: '2px' }}>
                                                        <Badge text="Graphic Design" style={{ backgroundColor: '#ff5b059c' }} />
                                                        <Badge text="ETC" style={{ marginLeft: 5, backgroundColor: '#ff5b059c' }} />
                                                    </div>

                                                    <div style={{ display: 'flex', justifyContent: '', alignItems: 'center', fontSize: '0.04rem', padding: state.currentScreenWidth >= 500 && '5px 0' }}>
                                                        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', fontSize: '0.04rem' }}>
                                                            <img src={require('@/assets/location_icon.jpeg')} style={{ width: '0.08rem', height: '0.08rem' }} />

                                                            <div style={{ marginLeft: '2px' }}>
                                                                Japan{
                                                                    state.currentScreenWidth >= 500
                                                                        ? <span>, Tokyo</span>
                                                                        : <div>Tokyo</div>
                                                                }
                                                            </div>

                                                        </div>
                                                        {
                                                            screen.height > 568 && screen.width > 320 &&
                                                            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', fontSize: '10px', marginLeft: '40px' }}>
                                                                <img src={require('@/assets/duration_icon.jpeg')} style={{ width: '0.08rem', height: '0.08rem' }} />
                                                                <div style={{ marginLeft: '3px' }}>
                                                                    3 Years{
                                                                        state.currentScreenWidth >= 500
                                                                            ? <span> Duration</span>
                                                                            : <div>Duration</div>
                                                                    }
                                                                </div>
                                                            </div>
                                                        }

                                                    </div>
                                                    <div style={{ borderTop: '1px solid red' }}></div>
                                                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                                        <p style={{ fontSize: '0.04rem', fontStyle: 'oblique', fontWeight: 'bold' }}>Estimated Fees</p>
                                                        <div style={{ fontWeight: 'bold', display: 'flex', justifyContent: 'center', alignItems: 'center', fontSize: '12px', color: 'red' }}>
                                                            <p style={{ fontSize: '0.04rem' }}>RM</p>
                                                            <div>94500</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Card>
                                    </div>
                                })
                            }
                        </div>
                    </div>
                </div>

            </div>

        </DesktopLoading>
    </>


}