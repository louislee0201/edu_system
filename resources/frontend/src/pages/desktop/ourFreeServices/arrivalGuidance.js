import React, { useEffect, useState } from "react";
import { formatMessage } from 'umi/locale';
import './arrivalGuidance.less'
import { Divider } from 'antd';
import { MessageOutlined, MailOutlined, PhoneOutlined, InstagramOutlined, FacebookOutlined } from '@ant-design/icons';
import GoogleMap from '@/components/googleMap'
import DesktopTitleMenu from '@/components/desktop-title-menu'

export default (props) => {
    const [state, setOriState] = useState({
        currentPage: ''
    })
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <div className='desktop-content'>
            <div className='desktop-ourFreeServices-img'>
                <img src={require('@/assets/title_img/4.jpg')} />
            </div>

            <div className='desktop-ourFreeServices-content'>
                <div className='desktop-ourFreeServices-content-left'>
                    <div className='desktop-ourFreeServices-arrivalGuidance-maintitle'>
                        {formatMessage({ id: 'Arrival Guidance' })}
                    </div>
                    <Divider className='desktop-ourFreeServices-arrivalGuidance-divider' />

                    <div className='desktop-ourFreeServices-arrivalGuidance-content'>
                        This service is limited to students who apply the courses through EDU DOTCOM with free of charges. In the early stage of studying abroad, there are usually many things that need to be arranged and causing a lot of unnecessary trouble for student. We will provide suggestions and making arrangement for students if needed such aslocal airport pick-up transportation, foreign bank application, mobile phone number application, foreign Identidy Card application and other relevant issues.
                    </div>
                </div>
                <div className='desktop-ourFreeServices-content-right'>
                    <DesktopTitleMenu
                        title={formatMessage({ id: 'Our Free Services' })}
                        menu={[
                            {
                                label: formatMessage({ id: 'Counselor Services' }),
                                url: '/ourFreeServices/counselorServices'
                            },
                            {
                                label: formatMessage({ id: 'Application Services' }),
                                url: '/ourFreeServices/applicationServices'
                            },
                            {
                                label: formatMessage({ id: 'Visa Guidance' }),
                                url: '/ourFreeServices/visaGuidance'
                            },
                            {
                                label: formatMessage({ id: 'Accommadation' }),
                                url: '/ourFreeServices/accommadation'
                            },
                            {
                                label: formatMessage({ id: 'Arrival Guidance' }),
                                url: '/ourFreeServices/arrivalGuidance'
                            },
                            {
                                label: formatMessage({ id: 'Student Guardian' }),
                                url: '/ourFreeServices/studentGuardian'
                            }
                        ]}
                    />
                </div>
            </div>


        </div>

    )

}
