import React, { useEffect, useState } from "react";
import { formatMessage } from 'umi/locale';
import './visaGuidance.less'
import { Divider } from 'antd';
import { MessageOutlined, MailOutlined, PhoneOutlined, InstagramOutlined, FacebookOutlined } from '@ant-design/icons';
import GoogleMap from '@/components/googleMap'
import DesktopTitleMenu from '@/components/desktop-title-menu'

export default (props) => {
    const [state, setOriState] = useState({
        currentPage: ''
    })
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <div className='desktop-content'>
            <div className='desktop-ourFreeServices-img'>
                <img src={require('@/assets/title_img/4.jpg')} />
            </div>

            <div className='desktop-ourFreeServices-content'>
                <div className='desktop-ourFreeServices-content-left'>
                    <div className='desktop-ourFreeServices-visaGuidancec-maintitle'>
                        {formatMessage({ id: 'Visa Guidance' })}
                    </div>
                    <Divider className='desktop-ourFreeServices-visaGuidancec-divider' />

                    <div className='desktop-ourFreeServices-visaGuidancec-content'>
                        This service is limited to students who apply the courses through EDU DOTCOM with free of charges. Our counselor will guide students step-by-step on the preparations and explain on procedures which they need to go through. Students will be able to complete the visa process by themselves. For student living in rural area, we will assist in looking for runner (fee-based) to assist.
                    </div>
                </div>
                <div className='desktop-ourFreeServices-content-right'>
                    <DesktopTitleMenu
                        title={formatMessage({ id: 'Our Free Services' })}
                        menu={[
                            {
                                label: formatMessage({ id: 'Counselor Services' }),
                                url: '/ourFreeServices/counselorServices'
                            },
                            {
                                label: formatMessage({ id: 'Application Services' }),
                                url: '/ourFreeServices/applicationServices'
                            },
                            {
                                label: formatMessage({ id: 'Visa Guidance' }),
                                url: '/ourFreeServices/visaGuidance'
                            },
                            {
                                label: formatMessage({ id: 'Accommadation' }),
                                url: '/ourFreeServices/accommadation'
                            },
                            {
                                label: formatMessage({ id: 'Arrival Guidance' }),
                                url: '/ourFreeServices/ArrivalGuidance'
                            },
                            {
                                label: formatMessage({ id: 'Student Guardian' }),
                                url: '/ourFreeServices/studentGuardian'
                            }
                        ]}
                    />
                </div>
            </div>


        </div>

    )

}
