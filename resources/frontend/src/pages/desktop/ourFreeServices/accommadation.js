import React, { useEffect, useState } from "react";
import { formatMessage } from 'umi/locale';
import './accommadation.less'
import { Divider } from 'antd';
import { MessageOutlined, MailOutlined, PhoneOutlined, InstagramOutlined, FacebookOutlined } from '@ant-design/icons';
import GoogleMap from '@/components/googleMap'
import DesktopTitleMenu from '@/components/desktop-title-menu'

export default (props) => {
    const [state, setOriState] = useState({
        currentPage: ''
    })
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <div className='desktop-content'>
            <div className='desktop-ourFreeServices-img'>
                <img src={require('@/assets/title_img/4.jpg')} />
            </div>

            <div className='desktop-ourFreeServices-content'>
                <div className='desktop-ourFreeServices-content-left'>
                    <div className='desktop-ourFreeServices-accommadation-maintitle'>
                        {formatMessage({ id: 'Accommadation' })}
                    </div>
                    <Divider className='desktop-ourFreeServices-accommadation-divider' />

                    <div className='desktop-ourFreeServices-accommadation-content'>
                        This service is limited to students who apply the courses through EDU DOTCOM with free of charges. We will assist students to apply for dormitory under university or their coordinate accommodation provider. In certain case, we will also searching for external accommodation options so that students can gives a compare to get a better living place which meets with their expectations.
                        
                        <div style={{ marginTop: '5px' }}>
                            **External dormitory options in Japan will be open to the public**
                        </div>
                    </div>
                </div>
                <div className='desktop-ourFreeServices-content-right'>
                    <DesktopTitleMenu
                        title={formatMessage({ id: 'Our Free Services' })}
                        menu={[
                            {
                                label: formatMessage({ id: 'Counselor Services' }),
                                url: '/ourFreeServices/counselorServices'
                            },
                            {
                                label: formatMessage({ id: 'Application Services' }),
                                url: '/ourFreeServices/applicationServices'
                            },
                            {
                                label: formatMessage({ id: 'Visa Guidance' }),
                                url: '/ourFreeServices/visaGuidance'
                            },
                            {
                                label: formatMessage({ id: 'Accommadation' }),
                                url: '/ourFreeServices/accommadation'
                            },
                            {
                                label: formatMessage({ id: 'Arrival Guidance' }),
                                url: '/ourFreeServices/arrivalGuidance'
                            },
                            {
                                label: formatMessage({ id: 'Student Guardian' }),
                                url: '/ourFreeServices/studentGuardian'
                            }
                        ]}
                    />
                </div>
            </div>


        </div>

    )

}
