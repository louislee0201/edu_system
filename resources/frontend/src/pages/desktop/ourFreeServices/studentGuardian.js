import React, { useEffect, useState } from "react";
import { formatMessage } from 'umi/locale';
import './studentGuardian.less'
import { Divider } from 'antd';
import { MessageOutlined, MailOutlined, PhoneOutlined, InstagramOutlined, FacebookOutlined } from '@ant-design/icons';
import GoogleMap from '@/components/googleMap'
import DesktopTitleMenu from '@/components/desktop-title-menu'

export default (props) => {
    const [state, setOriState] = useState({
        currentPage: ''
    })
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <div className='desktop-content'>
            <div className='desktop-ourFreeServices-img'>
                <img src={require('@/assets/title_img/4.jpg')} />
            </div>

            <div className='desktop-ourFreeServices-content'>
                <div className='desktop-ourFreeServices-content-left'>
                    <div className='desktop-ourFreeServices-studentGuardian-maintitle'>
                        {formatMessage({ id: 'Student Guardian' })}
                    </div>
                    <Divider className='desktop-ourFreeServices-studentGuardian-divider' />

                    <div className='desktop-ourFreeServices-studentGuardian-content'>
                        This service is limited to students who apply the courses through EDU DOTCOM with free of charges. We understand parents are always worried with their children when study abroad. Our counselor will become guardian to the student by sharing the burden with parents. For example, when students have an accident while studying abroad and family members unable to contact with the school due to the language barriers, our center will play a great role in this part through serving as a communication bridge between the school, students and parents.
                    </div>
                </div>
                <div className='desktop-ourFreeServices-content-right'>
                    <DesktopTitleMenu
                        title={formatMessage({ id: 'Our Free Services' })}
                        menu={[
                            {
                                label: formatMessage({ id: 'Counselor Services' }),
                                url: '/ourFreeServices/counselorServices'
                            },
                            {
                                label: formatMessage({ id: 'Application Services' }),
                                url: '/ourFreeServices/applicationServices'
                            },
                            {
                                label: formatMessage({ id: 'Visa Guidance' }),
                                url: '/ourFreeServices/visaGuidance'
                            },
                            {
                                label: formatMessage({ id: 'Accommadation' }),
                                url: '/ourFreeServices/accommadation'
                            },
                            {
                                label: formatMessage({ id: 'Arrival Guidance' }),
                                url: '/ourFreeServices/arrivalGuidance'
                            },
                            {
                                label: formatMessage({ id: 'Student Guardian' }),
                                url: '/ourFreeServices/studentGuardian'
                            }
                        ]}
                    />
                </div>
            </div>


        </div>

    )

}
