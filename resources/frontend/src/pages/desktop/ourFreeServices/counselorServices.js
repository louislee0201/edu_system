import React, { useEffect, useState } from "react";
import { formatMessage } from 'umi/locale';
import './counselorServices.less'
import { Divider } from 'antd';
import { MessageOutlined, MailOutlined, PhoneOutlined, InstagramOutlined, FacebookOutlined } from '@ant-design/icons';
import GoogleMap from '@/components/googleMap'
import DesktopTitleMenu from '@/components/desktop-title-menu'

export default (props) => {
    const [state, setOriState] = useState({
        currentPage: ''
    })
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <div className='desktop-content'>
            <div className='desktop-ourFreeServices-img'>
                <img src={require('@/assets/title_img/4.jpg')} />
            </div>

            <div className='desktop-ourFreeServices-content'>
                <div className='desktop-ourFreeServices-content-left'>
                    <div className='desktop-ourFreeServices-counselorServices-maintitle'>
                        {formatMessage({ id: 'Counselor Services' })}
                    </div>
                    <Divider className='desktop-ourFreeServices-counselorServices-divider' />

                    <div className='desktop-ourFreeServices-counselorServices-content'>
                        This service is open to public with free of charges. We have many counselors with great experience in relevant fields who regularly updating on university and colleges to ensure latest information will be provide to student. In order to provide correct suggestions, counselor will give their best to understand student’s background, interested fields and individual conditions. Also, we will also explain all necessary info regarding on living, student works, regulations, etc about foreign study country.
                    </div>
                </div>
                <div className='desktop-ourFreeServices-content-right'>
                    <DesktopTitleMenu
                        title={formatMessage({ id: 'Our Free Services' })}
                        menu={[
                            {
                                label: formatMessage({ id: 'Counselor Services' }),
                                url: '/ourFreeServices/counselorServices'
                            },
                            {
                                label: formatMessage({ id: 'Application Services' }),
                                url: '/ourFreeServices/applicationServices'
                            },
                            {
                                label: formatMessage({ id: 'Visa Guidance' }),
                                url: '/ourFreeServices/visaGuidance'
                            },
                            {
                                label: formatMessage({ id: 'Accommadation' }),
                                url: '/ourFreeServices/accommadation'
                            },
                            {
                                label: formatMessage({ id: 'Arrival Guidance' }),
                                url: '/ourFreeServices/ArrivalGuidance'
                            },
                            {
                                label: formatMessage({ id: 'Student Guardian' }),
                                url: '/ourFreeServices/studentGuardian'
                            }
                        ]}
                    />
                </div>
            </div>


        </div>

    )

}
