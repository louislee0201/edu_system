import React, { useEffect, useState } from "react";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import { Toast, Menu, Badge } from 'antd-mobile';
import { Descriptions, Collapse, Divider, Card } from 'antd';
import { CaretRightOutlined, StarTwoTone } from '@ant-design/icons';
import { HOST } from '@/constants/api'
import router from 'umi/router';
import Loading from '@/components/loading'
import DesktopLoading from '@/components/desktopLoading'
import './index.less'
import { PRIMARY_COLOR } from '@/constants/default'
import DescriptionTable from '@/components/desktop-description-table'
import { Helmet } from "react-helmet";
import { baseUrl } from "../../../constants/api";

export default (props) => {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        loading: true,
        interestList: [],
        picture: '',
        schoolCourseArrangementList: [],
        schoolArrangementList: [],
        related_id: 0,
        school_name: "",
        school_name_cn: "",
    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    useEffect(() => {
        const related_id = props.location.query.school_id;
        if (related_id) {
            setState({ loading: true })
            dispatch({
                type: "school/e_getSchool",
                payload: {
                    isList: false,
                    school_id: related_id
                }
            }).then(result => {
                if (result) {
                    let country
                    result.countryList.forEach(c => {
                        if (c.id == result.country) {
                            country = c.country
                        }
                    });
                    setState({ ...result, related_id, loading: false, country })
                }
            });
        }
    }, [props.location.query.school_id]);

    function renderCollapse() {
        let data = []
        Object.values(state.schoolArrangementList).map((r) => {
            data.push(
                <Collapse.Panel
                    className="site-collapse-custom-panel"
                    header={
                        <span style={{ fontWeight: 'bold' }}>{r.title}</span>
                    }
                >
                    <div className='viewSchool-course-list'>
                        {
                            Object.values(state.schoolCourseArrangementList[r.id]).map((course, key) => {
                                return (
                                    <div
                                        style={{ cursor: 'pointer' }}
                                        className='viewSchool-course-list-child'
                                        onClick={() => router.push('/viewCourse?course_id=' + course.id)}
                                    >
                                        {course.course_name}
                                    </div>
                                )
                            })
                        }
                    </div>

                </Collapse.Panel>
            )
        })
        return <Collapse
            bordered={false}
            expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
            className="site-collapse-custom-collapse"
        >
            {data}
        </Collapse>
    }

    return <>
        <Helmet>
            <meta name="description" content={state.school_name + " " + state.school_name_cn} />
            <link rel="canonical" href={baseUrl + "viewSchool?school_id=" + state.related_id} />
        </Helmet>
        <DesktopLoading loading={state.loading}>
            <div className='desktop-content'>
                <div className='desktop-viewSchool2'>
                    <div
                        className='desktop-viewSchool'
                    >
                        <div className='desktop-viewSchool-school_info'>
                            <div className='desktop-viewSchool-school_info-logo'>
                                <img
                                    src={state.picture}
                                    width="100%"
                                    height='100%'
                                //style={{ borderRadius: '20px' }}
                                />
                            </div>
                            <div className='desktop-viewSchool-school_info-text'>

                                <div className='desktop-viewSchool-school_info-text-course_name'>
                                    <div
                                        className='desktop-viewSchool-school_info-text-course_name-text'
                                    //onClick={() => router.push('/viewSchool?school_id=' + state.school_id)}
                                    >
                                        {state.school_name}{/* {state.school_name_cn} */}
                                    </div>
                                    <div className='desktop-viewSchool-school_info-text-course_name-bookmark'>
                                        <StarTwoTone
                                            onClick={(e) => {
                                                e.stopPropagation()
                                            }}
                                            twoToneColor={'grey'}//#ffe00
                                            style={{ fontSize: '28px' }}
                                            className='desktop-viewSchool-school_info-text-course_name-bookmark-icon'
                                        />
                                    </div>
                                </div>

                                {/* <div className='desktop-viewSchool-school_info-text-school_name active'>
                            <div
                                className='desktop-viewSchool-school_info-text-school_name-icon'
                            >
                                <img src={require('@/assets/school_icon.jpeg')} style={{ width: '0.12rem', height: '0.12rem' }} />
                            </div>
                            <div className='desktop-viewSchool-school_info-text-school_name-text'>
                                {state.school_name}
                            </div>
                        </div> */}

                                <div className='desktop-viewSchool-school_info-text-school_name'>
                                    <div
                                        className='desktop-viewSchool-school_info-text-school_name-icon'
                                    >
                                        <img src={require('@/assets/location_icon.jpeg')} style={{ width: '0.12rem', height: '0.12rem' }} />
                                    </div>
                                    <div className='desktop-viewSchool-school_info-text-school_name-text'>
                                        {state.region != state.country && state.region + ', '}{state.country}
                                        <div className='desktop-viewSchool-school_info-text-school_name-text-address'>
                                            {state.address}
                                        </div>
                                    </div>
                                </div>

                                <div className='desktop-viewSchool-school_info-text-school_name'>
                                    <div
                                        className='desktop-viewSchool-school_info-text-school_name-icon'
                                    >
                                        <img src={require('@/assets/contact.png')} style={{ width: '0.12rem', height: '0.12rem' }} />
                                    </div>
                                    <div className='desktop-viewSchool-school_info-text-school_name-text'>
                                        {/* {
                                        state.contact_me
                                            ? <div>{state.school_tel} / {state.admin_tel + ' ' + formatMessage({ id: '(For Malaysian)' })}</div>
                                            : state.school_tel
                                    } */}
                                        {
                                            state.school_tel
                                        }
                                    </div>
                                </div>

                                <div className='desktop-viewSchool-school_info-text-school_name'>
                                    <div
                                        className='desktop-viewSchool-school_info-text-school_name-icon'
                                    >
                                        <img src={require('@/assets/email.png')} style={{ width: '0.12rem', height: '0.12rem' }} />
                                    </div>
                                    <div className='desktop-viewSchool-school_info-text-school_name-text'>
                                        {/* {
                                        state.contact_me
                                            ? <div>{state.email} <div>{state.admin_email + ' ' + formatMessage({ id: '(For Malaysian)' })}</div> </div>
                                            : state.email
                                    } */}
                                        {
                                            state.email
                                        }
                                    </div>
                                </div>

                                <div
                                    className='desktop-viewSchool-school_info-text-school_name'
                                >
                                    <div
                                        className='desktop-viewSchool-school_info-text-school_name-icon'
                                    >
                                        <img src={require('@/assets/officer.png')} style={{ width: '0.12rem', height: '0.12rem' }} />
                                    </div>
                                    <div
                                        className='desktop-viewSchool-school_info-text-school_name-text active'
                                        onClick={() => window.open(state.official_website, '_blank')}
                                    >
                                        {
                                            state.official_website
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='desktop-viewSchool-course_overView'>
                            <div className='desktop-viewSchool-course_overView'>
                                <div className='desktop-viewSchool-course_overView-title'>{formatMessage({ id: 'school overview' })}</div>
                                <Divider /* orientation="left" */>{/* {formatMessage({ id: 'course overview' })} */}</Divider>
                                <DescriptionTable
                                    classMode='school'
                                    related_id={state.related_id}
                                    setState={setState}
                                />
                            </div>
                        </div>

                        {
                            <div className='desktop-viewSchool-all_courses'>
                                <div className='desktop-viewSchool-all_courses'>
                                    <div className='desktop-viewSchool-all_courses-title'>{formatMessage({ id: 'course listing' })}</div>
                                    <Divider /* orientation="left" */>{/* {formatMessage({ id: 'course overview' })} */}</Divider>
                                    <div className='viewSchool-course'>
                                        <Collapse
                                            bordered={false}
                                            expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
                                            className="site-collapse-custom-collapse"
                                        >
                                            {
                                                state.schoolArrangementList.length > 0 && Object.keys(state.schoolCourseArrangementList).length > 0
                                                    ? renderCollapse()
                                                    : <Collapse.Panel
                                                        className="site-collapse-custom-panel"
                                                        header={
                                                            <span style={{ fontWeight: 'bold' }}>{formatMessage({ id: 'all courses' })}</span>
                                                        }
                                                    >

                                                        <div className='viewSchool-course-list'>
                                                            {
                                                                Object.keys(state.schoolCourseArrangementList).length > 0 &&
                                                                Object.values(state.schoolCourseArrangementList[0]).map((course, key) => {
                                                                    return (
                                                                        <div
                                                                            style={{ cursor: 'pointer' }}
                                                                            className='viewSchool-course-list-child'
                                                                            onClick={() => router.push('/viewCourse?course_id=' + course.id)}
                                                                        >
                                                                            {course.course_name}
                                                                        </div>
                                                                    )
                                                                })
                                                            }
                                                        </div>
                                                    </Collapse.Panel>

                                            }

                                        </Collapse>

                                    </div>
                                </div>
                            </div>
                        }


                    </div>

                    <div className='desktop-viewSchool-interest'>
                        <div className='desktop-viewSchool-interest-title'>
                            {formatMessage({ id: 'Other schools you may be interested' })} :
                        </div>

                        <div className='desktop-viewSchool-interest-course_list'>
                            {
                                Object.values(state.interestList).map((school, key) => {
                                    return <div
                                        className='desktop-viewSchool-interest-course_list-img'
                                        onClick={() => {
                                            router.push('/viewSchool?school_id=' + school.id)
                                        }}
                                    >
                                        <img
                                            alt="logo"
                                            style={{ height: '100%', width: '100%' }}
                                            src={HOST  /* 'http://api.edudotcom.my/' */ + 'auth/image/school/' + school.picture}
                                        />
                                    </div>
                                })
                            }
                        </div>
                    </div>
                </div>

            </div>

        </DesktopLoading >
    </>


}