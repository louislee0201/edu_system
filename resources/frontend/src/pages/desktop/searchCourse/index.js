import React, { useEffect, useState, useRef } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './index.less'
import { DownOutlined, SettingOutlined, CloseOutlined, DeleteOutlined, AppstoreAddOutlined } from '@ant-design/icons';
import { Badge } from 'antd-mobile';
import { Modal, Checkbox, Tag, Radio, Select, Tooltip, Spin, Popover, Popconfirm } from 'antd';
import { formatMessage } from 'umi/locale';
import { PRIMARY_COLOR } from '@/constants/default'
import router from 'umi/router';
import DesktopLoading from '@/components/desktopLoading'
import { HOST } from '@/constants/api'
import { Helmet } from "react-helmet";
import { baseUrl } from "../../../constants/api";

export default function (props) {
    const dispatch = useDispatch()
    const configState = useSelector(state => state.config)
    const courseState = useSelector(state => state.course)
    const courseListHeightDom = useRef(null)
    const [state, setOriState] = useState({
        pageSize: courseState.searchCourseFilter.pageSize,
        totalItem: courseState.searchCourseFilter.totalItem,
        currentPage: courseState.searchCourseFilter.currentPage,
        dataList: courseState.searchCourseFilter.dataList,
        loading: false,
        dataLoading: false,

        course_level_id: courseState.searchCourseFilter.course_level_id,
        study_field_id: courseState.searchCourseFilter.study_field_id,
        major_id: courseState.searchCourseFilter.major_id,
        country_id: courseState.searchCourseFilter.country_id,
        region_id: courseState.searchCourseFilter.region_id,
        qualificationActiveId: courseState.searchCourseFilter.qualificationActiveId,
        qualification_id: courseState.searchCourseFilter.qualification_id,
        secondary_complete: courseState.searchCourseFilter.secondary_complete,

        //studentQualificationList: {},
        studentQualificationSubjectList: {},

        addQualificationModalVisible: false,
        addQualificationSubjectEditMode: false,
        academicResultUpdate: false,

        qualificationTitle: '',

        firstLoaded: false,
        qualificationList: courseState.searchCourseFilter.qualificationList,
        qualificationSubjectList: courseState.searchCourseFilter.qualificationSubjectList,
        courseLevelList: courseState.searchCourseFilter.courseLevelList,
        studyFieldList: courseState.searchCourseFilter.studyFieldList,
        majorList: courseState.searchCourseFilter.majorList,
        countryList: courseState.searchCourseFilter.countryList,
        regionList: courseState.searchCourseFilter.regionList,
        qualificationGradeList: courseState.searchCourseFilter.qualificationGradeList,
        studentAcademicResult: courseState.searchCourseFilter.studentAcademicResult,
        currencyTypeList: courseState.searchCourseFilter.currencyTypeList,
        schoolTypeList: courseState.searchCourseFilter.schoolTypeList,

        ipCountryCode: configState.ipCountryCode,
        ipCountryCodeId: configState.ipCountryCodeId,
        position: courseState.searchCourseFilter.position,
        remainderTotal: courseState.searchCourseFilter.remainderTotal,

        validOriKey: courseState.searchCourseFilter.validOriKey,
        //validCountry: courseState.searchCourseFilter.validCountry,
        validLevel: courseState.searchCourseFilter.validLevel,
        validStudyField: courseState.searchCourseFilter.validStudyField,
        validMajor: courseState.searchCourseFilter.validMajor,
        selectionData: courseState.searchCourseFilter.selectionData,
    })

    useEffect(() => {
        if (!state.loading) {
            animateValue(state.totalItem)
        }
    }, [state.loading]);

    useEffect(() => {
        setState({ country_id: courseState.searchCourseFilter.country_id })
    }, [courseState.searchCourseFilter.country_id]);

    function loopRadio(lists, jsonKey) {
        let data = []
        Object.values(lists).map((list, key) => {
            if (key == 0) {
                data.push({ label: formatMessage({ id: 'all' }), value: 0 })
            }
            if (jsonKey == 'region' && state.country_id) {

                if (list.country_id == state.country_id) {
                    data.push({ label: list[jsonKey], value: list.id })
                }
            } else if (jsonKey != 'region') {
                data.push({ label: list[jsonKey], value: list.id })
            }
        })
        return data
    }

    function animateValue(end) {
        let current = 0
        let duration = end / 15
        var timer = setInterval(function () {
            current += duration;
            setState({ totalItem: parseInt(current) })
            if (current >= end) {
                setState({ totalItem: state.totalItem })
                clearInterval(timer);
            }
        }, 25);
    }

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    useEffect(() => {
        setState({ firstLoaded: true })
        window.scrollTo(0, state.position);
        if (courseState.searchCourseFilter.dataList.length <= 0) {
            setState({ loading: true })
            dispatch({
                type: 'course/e_getSelection',
                payload: {}
            }).then((value) => {
                if (value) {
                    setState({
                        ...value
                    })
                    requestData(1, false, true)
                }
            })
        }
    }, [])

    function updateValidSelection() {
        let levelAllow = false
        let studyFieldAllow = false
        let majorAllow = true

        if (state.validOriKey == 'major') {
            majorAllow = false
        } else if (state.validOriKey == 'level') {
            studyFieldAllow = true
        } else if (state.validOriKey == 'country') {
            levelAllow = true
            studyFieldAllow = true
        }

        let d = {
            validLevel: !levelAllow ? state.validLevel : {},
            validStudyField: !studyFieldAllow ? state.validStudyField : {},
            validMajor: !majorAllow ? state.validMajor : {}
        }

        state.selectionData.map((c) => {
            if (c.sub_tag_list != null && c.main_tag_list != null) {
                let majorList = c.sub_tag_list.split(',')
                let studyFieldList = c.main_tag_list.split(',')
                let level = c.school_course_level_id

                if (levelAllow) {
                    if (d.validLevel[level]) {
                        d.validLevel[level]++
                    } else {
                        d.validLevel[level] = 1
                    }
                }

                if (studyFieldAllow) {
                    studyFieldList.map((s) => {
                        if (d.validStudyField[s]) {
                            d.validStudyField[s]++
                        } else {
                            d.validStudyField[s] = 1
                        }
                    })
                }

                if (majorAllow) {
                    majorList.map((s) => {
                        if (d.validMajor[s]) {
                            d.validMajor[s]++
                        } else {
                            d.validMajor[s] = 1
                        }
                    })
                }
            }
        })
        return d
    }

    useEffect(() => {
        let d = updateValidSelection()
        setState({ ...d, validOriKey: 'country' })
        dispatch({
            type: 'course/r_updateState',
            payload: {
                searchCourseFilter: {
                    ...courseState.searchCourseFilter,
                    dataList: state.dataList,
                    pageSize: state.pageSize,
                    totalItem: state.totalItem,
                    currentPage: state.currentPage,
                    countryList: state.countryList,
                    courseLevelList: state.courseLevelList,
                    currencyTypeList: state.currencyTypeList,
                    majorList: state.majorList,
                    prificiencyList: state.prificiencyList,
                    qualificationGradeList: state.qualificationGradeList,
                    qualificationList: state.qualificationList,
                    qualificationSubjectList: state.qualificationSubjectList,
                    regionList: state.regionList,
                    schoolTypeList: state.schoolTypeList,
                    studentAcademicResult: state.studentAcademicResult,
                    studyFieldList: state.studyFieldList,
                    remainderTotal: state.remainderTotal,
                    selectionData: state.selectionData,
                    validOriKey: state.validOriKey,
                    ...d
                }
            }
        })
    }, [state.dataList])

    useEffect(() => {
        let studentQualificationSubjectList = state.studentQualificationSubjectList
        Object.keys(state.studentAcademicResult).map((qualification_id) => {
            state.studentAcademicResult[qualification_id].forEach(subject => {
                if (studentQualificationSubjectList[qualification_id]) {
                    studentQualificationSubjectList[qualification_id][subject.qualification_subject_id] = subject.qualification_grade_id
                } else {
                    studentQualificationSubjectList[qualification_id] = {
                        [subject.qualification_subject_id]: subject.qualification_grade_id
                    }
                }
            })
        })
        setState({ studentQualificationSubjectList })
    }, [state.studentAcademicResult])

    useEffect(() => {
        /* setState({ firstLoaded: true })
        window.scrollTo(0, state.position);
        if (courseState.searchCourseFilter.dataList.length <= 0) {
            setState({ loading: true })
            dispatch({
                type: 'course/e_getSelection',
                payload: {}
            }).then((value) => {
                if (value) {
                    setState({
                        ...value
                    })
                    
                }
            })
        } */

        if (
            (
                state.region_id > 0 ||
                state.country_id > 0 ||
                state.major_id > 0 ||
                state.study_field_id > 0 ||
                state.course_level_id > 0 ||
                state.ipCountryCodeId > 0 ||
                state.qualificationActiveId > 0 ||
                state.academicResultUpdate
            ) &&
            state.firstLoaded
        ) {
            setState({ loading: true, academicResultUpdate: false })
            requestData(1, false, true)
            dispatch({
                type: 'course/r_updateState',
                payload: {
                    searchCourseFilter: {
                        ...courseState.searchCourseFilter,
                        region_id: state.region_id,
                        country_id: state.country_id,
                        major_id: state.major_id,
                        study_field_id: state.study_field_id,
                        course_level_id: state.course_level_id,
                        qualificationActiveId: state.qualificationActiveId,
                        ipCountryCodeId: configState.ipCountryCodeId,
                        ipCountryCode: configState.ipCountryCode,
                        studentAcademicResult: state.studentAcademicResult
                    }
                }
            })
        }

    }, [
        state.region_id,
        state.country_id,
        state.major_id,
        state.study_field_id,
        state.course_level_id,
        state.qualificationActiveId,
        state.ipCountryCodeId,
        state.academicResultUpdate
    ])

    window.onscroll = function () {
        if (courseListHeightDom.current) {
            dispatch({
                type: 'course/r_updateState',
                payload: {
                    searchCourseFilter: {
                        ...courseState.searchCourseFilter,
                        position: window.pageYOffset
                    }
                }
            })

            const courseListHeight = courseListHeightDom.current.offsetHeight - 618 - 1000
            if (window.pageYOffset >= courseListHeight && !state.dataLoading && state.remainderTotal > state.pageSize) {
                setState({ dataLoading: true })
                requestData(state.currentPage + 1, true)
            }
        }
    };

    function requestData(clickPage = state.currentPage, combine = false, updateTotal = false) {
        dispatch({
            type: 'course/e_getCourse',
            payload: {
                isList: true,
                clickPage,
                filterArr: {
                    course_level_id: state.course_level_id,
                    study_field_id: state.study_field_id,
                    major_id: state.major_id,
                    country_id: state.country_id,
                    region_id: state.region_id
                },
                qualification_id: state.qualificationActiveId,
                currency_code: state.ipCountryCodeId,
                secondary_complete: state.secondary_complete
            }
        }).then((value) => {
            if (combine) {
                let dataList = state.dataList
                dataList = dataList.concat(value.dataList);
                value.dataList = dataList
            } else {
                window.scrollTo(0, 0)
            }
            value.remainderTotal = value.totalItem
            if (!updateTotal) {
                value.totalItem = state.totalItem
            }
            setState({
                loading: false,
                dataLoading: false,
                currentPage: clickPage,
                ...value
            })
        })
    }

    function getMenuList(lists, data_key, state_key, label, valid = null) {
        let element = []
        if (lists) {
            Object.values(lists).map((list, key) => {
                if (state_key != 'country_id') {
                    if (valid[list.id]) {
                        element.push(
                            <Select.Option value={list.id} key={key.id}>
                                {list[data_key]}
                            </Select.Option>
                        )
                    }
                } else {
                    element.push(
                        <Select.Option value={list.id} key={key.id}>
                            {list[data_key]}
                        </Select.Option>
                    )
                }
            })
        }

        return <Select
            style={{ width: '24.5%'/* , padding: '10px 0' */ }}
            className={state[state_key] > 0 && 'active'}
            value={state[state_key]}
            disabled={lists.length <= 1 && state_key == "country_id"}
            onChange={(e) => {
                if (state_key == 'study_field_id') {
                    setState({
                        major_id: 0,
                        validOriKey: 'study_field'
                    })
                } else if (state_key == 'country_id') {
                    setState({
                        region_id: 0,
                        course_level_id: 0,
                        major_id: 0,
                        study_field_id: 0,
                        validOriKey: 'country'
                    })
                } else if (state_key == 'course_level_id') {
                    setState({
                        major_id: 0,
                        study_field_id: 0,
                        validOriKey: 'level'
                    })
                } else if (state_key == 'major_id') {
                    setState({
                        validOriKey: 'major'
                    })
                }
                setState({ [state_key]: e })
            }}
        >
            <Select.Option value={0} key={0}>
                {formatMessage({ id: label })} ({formatMessage({ id: 'all' })})
            </Select.Option>
            {element}
        </Select>
    }

    function renderCheckBoxAddQualificationSubject() {
        let checkbox = []
        let activeList = []
        Object.values(state.qualificationSubjectList).map((subject, key) => {
            if (subject.qualification_id == state.qualification_id) {
                if (
                    state.studentQualificationSubjectList[state.qualification_id] &&
                    state.studentQualificationSubjectList[state.qualification_id][subject.id] >= 0
                ) {
                    activeList.push(subject.id)
                }

                checkbox.push({
                    label: subject.subject_name,
                    value: subject.id
                })
            }
        })
        return [checkbox, activeList]
    }

    function checkQualificationSubjectGradeSubmit() {
        let allow = false
        if (
            typeof state.studentQualificationSubjectList[state.qualification_id] != 'undefined' &&
            Object.keys(state.studentQualificationSubjectList[state.qualification_id]).length > 0
        ) {
            allow = true
            Object.keys(state.studentQualificationSubjectList[state.qualification_id]).map((arrKey, key) => {
                if (state.studentQualificationSubjectList[state.qualification_id][arrKey] <= 0) {
                    allow = false
                }
            })

            /* if (allow && state.addQualificationSubjectEditMode) {
                allow = true
            } */
        }
        return allow
    }

    function renderComponent() {
        let checkboxData = renderCheckBoxAddQualificationSubject()
        return (
            <React.Fragment>
                <Modal
                    title={
                        <div style={{ textAlign: 'center', width: '100%', fontSize: '30px' }}>{state.qualificationTitle}</div>
                    }
                    width={"90%"}
                    okButtonProps={{
                        disabled: (
                            state.addQualificationSubjectEditMode &&
                            (
                                typeof state.studentQualificationSubjectList[state.qualification_id] == 'undefined' ||
                                Object.keys(state.studentQualificationSubjectList[state.qualification_id]).length <= 0
                            )
                        ) ||
                            (
                                !state.addQualificationSubjectEditMode && !checkQualificationSubjectGradeSubmit()
                            )
                    }}
                    visible={state.addQualificationModalVisible}
                    onOk={() => {
                        if (state.addQualificationSubjectEditMode) {
                            setState({ addQualificationSubjectEditMode: false })
                        } else if (
                            state.addQualificationModalVisible &&
                            checkQualificationSubjectGradeSubmit()
                        ) {
                            setState({ loading: true })
                            dispatch({
                                type: 'course/e_updateStudentAcademicResult',
                                payload: {
                                    studentQualificationSubjectList: state.studentQualificationSubjectList[state.qualification_id],
                                    qualification_id: state.qualification_id

                                }
                            }).then((value) => {
                                if (value) {
                                    setState({
                                        qualificationActiveId: state.qualification_id,
                                        ...value,
                                        academicResultUpdate: true
                                    })
                                }
                                setState({ loading: false })
                            })
                            setState({ addQualificationModalVisible: false })
                        }
                    }}
                    onCancel={() => {
                        if (typeof state.studentQualificationSubjectList[state.qualification_id] == 'undefined') {
                            setState({
                                addQualificationModalVisible: false,
                                qualification_id: 0
                            })
                        } else {
                            let allow = true
                            Object.keys(state.studentQualificationSubjectList[state.qualification_id]).map((arrKey, key) => {
                                if (state.studentQualificationSubjectList[state.qualification_id][arrKey] <= 0) {
                                    allow = false
                                }
                            })
                            if (allow) {
                                setState({ loading: true })
                                dispatch({
                                    type: 'course/e_updateStudentAcademicResult',
                                    payload: {
                                        studentQualificationSubjectList: state.studentQualificationSubjectList[state.qualification_id],
                                        qualification_id: state.qualification_id

                                    }
                                }).then((value) => {
                                    if (value) {
                                        let qualificationActiveId = state.qualificationActiveId
                                        if (qualificationActiveId == state.qualification_id) {
                                            qualificationActiveId = 0
                                        }

                                        let studentQualificationSubjectList = state.studentQualificationSubjectList
                                        delete studentQualificationSubjectList[state.qualification_id]
                                        setState({
                                            ...value,
                                            qualificationActiveId,
                                            qualification_id: 0,
                                            addQualificationModalVisible: false,
                                            studentQualificationSubjectList,
                                            academicResultUpdate: true
                                        })
                                    }
                                    setState({ loading: false })
                                })
                            }
                        }
                        //setState({ addQualificationModalVisible: false })
                    }}
                    okText={
                        state.addQualificationSubjectEditMode
                            ? formatMessage({ id: 'next step' })
                            : formatMessage({ id: 'confirm' })
                    }
                    cancelText={formatMessage({ id: 'cancel' })}
                >
                    <div className='desktop-search-modal-addQualificationSubject'>
                        {
                            !state.addQualificationSubjectEditMode &&
                            state.studentQualificationSubjectList[state.qualification_id] &&
                            <div style={{ width: '100%' }}>
                                <div
                                    onClick={() => {
                                        setState({ addQualificationSubjectEditMode: true })
                                    }}
                                    style={{
                                        cursor: 'pointer',
                                        marginRight: '10px',
                                        border: '1px solid ' + PRIMARY_COLOR,
                                        borderRadius: '15px',
                                        padding: '5px 8px',
                                        //color: 'red',
                                        marginTop: '10px',
                                        width: '135px',
                                        color: 'white',
                                        background: PRIMARY_COLOR,
                                        textAlign: 'center'
                                        //flex: '0 0 30%'
                                    }}
                                >
                                    {formatMessage({ id: 'add subject' })} <SettingOutlined />
                                </div>
                                <div
                                    style={{ display: 'flex', alignItems: 'center', flexFlow: 'row wrap', marginTop: '10px' }}
                                >
                                    {
                                        Object.values(state.qualificationSubjectList).map((subject, key) => {
                                            if (
                                                subject.qualification_id == state.qualification_id &&
                                                Object.keys(state.studentQualificationSubjectList[state.qualification_id]).indexOf(subject.id.toString()) >= 0
                                            ) {
                                                return (
                                                    <div
                                                        className='desktop-search-modal-addQualificationSubject-subjectList'
                                                    >
                                                        <Popover
                                                            placement="bottom"
                                                            title={formatMessage({ id: 'select grade' })}
                                                            content={
                                                                <div
                                                                    className='desktop-search-modal-addQualificationSubject-subjectList-grade'
                                                                >
                                                                    {
                                                                        Object.values(state.qualificationGradeList).map((grade, gkey) => {
                                                                            if (grade.qualification_id == state.qualification_id) {
                                                                                return <div
                                                                                    onClick={() => {
                                                                                        let studentQualificationSubjectList = state.studentQualificationSubjectList
                                                                                        studentQualificationSubjectList[state.qualification_id][subject.id] = grade.id
                                                                                        setState({ studentQualificationSubjectList })
                                                                                    }}
                                                                                    className='desktop-search-modal-addQualificationSubject-subjectList-grade-child'
                                                                                >
                                                                                    {grade.grade}
                                                                                </div>
                                                                            }

                                                                        })
                                                                    }
                                                                </div>
                                                            }
                                                            trigger="click"
                                                        >
                                                            <div
                                                                className='desktop-search-modal-addQualificationSubject-subjectList-submited_list'
                                                                onClick={() => {

                                                                }}
                                                            >
                                                                {subject.subject_name} {
                                                                    state.studentQualificationSubjectList[state.qualification_id][subject.id] > 0
                                                                        ? Object.values(state.qualificationGradeList).map((grade, gkey) => {
                                                                            if (grade.id == state.studentQualificationSubjectList[state.qualification_id][subject.id]) {
                                                                                return <span style={{ color: PRIMARY_COLOR }}>{grade.grade} <DownOutlined /></span>
                                                                            }
                                                                        })
                                                                        : <span style={{ color: PRIMARY_COLOR }}>{formatMessage({ id: 'select grade' })} <DownOutlined /></span>
                                                                }
                                                            </div>


                                                        </Popover>
                                                        <Popconfirm
                                                            placement="top"
                                                            title={formatMessage({ id: 'are you sure???' })}
                                                            onConfirm={() => {
                                                                let studentQualificationSubjectList = state.studentQualificationSubjectList
                                                                delete studentQualificationSubjectList[state.qualification_id][subject.id]
                                                                setState({ studentQualificationSubjectList })
                                                            }}
                                                            okText={formatMessage({ id: 'confirm' })}
                                                            cancelText={formatMessage({ id: 'cancel' })}
                                                        >
                                                            <DeleteOutlined style={{ color: 'red', fontSize: '18px', cursor: 'pointer' }} />
                                                        </Popconfirm>

                                                    </div>
                                                )
                                            }

                                        })
                                    }
                                </div>
                            </div>
                        }
                        {
                            (
                                state.addQualificationSubjectEditMode ||
                                (
                                    typeof state.studentQualificationSubjectList[state.qualification_id] == 'undefined' ||
                                    Object.keys(state.studentQualificationSubjectList[state.qualification_id]).length == 0
                                )
                            ) && <Checkbox.Group
                                options={checkboxData[0]}
                                value={
                                    state.studentQualificationSubjectList[state.qualification_id]
                                        ? Object.keys(state.studentQualificationSubjectList[state.qualification_id]).map((sk) => {
                                            return parseInt(sk)
                                        })
                                        : []
                                }
                                onChange={(e) => {
                                    let studentQualificationSubjectList = state.studentQualificationSubjectList
                                    let subjectList = {}
                                    e.forEach(subject_id => {
                                        subjectList[subject_id] = 0
                                    })
                                    if (studentQualificationSubjectList[state.qualification_id]) {
                                        e.forEach(subject_id => {
                                            if (studentQualificationSubjectList[state.qualification_id][subject_id]) {
                                                subjectList[subject_id] = studentQualificationSubjectList[state.qualification_id][subject_id]
                                            }
                                        })
                                    }
                                    studentQualificationSubjectList[state.qualification_id] = subjectList
                                    setState({ studentQualificationSubjectList })
                                }}
                            />
                        }
                    </div>
                </Modal >
            </React.Fragment >
        )
    }

    function EditQualificationValid() {
        let allow = true
        Object.keys(state.studentQualificationSubjectList).map((qualification_id) => {
            Object.keys(state.studentQualificationSubjectList[qualification_id]).map((subject) => {
                if (state.studentQualificationSubjectList[qualification_id][subject] <= 0) {
                    allow = false
                }
            })
        })
        return Object.keys(state.studentQualificationSubjectList).length > 0 && allow
    }

    return (
        <>
            <Helmet>
                <meta name="description" content="Course" />
                <link rel="canonical" href={baseUrl + "searchCourse"} />
            </Helmet>
            <div className='desktop-search'>
                <div className='desktop-search-content'>
                    <div className='desktop-header_menu-content'>
                        {getMenuList(state.countryList, 'country', 'country_id', 'country')}

                        {/* {
                        state.country_id > 0 &&
                        getMenuList(state.regionList, 'region', 'region_id', 'region')
                    } */}

                        {getMenuList(state.courseLevelList, 'level_name', 'course_level_id', 'course level', state.validLevel)}

                        {getMenuList(state.studyFieldList, 'main_tag', 'study_field_id', 'study field', state.validStudyField)}

                        {getMenuList(state.majorList, 'sub_tag', 'major_id', 'major', state.validMajor)}

                        <div className='desktop-school_library-content-filter-region'>
                            <div className='desktop-school_library-content-filter-region-title'>
                                {formatMessage({ id: 'region' })}
                            </div>
                            {
                                courseState.searchCourseFilter.regionList &&
                                <Radio.Group
                                    options={loopRadio(courseState.searchCourseFilter.regionList, 'region')}
                                    onChange={(e) => {
                                        let newCountryId = state.country_id
                                        courseState.searchCourseFilter.regionList.map((list) => {
                                            if (list.id == e.target.value) {
                                                newCountryId = list.country_id
                                            }
                                        })
                                        setState({ region_id: e.target.value, country_id: newCountryId })
                                    }}
                                    value={state.region_id}
                                    optionType="button"
                                />
                            }
                        </div>
                        {/* <div className='desktop-search-content-qualification'>
                        <div className='desktop-search-content-qualification-title'>
                            {formatMessage({ id: 'qualification' })}:
                        </div>
                        <div className='desktop-search-content-qualification-list'>
                            {
                                Object.values(state.qualificationList).map((q) => {
                                    return <div className='desktop-search-content-qualification-list-child'>
                                        <div
                                            className={
                                                state.studentAcademicResult[q.id]
                                                    ? state.qualificationActiveId == q.id
                                                        ? 'desktop-search-content-qualification-list-child-name-active-selected'
                                                        : 'desktop-search-content-qualification-list-child-name-active'
                                                    : 'desktop-search-content-qualification-list-child-name'
                                            }
                                            onClick={() => {
                                                if (
                                                    typeof state.studentQualificationSubjectList[q.id] != 'undefined' &&
                                                    Object.keys(state.studentQualificationSubjectList[q.id]).length > 0
                                                ) {
                                                    if (state.qualificationActiveId == q.id) {
                                                        setState({ qualificationActiveId: 0 })
                                                    } else {
                                                        setState({ qualificationActiveId: q.id })
                                                    }
                                                }
                                            }}
                                        >
                                            {q.qualification_name}
                                        </div>
                                        <div className='desktop-search-content-qualification-list-child-func'>
                                            <div
                                                className={
                                                    state.studentAcademicResult[q.id]
                                                        ? 'desktop-search-content-qualification-list-child-func-button'
                                                        : 'desktop-search-content-qualification-list-child-func-button active'
                                                }

                                                onClick={() => {
                                                    if (typeof state.studentQualificationSubjectList[q.id] == 'undefined' || Object.keys(state.studentQualificationSubjectList[q.id]).length <= 0) {
                                                        setState({
                                                            qualificationTitle: q.qualification_name,
                                                            addQualificationModalVisible: true,
                                                            qualification_id: q.id,
                                                            addQualificationSubjectEditMode: state.studentQualificationSubjectList[q.id] ? false : true
                                                        })
                                                    }
                                                }}
                                            >
                                                <AppstoreAddOutlined />
                                            </div>
                                            <div
                                                style={{ width: '1px', color: '#b5acac' }}
                                            >
                                                |
                                            </div>
                                            <div
                                                className={
                                                    state.studentAcademicResult[q.id]
                                                        ? 'desktop-search-content-qualification-list-child-func-button active'
                                                        : 'desktop-search-content-qualification-list-child-func-button'
                                                }
                                                onClick={() => {
                                                    if (
                                                        state.studentQualificationSubjectList[q.id] &&
                                                        Object.keys(state.studentQualificationSubjectList[q.id]).length > 0
                                                    ) {
                                                        setState({
                                                            qualificationTitle: q.qualification_name,
                                                            addQualificationModalVisible: true,
                                                            qualification_id: q.id,
                                                            addQualificationSubjectEditMode: state.studentQualificationSubjectList[q.id] ? false : true
                                                        })
                                                    }
                                                }}
                                                {...(state.studentAcademicResult[q.id] ? {
                                                    style: {
                                                        color: 'black'
                                                    }
                                                } : {})}
                                            >
                                                <SettingOutlined />
                                            </div>
                                            <div
                                                style={{ width: '1px', color: '#b5acac' }}
                                            >
                                                |
                                            </div>
                                            {
                                                state.studentAcademicResult[q.id]
                                                    ? <Popconfirm
                                                        placement="top"
                                                        title={formatMessage({ id: 'are you sure???' })}
                                                        onConfirm={() => {
                                                            setState({ loading: true })
                                                            dispatch({
                                                                type: 'course/e_updateStudentAcademicResult',
                                                                payload: {
                                                                    studentQualificationSubjectList: {},
                                                                    qualification_id: q.id

                                                                }
                                                            }).then((result) => {
                                                                if (result) {
                                                                    let qualificationActiveId = state.qualificationActiveId
                                                                    if (qualificationActiveId == q.id) {
                                                                        qualificationActiveId = 0
                                                                    }

                                                                    let studentQualificationSubjectList = state.studentQualificationSubjectList
                                                                    delete studentQualificationSubjectList[q.id]
                                                                    setState({
                                                                        ...result,
                                                                        qualificationActiveId,
                                                                        qualification_id: 0,
                                                                        //qualificationBookEditMode: false,
                                                                        studentQualificationSubjectList,
                                                                        academicResultUpdate: true
                                                                    })
                                                                }
                                                                setState({ loading: false })
                                                            })
                                                        }}
                                                        okText={formatMessage({ id: 'confirm' })}
                                                        cancelText={formatMessage({ id: 'cancel' })}
                                                    >
                                                        <div
                                                            className='desktop-search-content-qualification-list-child-func-button active'
                                                            {...(state.studentAcademicResult[q.id] ? {
                                                                style: {
                                                                    color: 'red'
                                                                }
                                                            } : {})}
                                                        >
                                                            <DeleteOutlined />
                                                        </div>
                                                    </Popconfirm>
                                                    : <div
                                                        className='desktop-search-content-qualification-list-child-func-button'
                                                        {...(state.studentAcademicResult[q.id] ? {
                                                            style: {
                                                                color: 'red'
                                                            }
                                                        } : {})}
                                               
                                                    >
                                                        <DeleteOutlined />
                                                    </div>
                                            }


                                        </div>
                                    </div>
                                })
                            }
                        </div>
                    </div> */}
                        <div className='desktop-search-content-total'>
                            <div className='desktop-search-content-total-child'>
                                <div className='desktop-search-content-total-child-number'>{state.totalItem} </div>
                                <div className='desktop-search-content-total-child-available'>
                                    {
                                        state.dataList.length > 1
                                            ? formatMessage({ id: 'courses available' })
                                            : formatMessage({ id: 'course available' })
                                    }

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <div className='desktop-search-total'>
                <div className='desktop-search-total-number'>{state.totalItem} </div>
                <div className='desktop-search-total-available'>
                    {
                        state.dataList.length > 1
                            ? formatMessage({ id: 'courses available' })
                            : formatMessage({ id: 'course available' })
                    }

                </div>
            </div> */}
                <DesktopLoading loading={state.loading}>
                    {renderComponent()}
                    <div className='desktop-content'>
                        <div className='desktop-search-course_list' ref={courseListHeightDom}>
                            {
                                Object.values(state.dataList).map((course, key) => {
                                    return <div className='desktop-search-course_list-box' onClick={() => router.push('/viewCourse?course_id=' + course.id)}>
                                        <div className='desktop-search-course_list-box-ribbon'>
                                            <div className='desktop-search-course_list-box-ribbon-title'>
                                                <div className='desktop-search-course_list-box-ef-country-duration'>
                                                    {
                                                        course.duration > 0
                                                            ? parseInt(course.duration / 12) > 0
                                                                ? parseInt(course.duration / 12).toFixed() + (parseInt(course.duration / 12).toFixed() > 1 ? formatMessage({ id: 'years' }) : formatMessage({ id: 'year' })) + ' '
                                                                : ''
                                                            : formatMessage({ id: 'unknow' })


                                                    }
                                                    {
                                                        course.duration &&
                                                            course.duration % 12 > 0
                                                            ? course.duration % 12 + (course.duration % 12 > 1 ? formatMessage({ id: 'month\'s' }) : formatMessage({ id: 'month' }))
                                                            : ''
                                                    }
                                                </div>
                                            </div>
                                            <div className='desktop-search-course_list-box-ribbon-tail'>

                                            </div>
                                        </div>
                                        <div className='desktop-search-course_list-box-img'>
                                            <img
                                                src={/* HOST */'https://api.edudotcom.my/' + 'auth/image/school/' + course.logo}
                                                width="100%"
                                                height='auto'
                                                style={{ /* borderRadius: '20px' */ }}
                                            />
                                        </div>

                                        <div className='desktop-search-course_list-box-tag'>
                                            {
                                                state.schoolTypeList &&
                                                Object.keys(state.schoolTypeList).map((stype) => {
                                                    if (stype == course.school_type) {
                                                        return <Tag color={state.schoolTypeList[stype] == 'private' ? '#673ab7' : '#cd201f'}>
                                                            {formatMessage({ id: state.schoolTypeList[stype] })}
                                                        </Tag>
                                                    }
                                                })
                                            }

                                            {
                                                course.sub_tag_list &&
                                                Object.values(course.sub_tag_list.split(",")).map((id, atkey) => {
                                                    if (atkey == 0) {
                                                        return Object.values(state.majorList).map((major) => {
                                                            if (major.id == id) {
                                                                return <React.Fragment>
                                                                    <Tag color="cyan">{major.sub_tag}</Tag>
                                                                    {/* <Tag color="cyan">ETC</Tag> */}
                                                                </React.Fragment>
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        </div>

                                        <div className='desktop-search-course_list-box-course_name' style={{ WebkitBoxOrient: 'vertical' }}>
                                            <Tooltip placement="topLeft" title={course.course_name}>
                                                {course.course_name}
                                            </Tooltip>
                                        </div>

                                        <div className='desktop-search-course_list-box-ef'>
                                            {/* ET.F: <span style={{ marginLeft: '5px' }}>{state.ipCountryCode} {parseFloat(course.exchanged_tuition_fee).toFixed().replace(/(?=(\B\d{3})+$)/g, ',')}</span> */}
                                            {/* <div className='desktop-search-course_list-box-ef-country-duration'>
                                            {
                                                course.duration > 0
                                                    ? parseInt(course.duration / 12) > 0
                                                        ? parseInt(course.duration / 12).toFixed() + (parseInt(course.duration / 12).toFixed() > 1 ? formatMessage({ id: 'years' }) : formatMessage({ id: 'year' })) + ' '
                                                        : ''
                                                    : formatMessage({ id: 'unknow' })


                                            }
                                            {
                                                course.duration &&
                                                    course.duration % 12 > 0
                                                    ? course.duration % 12 + (course.duration % 12 > 1 ? formatMessage({ id: 'month\'s' }) : formatMessage({ id: 'month' }))
                                                    : ''
                                            }
                                        </div> */}
                                            {course.country}{course.region != course.country && ', ' + course.region}
                                        </div>

                                        <div className='desktop-search-course_list-box-bottom_right'>
                                            <div className='desktop-search-course_list-box-bottom_right-school' style={{ WebkitBoxOrient: 'vertical' }}>
                                                {course.school_name}
                                            </div>

                                            <div className='desktop-search-course_list-box-bottom_right-country'>

                                                <span style={{ marginLeft: '5px' }}>{state.ipCountryCode} {parseFloat(course.exchanged_tuition_fee).toFixed().replace(/(?=(\B\d{3})+$)/g, ',')}</span>
                                            </div>
                                        </div>


                                    </div>

                                })
                            }
                        </div>
                        {
                            state.dataLoading && <Spin />
                        }
                    </div>

                </DesktopLoading>
            </div >
        </>

    )

}
