import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './index.less'
import { DownOutlined, SearchOutlined } from '@ant-design/icons';
import { Radio, Dropdown, Menu, Button } from 'antd';
import { formatMessage } from 'umi/locale';
import { PRIMARY_COLOR } from '@/constants/default'
import router from 'umi/router';

export default (props) => {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        loading: false,
        searchStatus: 'course',

        country_id: 0,
        region_id: 0,
        course_level_id: 0,
        study_field_id: 0,
        major_id: 0
    })
    const courseState = useSelector(state => state.course)
    useEffect(() => {
        /* dispatch({
            type: "home/e_getHomePageData",
            payload: {}
        }).then(result => {
            if (result) {
                setState({ ...result, loading: false })
            }
        }); */
        if (courseState.searchCourseFilter.countryList.length <= 0) {
            setState({ loading: true })
            dispatch({
                type: 'course/e_getSelection',
                payload: {}
            }).then((value) => {
                if (value) {
                    dispatch({
                        type: 'course/r_updateState',
                        payload: {
                            searchCourseFilter: {
                                ...courseState.searchCourseFilter,
                                ...value
                            }
                        }
                    })
                    setState({ loading: false })
                }
            })
        }
    }, []);

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    function readMenu(lists, key, modelKey) {
        let menu = []
        lists.map((list) => {
            if (modelKey == 'region_id' && state.country_id > 0) {
                if (list.country_id == state.country_id) {
                    menu.push(
                        <Menu.Item key={list.id}>
                            {list[key]}
                        </Menu.Item>
                    )
                }
            } else if (modelKey == 'major_id' && state.study_field_id > 0) {
                if (list.main_tag_id == state.study_field_id) {
                    menu.push(
                        <Menu.Item key={list.id}>
                            {list[key]}
                        </Menu.Item>
                    )
                }
            } else {
                menu.push(
                    <Menu.Item key={list.id}>
                        {list[key]}
                    </Menu.Item>
                )
            }
        })
        return <Menu onClick={(e) => {
            let updateState = {}
            if (modelKey == 'country_id') {
                updateState.region_id = 0
            } else if (modelKey == 'study_field_id') {
                updateState.major_id = 0
            }
            setState({
                [modelKey]: parseInt(e.key),
                ...updateState
            })
        }}>
            {menu}
        </Menu>
    }

    function renderRadioSelected() {
        if (state.searchStatus == 'course') {
            return <React.Fragment>
                <Dropdown arrow overlay={readMenu(courseState.searchCourseFilter.countryList, 'country', 'country_id')} trigger={['click']}>
                    <Button>
                        <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '0.85rem' }}>
                            {
                                state.country_id
                                    ? Object.values(courseState.searchCourseFilter.countryList).map((list) => {
                                        if (list.id == state.country_id) {
                                            return list.country
                                        }
                                    })
                                    : formatMessage({ id: 'country' })
                            } <DownOutlined style={{ fontSize: '10px' }} />
                        </div>

                    </Button>
                </Dropdown>

                <Dropdown disabled={state.country_id == 0} arrow overlay={readMenu(courseState.searchCourseFilter.regionList, 'region', 'region_id')} trigger={['click']}>
                    <Button>
                        <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '0.85rem' }}>
                            {
                                state.region_id
                                    ? Object.values(courseState.searchCourseFilter.regionList).map((list) => {
                                        if (list.id == state.region_id) {
                                            return list.region
                                        }
                                    })
                                    : formatMessage({ id: 'region' })
                            } <DownOutlined style={{ fontSize: '10px' }} />
                        </div>

                    </Button>
                </Dropdown>


                <Dropdown arrow overlay={readMenu(courseState.searchCourseFilter.courseLevelList, 'level_name', 'course_level_id')} trigger={['click']}>
                    <Button>
                        <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '0.85rem' }}>
                            {
                                state.course_level_id
                                    ? Object.values(courseState.searchCourseFilter.courseLevelList).map((list) => {
                                        if (list.id == state.course_level_id) {
                                            return list.level_name
                                        }
                                    })
                                    : formatMessage({ id: 'course level' })
                            } <DownOutlined style={{ fontSize: '10px' }} />
                        </div>

                    </Button>
                </Dropdown>

                <Dropdown arrow overlay={readMenu(courseState.searchCourseFilter.studyFieldList, 'main_tag', 'study_field_id')} trigger={['click']}>
                    <Button>
                        <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '0.85rem' }}>
                            {
                                state.study_field_id
                                    ? Object.values(courseState.searchCourseFilter.studyFieldList).map((list) => {
                                        if (list.id == state.study_field_id) {
                                            return list.main_tag
                                        }
                                    })
                                    : formatMessage({ id: 'study field' })
                            } <DownOutlined style={{ fontSize: '10px' }} />
                        </div>

                    </Button>
                </Dropdown>

                <Dropdown disabled={state.study_field_id == 0} arrow overlay={readMenu(courseState.searchCourseFilter.majorList, 'sub_tag', 'major_id')} trigger={['click']}>
                    <Button>
                        <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '0.85rem' }}>
                            {
                                state.major_id
                                    ? Object.values(courseState.searchCourseFilter.majorList).map((list) => {
                                        if (list.id == state.major_id) {
                                            return list.sub_tag
                                        }
                                    })
                                    : formatMessage({ id: 'major' })
                            } <DownOutlined style={{ fontSize: '10px' }} />
                        </div>

                    </Button>
                </Dropdown>


                <Button
                    style={{ borderRadius: '5px', backgroundColor: PRIMARY_COLOR, color: 'white' }}
                    onClick={() => {
                        dispatch({
                            type: 'course/r_updateState',
                            payload: {
                                searchCourseFilter: {
                                    ...courseState.searchCourseFilter,
                                    country_id: state.country_id,
                                    region_id: state.region_id,
                                    course_level_id: state.course_level_id,
                                    study_field_id: state.study_field_id,
                                    major_id: state.major_id
                                }
                            }
                        })
                        router.push('/searchCourse')
                    }}
                >
                    {formatMessage({ id: 'search' })}
                    <SearchOutlined />
                </Button>

            </React.Fragment>
        }
        return
    }

    return (
        <div className='desktop-searchBar'>
            <div className='desktop-searchBar-content'>
                <div className='desktop-searchBar-content-child'>
                    {/* <div className='desktop-searchBar-content-child-radio'>
                        <Radio.Group onChange={(e) => setState({ searchStatus: e.target.value })} value={state.searchStatus} >
                            <Radio value={'course'}>{formatMessage({ id: 'course' })}</Radio>
                            <Radio value={'school'}>{formatMessage({ id: 'school' })}</Radio>
                        </Radio.Group>
                    </div> */}

                    {renderRadioSelected()}
                </div>
            </div>
        </div>
    )

}
