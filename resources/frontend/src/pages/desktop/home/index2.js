import React, { useEffect, useState } from "react";
import { useDispatch } from 'react-redux'
import DesktopLoading from '@/components/desktopLoading'
import './index.less'
import { Carousel, Card } from 'antd';
import SearchBar from './searchBar'
import { formatMessage } from 'umi/locale';
import { UserOutlined, CaretUpOutlined, CaretDownOutlined } from '@ant-design/icons';
import DesktopFooter from '@/components/desktopFooter'
export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        activeKey: -1,
        loading: true,
        newsAndEvents: [
            {
                type: 'News',
                img: 'http://www.louis-blog.com/auth/image/description/1610870950.jpeg',
                date: '1 January 2021',
                topic: 'Kaplan\'s Application Fee waiver for January 2021',
                content: <div /* className='desktop-homePage-news-content-father-child-card-desc' */>
                    (1) Target Audience : Malaysian Students <br />
                    (2) Promotion : SGD$ 492.20 Application Fees Waiver<br />
                    (3) Qualifying Period : 1 Jan 2021 – 31 Jan 2021<br />
                    (4) Terms & Conditions :<br />
                    • Waiver shall only apply to the program and intake specified upon application form and/or Waiver Letter.<br />
                    • The waiver will not be indicated in the Student Contract.<br />
                    • Deferment of intake or change of program will result in forfeiting the waiver.<br />
                    • Withdrawal from program prior to commencement will result in forfeiting the waiver.<br />
                    • There shall be no duplication of waiver allowed.
                </div>,
            },
            {
                type: 'News',
                img: 'http://www.louis-blog.com/auth/image/description/1610871991.jpeg',
                date: '5 January 2021',
                topic: 'EDU Entrance Scholarship for Akamonkai',
                content: <div /* className='desktop-homePage-news-content-father-child-card-desc' */>
                    In order to encourage students joining for Akamonkai Japanese Language School, EDU DOTCOM now sponsoring JPY40,000 as New Entrance Scholarship for students. For more info and details, please refer to the relevant school page in our website or contact to our counsellor.
                </div>,
            },
            {
                type: 'Annoucement',
                img: 'http://www.louis-blog.com/auth/image/description/1610873974.jpeg',
                date: '14 January 2021',
                topic: 'IELTS Test is available as Scheduled during MCO Period',
                content: <div /* className='desktop-homePage-news-content-father-child-card-desc' */>
                    This is to inform that both Computer-Delivered and Paper-based IELTS will be available during the current MCO period. All precautions are taken under strict SOP guidelines to ensure the safety of everyone. Please contact to our counsellor at +6014-2773177 if you need to apply for IELTS or any enquiry.
                    Please take care to look after your health at this time by following the latest advice, available here: https://www.who.int/emergencies/diseases/novel-coronavirus-2019
                    Thank you for your understanding.
                </div>,
            },
            {
                type: 'News',
                img: 'http://www.louis-blog.com/auth/image/description/1610874527.jpeg',
                date: '15 January 2021',
                topic: 'Japan is closing national border for foreign students',
                content: <div /* className='desktop-homePage-news-content-father-child-card-desc' */>
                    Japan declares closing of national border due to the corovavirus-19 until further announcement.
                    However, all schools in Japan will still accept application from overseas and will not delay school entrance date at the moments.
                </div>,
            }/* ,
            {
                type: 'annoucement',
                img: 'http://www.louis-blog.com/auth/image/description/1610872498.jpeg',
                date: '17 January 2021',
                topic: 'Temporary closure of official website',
                content: <div className='desktop-homePage-news-content-father-child-card-desc'>
                    We are temporary closed down our official website and the brand new website will be launching on this coming March. Stay tune and don't forget to get the latest news from our offical facebook or instagram channel!
                </div>,
            }, */
        ]
    })

    useEffect(() => {
        dispatch({
            type: "home/e_getHomePageData",
            payload: {}
        }).then(result => {
            if (result) {
                setState({ ...result, loading: false })
            }
        });
    }, []);

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <DesktopLoading loading={state.loading}>
            <div className='desktop-homePage'>
                <div className='desktop-homePage-top_banner'>
                    <Carousel autoplay >
                        <div>
                            <img src='http://www.louis-blog.com/auth/image/description/1610889895.jpeg' style={{ height: '100%', width: '100%' }} />
                        </div>
                    </Carousel>

                    <SearchBar />
                </div>

                <div className='desktop-homePage-news'>
                    <div className='desktop-homePage-news-title'>
                        －{formatMessage({ id: 'news and events' })} －
                    </div>
                    {/* <div className='desktop-homePage-news-content'>
                        <CaretLeftOutlined className='desktop-homePage-news-content-icon' />
                        <div className='desktop-homePage-news-content-father'>
                            {
                                state.newsAndEvents &&
                                Object.values(state.newsAndEvents).map((value, key) => {
                                    return <Card
                                        className='desktop-homePage-news-content-father-child'
                                        hoverable
                                        style={{
                                            width: 240,
                                            marginLeft: key == 0 && '10px',
                                            marginRight: key == state.newsAndEvents.length - 1 && '10px',
                                        }}
                                        cover={<img alt="example" src={value.img} />}
                                    >
                                        <Card.Meta
                                            title={
                                                <div className='desktop-homePage-news-content-father-child-card'>
                                                    <div className='desktop-homePage-news-content-father-child-card-date'>
                                                        {value.date} ( {formatMessage({ id: value.type })} )
                                                    </div>
                                                    <div className='desktop-homePage-news-content-father-child-card-title'>
                                                        {value.topic}
                                                    </div>
                                                </div>
                                            }

                                            description={
                                                value.content
                                                // <div className='desktop-homePage-news-content-father-child-card-desc'>
                                                //     {value.content}
                                                // </div>
                                            }
                                        />
                                    </Card>
                                })
                            }
                        </div>
                        <CaretRightOutlined className='desktop-homePage-news-content-icon' />
                    </div> */}
                </div>

                <div className='newsAndEvents'>
                    {
                        Object.values(state.newsAndEvents).map((list, key) => {
                            return <div
                                className={state.activeKey == key ? 'newsAndEvents-box active' : 'newsAndEvents-box'}
                                onClick={() => {
                                    let newKey = key
                                    if (state.activeKey == key) {
                                        newKey = -1
                                    }
                                    setState({ activeKey: newKey })
                                }}
                            >
                                <div className='newsAndEvents-box-child'>

                                    <div className='newsAndEvents-box-child-img'>
                                        <img src={list.img} />
                                    </div>

                                    <div className='newsAndEvents-box-child-detail'>
                                        <div className='newsAndEvents-box-child-detail-type'>
                                            {list.type}
                                        </div>
                                        <div className='newsAndEvents-box-child-detail-date'>
                                            {list.date}
                                        </div>
                                        <div className='newsAndEvents-box-child-detail-topic'>
                                            {list.topic}
                                        </div>

                                        <div className='newsAndEvents-box-child-detail-viewMore'>
                                            {formatMessage({ id: 'view more' })}
                                            {
                                                key == state.activeKey
                                                    ? <CaretUpOutlined />
                                                    : <CaretDownOutlined />
                                            }
                                        </div>
                                    </div>
                                </div>

                                <div className='newsAndEvents-box-content'>
                                    {list.content}
                                </div>
                            </div>
                        })
                    }

                </div>



                <div className='desktop-homePage-about_us'>
                    <div className='desktop-homePage-about_us-image'>
                        <img src='http://www.louis-blog.com/auth/image/description/1610959599.jpeg' style={{ height: '100%', width: '100%' }} />
                    </div>
                    {/* <div className='desktop-homePage-about_us-content'>
                        <div className='desktop-homePage-about_us-content-title'>
                            －{formatMessage({ id: 'about us' })} －
                        </div>
                        <div className='desktop-homePage-about_us-content-content'>
                            {formatMessage({ id: 'about us content' })}
                        </div>
                    </div> */}
                </div>

                <img src='http://www.louis-blog.com/auth/image/description/1610897013.jpeg' width='100%' />

                <DesktopFooter />
            </div>

        </DesktopLoading >

    )

}
