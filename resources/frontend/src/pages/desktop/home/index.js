import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import DesktopLoading from '@/components/desktopLoading'
import './index.less'
import { Carousel, Tag, Skeleton } from 'antd';
import SearchBar from './searchBar'
import { formatMessage } from 'umi/locale';
import { UserOutlined, CaretUpOutlined, RightOutlined } from '@ant-design/icons';
import DesktopFooter from '@/components/desktopFooter'
import router from "umi/router";
import { Helmet } from "react-helmet";
import { baseUrl } from "../../../constants/api";

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        activeKey: -1,
        loading: false,
        faqs: [
            {
                topic: 'What is EDU DOTCOM?',
                content: <div>
                    EDU DOTCOM is an higher education consultancy private company which provide platform to both university and students.
                    For more details, please refer to "About Us" for more details.
                </div>
            },
            {
                topic: 'What kind of the services provide by EDU DOTCOM and how much for the charges?',
                content: <div>
                    Absolutely FREE for all the services! For more info, please refer to "Our Services" for more details.
                </div>
            },
            {
                topic: 'How do EDU DOTCOM sustain if the services are all free to the public?',
                content: <div>
                    We receive financial support from the university when they publish information in our website or application assist from us to each students.
                </div>
            },
            {
                topic: 'Is there any different if I apply through EDU DOTCOM or direct to the university?',
                content: <div>
                    The course fees and scholarships that you apply through us is totally same if you apply directly to university.
                    We have signed an official contract with all our partner schools as a recruitment representative in Malaysia.
                    If you are still in doubt, feel free to contact relevant university for further clarification.
                </div>
            },
            {
                topic: 'Which university is being partner schools to the EDU DOTCOM?',
                content: <div>
                    You may refer to the contact detail in from each school page. If you unclear on this part, feel free to contact us or chat to our counselor.
                </div>
            },
            {
                topic: 'Can I apply directly to university?',
                content: <div>
                    Yes, you can. However, we got the exclusive scholarships or offer sometimes.
                    Also, you will get appointed counsellor to assist you regarding of application process, student services, etc for FREE!
                </div>
            },
        ],
        imgLoaded: false
    })

    /* useEffect(() => {
        dispatch({
            type: "home/e_getHomePageData",
            payload: {}
        }).then(result => {
            if (result) {
                setState({ ...result, loading: false })
            }
        });
    }, []); */

    const newsAndEvents = useSelector(state => state.config.newsAndEvents)

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return <>
        <Helmet>
            <meta name="description" content="最具公信力的免中介费日本留学一站式协助平台。 The most reputable one-stop service platform for studying in Japan without agency fees." />
            <meta name="keywords" content="最具公信力的免中介费日本留学一站式协助平台。 The most reputable one-stop service platform for studying in Japan without agency fees." />
            <link rel="canonical" href={baseUrl} />
        </Helmet>

        <DesktopLoading loading={state.loading}>
            <div className='desktop-content'>
                <div className='desktop-homePage'>
                    <div className='desktop-homePage-top_banner'>
                        {
                            !state.imgLoaded && <React.Fragment>
                                <Skeleton active />
                                <Skeleton active />
                            </React.Fragment>
                        }
                        <Carousel autoplay >
                            <div>
                                <img onLoad={() => setState({ imgLoaded: true })} src='https://www.edudotcom.my/auth/image/description/1619809397.jpeg' style={{ height: '100%', width: '100%' }} />
                            </div>
                            {/* <div>
                                <img src='https://www.edudotcom.my/auth/image/description/1619809398.jpeg' style={{ height: '100%', width: '100%' }} />
                            </div> */}
                        </Carousel>

                        {/* <SearchBar /> */}
                    </div>

                    <div className='desktop-homePage-box'>
                        <div className='desktop-homePage-box-news'>
                            <div className='desktop-homePage-box-news-part1'>
                                <div className='desktop-homePage-box-news-part1-title'>
                                    {formatMessage({ id: 'news and events' })}
                                </div>
                                <div className='desktop-homePage-box-news-part1-viewMore' onClick={() => router.push('/introduction/newsAndEvents')}>
                                    +
                                </div>
                            </div>
                            {
                                Object.values(newsAndEvents).map((v, k) => {
                                    if (k <= 4) {
                                        return <div className='desktop-homePage-box-news-part2' onClick={() => router.push('/introduction/newsAndEvents/view?_ar=' + k)}>
                                            <Tag color={v.color}>{v.type}</Tag> <span className='desktop-homePage-box-news-part2-topic'>{v.topic}</span>
                                        </div>
                                    }

                                })
                            }


                        </div>

                        <div className='desktop-homePage-box-faqs'>
                            <div className='desktop-homePage-box-faqs-part1'>
                                <div className='desktop-homePage-box-faqs-part1-title'>
                                    {formatMessage({ id: 'FAQs' })}
                                </div>
                                <div className='desktop-homePage-box-faqs-part1-viewMore'>
                                    +
                                </div>
                            </div>
                            {
                                Object.values(state.faqs).map((v, key) => {
                                    if (key < 5) {
                                        return <div className='desktop-homePage-box-faqs-part2'>
                                            {v.topic}
                                        </div>
                                    }
                                })
                            }
                        </div>

                        <div className='desktop-homePage-box-shortcut'>
                            <a
                                onClick={() => router.push('/searchCourse')}
                                className='desktop-homePage-box-shortcut-part1'
                                style={{ backgroundImage: 'url(https://www.edudotcom.my/auth/image/description/1618559077.png)' }}
                            >
                                <div>
                                    {formatMessage({ id: 'search course' })}
                                </div>

                            </a>
                            <a
                                onClick={() => router.push('/schoolLibrary')}
                                className='desktop-homePage-box-shortcut-part1'
                                style={{ backgroundImage: 'url(https://www.edudotcom.my/auth/image/description/1618559076.png)' }}
                            >
                                <div>
                                    {formatMessage({ id: 'school library' })}
                                </div>
                            </a>
                        </div>
                    </div>

                    {/* <div className='desktop-homePage-testimonial'>
                        <div className='desktop-homePage-testimonial-content'>
                            <div className='desktop-homePage-testimonial-content-child'>
                                <div className='desktop-homePage-testimonial-content-child-avatar'>
                                    <img src={require('@/assets/testimonial/1.png')} />
                                </div>
                                <div className='desktop-homePage-testimonial-content-child-content'>
                                    <div className='desktop-homePage-testimonial-content-child-content-name'>
                                        LEE KEE HAO
                                    </div>
                                    <div className='desktop-homePage-testimonial-content-child-content-school'>
                                        James Cook University (Singapore)
                                    </div>
                                    <div className='desktop-homePage-testimonial-content-child-content-testimonial'>
                                        "Your guys are the one who made me become a person which I never think of it! I sincerely thanks to EDU DOTCOM for being patiently to explain and analyzing various possible options for my further studies when I graduated from high school. I hope that more students will meet you in the future."
                                    </div>
                                </div>
                            </div>
                            <div className='desktop-homePage-testimonial-content-child'>
                                <div className='desktop-homePage-testimonial-content-child-avatar'>
                                    <img src={require('@/assets/testimonial/2.png')} />
                                </div>
                                <div className='desktop-homePage-testimonial-content-child-content'>
                                    <div className='desktop-homePage-testimonial-content-child-content-name'>
                                        Chloe Ng
                                    </div>
                                    <div className='desktop-homePage-testimonial-content-child-content-location'>
                                        Kyung Hee University
                                    </div>
                                    <div className='desktop-homePage-testimonial-content-child-content-testimonial'>
                                        "Thank you EDU DOTCOM helping me to fulfill my dream to study in Korea. The counselor has a strong responsibility and great experience to answer all the questions which I raised to them. I wish to take this opportunity to express our sincerely gratitude to your guys! "
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='desktop-homePage-testimonial-background_img'>
                            <div className='desktop-homePage-testimonial-background_img-box' />
                            <div className='desktop-homePage-testimonial-background_img-img'>
                                <img src={require('@/assets/testimonial/backgroud.jpg')} style={{ width: '100% ' }} />
                            </div>
                        </div>
                    </div> */}

                    <div className='desktop-homePage-testimonial'>
                        <div className='desktop-homePage-testimonial-child'>
                            <div className='desktop-homePage-testimonial-child-img'>
                                <div className='desktop-homePage-testimonial-child-img-child'>
                                    <img src={require('@/assets/testimonial/1.jpg')} />
                                </div>
                            </div>
                            <div className='desktop-homePage-testimonial-child-content'>
                                <div className='desktop-homePage-testimonial-child-content-top'>
                                    <div className='desktop-homePage-testimonial-child-content-top-left'>
                                        “
                                    </div>
                                    <div className='desktop-homePage-testimonial-child-content-top-center'>
                                        Thank you EDU DOTCOM helping me to fulfill my dream to study in Korea.
                                        The counselor has a strong responsibility and great experience to answer all my questions which I raised to them.
                                        I wish to take this opportunity to express our sincerely gratitude to your guys!
                                    </div>
                                    <div className='desktop-homePage-testimonial-child-content-top-right'>
                                        ”
                                    </div>
                                </div>
                                <div className='desktop-homePage-testimonial-child-content-bottom'>
                                    <div className='desktop-homePage-testimonial-child-content-bottom-name'>
                                        Chloe Ng
                                    </div>
                                    <div className='desktop-homePage-testimonial-child-content-bottom-school'>
                                        <div className='desktop-homePage-testimonial-child-content-bottom-school-img'>
                                            <img src={require('@/assets/testimonial/korea.png')} />
                                        </div> Kyung Hee University
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='desktop-homePage-testimonial-viewMore' onClick={() => router.push('/introduction/testimonial')}>
                            <RightOutlined />
                        </div>
                    </div>
                    {/* <div className='desktop-homePage-news'>
                    <div className='desktop-homePage-news-title'>
                        －{formatMessage({ id: 'news and events' })} －
                    </div>
                </div> */}

                    {/* <div className='newsAndEvents'>
                    {
                        Object.values(state.newsAndEvents).map((list, key) => {
                            return <div
                                className={state.activeKey == key ? 'newsAndEvents-box active' : 'newsAndEvents-box'}
                                onClick={() => {
                                    let newKey = key
                                    if (state.activeKey == key) {
                                        newKey = -1
                                    }
                                    setState({ activeKey: newKey })
                                }}
                            >
                                <div className='newsAndEvents-box-child'>

                                    <div className='newsAndEvents-box-child-img'>
                                        <img src={list.img} />
                                    </div>

                                    <div className='newsAndEvents-box-child-detail'>
                                        <div className='newsAndEvents-box-child-detail-type'>
                                            {list.type}
                                        </div>
                                        <div className='newsAndEvents-box-child-detail-date'>
                                            {list.date}
                                        </div>
                                        <div className='newsAndEvents-box-child-detail-topic'>
                                            {list.topic}
                                        </div>

                                        <div className='newsAndEvents-box-child-detail-viewMore'>
                                            {formatMessage({ id: 'view more' })}
                                            {
                                                key == state.activeKey
                                                    ? <CaretUpOutlined />
                                                    : <CaretDownOutlined />
                                            }
                                        </div>
                                    </div>
                                </div>

                                <div className='newsAndEvents-box-content'>
                                    {list.content}
                                </div>
                            </div>
                        })
                    }

                </div> */}
                </div>

            </div>
        </DesktopLoading >
    </>
}
