import React, { useEffect, useState } from "react";
import { Modal, List, Button, WhiteSpace, WingBlank, Icon } from 'antd-mobile';
import router from 'umi/router';

export default () => {
    const [state, setOriState] = useState({
        visible: true
    })
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <Modal
            visible={state.visible}
            transparent
            maskClosable={false}
            footer={[{
                text: 'OK',
                onPress: () => {
                    setState({ visible: false })
                    router.push('/home')
                }
            }]}
        >
            123
        </Modal>
    )
}