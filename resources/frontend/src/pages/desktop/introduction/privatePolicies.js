import React, { useState } from 'react';
import router from 'umi/router';
import './privatePolicies.less'
import { formatMessage } from 'umi/locale';
import { useDispatch, useSelector } from 'react-redux'
import { Divider } from 'antd';
import DesktopTitleMenu from '@/components/desktop-title-menu'

export default (props) => {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({

    })
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <div className='desktop-content'>
            <div className='desktop-introduction-img'>
                <img src={require('@/assets/title_img/about_us.jpg')} />
            </div>

            <div className='desktop-introduction-content'>
                <div className='desktop-introduction-content-left'>
                    <div className='desktop-introduction-policies'>
                        <div className='desktop-introduction-policies-maintitle'>
                            {formatMessage({ id: 'Private Policies' })}
                        </div>
                        <Divider className='desktop-introduction-policies-divider' />

                        <div className='desktop-introduction-policies-content'>
                            EDU DOTCOM is complying with the Personal Data Protection Act 2010 of Malaysia.
                            This privacy policy describes our policies and procedures on the collection, use and disclosure of your information when you use the service or visit to our website.
                            By using the service, you agree to the collection and use of information in accordance with this privacy policy.
            </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Interpretation and Definitions
            </div>

                        <div className='desktop-introduction-policies-important'>
                            Interpretation
            </div>

                        <div className='desktop-introduction-policies-content'>
                            The word of which the initial letter is capitalized have meanings under the following conditions.
                            The following definitions shall have the same meaning regardless of whether they appear in singular or in plural.
            </div>

                        <div className='desktop-introduction-policies-important'>
                            Definitions
            </div>

                        <div className='desktop-introduction-policies-content'>
                            For the purposes of this Privacy Policy:<br />
                •	Account means a unique account created for you to access our service or parts of our service.<br />
                •	Company (referred to as either “the company”, “we”, “us” or “our” in this agreement) refers to EDU DOTCOM.<br />
                •	Cookies are small files that are placed on your computer, mobile device or any other device by a website, containing the details of your browsing history on that website among its many uses.<br />
                •	Country refers to Malaysia.<br />
                •	Device means any device that can access the service such as a computer, a cellphone or a digital tablet.<br />
                •	Personal Date is any information that relates to an identified or identifiable individual.<br />
                •	Service refers to the website.<br />
                •	Service Provider means any natural or legal person who processes the data on behalf of the company. It refers to third-party companies or individuals employed by the company to facilitate the service, to provide the service on behalf of the company, to perform services related to the service or to assist the company in analyzing how the service is used.<br />
                •	Third-party Social Media Service refers to any website or any social network website through which a user can log in or create an account to use the service.<br />
                •	Usage Data refers to data collected automatically, either generated by the use of the service or from the service infrastructure itself.<br />
                •	Website refers to EDU DOTCOM, accessible from www.edudotcm.my<br />
                •	You means the individual accessing or using the service, or the company, or other legal  entity on behalf of which such individual is accessing or using the service, as applicable.<br />

                        </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Types of Data Collected
            </div>

                        <div className='desktop-introduction-policies-important'>
                            Personal Data
            </div>

                        <div className='desktop-introduction-policies-content'>
                            While using our service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you.
                Personally identifiable information may include, but is not limited to:<br />
                •	Email address<br />
                •	First name and last name<br />
                •	Phone number<br />
                •	Usage Data<br />
                        </div>

                        <div className='desktop-introduction-policies-important'>
                            Usage Data
            </div>

                        <div className='desktop-introduction-policies-content'>
                            Usage Data is collected automatically when using the service.
                            Usage Data may include such as your Device’s Internet Protocol address, browser type, browser version, the pages of our service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers and other diagnostic data.
                            When you access the service by or through a mobile device we may certain information automatically, including, but not limited to, the type of mobile device you use, your mobile device unique ID, the IP address of your mobile device, your mobile operating system, the type of mobile internet browser you use unique device identifiers and other diagnostic data.
                            We may also collect information that your browser sends whenever you visit our service or when you access the service by or through a mobile device.
            </div>

                        <div className='desktop-introduction-policies-important'>
                            Information from Third-Party Social Media Services
            </div>

                        <div className='desktop-introduction-policies-content'>
                            EDU DOTCOM allows you to create an account and log in to use the service through the following third-party social media services:<br />
                •	Google<br />
                •	Facebook<br />
                If you decide to register through or otherwise grant us access to a third-party social media service, we may collect personal data that is already associated with your third-party social media service’s account, such as your name, your email address, your activities or your contact list associated with that account.
                You may also have the options of sharing additional information with the company through your third-party social media service’s account. If you choose to provide such information and personal data, during registration or otherwise, you are giving the company permission to use, share, and store it in a manner consistent with this privacy policy.
            </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Tracking Technologies and Cookies
            </div>

                        <div className='desktop-introduction-policies-content'>
                            We use cookies and similar tracking technologies to track the activity on our service and store certain information. Tracking technologies used are beacons, tags, and scripts to collect and track information and to improve and analyze our service. The technologies we use may include:<br />
                •	Cookies or browser Cookies. A cookie is a small file placed on your device. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do no accept cookies, you may not be able to use some parts of our service. Unless you have adjusted your browser setting so that it will refuse cookies, our service may use cookies.<br />
                •	Flash Cookies. Certain features of our Service may use local stored objects (or Flash Cookies) to collect and store information about your preference or your activity on our service. Flash cookies are not managed by the same browser settings as those used for Browser settings as those used for Browser Cookies.<br />
                •	Web Beacons. Certain sections of our service and our emails may contain small electronic files known as web beacons (also referred to as clear gifs, pixel tags, and single-pixel gifs) that permit the company, for example, to count users who have visited those pages or opened an email and for other related website statistics )for example, recording the popularity of a certain section and verifying system and server integrity).<br />
                Cookies can be “Persistent” or “Session” cookies. Persistent cookies remain on your personal computer or mobile device when you go offline, while session cookies are deleted as soon as you close your web browser.
            </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Use of Your Personal Data
            </div>

                        <div className='desktop-introduction-policies-content'>
                            EDU DOTCOM may use personal data for the following purposes:<br />
                •	To provide and maintain our service, including to monitor the usage of our service.<br />
                •	To manage your account: to manage your registration as a user of the service. The personal data you provide can give you access to different functionalities of the service that are available to you as a registered user.<br />
                •	For the performance of a contract: the development, compliance and undertaking of the purchase contract for the products, items or services you have purchased or any other contract with us through the services.<br />
                •	To contact you: To contact you by email, telephone calls, SMS, or other equivalent forms or electronic communication, such as a mobile application’s push notifications regarding updates or information communications related to the functionalities, products or contracted services.<br />
                •	Including the security updates when necessary or reasonable for their implementation.<br />
                •	To provide you with news, special offers and general information about other gods, services and events which we offer that are similar to those that you have already purchased or enquired about unless you have opted not to receive such information.<br />
                •	To manage your requests: To attend and manage your requests to us.<br />
                •	For business transfers: We may use your information to evaluate or conduct a merger, divestiture, restructuring, reorganization, dissolution, or other sale or transfer of some or all of our assets, whether as a going concern or as part of bankruptcy, liquidation, or similar proceeding, in which personal date held by us about our service users is among the assets transferred.<br />
                •	For other purposes: We may use your information for other purposes, such as data analysis, identifying usage trends, determining the effectiveness of our promotional campaigns and t evaluate and improve our service, products, services, marketing and your experience.<br />
                We may share your personal information in the following situations:<br />
                •	With service providers: We may share your personal information with service providers to monitor and analyze the use of our service, to contact you.<br />
                •	For business transfers: We may share or transfer your personal information in connection with, or during negotiations of, any merger, sale of company assets, financing, or acquisition of all or a portion of our business to another company.<br />
                •	With Affiliates: We may use your information with our affiliates, in which case we will require those affiliates to honor this privacy policy. Affiliates include our parent company and any other subsidiaries, joint venture partners or other companies that we control or that are under common control with us.<br />
                •	With business partners: We may share your information with our business partners to offer you certain products, services or promotions.<br />
                •	With other users: when you share personal information or otherwise interact in the public areas with other users, such information may be viewed by all users and may be publicly distributed outside. If you interact wit other users or register through a Third-Party Social Media Service, your contacts on the Third-Party social media service may see your name, profile, pictures and description of your activity. Similarly, other users will be able to view descriptions of your activity, communicate with you and view your profile.<br />
                •	With your consent: We may disclose your personal information for nay other purpose with your consent.<br />
                        </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Retention of Your Personal Data
            </div>

                        <div className='desktop-introduction-policies-content'>
                            EDU DOTCOM will remain your personal data only for as long as is necessary for the purposes set up in this privacy policy.
                            We will retain and use your personal data to the extent necessary to comply with our legal obligations, resolve disputes, and enforce our legal agreements and policies.

                            The company will also retain usage data for internal analysis purposes.
                            Usage Data is generally retained for a shorter period of time, except when this data is used to strengthen the security or to improve the functionality of our service, or we are legally obligated to retain this data for longer time periods.
            </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Transfer of your Personal Data
            </div>

                        <div className='desktop-introduction-policies-content'>
                            Your information, including personal data, is processed at the company’s operating offices and in any other places where the parties involved in the processing are located.
                            Your consent to this privacy policy followed by your submission of such information represents your agreement to that transfer.
                            EDU DOTCOM will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy and no transfer of your personal data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.
            </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Disclosure of Your Personal Data
            </div>

                        <div className='desktop-introduction-policies-important'>
                            Business Transactions
            </div>

                        <div className='desktop-introduction-policies-content'>
                            If EDU DOTCOM is involved in a merger, acquisition or asset sale, your personal data may be transferred.
                            We will provide notice before your personal data is transferred and becomes subject to a different privacy policy.
            </div>

                        <div className='desktop-introduction-policies-important'>
                            Law enforcement
            </div>

                        <div className='desktop-introduction-policies-content'>
                            Under the certain circumstances, EDU DOTCOM may be required to disclose your personal data if required to do so by law or in response to valid request by public authorities.
            </div>

                        <div className='desktop-introduction-policies-important'>
                            Other legal requirements
            </div>

                        <div className='desktop-introduction-policies-content'>
                            EDU DOTCOM may disclose your personal data in the good faith belief that such action is necessary to:<br />
                •	Comply with a legal obligation<br />
                •	Protect and defend the rights or property of the company<br />
                •	Prevent or investigate possible wrongdoing in connection with the service<br />
                •	Protect the personal safety of users of the service or the public<br />
                •	Protect against legal liability<br />
                        </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Security of Your personal Data
            </div>

                        <div className='desktop-introduction-policies-content'>
                            The security of your personal data is important to us, nut remember that no method of transmission over the internet, or method of electronic storage is 10% secure.
                            While we strive to use commercially acceptable means to protect your personal data, we cannot guarantee its absolute security.
            </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Links to Other Websites
            </div>

                        <div className='desktop-introduction-policies-content'>
                            Our service may contain links to other websites that are not operated by us.
                            If you click on a third party link, you will be directed to that third party’s site. We strongly advise you to review the privacy policy of every site you visit.
                            We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.
            </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Changes to this Privacy Policy
            </div>

                        <div className='desktop-introduction-policies-content'>
                            We may update our privacy policy from time to time. We will notify you of any changes by posting the new privacy policy on this page.
                            You are advised to review this privacy policy periodically for any changes. Changes to this privacy policy are effective when they are posted on this page.
            </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Intellectual Property
            </div>

                        <div className='desktop-introduction-policies-content'>
                            All the contents showing in our website including graphics, logos or trademarks, etc, do not grant you any rights to download or reproduce in other places.
                            EDU DOTCOM and third-party reserves the rights and copyrights to those content and informations.
            </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Limitations of Liability
            </div>

                        <div className='desktop-introduction-policies-content'>
                            Our website is provided on an “as is” and “as available” basis. By using our service, you should understand and agree that we do not guarantee the accuracy, relevancy or completeness of any information in or provided on or from our website. We are not responsible for any errors or omissions for the results obtained from the use of such information.
                            Your use and participation in our website and services are solely at your own risk.  We make no warranty that our website will meet your requirements, or that access to our website will be uninterrupted, timely, secure, or error free. All the information and contents which show up in our service should be treated as references and we encourage you to verify directly to us or relevant third parties.
            </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Governing Law and Jurisdiction
            </div>

                        <div className='desktop-introduction-policies-content'>
                            This private policy is governed by Malaysia’ laws. Any dispute shall be subject to the non-exclusive jurisdiction of the Malaysian courts.
            </div>

                        <div className='desktop-introduction-policies-subTitle'>
                            Contact Us
            </div>

                        <div className='desktop-introduction-policies-content'>
                            If you have any question about this privacy policy, you can contact us via enquiry@edudotcom.my
            </div>
                    </div>

                </div>
                <div className='desktop-introduction-content-right'>
                    <DesktopTitleMenu
                        title={formatMessage({ id: 'introduction' })}
                        menu={[
                            {
                                label: formatMessage({ id: 'about us' }),
                                url: '/introduction/aboutUs'
                            },
                            {
                                label: formatMessage({ id: 'newsAndEvents' }),
                                url: '/introduction/newsAndEvents'
                            },
                            {
                                label: formatMessage({ id: 'our partners' }),
                                url: '/introduction/ourPartners'
                            },
                            {
                                label: formatMessage({ id: 'Testimonial' }),
                                url: '/introduction/testimonial'
                            },
                            {
                                label: formatMessage({ id: 'Private Policies' }),
                                url: '/introduction/privatePolicies'
                            }
                        ]}
                    />
                </div>
            </div>
        </div>

    );
}