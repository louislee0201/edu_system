import React, { useState } from 'react';
import router from 'umi/router';
import Desktop from '@/pages/desktop'
import './testimonial.less'
import { formatMessage } from 'umi/locale';
import { Divider, Tag } from 'antd';
import { useDispatch, useSelector } from 'react-redux'
import { UserOutlined, CaretUpOutlined, RightOutlined } from '@ant-design/icons';
import DesktopTitleMenu from '@/components/desktop-title-menu'

export default (props) => {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({

    })
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <div className='desktop-content'>
            <div className='desktop-introduction-img'>
                <img src={require('@/assets/title_img/about_us.jpg')} />
            </div>

            <div className='desktop-introduction-content'>
                <div className='desktop-introduction-content-left'>
                    <div className='desktop-introduction-testimonial'>
                        <div className='desktop-introduction-testimonial-maintitle'>
                            {formatMessage({ id: 'Testimonial' })}
                        </div>
                        <Divider className='desktop-introduction-testimonial-divider' />
                        <div className='desktop-introduction-testimonial-content'>

                            <div className='desktop-introduction-testimonial-content-korea'>
                                <div className='desktop-introduction-testimonial-content-korea-child'>
                                    <div className='desktop-introduction-testimonial-content-korea-child-left'>
                                        <div className='desktop-introduction-testimonial-content-korea-child-left-img'>
                                            <img src={require('@/assets/testimonial/1.jpg')} />
                                        </div>
                                    </div>
                                    <div className='desktop-introduction-testimonial-content-korea-child-right'>
                                        <div className='desktop-introduction-testimonial-content-korea-child-right-top'>
                                            <div className='desktop-introduction-testimonial-content-korea-child-right-top-box1'>
                                                “
                                            </div>
                                            <div className='desktop-introduction-testimonial-content-korea-child-right-top-box2'>
                                                Thank you EDU DOTCOM helping me to fulfill my dream to study in Korea.
                                                The counselor has a strong responsibility and great experience to answer all my questions which I raised to them.
                                                I wish to take this opportunity to express our sincerely gratitude to your guys!
                                             </div>
                                            <div className='desktop-introduction-testimonial-content-korea-child-right-top-box3'>
                                                ”
                                            </div>
                                        </div>
                                        <div className='desktop-introduction-testimonial-content-korea-child-right-bottom'>
                                            <div className='desktop-introduction-testimonial-content-korea-child-right-bottom-name'>
                                                Chloe Ng
                                            </div>
                                            <div className='desktop-introduction-testimonial-content-korea-child-right-bottom-country'>
                                                <div className='desktop-introduction-testimonial-content-korea-child-right-bottom-country-img'>
                                                    <img src={require('@/assets/testimonial/korea.png')} />
                                                </div> Kyung Hee University
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                {/* <div className='desktop-homePage-testimonial-viewMore' onClick={() => router.push('/introduction/testimonial')}>
                                    <RightOutlined />
                                </div> */}
                            </div>

                            <div className='desktop-introduction-testimonial-content-singapore'>
                                <div className='desktop-introduction-testimonial-content-singapore-child'>
                                    <div className='desktop-introduction-testimonial-content-singapore-child-left'>
                                        <div className='desktop-introduction-testimonial-content-singapore-child-left-img'>
                                            <img src={require('@/assets/testimonial/2.jpg')} />
                                        </div>
                                    </div>
                                    <div className='desktop-introduction-testimonial-content-singapore-child-right'>
                                        <div className='desktop-introduction-testimonial-content-singapore-child-right-top'>
                                            <div className='desktop-introduction-testimonial-content-singapore-child-right-top-box1'>
                                                “
                                            </div>
                                            <div className='desktop-introduction-testimonial-content-singapore-child-right-top-box2'>
                                                Your guys are the one who made me become a person which I never think of it!
                                                I sincerely thanks to EDU DOTCOM for being patiently to explain and analyzing various possible options for my further studies when I graduated from high school.
                                                I hope that more students will meet you in the future.
                                             </div>
                                            <div className='desktop-introduction-testimonial-content-singapore-child-right-top-box3'>
                                                ”
                                            </div>
                                        </div>
                                        <div className='desktop-introduction-testimonial-content-singapore-child-right-bottom'>
                                            <div className='desktop-introduction-testimonial-content-singapore-child-right-bottom-name'>
                                                Lee Kee Hao
                                            </div>
                                            <div className='desktop-introduction-testimonial-content-singapore-child-right-bottom-country'>
                                                <div className='desktop-introduction-testimonial-content-singapore-child-right-bottom-country-img'>
                                                    <img src={require('@/assets/testimonial/singapore.png')} />
                                                </div> James Cook University (Singapore)
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                {/* <div className='desktop-homePage-testimonial-viewMore' onClick={() => router.push('/introduction/testimonial')}>
                                    <RightOutlined />
                                </div> */}
                            </div>

                        </div>
                    </div>
                </div>
                <div className='desktop-introduction-content-right'>
                    <DesktopTitleMenu
                        title={formatMessage({ id: 'introduction' })}
                        menu={[
                            {
                                label: formatMessage({ id: 'about us' }),
                                url: '/introduction/aboutUs'
                            },
                            {
                                label: formatMessage({ id: 'newsAndEvents' }),
                                url: '/introduction/newsAndEvents'
                            },
                            {
                                label: formatMessage({ id: 'our partners' }),
                                url: '/introduction/ourPartners'
                            },
                            {
                                label: formatMessage({ id: 'Testimonial' }),
                                url: '/introduction/testimonial'
                            },
                            {
                                label: formatMessage({ id: 'Private Policies' }),
                                url: '/introduction/privatePolicies'
                            }
                        ]}
                    />
                </div>
            </div>
        </div>

    );
}