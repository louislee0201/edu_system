import React, { useState } from 'react';
import router from 'umi/router';
import Desktop from '@/pages/desktop'
/* import './newsAndEvents.less' */
import { formatMessage } from 'umi/locale';
import { Divider, Tag } from 'antd';
import { useDispatch, useSelector } from 'react-redux'
import DesktopTitleMenu from '@/components/desktop-title-menu'

export default (props) => {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
    })

    const newsAndEvents = useSelector(state => state.config.newsAndEvents)
    
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <div className='desktop-content'>
            <div className='desktop-introduction-img'>
                <img src={require('@/assets/title_img/about_us.jpg')} />
            </div>

            <div className='desktop-introduction-content'>
                <div className='desktop-introduction-content-left'>
                    <div className='desktop-introduction-newsAndEvents'>
                        <div className='desktop-introduction-newsAndEvents-maintitle'>
                            {formatMessage({ id: 'newsAndEvents' })}
                        </div>
                        <Divider className='desktop-introduction-newsAndEvents-divider' />
                        <div className='desktop-introduction-newsAndEvents-content'>
                            <div className='desktop-introduction-newsAndEvents-content-list'>
                                {
                                    Object.values(newsAndEvents).map((s, k) => {
                                        return <div
                                            key={k}
                                            className='desktop-introduction-newsAndEvents-content-list-child'
                                            onClick={() => router.push('/introduction/newsAndEvents/view?_ar=' + k)}
                                        >
                                            <div className='desktop-introduction-newsAndEvents-content-list-child-img'>
                                                <img src={s.img} />
                                            </div>
                                            <div className='desktop-introduction-newsAndEvents-content-list-child-topic' style={{ WebkitBoxOrient: 'vertical' }}>
                                                {s.topic}
                                            </div>
                                            <div className='desktop-introduction-newsAndEvents-content-list-child-date'>
                                                {s.date}
                                            </div>
                                            
                                        </div>
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div className='desktop-introduction-content-right'>
                    <DesktopTitleMenu
                        title={formatMessage({ id: 'introduction' })}
                        menu={[
                            {
                                label: formatMessage({ id: 'about us' }),
                                url: '/introduction/aboutUs'
                            },
                            {
                                label: formatMessage({ id: 'newsAndEvents' }),
                                url: '/introduction/newsAndEvents'
                            },
                            {
                                label: formatMessage({ id: 'our partners' }),
                                url: '/introduction/ourPartners'
                            },
                            {
                                label: formatMessage({ id: 'Testimonial' }),
                                url: '/introduction/testimonial'
                            },
                            {
                                label: formatMessage({ id: 'Private Policies' }),
                                url: '/introduction/privatePolicies'
                            }
                        ]}
                    />
                </div>
            </div>
        </div>

    );
}