import React, { useState, useEffect } from 'react';
import router from 'umi/router';
import Desktop from '@/pages/desktop'
import './view.less'
import { formatMessage } from 'umi/locale';
import { Divider, Tag, Skeleton } from 'antd';
import { useDispatch, useSelector } from 'react-redux'
import DesktopTitleMenu from '@/components/desktop-title-menu'

export default (props) => {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        arKey: -1,
        loading: true
    })

    const newsAndEvents = useSelector(state => state.config.newsAndEvents)

    useEffect(() => {
        const related_id = parseInt(props.location.query._ar);
        if (related_id >= 0) {
            setState({ arKey: related_id, loading: false })
        }
    }, []);

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <div className='desktop-content'>
            <div className='desktop-introduction-img'>
                <img src={require('@/assets/title_img/about_us.jpg')} />
            </div>

            <div className='desktop-introduction-content'>
                <div className='desktop-introduction-content-left'>
                    {
                        state.loading
                            ? <Skeleton active />
                            : <div className='desktop-introduction-newsAndEvents-view'>
                                <div className='desktop-introduction-newsAndEvents-view-maintitle'>
                                    {newsAndEvents[state.arKey].topic}
                                </div>
                                <Divider className='desktop-introduction-newsAndEvents-view-divider' />
                                <div className='desktop-introduction-newsAndEvents-view-content'>
                                    <Tag color={newsAndEvents[state.arKey].color}>{formatMessage({ id: newsAndEvents[state.arKey].type })}</Tag>
                                    <div className='desktop-introduction-newsAndEvents-view-content-date'>{newsAndEvents[state.arKey].date}</div>
                                    <div className='desktop-introduction-newsAndEvents-view-content-pic'>
                                        <img src={newsAndEvents[state.arKey].img} />
                                    </div>
                                    <div className='desktop-introduction-newsAndEvents-view-content-content'>{newsAndEvents[state.arKey].content}</div>
                                </div>
                            </div>
                    }

                </div>
                <div className='desktop-introduction-content-right'>
                    <DesktopTitleMenu
                        title={formatMessage({ id: 'introduction' })}
                        menu={[
                            {
                                label: formatMessage({ id: 'about us' }),
                                url: '/introduction/aboutUs'
                            },
                            {
                                label: formatMessage({ id: 'newsAndEvents' }),
                                url: '/introduction/newsAndEvents'
                            },
                            {
                                label: formatMessage({ id: 'our partners' }),
                                url: '/introduction/ourPartners'
                            },
                            {
                                label: formatMessage({ id: 'Testimonial' }),
                                url: '/introduction/testimonial'
                            },
                            {
                                label: formatMessage({ id: 'Private Policies' }),
                                url: '/introduction/privatePolicies'
                            }
                        ]}
                    />
                </div>
            </div>
        </div>

    );
}