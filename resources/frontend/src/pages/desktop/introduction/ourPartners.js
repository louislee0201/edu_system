import React, { useState } from 'react';
import router from 'umi/router';
import Desktop from '@/pages/desktop'
import './ourPartners.less'
import { formatMessage } from 'umi/locale';
import { Divider, Tabs } from 'antd';
import { useDispatch, useSelector } from 'react-redux'
import DesktopTitleMenu from '@/components/desktop-title-menu'

export default (props) => {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        schoolListJapan: [
            {
                school_name: 'Akamonkai Japanese Language School',
                url: '/viewSchool?school_id=28'
            },
            {
                school_name: 'Hokkaido University',
                url: '/viewSchool?school_id=39',
                note: '* Only for Bachelor Programs (ISP & MJSP)'
            },
            {
                school_name: 'Japanese Language School affiliated with Tokyo International University',
                url: '/viewSchool?school_id=55',
            },
            {
                school_name: 'Kobe YMCA Gakuin College',
                url: '/viewSchool?school_id=49',
                note: '* Only for Faculty of Japanese Language'
            },
            {
                school_name: 'Kudan Institute of Japanese Language & Culture'
            },
            {
                school_name: 'Kyushu Designer Gakuin'
            },
            {
                school_name: 'Kyushu Institute of Tourism'
            },
            {
                school_name: 'Kyushu School of Business'
            },
            {
                school_name: 'Kyoto University of Advanced Science',
                url: '/viewSchool?school_id=47',
                note: '* Only for Faculty of Engineering’s english track program'
            },
            {
                school_name: 'Kyushu Visual Arts'
            },
            {
                school_name: 'Meros Language School',
                url: '/viewSchool?school_id=29'
            },
            {
                school_name: 'Nagoya Designer Gakuin'
            },
            {
                school_name: 'Nagoya Institute of Tourism'
            },
            {
                school_name: 'Nagoya School of Business'
            },
            {
                school_name: 'Nagoya Visual Arts'
            },
            {
                school_name: 'Nihongo Center'
            },
            {
                school_name: 'Nippon Language Academy'
            },
            {
                school_name: 'Okayama Institute of Languages'
            },
            {
                school_name: 'Osaka Business College'
            },
            {
                school_name: 'Osaka Designer College'
            },
            {
                school_name: 'Osaka Institute of Tourism'
            },
            {
                school_name: 'Osaka Visual Arts'
            },
            {
                school_name: 'Osaka YMCA International College',
                url: '/viewSchool?school_id=48',
                note: '* Only for Bachelor Programs (CANCEL)'
            },
            {
                school_name: 'Osaka YMCA Japanese Language School',
                url: '/viewSchool?school_id=35',
            },
            {
                school_name: 'Ritsumeikan Asia Pacific University',
                url: '/viewSchool?school_id=46',
                note: '* Only for Bachelor Programs'
            },
            {
                school_name: 'Sendagaya Japanese Institute',
                url: '/viewSchool?school_id=59',
            },
            // {
            //     school_name: 'System Toyo Gaigo'
            // },
            {
                school_name: 'Tokyo Cool Japan'
            },
            {
                school_name: 'Tokyo Designer Gakuin College',
                url: '/viewSchool?school_id=42',
            },
            {
                school_name: 'Tokyo Designer Gakuin',
                url: '/viewSchool?school_id=42'
            },
            {
                school_name: 'Tokyo Institute of Tourism'
            },
            {
                school_name: 'Tokyo School of Business'
            },
            {
                school_name: 'Tokyo Visual Arts'
            },
            {
                school_name: 'Yamano Japanese Language School',
                url: '/viewSchool?school_id=56'
            },
            {
                school_name: 'Tokyo Design Techonology Center *',
                url: '/viewSchool?school_id=57'
            },
            {
                school_name: 'ISI Japanese Language School (Kyoto)',
                url: '/viewSchool?school_id=52'
            },
            {
                school_name: 'ISI Japanese Language School (Takadanobaba)',
                url: '/viewSchool?school_id=50'
            },
            {
                school_name: 'ISI Japanese Language School (Nagano)',
                url: '/viewSchool?school_id=53'
            },
            {
                school_name: 'ISI Japanese Language School (Ikebukuro)',
                url: '/viewSchool?school_id=51'
            },
            {
                school_name: 'Tokyo School of Music Shibuya *',
                url: '/viewSchool?school_id=58'
            },
            {
                school_name: 'Kansai College of Business & Languages',
                url: '/viewSchool?school_id=45'
            },
            {
                school_name: 'ISI Career and Language Academy (Harajuku)',
                url: '/viewSchool?school_id=54'
            },
            {
                school_name: 'Tokyo Adachi Japanese Language School',
            },
            {
                school_name: 'Sapporo School of Music and Dance . Broadcast',
            },
            {
                school_name: 'Sapporo College of Design and Technology',
            },
            {
                school_name: 'Sendai College of Design and Technology',
            },
            {
                school_name: 'Sendai College of Eco and Animals',
            },
            {
                school_name: 'Sendai School of Music and Dance',
            },
            {
                school_name: 'Sendai College of AgriTech and Culinary Technology',
            },
            {
                school_name: 'Sendai College of Medical Health and Sports',
            },
            {
                school_name: 'Tokyo Design Technology Center College',
            },
            {
                school_name: 'Tokyo Dance and Actors Performing Arts School',
            },
            {
                school_name: 'Tokyo Communication Arts College',
            },
            {
                school_name: 'TCA Tokyo College of Eco and Animals',
            },
            {
                school_name: 'Tokyo School of Music and Dance',
            },
            {
                school_name: 'Tokyo Film Center College of Arts',
            },
            {
                school_name: 'Tokyo College of Anime and e-Sports',
            },
            {
                school_name: 'Tokyo Hotel, Tourism and Hospitality College',
            },
            {
                school_name: 'Nagoya College of Design and Technology',
            },
            {
                school_name: 'Nagoya College of Eco and Animals',
            },
            {
                school_name: 'Nagoya School of Music and Dance',
            },
            {
                school_name: 'Nagoya College of Agritech and Culinary Technology',
            },
            {
                school_name: 'Nagoya College of Medical Health and Sports',
            },
            {
                school_name: 'OCA Osaka College of Design and Technology',
            },
            {
                school_name: 'Osaka College of Eco and Animals',
            },
            {
                school_name: 'Osaka School of Music',
            },
            {
                school_name: 'Osaka Dance & Actors Performing Arts School',
            },
            {
                school_name: 'Broadcasting Arts College',
            },
            {
                school_name: 'Osaka College of Animation and e-Sports',
            },
            {
                school_name: 'Osaka Hotel.Tourism & Wedding college',
            },
            {
                school_name: 'Osaka Coiiege of Agri Tech,Horti Tech and Food Tech',
            },
            {
                school_name: 'Kyoto College of Design and Technology',
            },
            {
                school_name: 'Kyoto College of Medical Health',
            },
            {
                school_name: 'Kobe Koyo School of Music and Dance',
            },
            {
                school_name: 'Fukuoka College of Design and Technology',
            },
            {
                school_name: 'Fukuoka College of Eco and Animals',
            },
            {
                school_name: 'Fukuoka School of Music and Dance',
            },
            {
                school_name: 'Fukuoka College of AgriTech and Culinary Technology',
            },
            {
                school_name: 'Fukuoka College of Medical Health and Sports',
            },
            {
                school_name: 'Fukuoka Hotel, Tourism and Wedding College',
            },
            {
                school_name: 'Kyushu Eisu Gakkan',
            },
            {
                school_name: 'Kyushu Eisu Gakkan Japanese Language School',
            },
        ],
        schoolListSingapore: [
            {
                school_name: 'HTMi Hotel and Tourism Management Institute (Singapore)',
                url: '/viewSchool?school_id=40'
            },
            {
                school_name: 'Kaplan Higher Education Academy',
                url: '/viewSchool?school_id=27'
            },
            {
                school_name: 'Management Development Institute of Singapore',
                url: '/viewSchool?school_id=41'
            },
            {
                school_name: 'Raffles College of Higher Education',
                url: '/viewSchool?school_id=44'
            },
        ],
    })
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <div className='desktop-content'>
            <div className='desktop-introduction-img'>
                <img src={require('@/assets/title_img/about_us.jpg')} />
            </div>

            <div className='desktop-introduction-content'>
                <div className='desktop-introduction-content-left'>
                    <div className='desktop-introduction-ourPartners'>
                        <div className='desktop-introduction-ourPartners-maintitle'>
                            {formatMessage({ id: 'our partners' })}
                        </div>
                        <Divider className='desktop-introduction-ourPartners-divider' />
                        <div className='desktop-introduction-ourPartners-content'>

                            <Tabs defaultActiveKey="1" size='small' style={{ marginBottom: 32 }}>
                                <Tabs.TabPane tab={formatMessage({ id: 'japan' })} key="1">
                                    <b>
                                        EDU DOTCOM represent below schools for the recruitment and student admissions in Malaysia.
                                    </b>
                                    <div className='desktop-introduction-ourPartners-content-tab'>
                                        {
                                            Object.values(state.schoolListJapan).map((s, k) => {
                                                if (s.url) {
                                                    return <a
                                                        onClick={() => router.push(s.url)}
                                                        className='desktop-introduction-ourPartners-content-tab-child'
                                                    >
                                                        {s.school_name}
                                                        <div className='desktop-introduction-ourPartners-content-tab-child-note'>
                                                            {s.note}
                                                        </div>
                                                    </a>
                                                }
                                                return <div
                                                    className='desktop-introduction-ourPartners-content-tab-child'
                                                >
                                                    {s.school_name}
                                                    <div className='desktop-introduction-ourPartners-content-tab-child-note'>
                                                        {s.note}
                                                    </div>
                                                </div>
                                            })
                                        }
                                    </div>
                                    <br />
                                    <div style={{ borderTop: '1px dashed #888' }} />
                                    <br />
                                    <b>
                                        EDU DOTCOM do not represent below schools for the recruitment and student admissions in Malaysia.
                                        Hence, we have no authorization or permission to handle any enquiry on the behalf of those schools.
                                        For any enquiry, please direct contact to relevant schools.
                                    </b>
                                    <br />
                                    <br />
                                    <a
                                        onClick={() => router.push('/viewSchool?school_id=43')}
                                        className='desktop-introduction-ourPartners-content-tab-child'
                                    >
                                        Digital Hollywood University
                                        <div className='desktop-introduction-ourPartners-content-tab-child-note'>
                                        </div>
                                    </a>
                                </Tabs.TabPane>
                                {/* <Tabs.TabPane tab={formatMessage({ id: 'singapore' })} key="2">
                                    <b>
                                        EDU DOTCOM represent below schools for the recruitment and student admissions in Malaysia.
                                    </b>
                                    <div className='desktop-introduction-ourPartners-content-tab'>
                                        {
                                            Object.values(state.schoolListSingapore).map((s, k) => {
                                                if (s.url) {
                                                    return <a
                                                        onClick={() => router.push(s.url)}
                                                        className='desktop-introduction-ourPartners-content-tab-child'
                                                    >
                                                        {s.school_name}
                                                        <div className='desktop-introduction-ourPartners-content-tab-child-note'>
                                                            {s.note}
                                                        </div>
                                                    </a>
                                                }
                                                return <div
                                                    className='desktop-introduction-ourPartners-content-tab-child'
                                                >
                                                    {s.school_name}
                                                    <div className='desktop-introduction-ourPartners-content-tab-child-note'>
                                                        {s.note}
                                                    </div>
                                                </div>
                                            })
                                        }
                                    </div>
                                    <br />
                                    <div style={{ borderTop: '1px dashed #888' }} />
                                    <br />
                                    <b>
                                        EDU DOTCOM do not represent below schools for the recruitment and student admissions in Malaysia.
                                        Hence, we have no authorization or permission to handle any enquiry on the behalf of those schools.
                                        For any enquiry, please direct contact to relevant schools.
                                    </b>
                                    <br />
                                    <br />
                                    <div style={{ fontStyle: 'italic', color: 'grey' }}>
                                        No content
                                    </div>
                                </Tabs.TabPane> */}
                            </Tabs>
                        </div>
                    </div>
                </div>
                <div className='desktop-introduction-content-right'>
                    <DesktopTitleMenu
                        title={formatMessage({ id: 'introduction' })}
                        menu={[
                            {
                                label: formatMessage({ id: 'about us' }),
                                url: '/introduction/aboutUs'
                            },
                            {
                                label: formatMessage({ id: 'newsAndEvents' }),
                                url: '/introduction/newsAndEvents'
                            },
                            {
                                label: formatMessage({ id: 'our partners' }),
                                url: '/introduction/ourPartners'
                            },
                            {
                                label: formatMessage({ id: 'Testimonial' }),
                                url: '/introduction/testimonial'
                            },
                            {
                                label: formatMessage({ id: 'Private Policies' }),
                                url: '/introduction/privatePolicies'
                            }
                        ]}
                    />
                </div>
            </div>
        </div >

    );
}