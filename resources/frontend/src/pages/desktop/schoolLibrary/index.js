import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './index.less'
import { DownOutlined, FolderAddOutlined } from '@ant-design/icons';
import { Badge } from 'antd-mobile';
import { Menu, Radio, Divider, Button, Tag, Input } from 'antd';
import { formatMessage } from 'umi/locale';
import { PRIMARY_COLOR } from '@/constants/default'
import router from 'umi/router';
import DesktopLoading from '@/components/desktopLoading'
import { HOST } from '@/constants/api'
import { Helmet } from "react-helmet";
import { baseUrl } from "../../../constants/api";

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        loading: false,
        qualificationSubmitList: [],
        proficiencySubmitList: [],
        totalItem: 0,
        dataList: [],
        dataList2: [],
        country_id: 0,
        region_id: 0,
        countryList: [],
        regionList: [],
        regionOption: [],
        schoolName: '',
        characterList: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
        character: 0
        //searchStatus: 'course'
    })
    const courseState = useSelector(state => state.course)

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    useEffect(() => {
        setState({ loading: true })
        if (courseState.searchCourseFilter.countryList.length <= 0) {
            dispatch({
                type: 'course/e_getSelection',
                payload: {}
            }).then((value) => {
                requestData()
                if (value) {
                    dispatch({
                        type: 'course/r_updateState',
                        payload: {
                            searchCourseFilter: {
                                ...courseState.searchCourseFilter,
                                ...value
                            }
                        }
                    })
                }
            })
        } else {
            requestData()
        }
    }, []);

    useEffect(() => {
        if (courseState.searchCourseFilter.countryList.length == 1) {
            setState({ country_id: courseState.searchCourseFilter.countryList[0].id })
        }
    }, [courseState.searchCourseFilter.countryList]);

    function requestData(clickPage = 1) {
        setState({ loading: true })
        dispatch({
            type: 'school/e_getSchool',
            payload: {
                isList: true,
                clickPage: 1,
                filterArr: {
                    //active: true
                }
            }
        }).then((value) => {
            setState({
                loading: false,
                ...value
            })
        })
    }

    function animateValue(end) {
        let current = 0
        let duration = end / 15
        var timer = setInterval(function () {
            current += duration;
            setState({ totalItem: parseInt(current) })
            if (current >= end) {
                setState({ totalItem: parseInt(end) })
                clearInterval(timer);
            }
        }, 25);
    }

    function loopRadio(lists, jsonKey) {
        let data = []
        Object.values(lists).map((list, key) => {
            if (key == 0) {
                data.push({ label: formatMessage({ id: 'all' }), value: 0, disabled: lists.length <= 1 })
            }
            if (jsonKey == 'region' && state.country_id) {

                if (list.country_id == state.country_id) {
                    data.push({ label: list[jsonKey], value: list.id })
                }
            } else if (jsonKey != 'region') {
                data.push({ label: list[jsonKey], value: list.id })
            }
        })
        return data
    }

    useEffect(() => {
        if (state.dataList.length > 0) {
            let dataList2 = []
            state.dataList.map((school) => {
                let cAllow = true
                let rAllow = true
                let nAllow = true

                if (state.country_id) {
                    cAllow = false
                    if (school.country == state.country_id) {
                        cAllow = true
                    }
                }
                if (state.region_id) {
                    rAllow = false
                    if (school.region == state.region_id) {
                        rAllow = true
                    }
                }

                if (state.schoolName) {
                    nAllow = false
                    if (
                        school.school_name.toUpperCase().indexOf(state.schoolName.toUpperCase()) >= 0 ||
                        school.school_name_cn.toUpperCase().indexOf(state.schoolName.toUpperCase()) >= 0
                    ) {
                        nAllow = true
                    }
                }

                if (cAllow && rAllow && nAllow) {
                    dataList2.push(school)
                }
            })
            animateValue(dataList2.length)
            setState({ dataList2 })
        }
    }, [state.country_id, state.region_id, state.schoolName, state.loading]);

    return (
        <>
            <Helmet>
                <meta name="description" content="School" />
                <link rel="canonical" href={baseUrl + "schoolLibrary"} />
            </Helmet>
            <DesktopLoading loading={state.loading}>
                <div className='desktop-content'>
                    <div className='desktop-school_library'>
                        <div className='desktop-school_library-content'>
                            <div className='desktop-school_library-content-title'>
                                {formatMessage({ id: 'school library' })}
                            </div>
                            <Divider className='desktop-school_library-content-divider' />
                            <div className='desktop-school_library-content-filter'>
                                <div className='desktop-school_library-content-filter-country'>
                                    <div className='desktop-school_library-content-filter-country-title'>
                                        {formatMessage({ id: 'country' })}
                                    </div>
                                    {
                                        courseState.searchCourseFilter.countryList &&
                                        <Radio.Group
                                            className='desktop-school_library-content-filter-country-selection'
                                            options={loopRadio(courseState.searchCourseFilter.countryList, 'country')}
                                            onChange={(e) => {
                                                setState({ country_id: e.target.value, region_id: 0 })
                                            }}
                                            value={state.country_id}
                                            optionType="button"
                                        />
                                    }
                                </div>

                                <div className='desktop-school_library-content-filter-region'>
                                    <div className='desktop-school_library-content-filter-region-title'>
                                        {formatMessage({ id: 'region' })}
                                    </div>
                                    {
                                        courseState.searchCourseFilter.regionList &&
                                        <Radio.Group
                                            options={loopRadio(courseState.searchCourseFilter.regionList, 'region')}
                                            onChange={(e) => {
                                                let newCountryId = state.country_id
                                                courseState.searchCourseFilter.regionList.map((list) => {
                                                    if (list.id == e.target.value) {
                                                        newCountryId = list.country_id
                                                    }
                                                })
                                                setState({ region_id: e.target.value, country_id: newCountryId })
                                            }}
                                            value={state.region_id}
                                            optionType="button"
                                        />
                                    }
                                </div>
                                <Input
                                    value={state.schoolName}
                                    placeholder={formatMessage({ id: 'search with school name' })}
                                    onChange={(e) => setState({ schoolName: e.target.value })}
                                />

                                {/* <div className='desktop-school_library-content-filter-character'>
                            {
                                Object.values(state.characterList).map((value, key) => {
                                    return (
                                        <Button
                                            type='ghost'
                                            className={'desktop-school_library-content-filter-character-child' + (key == state.character ? '-active' : '')}
                                            onClick={() => setState({ character: key })}
                                        >
                                            {value}
                                        </Button>
                                    )
                                })
                            }
                        </div> */}
                            </div>
                        </div>
                        <div className='desktop-school_library-total'>
                            <div className='desktop-school_library-total-number'>{state.totalItem} </div>
                            <div className='desktop-school_library-total-available'>
                                {
                                    state.dataList2.length > 1
                                        ? formatMessage({ id: 'schools available' })
                                        : formatMessage({ id: 'school available' })
                                }
                            </div>
                        </div>

                        <div className='desktop-school_library-school_list'>
                            {
                                Object.values(state.dataList2).map((school, key) => {
                                    return (
                                        <div
                                            className='desktop-school_library-school_list-child'
                                            onClick={() => router.push('/viewSchool?school_id=' + school.school_id)}
                                        >
                                            <div className='desktop-school_library-school_list-child-img'>
                                                <img
                                                    src={HOST   /* 'http://api.louis-blog.com/' */ + 'auth/image/school/' + school.picture}
                                                    style={{ borderRadius: '20px', width: '1.75rem', height: '1.22rem' }}
                                                />
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>

                    </div>

                </div>

            </DesktopLoading>
        </>

    )

}
