import React, { useEffect, useState } from "react";
import { List, Picker, Modal, Icon, Button as MButton } from 'antd-mobile';
import { TranslationOutlined, FormOutlined, BankOutlined, ReadOutlined, CustomerServiceOutlined, FilePptOutlined, ExportOutlined } from '@ant-design/icons';
import { formatMessage } from 'umi/locale';
import './index.less';
import { getLanguageList } from '@/tools'
import { useDispatch, useSelector } from 'react-redux'
import { getUserLanguage, getAccLevel, getUserData } from '@/tools/user'
import { setLocale } from 'umi/locale'
import { getLocalStorage, saveLocalStorage } from '@/tools'
import { Button } from 'antd';
import router from 'umi/router';
import { PRIMARY_COLOR } from '@/constants/default'
import Loading from '@/components/loading'

export default function (props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        language: '',
        defaultLanguage: getUserLanguage(),
        name: '',
        email: '',
        acc_level: 1,
        tel: '',
        loading: false
    })
    const configState = useSelector(state => state.config)
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }
    useEffect(() => {
        setState(configState)
    }, [configState]);

    const languageList = getLanguageList()
    return (
        <div className='my'>
            <Loading loading={state.loading} />
            <div
                className='my-beauty'
                onClick={() => {
                    if(configState.account_level == 1)
                    {
                        router.push('/login')
                    }
                    //router.push(configState.account_level == 1 ? '/login' : ''/* '/personal' */)
                }}
            >
                <div className="my-beauty-avatar">
                    <img src={require('@/assets/mobile_avatar.jpg')} style={{ height: 'auto', width: '100%', borderRadius: '50px' }} />
                </div>
                <div className="my-beauty-status" /* onClick={() => router.push(configState.account_level == 1 ? '/login' : '/personal')} */>
                    {
                        configState.account_level == 1 &&
                        <div
                            className='my-beauty-status-loginBtn'
                        >
                            {formatMessage({ id: 'login' })} / {formatMessage({ id: 'register' })}
                        </div>
                    }

                    {
                        configState.account_level == 2 &&
                        <div
                            className='my-beauty-status-info'
                        >
                            <div>
                                {formatMessage({ id: 'hi' })}, {configState.name} !
                            </div>
                            <div>
                                {configState.email}
                            </div>
                        </div>
                    }
                </div>
                <div
                    className='my-beauty-icon'
                //onClick={() => router.push(configState.account_level == 1 ? '/login' : '/personal')}
                >
                    <Icon type='right' size='lg' style={{ flex: '1', textAlign: 'right', marginTop: '10px' }} />
                </div>
            </div>
            {/* <div className='my-info'>
                <div className="my-info_sub">


                </div>

            </div> */}

            <div className='my-scroll'>
                <List className='my-list'>

                    {/* <Picker
                        okText={formatMessage({ id: 'confirm' })}
                        dismissText={formatMessage({ id: 'cancel' })}
                        onOk={(e) => {
                            setState({ defaultLanguage: e[0] })
                        }}
                        onDismiss={() => setLocale(state.defaultLanguage, false)}
                        onPickerChange={(e) => setLocale(e[0], false)}
                        data={
                            Object.keys(languageList).map((key) => {
                                return {
                                    value: key,
                                    label: languageList[key]
                                }
                            })
                        }
                        value={[getUserLanguage()]}
                        extra={languageList[getUserLanguage()]}
                        cols={1}
                        className="forss"
                    >
                        <List.Item
                            className='my-list_child'
                            thumb={<TranslationOutlined className='my-list_child-icon_language' />}
                            arrow="horizontal"
                            onClick={() => {

                            }}
                        >
                            {formatMessage({ id: 'select language' })}
                        </List.Item>

                    </Picker>

                    <List.Item
                        className='my-list_child'
                        thumb={<FilePptOutlined className='my-list_child-icon_language' />}
                        arrow="horizontal"
                        onClick={() => {

                        }}
                    >
                        {formatMessage({ id: 'personal academic record' })}
                    </List.Item>

                    <List.Item
                        className='my-list_child'
                        thumb={<ReadOutlined className='my-list_child-icon_language' />}
                        arrow="horizontal"
                        onClick={() => {

                        }}
                    >
                        {formatMessage({ id: 'course bookmark' })}
                    </List.Item>

                    <List.Item
                        className='my-list_child'
                        thumb={<BankOutlined className='my-list_child-icon_language' />}
                        arrow="horizontal"
                        onClick={() => {

                        }}
                    >
                        {formatMessage({ id: 'school bookmark' })}
                    </List.Item> */}

                    <List.Item
                        className='my-list_child'
                        thumb={<CustomerServiceOutlined className='my-list_child-icon_language' />}
                        arrow="horizontal"
                        onClick={() => {

                        }}
                    >
                        {formatMessage({ id: 'contact us' })}
                    </List.Item>

                    <List.Item
                        className='my-list_child'
                        thumb={<FormOutlined className='my-list_child-icon_language' />}
                        arrow="horizontal"
                        onClick={() => {

                        }}
                    >
                        {formatMessage({ id: 'feedback' })}
                    </List.Item>

                </List>

                {
                    configState.account_level == 2 &&
                    <MButton
                        className='my-logout'
                        onClick={() => {
                            setState({
                                loading: true
                            })
                            dispatch({
                                type: 'config/e_logout',
                                payload: {}
                            }).then((value) => {
                                setState({
                                    loading: false
                                })
                            })
                        }}
                    >
                        <ExportOutlined style={{ color: 'rgb(242,105,49)' }} /> {formatMessage({ id: 'logout' })}
                    </MButton>
                }
            </div>

        </div >
    )

}
