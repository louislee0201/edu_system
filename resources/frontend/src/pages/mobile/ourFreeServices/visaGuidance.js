import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './visaGuidance.less'
import router from 'umi/router';
import { Card, Divider, Tag } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    RightOutlined, LeftOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({

    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    function setKey(activeKey) {
        if (activeKey == state.activeKey) {
            activeKey = ''
        }
        setState({ activeKey })
    }

    return (
        <div className='desktop-visaGuidance'>
            <MobileTopBar />
            <div className='desktop-visaGuidance-img'>
                <img src={require('@/assets/about_us/mobile_ourFreeServices.jpg')} />
            </div>

            <div className='desktop-visaGuidance-title'>
                {formatMessage({ id: 'Visa Guidance' })}
            </div>

            <div className='desktop-visaGuidance-content'>
                This service is limited to students who apply the courses through EDU DOTCOM with free of charges. Our counselor will guide students step-by-step on the preparations and explain on procedures which they need to go through. Students will be able to complete the visa process by themselves. For student living in rural area, we will assist in looking for runner (fee-based) to assist.
            </div>

        </div>
    )
}
