import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './accommadation.less'
import router from 'umi/router';
import { Card, Divider, Tag } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    RightOutlined, LeftOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({

    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    function setKey(activeKey) {
        if (activeKey == state.activeKey) {
            activeKey = ''
        }
        setState({ activeKey })
    }

    return (
        <div className='desktop-accommadation'>
            <MobileTopBar />
            <div className='desktop-accommadation-img'>
                <img src={require('@/assets/about_us/mobile_ourFreeServices.jpg')} />
            </div>

            <div className='desktop-accommadation-title'>
                {formatMessage({ id: 'Accommadation' })}
            </div>

            <div className='desktop-accommadation-content'>
                This service is limited to students who apply the courses through EDU DOTCOM with free of charges. We will assist students to apply for dormitory under university or their coordinate accommodation provider. In certain case, we will also searching for external accommodation options so that students can gives a compare to get a better living place which meets with their expectations.
                <div style={{ marginTop: '10px' }}>
                    **External dormitory options in Japan will be open to the public**
                </div>
            </div>

        </div>
    )
}
