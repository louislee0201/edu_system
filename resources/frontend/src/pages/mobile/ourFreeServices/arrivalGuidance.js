import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './arrivalGuidance.less'
import router from 'umi/router';
import { Card, Divider, Tag } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    RightOutlined, LeftOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({

    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    function setKey(activeKey) {
        if (activeKey == state.activeKey) {
            activeKey = ''
        }
        setState({ activeKey })
    }

    return (
        <div className='desktop-arrivalGuidance'>
            <MobileTopBar />
            <div className='desktop-arrivalGuidance-img'>
                <img src={require('@/assets/about_us/mobile_ourFreeServices.jpg')} />
            </div>

            <div className='desktop-arrivalGuidance-title'>
                {formatMessage({ id: 'Arrival Guidance' })}
            </div>

            <div className='desktop-arrivalGuidance-content'>
                This service is limited to students who apply the courses through EDU DOTCOM with free of charges. In the early stage of studying abroad, there are usually many things that need to be arranged and causing a lot of unnecessary trouble for student. We will provide suggestions and making arrangement for students if needed such aslocal airport pick-up transportation, foreign bank application, mobile phone number application, foreign Identidy Card application and other relevant issues.
            </div>

        </div>
    )
}
