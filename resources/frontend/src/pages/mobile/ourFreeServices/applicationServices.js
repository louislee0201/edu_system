import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './applicationServices.less'
import router from 'umi/router';
import { Card, Divider, Tag } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    RightOutlined, LeftOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({

    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    function setKey(activeKey) {
        if (activeKey == state.activeKey) {
            activeKey = ''
        }
        setState({ activeKey })
    }

    return (
        <div className='desktop-applicationServices'>
            <MobileTopBar />
            <div className='desktop-applicationServices-img'>
                <img src={require('@/assets/about_us/mobile_ourFreeServices.jpg')} />
            </div>

            <div className='desktop-applicationServices-title'>
                {formatMessage({ id: 'Application Services' })}
            </div>

            <div className='desktop-applicationServices-content'>
                This service is open to public with free of charges. EDU DOTCOM is officially signed agreements with many universities as representative to provide students with faster and more convenient application services. Students now can apply the courses with us just like apply to the university directly. Students will get a dedicated counselors from us to assist in their application.
            </div>

        </div>
    )
}
