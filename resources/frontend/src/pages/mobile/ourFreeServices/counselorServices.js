import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './counselorServices.less'
import router from 'umi/router';
import { Card, Divider, Tag } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    RightOutlined, LeftOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({

    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    function setKey(activeKey) {
        if (activeKey == state.activeKey) {
            activeKey = ''
        }
        setState({ activeKey })
    }

    return (
        <div className='desktop-counselorServices'>
            <MobileTopBar />
            <div className='desktop-counselorServices-img'>
                <img src={require('@/assets/about_us/mobile_ourFreeServices.jpg')} />
            </div>

            <div className='desktop-counselorServices-title'>
                {formatMessage({ id: 'Counselor Services' })}
            </div>

            <div className='desktop-counselorServices-content'>
                This service is open to public with free of charges. We have many counselors with great experience in relevant fields who regularly updating on university and colleges to ensure latest information will be provide to student. In order to provide correct suggestions, counselor will give their best to understand student’s background, interested fields and individual conditions. Also, we will also explain all necessary info regarding on living, student works, regulations, etc about foreign study country.
            </div>

        </div>
    )
}
