import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './studentGuardian.less'
import router from 'umi/router';
import { Card, Divider, Tag } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    RightOutlined, LeftOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({

    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    function setKey(activeKey) {
        if (activeKey == state.activeKey) {
            activeKey = ''
        }
        setState({ activeKey })
    }

    return (
        <div className='desktop-studentGuardian'>
            <MobileTopBar />
            <div className='desktop-studentGuardian-img'>
                <img src={require('@/assets/about_us/mobile_ourFreeServices.jpg')} />
            </div>

            <div className='desktop-studentGuardian-title'>
                {formatMessage({ id: 'Student Guardian' })}
            </div>

            <div className='desktop-studentGuardian-content'>
            This service is limited to students who apply the courses through EDU DOTCOM with free of charges. We understand parents are always worried with their children when study abroad. Our counselor will become guardian to the student by sharing the burden with parents. For example, when students have an accident while studying abroad and family members unable to contact with the school due to the language barriers, our center will play a great role in this part through serving as a communication bridge between the school, students and parents.
            </div>

        </div>
    )
}
