import React, { useEffect, useState } from "react";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import { Toast, Menu, Icon, WhiteSpace } from 'antd-mobile';
import { Descriptions, Collapse, Divider, Button } from 'antd';
import TopBanner from '@/components/top-banner'
import { getUserLanguage } from '@/tools/user'
import Loading from '@/components/loading'
import './index.less'
import { CaretRightOutlined, ShareAltOutlined } from '@ant-design/icons';
import DescriptionTable from '@/components/mobile-description-table'
import { HOST } from '@/constants/api'
import router from 'umi/router';
import { Helmet } from "react-helmet";
import { baseUrl } from "../../../constants/api";

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        loading: true,
        schoolCourseArrangementList: [],
        schoolArrangementList: [],
        descriptionList: [],
        courseList: [],
        related_id: 0,
        school_name: "",
        school_name_cn: "",
    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    useEffect(() => {
        const related_id = props.location.query.school_id;
        if (related_id) {
            dispatch({
                type: "school/e_getSchool",
                payload: {
                    isList: false,
                    school_id: related_id
                }
            }).then(result => {
                if (result) {
                    setState({
                        ...result,
                        related_id,
                        loading: false
                    })
                }
            });
        }
    }, []);

    function renderCollapse() {
        let data = []
        Object.values(state.schoolArrangementList).map((r) => {
            data.push(
                <Collapse.Panel
                    className="site-collapse-custom-panel"
                    header={
                        <span style={{ fontWeight: 'bold' }}>{r.title}</span>
                    }
                >
                    <div className='viewSchool-course-list'>
                        {
                            Object.values(state.schoolCourseArrangementList[r.id]).map((course, key) => {
                                return (
                                    <React.Fragment>
                                        <div
                                            className='viewSchool-course-list-child'
                                            onClick={() => router.push('/viewCourse?course_id=' + course.id)}
                                            style={{ display: 'block', width: '100%', marginTop: '10px', marginBottom: '10px', marginLeft: 'unset', flex: 'unset' }}
                                        >
                                            {course.course_name}
                                        </div>
                                        <div style={{ width: '100%', justifyContent: 'center', display: 'flex' }}>
                                            <div style={{ borderBottom: '1px dashed #80808057', width: '100%' }} />
                                        </div>
                                    </React.Fragment>
                                    /*  <div
                                         className='viewSchool-course-list-child'
                                         onClick={() => router.push('/viewCourse?course_id=' + course.id)}
                                     >
                                         {course.course_name}
                                     </div> */
                                )
                            })
                        }
                    </div>

                </Collapse.Panel>
            )
        })
        return <Collapse
            bordered={false}
            expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
            className="site-collapse-custom-collapse"
        >
            {data}
        </Collapse>
    }

    function renderContent() {
        if (!state.loading) {
            return <React.Fragment>
                <TopBanner
                    title={getUserLanguage() == 'zh-CN' ? state.school_name_cn : state.school_name}
                />

                <div className='viewSchool-img'>
                    <img src={state.picture} alt="School" style={{ width: '100%' }} />
                </div>

                <div className='viewSchool'>
                    <Descriptions size='small' bordered>
                        {/* <Descriptions.Item label={formatMessage({ id: 'school_name' })}>{state.school_name}</Descriptions.Item>
                        <Descriptions.Item label={formatMessage({ id: 'school_name_cn' })}>{state.school_name_cn}</Descriptions.Item> */}
                        <Descriptions.Item label={formatMessage({ id: 'contact' })}>
                            {/* {
                                state.contact_me
                                    ? [
                                        <div>{state.school_tel}</div>,
                                        state.admin_tel + ' ' + formatMessage({ id: '(For Malaysian)' }),
                                    ]
                                    : state.school_tel
                            } */}
                            {
                                state.school_tel
                            }
                        </Descriptions.Item>
                        <Descriptions.Item label={formatMessage({ id: 'country' })}>
                            {
                                Object.values(state.countryList).map(value => {
                                    if (value.id == state.country) {
                                        return value.country
                                    }
                                })
                            }
                        </Descriptions.Item>
                        <Descriptions.Item label={formatMessage({ id: 'region' })}>{state.region}</Descriptions.Item>
                        <Descriptions.Item label={formatMessage({ id: 'address' })}>{state.address}</Descriptions.Item>
                        <Descriptions.Item label={formatMessage({ id: 'email' })}>
                            {/* {
                                state.contact_me
                                    ? [
                                        <div>{state.email}</div>,
                                        state.admin_email + ' ' + formatMessage({ id: '(For Malaysian)' })
                                    ]
                                    : state.email
                            } */}
                            {
                                state.email
                            }
                        </Descriptions.Item>
                    </Descriptions>

                    <div className='viewSchool-official_website'>
                        <Button
                            type='link'
                            disabled={state.official_website === null}
                            onClick={() => {
                                window.open(state.official_website, '_blank')
                            }}
                        >
                            {formatMessage({ id: 'official_website' })} <ShareAltOutlined />
                        </Button>
                    </div>

                    {
                        state.descriptionList.length > 0 &&
                        <div className='viewSchool-description'>
                            <div className='viewSchool-description-schoolTitle'>
                                {formatMessage({ id: 'school overview' })}
                            </div>
                            <Divider
                                className='viewSchool-description-title'
                            /*orientation="left" */
                            >
                            </Divider>
                            <DescriptionTable
                                classMode='school'
                                related_id={state.related_id}
                            />
                        </div>
                    }

                    {/* <Divider
                        className='viewSchool-description-title'
                        orientation="left"
                    >
                        {formatMessage({ id: 'all courses' })}
                    </Divider> */}

                    <div className='viewSchool-course'>
                        <div className='viewSchool-description-schoolTitle'>
                            {formatMessage({ id: 'course listing' })}
                        </div>
                        <Divider
                            className='viewSchool-description-title'
                        //orientation="left"
                        >

                        </Divider>
                        <Collapse
                            bordered={false}
                            expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
                            className="site-collapse-custom-collapse"
                        >

                            {
                                state.schoolArrangementList.length > 0 && Object.keys(state.schoolCourseArrangementList).length > 0
                                    ? renderCollapse()
                                    : <Collapse.Panel
                                        className="site-collapse-custom-panel"
                                        header={
                                            <span style={{ fontWeight: 'bold' }}>{formatMessage({ id: 'all courses' })}</span>
                                        }
                                    >

                                        <div className='viewSchool-course-list' style={{ display: 'block !important' }}>
                                            {
                                                Object.keys(state.schoolCourseArrangementList).length > 0 &&
                                                Object.values(state.schoolCourseArrangementList[0]).map((course, key) => {
                                                    return (
                                                        <React.Fragment>
                                                            <div
                                                                className='viewSchool-course-list-child'
                                                                onClick={() => router.push('/viewCourse?course_id=' + course.id)}
                                                                style={{ display: 'block', width: '100%', marginTop: '10px', marginBottom: '10px', marginLeft: 'unset', flex: 'unset' }}
                                                            >
                                                                {course.course_name}
                                                            </div>
                                                            <div style={{ width: '100%', justifyContent: 'center', display: 'flex' }}>
                                                                <div style={{ borderBottom: '1px dashed #80808057', width: '100%' }} />
                                                            </div>
                                                        </React.Fragment>
                                                    )
                                                })
                                            }
                                        </div>
                                    </Collapse.Panel>

                            }

                        </Collapse>

                    </div>

                </div>

            </React.Fragment>
        }
        return <Loading loading={state.loading} />
    }

    return <>
        <Helmet>
            <meta name="description" content={state.school_name + " " + state.school_name_cn} />
            <link rel="canonical" href={baseUrl + "viewSchool?school_id=" + state.related_id} />
        </Helmet>
        {
            renderContent()
        }
    </>
}