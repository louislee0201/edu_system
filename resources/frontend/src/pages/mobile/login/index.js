import React, { useEffect, useState, useRef } from "react";
import { List, Picker, Icon, Button } from 'antd-mobile';
import { LockOutlined, MailOutlined, PhoneOutlined, EyeInvisibleOutlined } from '@ant-design/icons';
import { formatMessage } from 'umi/locale';
import './index.less';
import { getLanguageList } from '@/tools'
import { useDispatch } from 'react-redux'
import { getUserLanguage, getAccLevel } from '@/tools/user'
import { setLocale } from 'umi/locale'
import { getLocalStorage, saveLocalStorage } from '@/tools'
import Loading from '@/components/loading'
import { Input } from 'antd';
import router from 'umi/router';
import { PRIMARY_COLOR } from '@/constants/default'

export default function (props) {
    const dispatch = useDispatch();
    const passwordRef = useRef(null);
    const emailRef = useRef(null);
    const [state, setOriState] = useState({
        mode: 'login',
        email: '',
        password: '',

        tel: '',
        passwordConfirm: '',

        erroMessage: '',
        loading: false
    })


    useEffect(() => {
        const mode = props.location.query.mode;
        if (mode) {
            setState({ mode })
        }
    }, []);

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    function submitLogin() {
        let password = state.password
        let passwordConfirm = state.passwordConfirm
        let email = state.email
        let tel = state.tel
        if (email.indexOf('@') < 0) {
            setState({ erroMessage: 'email not valid' })
            emailRef.current.focus()
            return
        }
        if (state.mode == 'register') {
            if (email.indexOf('@') < 0) {
                setState({ erroMessage: 'email not valid' })
                emailRef.current.focus()
                return
            }

            if (password.length < 8) {
                setState({ erroMessage: 'Password at least 8 characters' })
                passwordRef.current.focus()
                return
            }

            if (password != passwordConfirm) {
                setState({
                    erroMessage: 'password and repeat password not match'
                })
                passwordRef.current.focus()
                return
            }
            setState({ loading: true })
            dispatch({
                type: 'config/e_userRegister',
                payload: {
                    password,
                    email,
                    tel
                }
            }).then((value) => {
                if (!value.register) {
                    setState({ id: 'Repeat Email' })
                }
                setState({
                    loading: false
                })
            })
        } else {
            if (password.length <= 0) {
                setState({ erroMessage: 'password invalid' })
                emailRef.current.focus()
                return
            }
            setState({ loading: true })
            dispatch({
                type: 'config/e_login',
                payload: {
                    password,
                    email
                }
            }).then((value) => {
                if (!value.login) {
                    if (value.message == 'Invalid Student') {
                        setState({ erroMessage: 'email or password not match' })
                    } else {
                        setState({ erroMessage: value.message })
                    }
                }
                setState({
                    loading: false
                })
            })
        }
    }
    return (
        <React.Fragment>
            <Loading loading={state.loading} />
            <div className='desktop-login_background' />
            <div
                style={{
                    zIndex: '10000',
                    position: 'fixed',
                    left: '0',
                    top: '0 ',
                    height: '70px',
                    width: 'auto',
                    padding: '17px 10px',
                    display: 'flex',
                    color: 'white',
                    cursor: 'pointer'
                }}
                onClick={() => router.goBack()}
            >
                <div><Icon size='lg' type='left' /></div>
                <div style={{ display: 'flex', alignItems: 'center', fontSize: '18px' }}>
                    {formatMessage({ id: 'back' })}
                </div>
            </div>
            <div className='desktop-login-main'>
                <div className='desktop-login-main-content'>
                    <div className='desktop-login-main-content-logo'>
                        <img src={require('@/assets/logo.png')} style={{ height: '100%', width: '60%' }} />
                    </div>

                    <div className='desktop-login-main-content-login_info'>
                        <div className='desktop-login-main-content-login_info_address'>
                            <div className='desktop-login-main-content-login_info_address_icon'><MailOutlined /></div>
                            <Input
                                ref={emailRef}
                                onPressEnter={() => {
                                    passwordRef.current.focus()
                                }}
                                value={state.email}
                                allowClear
                                className='desktop-login-main-content-login_info_address_input'
                                placeholder={formatMessage({ id: 'email' })}
                                onChange={(e) => {
                                    setState({ email: e.target.value })
                                }}
                            />
                        </div>
                        <hr />
                        <div className='desktop-login-main-content-login_info_address'>
                            <div className='desktop-login-main-content-login_info_address_icon'><LockOutlined /></div>
                            <Input.Password
                                onPressEnter={() => {
                                    submitLogin()
                                }}
                                value={state.password}
                                ref={passwordRef}
                                allowClear
                                className='desktop-login-main-content-login_info_address_input'
                                placeholder={formatMessage({ id: 'password' })}
                                onChange={(e) => {
                                    setState({ password: e.target.value })
                                }}
                            />
                        </div>
                        {
                            state.mode == 'register' &&
                            <React.Fragment>
                                <hr />
                                <div className='desktop-login-main-content-login_info_address'>
                                    <div className='desktop-login-main-content-login_info_address_icon'><LockOutlined /></div>
                                    <Input.Password
                                        value={state.passwordConfirm}
                                        allowClear
                                        className='desktop-login-main-content-login_info_address_input'
                                        placeholder={formatMessage({ id: 'repeat password' })}
                                        onChange={(e) => {
                                            setState({ passwordConfirm: e.target.value })
                                        }}
                                    />
                                </div>
                                <hr />
                                <div className='desktop-login-main-content-login_info_address'>
                                    <div className='desktop-login-main-content-login_info_address_icon'><PhoneOutlined /></div>
                                    <Input
                                        value={state.tel}
                                        allowClear
                                        className='desktop-login-main-content-login_info_address_input'
                                        placeholder={formatMessage({ id: 'contact' })}
                                        onChange={(e) => {
                                            setState({ tel: e.target.value })
                                        }}
                                    />
                                </div>
                            </React.Fragment>
                        }
                    </div>

                    <div className='desktop-login-main-content-error_message'>
                        {state.erroMessage != '' && formatMessage({ id: state.erroMessage })}
                    </div>

                    <Button
                        className='desktop-login-main-content-login_btn'
                        loading={state.loading}
                        onClick={() => {
                            submitLogin()
                        }}
                    >
                        {formatMessage({ id: state.mode })}
                    </Button>

                    <div
                        style={{
                            fontSize: state.mode == 'register' ? '13px' : '15px'
                        }}
                        className='desktop-login-main-content-register_btn'
                        onClick={() => {
                            setState({ mode: state.mode == 'register' ? 'login' : 'register' })
                        }}
                    >
                        {formatMessage({ id: state.mode == 'login' ? 'create an account' : 'already have account? login now!' })}
                    </div>

                    {
                        state.mode == 'login' &&
                        <div
                            className='desktop-login-main-content-forgetPassword_btn'
                            onClick={() => {
                                //setState({ mode: state.mode == 'register' ? 'login' : 'register' })
                            }}
                        >
                            {formatMessage({ id: 'forgetpassword' })}
                        </div>
                    }

                </div>
            </div>
        </React.Fragment >
    )

}
