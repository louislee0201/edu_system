import React, { useEffect, useState, useRef } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './index.less'
import router from 'umi/router';
import { Card, Divider, Tag, Skeleton } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    CaretRightOutlined, RightOutlined, LeftOutlined, UpOutlined, InfoCircleOutlined,
    BookOutlined, EllipsisOutlined, SafetyCertificateOutlined, TeamOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        loading: false,
        slideIndex: 0,
        drawerVisible: false,

        activeKey: '',
        imgLoaded: false,
        faqs: [
            {
                topic: 'What is EDU DOTCOM?',
                content: <div>
                    EDU DOTCOM is an higher education consultancy private company which provide platform to both university and students.
                    For more details, please refer to "About Us" for more details.
                </div>
            },
            {
                topic: 'What kind of the services provide by EDU DOTCOM and how much for the charges?',
                content: <div>
                    Absolutely FREE for all the services! For more info, please refer to "Our Services" for more details.
                </div>
            },
            {
                topic: 'How do EDU DOTCOM sustain if the services are all free to the public?',
                content: <div>
                    We receive financial support from the university when they publish information in our website or application assist from us to each students.
                </div>
            },
            {
                topic: 'Is there any different if I apply through EDU DOTCOM or direct to the university?',
                content: <div>
                    The course fees and scholarships that you apply through us is totally same if you apply directly to university.
                    We have signed an official contract with all our partner schools as a recruitment representative in Malaysia.
                    If you are still in doubt, feel free to contact relevant university for further clarification.
                </div>
            },
            {
                topic: 'Which university is being partner schools to the EDU DOTCOM?',
                content: <div>
                    You may refer to the contact detail in from each school page. If you unclear on this part, feel free to contact us or chat to our counselor.
                </div>
            },
            {
                topic: 'Can I apply directly to university?',
                content: <div>
                    Yes, you can. However, we got the exclusive scholarships or offer sometimes.
                    Also, you will get appointed counsellor to assist you regarding of application process, student services, etc for FREE!
                </div>
            },
        ],
    })

    const newsAndEvents = useSelector(state => state.config.newsAndEvents)

    /*  useEffect(() => {
         dispatch({
             type: "home/e_getHomePageData",
             payload: {}
         }).then(result => {
             if (result) {
                 setState({ ...result, loading: false })
             }
         });
         setState({ newsAndEvents: state.newsAndEvents.reverse() })
 
     }, []); */

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    function setKey(activeKey) {
        if (activeKey == state.activeKey) {
            activeKey = ''
        }
        setState({ activeKey })
    }

    const configState = useSelector(state => state.config)

    return (
        <div className='mobile-homePage'>
            <Loading loading={state.loading} />
            <MobileTopBar />
            <div className='mobile-homePage-top_banner'>
                {
                    !state.imgLoaded && <React.Fragment>
                        <Skeleton active />
                        <Skeleton active />
                    </React.Fragment>
                }
                <Carousel
                    autoplay
                    style={{ height: '100%' }}
                    infinite
                >
                    <img
                        src='https://www.edudotcom.my/auth/image/description/1619845578.jpeg'
                        style={{
                            height: '100%',
                            width: '100%',
                            verticalAlign: 'top'
                        }}
                        onLoad={() => setState({ imgLoaded: true })}
                    />
                    {/* <img
                        src='https://www.edudotcom.my/auth/image/description/1619809611.jpeg'
                        style={{
                            height: '100%',
                            width: '100%',
                            verticalAlign: 'top'
                        }}
                    /> */}
                </Carousel>

            </div>

            <div className='mobile-homePage-newsAndEvents'>
                <div className='mobile-homePage-newsAndEvents-top' onClick={() => router.push('/introduction/newsAndEvents')}>
                    <div className='mobile-homePage-newsAndEvents-top-left'>
                        {formatMessage({ id: 'news and events' })}
                    </div>
                    <div className='mobile-homePage-newsAndEvents-top-right'>
                        +
                    </div>
                </div>
                <div className='mobile-homePage-newsAndEvents-bottom'>
                    {
                        Object.values(newsAndEvents).map((v, k) => {
                            return <div className='mobile-homePage-newsAndEvents-bottom-child' onClick={() => router.push('/introduction/newsAndEvents/view?_ar=' + k)}>
                                <Tag color={v.color}>{v.type}</Tag> <span className='mobile-homePage-newsAndEvents-bottom-child-topic'></span>{v.topic}
                            </div>
                        })
                    }
                </div>
            </div>

            <div className='mobile-homePage-func'>
                <div
                    className='mobile-homePage-func-btn'
                    style={{ backgroundImage: 'url(https://www.edudotcom.my/auth/image/description/1618559077.png)' }}
                    onClick={() => router.push('/searchCourse')}
                >
                    <div className='mobile-homePage-func-btn-title'>
                        {formatMessage({ id: 'search course' })}
                    </div>
                </div>
                <div
                    className='mobile-homePage-func-btn'
                    style={{ backgroundImage: 'url(https://www.edudotcom.my/auth/image/description/1618559076.png)' }}
                    onClick={() => router.push('/schoolLibrary')}
                >
                    <div className='mobile-homePage-func-btn-title'>
                        {formatMessage({ id: 'school library' })}
                    </div>
                </div>
            </div>

            <div className='mobile-homePage-faqs' onClick={() => router.push('/other/faqs')}>
                <div className='mobile-homePage-faqs-top'>
                    <div className='mobile-homePage-faqs-top-left'>
                        {formatMessage({ id: 'FAQs' })}
                    </div>
                    <div className='mobile-homePage-faqs-top-right' onClick={() => router.push('/other/faqs')}>
                        +
                    </div>
                </div>
                <div className='mobile-homePage-faqs-bottom'>
                    {
                        Object.values(state.faqs).map((v) => {
                            return <div className='mobile-homePage-faqs-bottom-child'>
                                <CaretRightOutlined /> {v.topic}
                            </div>
                        })
                    }
                </div>
            </div>


            <div className='mobile-homePage-testimonial' onClick={() => router.push('/introduction/testimonial')}>
                <div className='mobile-homePage-testimonial-top'>
                    <div className='mobile-homePage-testimonial-top-left'>
                        {formatMessage({ id: 'Testimonial' })}
                    </div>
                    <div className='mobile-homePage-testimonial-top-right' onClick={() => router.push('/introduction/testimonial')}>
                        +
                    </div>
                </div>
                <div className='mobile-homePage-testimonial-bottom'>
                    <div
                        className='desktop-testimonial-content-child'
                        onClick={(e) => {
                            e.stopPropagation()
                            setKey(1)
                        }}
                        style={{
                            background: `url(${require('@/assets/testimonial/backgroud.jpg')}) no-repeat center center`,
                            color: 'red'
                        }}

                    >
                        <div
                            className='desktop-testimonial-content-child-img'
                        >
                            <img src={require('@/assets/testimonial/2.png')} />
                        </div>

                        <div className='desktop-testimonial-content-child-right'>
                            <div className='desktop-testimonial-content-child-right-name'>
                                Chloe Ng
                        </div>
                            <div className='desktop-testimonial-content-child-right-country'>
                                <div className='desktop-testimonial-content-child-right-country-img'>
                                    <img src={require('@/assets/testimonial/korea.png')} />
                                </div> Kyung Hee University
                        </div>
                        </div>
                        {
                            state.activeKey != '1' && <div className='desktop-testimonial-content-child-btn'>
                                <div className='desktop-testimonial-content-child-btn-box'>
                                    <RightOutlined />
                                </div>
                            </div>
                        }
                        <div
                            style={{
                                right: state.activeKey == '1' ? '0' : '-70%',
                                opacity: state.activeKey == '1' ? '1' : '0'
                            }}
                            className='desktop-testimonial-content-child-content'
                        >
                            <div className='desktop-testimonial-content-child-content-btn'>
                                <div className='desktop-testimonial-content-child-content-btn-box'>
                                    <LeftOutlined />
                                </div>
                            </div>
                        “ Thank you EDU DOTCOM helping me to fulfill my dream to study in Korea.
                        The counselor has a strong responsibility and great experience to answer all my questions which I raised to them.
                        I wish to take this opportunity to express our sincerely gratitude to your guys! ”
                    </div>

                    </div>
                </div>
            </div>


            {/*  <Divider
                className='mobile-homePage-space_carousel-title'
                orientation="center"
            >
                {formatMessage({ id: 'news and events' })}
            </Divider>

            <Carousel
                className="mobile-homePage-space_carousel"
                frameOverflow="visible"
                cellSpacing={10}
                slideWidth={0.6}
                //autoplay
                infinite
                afterChange={index => setState({ slideIndex: index })}
            >

                {
                    state.newsAndEvents &&
                    Object.values(state.newsAndEvents).map((value, key) => {
                        return <Card
                            key={key}
                            //hoverable
                            style={{
                                width: 250,
                                display: 'block',
                                position: 'relative',
                                top: state.slideIndex === key ? -15 : 0,
                                //height: state.imgHeight,
                                zIndex: state.slideIndex === key && '1000',
                                boxShadow: state.slideIndex === key ? ' 5px 5px 15px 1px rgba(0, 0, 0, 0.5)' : '2px 1px 1px rgba(0, 0, 0, 0.2)',
                            }}
                            cover={<img alt="example" src={value.img} />}
                        >
                            <Card.Meta
                                title={
                                    <div className='mobile-homePage-space_carousel-news-content-father-child-card'>
                                        <div className='mobile-homePage-space_carousel-news-content-father-child-card-date' >
                                            {value.date} ( {formatMessage({ id: value.type })} )
                                            </div>
                                        <div className='mobile-homePage-space_carousel-news-content-father-child-card-title' >
                                            {value.topic}
                                        </div>
                                    </div>
                                }

                                description={
                                    value.content
                                    // <div className='desktop-homePage-news-content-father-child-card-desc'>
                                    //     {value.content}
                                    // </div>
                                }
                            />
                        </Card>
                    })
                }

            </Carousel>

            <img src='http://www.louis-blog.com/auth/image/description/1610966880.jpeg' style={{ height: 'auto', width: '100%' }} />

            <img src='http://www.louis-blog.com/auth/image/description/1611125736.jpeg' width='100%' /> */}
        </div>
    )

}
