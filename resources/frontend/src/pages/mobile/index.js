import React, { useEffect, useState } from "react";
import Home from './home'
import PageInvalid from './exception/404'

export default function (props) {
    const [state, setOriState] = useState({
        currentPage: '',
        pageInvalidVisible: true
    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    useEffect(() => {
        let validPage = false
        props.route.routes.forEach(pathname => {
            if (typeof pathname.path !== 'undefined') {
                if (pathname.path.indexOf('/mobile' + props.location.pathname) >= 0) {
                    validPage = true
                }
            }

        })
        if (validPage) {
            setState({
                pageInvalidVisible: false,
                currentPage: require('.' + props.location.pathname).default
            })
        }

    }, [props.location.pathname])

    if (props.location.pathname == '/' || props.location.pathname == '/home') {
        return (
            <Home {...props} />
        )
    } else if (state.currentPage !== '') {
        return (
            <state.currentPage {...props} />
        );
    }
    
    return (
        <PageInvalid {...props} />
    )

    if (props.location.pathname == '/' || props.location.pathname == '/home') {
        return (
            <state.currentPage {...props} />
        );
    }

    return (
        <PageInvalid visible={state.pageInvalidVisible} {...props} />
    )
}
