import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './view.less'
import router from 'umi/router';
import { Modal, Input, Skeleton, message, Button } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    UnorderedListOutlined, BulbOutlined, DownOutlined, UpOutlined, InfoCircleOutlined,
    BookOutlined, EllipsisOutlined, SafetyCertificateOutlined, TeamOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        arKey: -1,
        loading: false,

        email: "",
        contact: "",
        name: "",
        proficiency_level: "",
        nationality: "",
        japan_school: "",

        proficiency_level_list: [
            "JLPT N1",
            "JLPT N2",
            "JLPT N3",
            "JLPT N4",
            "JLPT N5",
        ],

        nationality_list: [
            "马来西亚 Malaysia",
            "中国 China",
            "中国台湾 Taiwan",
            "中国香港 Hong Kong",
            "新加坡 Singapore",
            "其他 Others",
        ],

        buttonloading: false,
    })
    const newsAndEvents = useSelector(state => state.config.newsAndEvents)

    // useEffect(() => {
    //     const related_id = parseInt(props.location.query._ar);
    //     if (related_id >= 0) {
    //         setState({ arKey: related_id, loading: false })
    //     }
    // }, []);

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }
    return (
        <div className='desktop-newsAndEvents-view'>
            <MobileTopBar />
            {
                state.loading
                    ? <Skeleton active />
                    : <div className='desktop-newsAndEvents-view-content'>

                        <div className='desktop-newsAndEvents-view-content-img'>
                            <img src="https://edudotcom.my/auth/image/description/1632921560.jpeg" />
                        </div>



                        <div className='desktop-newsAndEvents-view-content-content'>
                            <div style={{ fontSize: "17px", fontWeight: "bold", padding: "10px 0" }}>立命館アジア太平洋大学线上说明会</div>

                            欢迎参加本次由EDU DOTCOM升学中心协同立命馆亚洲太平洋大学连同主办的线上说明会。本次说明会将经由Zoom平台上进行，将使用日语进行解说。请填写以下问卷，相关登入信息会发送至相关电邮。<br /><br />
                            <b>日期：</b>06-10-2021（星期三）<br />
                            <b>时间：</b>15:00~14:00 （日本、韩国时间）/ 14:00~15:00 （中国、马来西亚、新加坡时间）<br /><br />

                            Welcome to participate in our online seminar with Ritsumeikan Asia Pacific University. This seminar will conducted in Japanese via Zoom online. Please fill in your personal information in below and relevant login details will be send to your email.<br /><br />
                            <b>Date:</b> 06-10-2021 ( Wed )<br />
                            <b>Time:</b>15:00~14:00 （Japan, Korea @GMT+9）/ 14:00~15:00 （China, Malaysia, Singapore @GMT+8）<br /><br />
                            <Input
                                value={state.name}
                                placeholder={"姓名（护照英文全名）/ Full name as per passport"}
                                onChange={(e) => {
                                    setState({ name: e.target.value })
                                }}
                            />

                            <Input
                                value={state.email}
                                placeholder={"电邮 / Email"}
                                onChange={(e) => {
                                    setState({ email: e.target.value })
                                }}
                            />

                            <Input
                                value={state.contact}
                                placeholder={"联络手机号 / Mobile No."}
                                onChange={(e) => {
                                    setState({ contact: e.target.value })
                                }}
                            />

                            <select
                                style={{
                                    width: "100%",
                                    borderRadius: "2px",
                                    height: "30px",
                                    border: "1px solid #d1d1d1",
                                    color: state.proficiency_level == "" && "#bfbfbf",
                                    padding: "0 8px"
                                }}
                                value={state.proficiency_level}
                                onChange={(e) => setState({ proficiency_level: e.target.value })}
                            >
                                <option value={""}>日语等级 / Japanese Proficiency Level</option>
                                {
                                    Object.values(state.proficiency_level_list).map((value, key) => {
                                        return <option style={{ color: "black" }} value={value} key={key}>
                                            {value}
                                        </option>
                                    })
                                }
                            </select>

                            <select
                                style={{
                                    width: "100%",
                                    borderRadius: "2px",
                                    height: "30px",
                                    border: "1px solid #d1d1d1",
                                    color: state.nationality == "" && "#bfbfbf",
                                    padding: "0 8px"
                                }}
                                value={state.nationality}
                                onChange={(e) => setState({ nationality: e.target.value })}
                            >
                                <option value={""}>国籍 / Nationality</option>
                                {
                                    Object.values(state.nationality_list).map((value, key) => {
                                        return <option style={{ color: "black" }} value={value} key={key}>
                                            {value}
                                        </option>
                                    })
                                }
                            </select>

                            <Input
                                value={state.japan_school}
                                placeholder={"就读日语学院 / Japanese School ( Optional / 选填 )"}
                                onChange={(e) => {
                                    setState({ japan_school: e.target.value })
                                }}
                            />
                            <br />
                            <br />
                            <div style={{ textAlign: "center" }}>
                                <Button
                                    loading={state.buttonloading}
                                    disabled={state.name == "" || state.email == "" || state.nationality == "" || state.proficiency_level == "" || state.contact == ""}
                                    onClick={() => {

                                        setState({ buttonloading: true })
                                        dispatch({
                                            type: "config/e_submitSeminarsForm",
                                            payload: {
                                                name: state.name,
                                                email: state.email,
                                                contact: state.contact,
                                                proficiency_level: state.proficiency_level,
                                                nationality: state.nationality,
                                                japan_school: state.japan_school
                                            }
                                        }).then(result => {
                                            if (result) {
                                                setState({
                                                    name: "",
                                                    email: "",
                                                    contact: "",
                                                    proficiency_level: "",
                                                    nationality: "",
                                                    japan_school: "",
                                                    buttonloading: false
                                                })

                                                Modal.success({
                                                    title: <div>
                                                        Your application is successful! <br />Please check your email.
                                                    </div>,
                                                    onOk: () => {
                                                        router.push("/home")
                                                    },
                                                    okText: "Back to home"
                                                });
                                                //message.success(formatMessage({ id: "successfully submit" }))
                                            }
                                        });
                                    }}
                                    style={{
                                        borderRadius: "5px",
                                        background: state.name == "" || state.email == "" || state.nationality == "" || state.proficiency_level == "" || state.contact == "" ? "#ddd" : PRIMARY_COLOR,
                                        color: "white"
                                    }}
                                    type='ghost'
                                //className='desktop-footer-desc-child-content-icon-text'
                                >
                                    {formatMessage({ id: "submit" })}
                                </Button>
                            </div>

                        </div>
                    </div>
            }
        </div >
    )
}
