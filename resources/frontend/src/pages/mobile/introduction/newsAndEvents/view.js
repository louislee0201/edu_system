import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './view.less'
import router from 'umi/router';
import { Tag, Input, Skeleton, message, Button } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    UnorderedListOutlined, BulbOutlined, DownOutlined, UpOutlined, InfoCircleOutlined,
    BookOutlined, EllipsisOutlined, SafetyCertificateOutlined, TeamOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        arKey: -1,
        loading: true,

        email: "",
        contact: "",
        name: "",
        proficiency_level: "",
        nationality: "",
        japan_school: "",

        proficiency_level_list: [
            "JLPT N1",
            "JLPT N2",
            "JLPT N3",
            "JLPT N4",
            "JLPT N5",
        ],

        nationality_list: [
            "马来西亚 Malaysia",
            "中国 China",
            "中国台湾 Taiwan",
            "中国香港 Hong Kong",
            "新加坡 Singapore",
            "其他 Others",
        ]
    })

    const newsAndEvents = useSelector(state => state.config.newsAndEvents)

    useEffect(() => {
        const related_id = parseInt(props.location.query._ar);
        if (related_id >= 0) {
            setState({ arKey: related_id, loading: false })
        }
    }, []);

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <div className='desktop-newsAndEvents-view'>
            <MobileTopBar />
            {
                state.loading
                    ? <Skeleton active />
                    : <div className='desktop-newsAndEvents-view-content'>
                        <div className='desktop-newsAndEvents-view-content-img'>
                            <img src={newsAndEvents[state.arKey].img} />
                        </div>

                        <div className='desktop-newsAndEvents-view-content-attr'>
                            <Tag color={newsAndEvents[state.arKey].color}>{formatMessage({ id: newsAndEvents[state.arKey].type })}</Tag>
                            <div className='desktop-newsAndEvents-view-content-attr-date'>{newsAndEvents[state.arKey].date}</div>
                        </div>

                        <div className='desktop-newsAndEvents-view-content-title'>
                            {newsAndEvents[state.arKey].topic}
                        </div>

                        <div className='desktop-newsAndEvents-view-content-content'>
                            {newsAndEvents[state.arKey].content}
                        </div>
                    </div>
            }
        </div >
    )
}
