import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './newsAndEvents.less'
import router from 'umi/router';
import { Card, Divider, Tag } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    UnorderedListOutlined, BulbOutlined, DownOutlined, UpOutlined, InfoCircleOutlined,
    BookOutlined, EllipsisOutlined, SafetyCertificateOutlined, TeamOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    const newsAndEvents = useSelector(state => state.config.newsAndEvents)

    return (
        <div className='desktop-newsAndEvents'>
            <MobileTopBar />
            <div className='desktop-newsAndEvents-img'>
                <img src={require('@/assets/about_us/desktop_about_us.jpg')} />
            </div>

            <div className='desktop-newsAndEvents-title'>
                {formatMessage({ id: 'newsAndEvents' })}
            </div>

            <div className='desktop-newsAndEvents-content'>
                <div className='desktop-newsAndEvents-content-list'>
                    {
                        Object.values(newsAndEvents).map((v, k) => {
                            return <div className='desktop-newsAndEvents-content-list-box' onClick={() => router.push('/introduction/newsAndEvents/view?_ar=' + k)}>
                                <div className='desktop-newsAndEvents-content-list-box-left'>
                                    <div className='desktop-newsAndEvents-content-list-box-left-img'>
                                        <img src={v.img} />
                                    </div>
                                    <div className='desktop-newsAndEvents-content-list-box-left-type'>
                                        <Tag color={v.color}>{formatMessage({ id: v.type })}</Tag>
                                    </div>
                                </div>
                                <div className='desktop-newsAndEvents-content-list-box-right'>
                                    <div className='desktop-newsAndEvents-content-list-box-right-content'>
                                        {v.topic}
                                    </div>
                                    <div className='desktop-newsAndEvents-content-list-box-right-date'>
                                        {v.date}
                                    </div>
                                </div>
                            </div>
                        })
                    }
                </div>
            </div>

        </div>
    )
}
