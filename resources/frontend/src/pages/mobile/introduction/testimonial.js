import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './testimonial.less'
import router from 'umi/router';
import { Card, Divider, Tag } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    RightOutlined, LeftOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        activeKey: ''
    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    function setKey(activeKey) {
        if (activeKey == state.activeKey) {
            activeKey = ''
        }
        setState({ activeKey })
    }

    return (
        <div className='desktop-testimonial'>
            <MobileTopBar />
            <div className='desktop-testimonial-img'>
                <img src={require('@/assets/about_us/desktop_about_us.jpg')} />
            </div>

            <div className='desktop-testimonial-title'>
                {formatMessage({ id: 'Testimonial' })}
            </div>

            <div className='desktop-testimonial-content'>
                <div
                    className='desktop-testimonial-content-child'
                    onClick={() => setKey(1)}
                    style={{
                        background: `url(${require('@/assets/testimonial/backgroud.jpg')}) no-repeat center center`,
                        color: 'red'
                    }}

                >
                    <div
                        className='desktop-testimonial-content-child-img'
                    >
                        <img src={require('@/assets/testimonial/2.png')} />
                    </div>

                    <div className='desktop-testimonial-content-child-right'>
                        <div className='desktop-testimonial-content-child-right-name'>
                            Chloe Ng
                        </div>
                        <div className='desktop-testimonial-content-child-right-country'>
                            <div className='desktop-testimonial-content-child-right-country-img'>
                                <img src={require('@/assets/testimonial/korea.png')} />
                            </div> Kyung Hee University
                        </div>
                    </div>
                    {
                        state.activeKey != '1' && <div className='desktop-testimonial-content-child-btn'>
                            <div className='desktop-testimonial-content-child-btn-box'>
                                <RightOutlined />
                            </div>
                        </div>
                    }
                    <div
                        style={{
                            right: state.activeKey == '1' ? '0' : '-70%',
                            opacity: state.activeKey == '1' ? '1' : '0'
                        }}
                        className='desktop-testimonial-content-child-content'
                    >
                        <div className='desktop-testimonial-content-child-content-btn'>
                            <div className='desktop-testimonial-content-child-content-btn-box'>
                                <LeftOutlined />
                            </div>
                        </div>
                        “ Thank you EDU DOTCOM helping me to fulfill my dream to study in Korea.
                        The counselor has a strong responsibility and great experience to answer all my questions which I raised to them.
                        I wish to take this opportunity to express our sincerely gratitude to your guys! ”
                    </div>

                </div>

                <div
                    className='desktop-testimonial-content-child'
                    onClick={() => setKey(2)}
                    style={{
                        background: `url(${require('@/assets/testimonial/singapore_backgroud.jpg')}) no-repeat center center`,
                        color: 'red'
                    }}

                >
                    <div
                        className='desktop-testimonial-content-child-img'
                        style={{ width: '75px' }}
                    >
                        <img src={require('@/assets/testimonial/1.png')} />
                    </div>

                    <div className='desktop-testimonial-content-child-right'>
                        <div className='desktop-testimonial-content-child-right-name'>
                            Lee Kee Hao
                        </div>
                        <div className='desktop-testimonial-content-child-right-country'>
                            <div className='desktop-testimonial-content-child-right-country-img'>
                                <img src={require('@/assets/testimonial/singapore.png')} />
                            </div> James Cook University (Singapore)
                        </div>
                    </div>
                    {
                        state.activeKey != '2' && <div className='desktop-testimonial-content-child-btn'>
                            <div className='desktop-testimonial-content-child-btn-box'>
                                <RightOutlined />
                            </div>
                        </div>
                    }
                    <div
                        style={{
                            right: state.activeKey == '2' ? '0' : '-70%',
                            opacity: state.activeKey == '2' ? '1' : '0'
                        }}
                        className='desktop-testimonial-content-child-content'
                    >
                        <div className='desktop-testimonial-content-child-content-btn'>
                            <div className='desktop-testimonial-content-child-content-btn-box'>
                                <LeftOutlined />
                            </div>
                        </div>
                        “ Your guys are the one who made me become a person which I never think of it!
                        I sincerely thanks to EDU DOTCOM for being patiently to explain and analyzing various possible options for my further studies when I graduated from high school.
                        I hope that more students will meet you in the future. ”
                    </div>

                </div>
           
            </div>

        </div>
    )
}
