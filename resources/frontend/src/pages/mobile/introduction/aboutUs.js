import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './aboutUs.less'
import router from 'umi/router';
import { Card, Divider, Drawer } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    UnorderedListOutlined, BulbOutlined, DownOutlined, UpOutlined, InfoCircleOutlined,
    BookOutlined, EllipsisOutlined, SafetyCertificateOutlined, TeamOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({

    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <div className='desktop-about_us'>
            <MobileTopBar />
            <div className='desktop-about_us-img'>
                <img src={require('@/assets/about_us/desktop_about_us.jpg')} />
            </div>

            <div className='desktop-about_us-title'>
                {formatMessage({ id: 'about us' })}
            </div>

            <div className='desktop-about_us-content'>
                EDU DOTCOM is a higher education consulting company which has been established in Malaysia by many senior education consultants in 2012, and devoted to pull in the distance of people and education as a background concept. With years of development, we have always adhered to the responsibility of education, initiating a study abroad platform and providing services for student study abroad services.
                <br />
                <br />
                <div className='desktop-about_us-content-title'>
                    Our Missions
                </div>

                Accompanied by the trend of globalization, we have originated many different channels for studying abroad such as Japan, Singapore, South Korea, etc. and at the same time to provide high-quality study abroad services, and successfully assisted more than 1,000 students to complete their higher education. Counsellors of our centre have been committed in stepping into the campus to participate in many educational exhibitions and seminars, and also organizing a series of activities such as holding overseas seminars in helping students to prevail their self-willed to further study and to stimulate their desire in pursuing knowledge.
                <br />
                <br />
                <div className='desktop-about_us-content-title'>
                    Our Services
                </div>

                With the support of the Government, many foreign and local universities, and the education industry, our centre has established one-stop education service and assistance for free. From personal tutorship, course search, visa guidance, dormitory arrangements and other common services, we also provide a 24-hour hotline for emergency service to assist various problems faced by students and parents. Counsellors will also regularly check on students’ personal living conditions according to the student tracking system. Furthermore, our centre also provides Japanese language courses to help students in mastering the basics of Japanese language in order to meet the requirements for studying in Japan.
                <br />
                <br />
                <div className='desktop-about_us-content-title'>
                    Our Visions
                </div>

                In order to cope with the increased demand for online services, we designed a new open official website system in 2021 for public convenience to inquire about all courses at any time and also to get the latest relevant higher education information in a timely manner. We will also look forward to open up more studying abroad channels in the future and will always uphold the position with “Dedicated” and “Neutrally” to bring services to even more students and school companion!
            </div>

        </div>
    )
}
