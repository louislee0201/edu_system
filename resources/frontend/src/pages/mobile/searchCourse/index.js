import React, { useState, useEffect, useRef } from 'react'
import { connect } from 'dva';
import { SearchBar, Menu, Button, Badge, Modal as Mmodal, List, Checkbox } from 'antd-mobile';
import { Card, Modal, Drawer, Divider, Spin, Empty } from 'antd';
import router from 'umi/router';
import { HOST } from '@/constants/api'
import TopBanner from '@/components/top-banner'
import { formatMessage } from 'umi/locale';
import { CaretDownOutlined, StarTwoTone, FilterOutlined, SettingOutlined, DownOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux'
import Loading from '@/components/loading'
import './index.less'
import { PRIMARY_COLOR } from '@/constants/default'
import { Helmet } from 'react-helmet';
import { baseUrl } from '../../../constants/api';

export default () => {
    const dispatch = useDispatch()
    const level = useRef(null)
    const field = useRef(null)
    const major = useRef(null)
    const currency = useRef(null)
    const region = useRef(null)
    const country = useRef(null)
    const courseListHeightDom = useRef(null)
    /* const subMenu = useRef(null) */
    const configState = useSelector(state => state.config)
    const courseState = useSelector(state => state.course)
    const [state, setOriState] = useState({
        pageSize: courseState.searchCourseFilter.pageSize,
        totalItem: courseState.searchCourseFilter.totalItem,
        currentPage: courseState.searchCourseFilter.currentPage,
        dataList: courseState.searchCourseFilter.dataList,
        loading: false,
        dataLoading: false,
        filterModalVisible: false,
        currentScreenHeight: document.documentElement.clientHeight,
        currentScreenWidth: document.documentElement.clientWidth,

        course_level_id: courseState.searchCourseFilter.course_level_id,
        study_field_id: courseState.searchCourseFilter.study_field_id,
        major_id: courseState.searchCourseFilter.major_id,
        country_id: courseState.searchCourseFilter.country_id,
        region_id: courseState.searchCourseFilter.region_id,
        qualificationActiveId: courseState.searchCourseFilter.qualificationActiveId,
        qualification_id: courseState.searchCourseFilter.qualification_id,
        secondary_complete: courseState.searchCourseFilter.secondary_complete,

        //studentQualificationList: {},
        studentQualificationSubjectList: {},

        addQualificationModalVisible: false,
        addQualificationSubjectEditMode: false,

        qualificationTitle: '',
        bookMode: 'location',

        qualificationList: courseState.searchCourseFilter.qualificationList,
        qualificationSubjectList: courseState.searchCourseFilter.qualificationSubjectList,
        courseLevelList: courseState.searchCourseFilter.courseLevelList,
        studyFieldList: courseState.searchCourseFilter.studyFieldList,
        majorList: courseState.searchCourseFilter.majorList,
        countryList: courseState.searchCourseFilter.countryList,
        regionList: courseState.searchCourseFilter.regionList,
        qualificationGradeList: courseState.searchCourseFilter.qualificationGradeList,
        studentAcademicResult: courseState.searchCourseFilter.studentAcademicResult,
        currencyTypeList: courseState.searchCourseFilter.currencyTypeList,
        schoolTypeList: courseState.searchCourseFilter.schoolTypeList,

        ipCountryCode: configState.ipCountryCode,
        ipCountryCodeId: configState.ipCountryCodeId,
        checkedIp: false,

        qualificationBookEditMode: false,
        academicResultUpdate: false,
        firstLoaded: false,

        position: courseState.searchCourseFilter.position,

        validOriKey: courseState.searchCourseFilter.validOriKey,
        //validCountry: courseState.searchCourseFilter.validCountry,
        validLevel: courseState.searchCourseFilter.validLevel,
        validStudyField: courseState.searchCourseFilter.validStudyField,
        validMajor: courseState.searchCourseFilter.validMajor,
        selectionData: courseState.searchCourseFilter.selectionData,

    })

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }

    window.onresize = function (event) {
        setState({
            currentScreenHeight: document.documentElement.clientHeight,
            currentScreenWidth: document.documentElement.clientWidth
        })
    };

    window.onscroll = function () {
        /* if (courseState.searchCourseFilter.position > window.pageYOffset) {
            if (courseState.searchCourseFilter.position - window.pageYOffset > 30) {
                subMenu.current.style.height = "33px";
                subMenu.current.style.opacity = "1";
            }
        }

        if (courseState.searchCourseFilter.position < window.pageYOffset) {
            if (window.pageYOffset - courseState.searchCourseFilter.position > 30) {
                subMenu.current.style.height = "0px";
                subMenu.current.style.opacity = "0";
            }
        } */

        if (courseListHeightDom.current) {
            dispatch({
                type: 'course/r_updateState',
                payload: {
                    searchCourseFilter: {
                        ...courseState.searchCourseFilter,
                        position: window.pageYOffset
                    }
                }
            })
            const courseListHeight = courseListHeightDom.current.offsetHeight - 618 - 500
            if (window.pageYOffset >= courseListHeight && !state.dataLoading && state.totalItem > state.pageSize) {
                setState({ dataLoading: true })
                requestData(state.currentPage + 1, true)
            }
        }
    };

    useEffect(() => {
        if (
            (
                state.region_id > 0 ||
                state.country_id > 0 ||
                state.major_id > 0 ||
                state.study_field_id > 0 ||
                state.course_level_id > 0 ||
                state.ipCountryCodeId > 0 ||
                state.qualificationActiveId > 0 ||
                state.academicResultUpdate
            ) &&
            state.firstLoaded
        ) {
            setState({ loading: true, academicResultUpdate: false })
            requestData(1)
            dispatch({
                type: 'course/r_updateState',
                payload: {
                    searchCourseFilter: {
                        ...courseState.searchCourseFilter,
                        region_id: state.region_id,
                        country_id: state.country_id,
                        major_id: state.major_id,
                        study_field_id: state.study_field_id,
                        course_level_id: state.course_level_id,
                        qualificationActiveId: state.qualificationActiveId,
                        ipCountryCodeId: configState.ipCountryCodeId,
                        ipCountryCode: configState.ipCountryCode,
                        studentAcademicResult: state.studentAcademicResult
                    }
                }
            })
        }
    }, [
        state.region_id,
        state.country_id,
        state.major_id,
        state.study_field_id,
        state.course_level_id,
        state.qualificationActiveId,
        state.ipCountryCodeId,
        state.academicResultUpdate
    ])

    useEffect(() => {
        setState({ firstLoaded: true })

        window.scrollTo(0, state.position);
        if (courseState.searchCourseFilter.dataList.length <= 0) {
            setState({ loading: true })
            dispatch({
                type: 'course/e_getSelection',
                payload: {}
            }).then((value) => {
                if (value) {
                    setState({
                        ...value
                    })
                    requestData(1)
                }
            })
        }

    }, [])

    useEffect(() => {
        if (state.ipCountryCode != 'USD' && state.checkedIp) {
            Object.keys(state.currencyTypeList).map((codeId) => {
                if (state.currencyTypeList[codeId] == state.ipCountryCode) {
                    setState({ ipCountryCodeId: codeId, checkedIp: true })
                }
            })
        }
    }, [state.ipCountryCodeId, state.currencyTypeList])

    useEffect(() => {
        let studentQualificationSubjectList = state.studentQualificationSubjectList
        Object.keys(state.studentAcademicResult).map((qualification_id) => {
            state.studentAcademicResult[qualification_id].forEach(subject => {
                if (studentQualificationSubjectList[qualification_id]) {
                    studentQualificationSubjectList[qualification_id][subject.qualification_subject_id] = subject.qualification_grade_id
                } else {
                    studentQualificationSubjectList[qualification_id] = {
                        [subject.qualification_subject_id]: subject.qualification_grade_id
                    }
                }
            })
        })
        setState({ studentQualificationSubjectList })
    }, [state.studentAcademicResult])

    useEffect(() => {
        if (window.document.getElementsByClassName("ant-drawer-open")[0] && state.filterModalVisible) {
            if (window.document.getElementsByClassName("drawer-book")[0]) {
                window.document.getElementsByClassName("drawer-book")[0].remove()
            }
            var fatherDiv = document.createElement("div")
            fatherDiv.className = 'drawer-book'

            var box1 = document.createElement("div")
            box1.className = 'drawer-book-child-box'

            var childDiv1 = document.createElement("div")
            var textnode1 = document.createTextNode(formatMessage({ id: 'location' }))
            var contentDiv1 = document.createElement("div")
            contentDiv1.style.margin = 'auto'
            contentDiv1.appendChild(textnode1)
            childDiv1.appendChild(contentDiv1)
            childDiv1.appendChild(box1)
            childDiv1.className = state.bookMode == 'location' ? 'drawer-book-child active' : 'drawer-book-child'
            childDiv1.onclick = () => {
                setState({
                    bookMode: 'location'
                })
            }

            var box2 = document.createElement("div")
            box2.className = 'drawer-book-child-box'

            var childDiv2 = document.createElement("div")
            var textnode2 = document.createTextNode(formatMessage({ id: 'qualification' }))
            var contentDiv2 = document.createElement("div")
            contentDiv2.style.margin = 'auto'
            contentDiv2.appendChild(textnode2)
            childDiv2.appendChild(contentDiv2)
            childDiv2.appendChild(box2)
            childDiv2.className = state.bookMode == 'qualification' ? 'drawer-book-child active' : 'drawer-book-child'
            childDiv2.onclick = () => {
                setState({
                    bookMode: 'qualification'
                })
            }

            /* var box3 = document.createElement("div")
            box3.className = 'drawer-book-child-box'

            var childDiv3 = document.createElement("div")
            var textnode3 = document.createTextNode(formatMessage({ id: 'proficiency' }))
            var contentDiv3 = document.createElement("div")
            contentDiv3.style.margin = 'auto'
            contentDiv3.appendChild(textnode3)
            childDiv3.appendChild(contentDiv3)
            childDiv3.appendChild(box3)
            childDiv3.className = state.bookMode == 'proficiency' ? 'drawer-book-child active' : 'drawer-book-child'
            childDiv3.onclick = () => {
                setState({
                    bookMode: 'proficiency'
                })
            } */

            fatherDiv.appendChild(childDiv1)
            fatherDiv.appendChild(childDiv2)
            //fatherDiv.appendChild(childDiv3)
            window.document.getElementsByClassName("ant-drawer-open")[0].appendChild(fatherDiv)
        }
    }, [state.filterModalVisible, state.bookMode])

    function updateValidSelection() {
        let levelAllow = false
        let studyFieldAllow = false
        let majorAllow = true

        if (state.validOriKey == 'major') {
            majorAllow = false
        } else if (state.validOriKey == 'level') {
            studyFieldAllow = true
        } else if (state.validOriKey == 'country' || state.validOriKey == 'region') {
            levelAllow = true
            studyFieldAllow = true
        }

        let d = {
            validLevel: !levelAllow ? state.validLevel : {},
            validStudyField: !studyFieldAllow ? state.validStudyField : {},
            validMajor: !majorAllow ? state.validMajor : {}
        }

        state.selectionData.map((c) => {
            if (c.sub_tag_list != null && c.main_tag_list != null) {
                let majorList = c.sub_tag_list.split(',')
                let studyFieldList = c.main_tag_list.split(',')
                let level = c.school_course_level_id

                if (levelAllow) {
                    if (d.validLevel[level]) {
                        d.validLevel[level]++
                    } else {
                        d.validLevel[level] = 1
                    }
                }

                if (studyFieldAllow) {
                    studyFieldList.map((s) => {
                        if (d.validStudyField[s]) {
                            d.validStudyField[s]++
                        } else {
                            d.validStudyField[s] = 1
                        }
                    })
                }

                if (majorAllow) {
                    majorList.map((s) => {
                        if (d.validMajor[s]) {
                            d.validMajor[s]++
                        } else {
                            d.validMajor[s] = 1
                        }
                    })
                }
            }

        })
        return d
    }

    useEffect(() => {
        let d = updateValidSelection()
        setState({ ...d, validOriKey: 'country' })
        dispatch({
            type: 'course/r_updateState',
            payload: {
                searchCourseFilter: {
                    ...courseState.searchCourseFilter,
                    dataList: state.dataList,
                    pageSize: state.pageSize,
                    totalItem: state.totalItem,
                    currentPage: state.currentPage,
                    countryList: state.countryList,
                    courseLevelList: state.courseLevelList,
                    currencyTypeList: state.currencyTypeList,
                    majorList: state.majorList,
                    prificiencyList: state.prificiencyList,
                    qualificationGradeList: state.qualificationGradeList,
                    qualificationList: state.qualificationList,
                    qualificationSubjectList: state.qualificationSubjectList,
                    regionList: state.regionList,
                    schoolTypeList: state.schoolTypeList,
                    studentAcademicResult: state.studentAcademicResult,
                    studyFieldList: state.studyFieldList,
                    selectionData: state.selectionData,
                    validOriKey: state.validOriKey,
                    ...d
                }
            }
        })
    }, [state.dataList])

    function requestData(clickPage = state.currentPage, combine = false) {
        dispatch({
            type: 'course/e_getCourse',
            payload: {
                isList: true,
                clickPage,
                filterArr: {
                    course_level_id: state.course_level_id,
                    study_field_id: state.study_field_id,
                    major_id: state.major_id,
                    country_id: state.country_id,
                    region_id: state.region_id
                },
                qualification_id: state.qualificationActiveId,
                currency_code: state.ipCountryCodeId,
                secondary_complete: state.secondary_complete
            }
        }).then((value) => {
            if (combine) {
                let dataList = state.dataList
                dataList = dataList.concat(value.dataList);
                value.dataList = dataList
            } else {
                window.scrollTo(0, 0)
            }

            setState({
                loading: false,
                dataLoading: false,
                currentPage: clickPage,
                ...value
            })
        })
    }

    function checkQualificationSubjectGradeSubmit() {
        let allow = true
        if (
            typeof state.studentQualificationSubjectList[state.qualification_id] != 'undefined' &&
            Object.keys(state.studentQualificationSubjectList[state.qualification_id]).length > 0
        ) {
            allow = false
            Object.keys(state.studentQualificationSubjectList[state.qualification_id]).map((arrKey, key) => {
                if (state.studentQualificationSubjectList[state.qualification_id][arrKey] <= 0) {
                    allow = true
                }
            })

            if (allow && state.addQualificationSubjectEditMode) {
                allow = false
            }
        }
        return allow
    }

    function renderComponent() {
        return (
            <React.Fragment>
                <Helmet>
                    <meta name="description" content="Course" />
                    <link rel="canonical" href={baseUrl + "searchCourse"} />
                </Helmet>

                <Loading loading={state.loading} />
                <Mmodal
                    popup
                    visible={state.addQualificationModalVisible}
                    onClose={() => {
                        if (typeof state.studentQualificationSubjectList[state.qualification_id] == 'undefined') {
                            setState({
                                addQualificationModalVisible: false,
                                qualification_id: 0
                            })
                        } else {
                            let allow = true
                            Object.keys(state.studentQualificationSubjectList[state.qualification_id]).map((arrKey, key) => {
                                if (state.studentQualificationSubjectList[state.qualification_id][arrKey] <= 0) {
                                    allow = false
                                }
                            })
                            if (allow) {
                                setState({ loading: true })
                                dispatch({
                                    type: 'course/e_updateStudentAcademicResult',
                                    payload: {
                                        studentQualificationSubjectList: state.studentQualificationSubjectList[state.qualification_id],
                                        qualification_id: state.qualification_id

                                    }
                                }).then((value) => {
                                    if (value) {
                                        let qualificationActiveId = state.qualificationActiveId
                                        if (qualificationActiveId == state.qualification_id) {
                                            qualificationActiveId = 0
                                        }

                                        let studentQualificationSubjectList = state.studentQualificationSubjectList
                                        delete studentQualificationSubjectList[state.qualification_id]
                                        setState({
                                            ...value,
                                            qualificationActiveId,
                                            qualification_id: 0,
                                            addQualificationModalVisible: false,
                                            studentQualificationSubjectList,
                                            academicResultUpdate: true
                                        })
                                    }
                                    setState({ loading: false })
                                })
                            }
                        }

                    }}
                    animationType="slide-up"
                    //afterClose={() => { alert('afterClose'); }}
                    //style={{ zIndex: '10000' }}
                    wrapClassName='addQualificationMobileModal'
                >


                    <List renderHeader={() =>
                        <div className='addQualificationMobileModal-title'>
                            <div className='addQualificationMobileModal-title-text'>
                                {formatMessage({ id: 'add your qualification' }) + ' ( ' + state.qualificationTitle + ' )'}
                            </div>
                            <div
                                className={state.addQualificationSubjectEditMode ? 'addQualificationMobileModal-title-setting-active' : 'addQualificationMobileModal-title-setting'}
                                onClick={() => setState({ addQualificationSubjectEditMode: true })}
                            >
                                <SettingOutlined />
                            </div>
                        </div>
                    }
                        className="popup-list"
                    >
                        {
                            (
                                state.addQualificationSubjectEditMode ||
                                (
                                    typeof state.studentQualificationSubjectList[state.qualification_id] == 'undefined' ||
                                    Object.keys(state.studentQualificationSubjectList[state.qualification_id]).length == 0
                                )
                            ) &&
                            Object.values(state.qualificationSubjectList).map((subject, key) => {
                                if (subject.qualification_id == state.qualification_id) {
                                    //return (<List.Item key={key}>{subject.subject_name}</List.Item>)
                                    return (
                                        <Checkbox.CheckboxItem
                                            checked={
                                                state.studentQualificationSubjectList[state.qualification_id] &&
                                                state.studentQualificationSubjectList[state.qualification_id][subject.id] >= 0
                                            }
                                            key={key}
                                            onChange={(e) => {
                                                let studentQualificationSubjectList = state.studentQualificationSubjectList
                                                if (e.target.checked) {

                                                    if (studentQualificationSubjectList[state.qualification_id]) {
                                                        studentQualificationSubjectList[state.qualification_id][subject.id] = 0
                                                    } else {
                                                        studentQualificationSubjectList[state.qualification_id] = {
                                                            [subject.id]: 0
                                                        }
                                                    }
                                                } else {
                                                    delete studentQualificationSubjectList[state.qualification_id][subject.id]
                                                }

                                                setState({ studentQualificationSubjectList })
                                            }}
                                        >
                                            {subject.subject_name}
                                        </Checkbox.CheckboxItem>
                                    )
                                }

                            })
                        }
                        {
                            !state.addQualificationSubjectEditMode &&
                            Object.values(state.qualificationSubjectList).map((subject, key) => {
                                if (
                                    subject.qualification_id == state.qualification_id &&
                                    state.studentQualificationSubjectList[state.qualification_id] &&
                                    Object.keys(state.studentQualificationSubjectList[state.qualification_id]).indexOf(subject.id.toString()) >= 0
                                ) {
                                    //return (<List.Item key={key}>{subject.subject_name}</List.Item>)
                                    return (
                                        <List.Item
                                            checked={
                                                state.studentQualificationSubjectList[state.qualification_id] &&
                                                state.studentQualificationSubjectList[state.qualification_id][subject.id] >= 0
                                            }
                                            key={key}
                                            onChange={(e) => {
                                                let studentQualificationSubjectList = state.studentQualificationSubjectList
                                                if (e.target.checked) {

                                                    if (studentQualificationSubjectList[state.qualification_id]) {
                                                        studentQualificationSubjectList[state.qualification_id][subject.id] = 0
                                                    } else {
                                                        studentQualificationSubjectList[state.qualification_id] = {
                                                            [subject.id]: 0
                                                        }
                                                    }
                                                } else {
                                                    delete studentQualificationSubjectList[state.qualification_id][subject.id]
                                                }

                                                setState({ studentQualificationSubjectList })
                                            }}
                                        >
                                            <div className='addQualificationMobileModal-subject'>
                                                {subject.subject_name}
                                            </div>

                                            <Button
                                                onClick={() => {
                                                    let gradeList = []
                                                    Object.values(state.qualificationGradeList).map((value, key) => {
                                                        if (value.qualification_id == state.qualification_id) {
                                                            gradeList.push(
                                                                {
                                                                    text: value.grade,
                                                                    onPress: () => {
                                                                        let studentQualificationSubjectList = state.studentQualificationSubjectList
                                                                        studentQualificationSubjectList[state.qualification_id][subject.id] = value.id
                                                                        setState({ studentQualificationSubjectList })
                                                                    }
                                                                }
                                                            )
                                                        }
                                                    })
                                                    Mmodal.operation(gradeList
                                                    )
                                                }}
                                                className='addQualificationMobileModal-grade_btn'
                                            >
                                                <div className='addQualificationMobileModal-grade'>
                                                    {
                                                        state.studentQualificationSubjectList[state.qualification_id][subject.id] == 0 &&
                                                        <DownOutlined />
                                                    }
                                                    {
                                                        Object.values(state.qualificationGradeList).map((value, key) => {
                                                            if (value.id == state.studentQualificationSubjectList[state.qualification_id][subject.id]) {
                                                                return <div style={{ color: PRIMARY_COLOR, width: '30px', textAlign: 'left' }}>{value.grade}</div>
                                                            }
                                                        })
                                                    }
                                                </div>
                                            </Button>
                                        </List.Item>
                                    )
                                }

                            })
                        }
                    </List>

                    <Button
                        className='submit'
                        disabled={checkQualificationSubjectGradeSubmit()}
                        onClick={() => {
                            if (state.addQualificationSubjectEditMode) {
                                setState({ addQualificationSubjectEditMode: false })
                            } else if (state.addQualificationModalVisible) {
                                setState({ loading: true })
                                dispatch({
                                    type: 'course/e_updateStudentAcademicResult',
                                    payload: {
                                        studentQualificationSubjectList: state.studentQualificationSubjectList[state.qualification_id],
                                        qualification_id: state.qualification_id

                                    }
                                }).then((value) => {
                                    if (value) {
                                        let qualificationActiveId = state.qualificationActiveId
                                        qualificationActiveId = state.qualification_id
                                        setState({
                                            qualificationActiveId,
                                            ...value,
                                            academicResultUpdate: true
                                        })
                                    }
                                    setState({ loading: false })
                                })
                                setState({ addQualificationModalVisible: false })
                            }
                        }}
                    >
                        {formatMessage({ id: state.addQualificationSubjectEditMode ? 'done' : 'submit' })}
                    </Button>
                </Mmodal>

                <Drawer
                    footer={
                        <div className='search-drawer-footer'>
                            <div className='search-drawer-footer-button'>
                                <div
                                    className='search-drawer-footer-button-reset'
                                    onClick={() => {
                                        setState({
                                            country_id: 0,
                                            region_id: 0,
                                            qualificationActiveId: 0
                                        })
                                    }}
                                >
                                    {formatMessage({ id: 'reset' })}
                                </div>
                                <div
                                    className='search-drawer-footer-button-ok'
                                    onClick={() => {
                                        setState({
                                            filterModalVisible: false,
                                            //qualificationBookEditMode: false
                                        })
                                        window.document.getElementsByClassName("drawer-book")[0].remove()
                                    }}
                                >
                                    {formatMessage({ id: 'submit' })}
                                </div>
                            </div>
                        </div>
                    }
                    forceRender
                    zIndex='10000'
                    placement='right'
                    closable={false}
                    onClose={() => {
                        window.document.getElementsByClassName("drawer-book")[0].remove()
                        setState({ filterModalVisible: false })
                    }}
                    visible={state.filterModalVisible}
                >
                    {
                        state.bookMode == 'location' &&
                        <React.Fragment>
                            {/* <div className='search-drawer'>
                                <div className='search-drawer-title'>
                                    {formatMessage({ id: 'country' })}
                                </div>
                                <div className='search-drawer-selection'>
                                    {
                                        Object.values(state.countryList).map((value, key) => {
                                            return <div
                                                key={key}
                                                className={value.id == state.country_id ? 'search-drawer-selection-child active' : 'search-drawer-selection-child'}
                                                onClick={() => {
                                                    if (state.country_id == value.id) {
                                                        setState({ country_id: 0, region_id: 0, validOriKey: 'country' })
                                                    } else {
                                                        setState({ country_id: value.id, region_id: 0, course_level_id: 0, study_field_id: 0, major_id: 0, validOriKey: 'country' })
                                                    }
                                                }}
                                            >
                                                {value.country}
                                            </div>
                                        })
                                    }
                                </div>
                            </div>
                            <Divider className='search-drawer-divider' /> */}

                            {
                                /* state.country_id > 0 && */
                                <div
                                    className='search-drawer'
                                    style={{ marginTop: '10px' }}
                                    onClick={() => {
                                        if (state.country_id > 0) {
                                            pushUpSelection(region.current)
                                        }
                                    }}
                                >
                                    <div className='search-drawer-title' style={{ marginBottom: 'unset' }}>
                                        <div>{formatMessage({ id: 'region' })}</div>
                                        <div
                                            className={state.region_id == 0 ? 'search-drawer-title-content' : 'search-drawer-title-content active'}
                                            //style={{ border: state.country_id > 0 ? 'unset' : '1px solid #80808066' }}
                                            {
                                            ...(
                                                state.country_id <= 0
                                                    ? {
                                                        style: {
                                                            border: '1px solid #80808066',
                                                            color: '#80808066'
                                                        }
                                                    }
                                                    : {}
                                            )
                                            }
                                        >
                                            {
                                                state.region_id == 0
                                                    ? <div className='search-drawer-title-content-text'>
                                                        {formatMessage({ id: 'all' })}
                                                    </div>
                                                    : Object.values(state.regionList).map((value, key) => {
                                                        if (value.id == state.region_id) {
                                                            return <div className='search-drawer-title-content-text active'>{value.region}</div>
                                                        }
                                                    })
                                            }
                                            <CaretDownOutlined style={{ fontSize: '10px' }} />
                                        </div>
                                    </div>
                                    <div className='search-drawer-dropdown'>
                                        <select
                                            ref={region}
                                            value={state.region_id}
                                            onChange={(e) => setState({ region_id: e.target.value })}
                                        >
                                            <option value={0}>{formatMessage({ id: 'all' })}</option>
                                            {
                                                Object.values(state.regionList).map((value, key) => {
                                                    if (state.country_id == value.country_id) {
                                                        return <option value={value.id} key={key}>
                                                            {value.region}
                                                        </option>
                                                    }
                                                })
                                            }
                                        </select>
                                    </div>
                                    <Divider className='search-drawer-divider' />
                                </div>
                            }
                        </React.Fragment>
                    }

                    {
                        state.bookMode == 'qualification' &&
                        <React.Fragment>
                            <div className='search-drawer'>
                                <div className='search-drawer-title2'>
                                    <div className='search-drawer-title2-content'>{formatMessage({ id: 'qualification' })}</div>
                                    <div className='search-drawer-title2-setting' onClick={() => setState({ qualificationBookEditMode: !state.qualificationBookEditMode })}>
                                        <SettingOutlined style={{ color: state.qualificationBookEditMode ? PRIMARY_COLOR : 'black' }} />
                                    </div>
                                </div>
                                <div className='search-drawer-selection_qualification'>
                                    {
                                        Object.values(state.qualificationList).map((value, key) => {
                                            return <div
                                                key={key}
                                                className={
                                                    (
                                                        typeof state.studentQualificationSubjectList[value.id] == 'undefined' ||
                                                        Object.keys(state.studentQualificationSubjectList[value.id]).length <= 0
                                                    )
                                                        ? 'search-drawer-selection-child'
                                                        : state.qualificationActiveId == value.id
                                                            ? 'search-drawer-selection-child active'
                                                            : 'search-drawer-selection-child sActive'
                                                }
                                                onClick={() => {
                                                    if (typeof state.studentQualificationSubjectList[value.id] == 'undefined' || Object.keys(state.studentQualificationSubjectList[value.id]).length <= 0) {
                                                        setState({
                                                            qualificationTitle: value.qualification_name,
                                                            addQualificationModalVisible: true,
                                                            qualification_id: value.id,
                                                            addQualificationSubjectEditMode: state.studentQualificationSubjectList[value.id] ? false : true
                                                        })
                                                    } else if (state.qualificationBookEditMode) {
                                                        setState({
                                                            qualificationTitle: value.qualification_name,
                                                            addQualificationModalVisible: true,
                                                            qualification_id: value.id,
                                                            addQualificationSubjectEditMode: state.studentQualificationSubjectList[value.id] ? false : true
                                                        })
                                                    } else if (
                                                        typeof state.studentQualificationSubjectList[value.id] != 'undefined' &&
                                                        Object.keys(state.studentQualificationSubjectList[value.id]).length > 0
                                                    ) {
                                                        if (state.qualificationActiveId == value.id) {
                                                            setState({ qualificationActiveId: 0 })
                                                        } else {
                                                            setState({ qualificationActiveId: value.id })
                                                        }
                                                    }
                                                }}
                                            >
                                                {value.qualification_name}
                                                {
                                                    state.qualificationBookEditMode &&
                                                    typeof state.studentQualificationSubjectList[value.id] != 'undefined' &&
                                                    Object.keys(state.studentQualificationSubjectList[value.id]).length > 0 &&
                                                    <div
                                                        className='delete'
                                                        onClick={(e) => {
                                                            e.stopPropagation()
                                                            Mmodal.alert(formatMessage({ id: 'delete' }), 'Are you sure???', [
                                                                { text: formatMessage({ id: 'cancel' }), onPress: () => console.log('cancel') },
                                                                {
                                                                    text: formatMessage({ id: 'confirm' }),
                                                                    onPress: () => {
                                                                        setState({ loading: true })
                                                                        dispatch({
                                                                            type: 'course/e_updateStudentAcademicResult',
                                                                            payload: {
                                                                                studentQualificationSubjectList: {},
                                                                                qualification_id: value.id

                                                                            }
                                                                        }).then((result) => {
                                                                            if (result) {
                                                                                let qualificationActiveId = state.qualificationActiveId
                                                                                if (qualificationActiveId == value.id) {
                                                                                    qualificationActiveId = 0
                                                                                }

                                                                                let studentQualificationSubjectList = state.studentQualificationSubjectList
                                                                                delete studentQualificationSubjectList[value.id]
                                                                                setState({
                                                                                    ...result,
                                                                                    qualificationActiveId,
                                                                                    qualification_id: 0,
                                                                                    qualificationBookEditMode: false,
                                                                                    studentQualificationSubjectList,
                                                                                    academicResultUpdate: true
                                                                                })
                                                                            }
                                                                            setState({ loading: false })
                                                                        })
                                                                    },
                                                                },
                                                            ])
                                                        }}
                                                    >
                                                        －
                                                    </div>
                                                }

                                            </div>
                                        })
                                    }

                                </div>
                            </div>
                            <Divider className='search-drawer-divider' />
                        </React.Fragment>
                    }

                </Drawer>
            </React.Fragment >
        )
    }

    function pushUpSelection(elem) {
        if (document.createEvent) {
            var e = document.createEvent("MouseEvents");
            e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            elem[0].dispatchEvent(e);
        } else if (element.fireEvent) {
            elem[0].fireEvent("onmousedown");
        }
    }



    return (
        <div className='search'>
            {renderComponent()}
            <TopBanner
                backBtn={false}
                leftButton={
                    <div className='search-currency_selection' onClick={() => pushUpSelection(currency.current)}>
                        <div className='search-currency_selection-text'>
                            {state.ipCountryCode}
                            <CaretDownOutlined style={{ fontSize: '10px', marginLeft: '5px' }} />
                        </div>
                        <div className='search-currency_selection-icon'>
                            <img src={require('@/assets/' + state.ipCountryCode + '.png')} style={{ width: '20px', height: '100%' }} />
                        </div>
                        <div className='search-currency_selection-option'>
                            <select
                                ref={currency}
                                value={state.ipCountryCodeId}
                                onChange={(e) => {
                                    let ipCountryCode = state.ipCountryCode
                                    Object.keys(state.currencyTypeList).map((key, arrkey) => {
                                        if (key == e.target.value) {
                                            ipCountryCode = state.currencyTypeList[key]
                                        }
                                    })
                                    setState({
                                        ipCountryCodeId: e.target.value,
                                        ipCountryCode
                                    })
                                }}
                            >
                                {
                                    Object.keys(state.currencyTypeList).map((key, arrkey) => {
                                        return <option value={parseInt(key)} key={arrkey}>
                                            {state.currencyTypeList[key]}
                                        </option>
                                    })
                                }
                            </select>
                        </div>
                    </div>
                }
                title={formatMessage({ id: 'search course' })}
                rightButton={
                    // <div className='search-currency_selection' onClick={() => pushUpSelection(currency.current)}>
                    //     <div className='search-currency_selection-icon'>
                    //         <img src={require('@/assets/' + state.ipCountryCode + '.png')} style={{ width: '20px', height: '100%' }} />
                    //     </div>
                    //     <div className='search-currency_selection-text'>
                    //         {state.ipCountryCode}
                    //         <CaretDownOutlined style={{ fontSize: '10px', marginLeft: '10px' }} />
                    //     </div>
                    //     <div className='search-currency_selection-option'>
                    //         <select
                    //             ref={currency}
                    //             value={state.ipCountryCodeId}
                    //             onChange={(e) => {
                    //                 let ipCountryCode = state.ipCountryCode
                    //                 Object.keys(state.currencyTypeList).map((key, arrkey) => {
                    //                     if (key == e.target.value) {
                    //                         ipCountryCode = state.currencyTypeList[key]
                    //                     }
                    //                 })
                    //                 setState({
                    //                     ipCountryCodeId: e.target.value,
                    //                     ipCountryCode
                    //                 })
                    //             }}
                    //         >
                    //             {
                    //                 Object.keys(state.currencyTypeList).map((key, arrkey) => {
                    //                     return <option value={parseInt(key)} key={arrkey}>
                    //                         {state.currencyTypeList[key]}
                    //                     </option>
                    //                 })
                    //             }
                    //         </select>
                    //     </div>
                    // </div>
                    // <div
                    //     className='search-filter'
                    //     style={{
                    //         //borderLeft: '1px solid #4a3c3c30',
                    //         textAlign: 'center',
                    //         //flex: '0 0 20%',
                    //         display: 'flex',
                    //         fontSize: state.country_id > 0 || state.qualificationActiveId ? '15px' : 'unset',
                    //         color: state.country_id > 0 || state.qualificationActiveId ? 'white' : 'unset'
                    //     }}
                    //     onClick={() => setState({ filterModalVisible: true })}
                    // >
                    //     <div style={{ marginRight: '5px' }}>{formatMessage({ id: 'filter' })}</div> <FilterOutlined style={{ fontSize: '14px', display: 'flex', alignItems: 'center' }} />
                    // </div>
                    <div
                        className='search-filter'
                        style={{
                            //borderLeft: '1px solid #4a3c3c30',
                            textAlign: 'center',
                            //flex: '0 0 20%',
                            display: 'flex',
                            fontSize: state.country_id > 0 || state.qualificationActiveId ? '15px' : 'unset',
                            color: state.country_id > 0 || state.qualificationActiveId ? 'white' : 'unset'
                        }}
                    //onClick={() => setState({ filterModalVisible: true })}
                    >

                        <div style={{ marginRight: '5px' }}>{formatMessage({
                            id:
                                Object.values(state.countryList).map((value) => {
                                    if (state.country_id == value.id) {
                                        return value.country
                                    }
                                })
                        })}</div>
                    </div>
                }
                /* extraFilter={
                    <div
                        className='search-filter'
                        style={{
                            borderLeft: '1px solid #4a3c3c30',
                            textAlign: 'center',
                            flex: '0 0 20%',
                            color: state.country_id > 0 || state.qualificationActiveId ? PRIMARY_COLOR : '#4a3c3cad'
                        }}
                        onClick={() => setState({ filterModalVisible: true })}
                    >
                        {formatMessage({ id: 'filter' })} <FilterOutlined style={{ fontSize: '14px' }} />
                    </div>
                } */
                subMenuButton={[
                    // {
                    //     label: <div
                    //         className='search-filter'
                    //         onClick={() => pushUpSelection(country.current)}
                    //     //style={{ display: 'flex', alignItems: 'center', position: 'relative' }}
                    //     >
                    //         <div className='search-filter-content' style={{ textAlign: 'center', borderRight: '1px solid #ddd' }}>
                    //             <div className={state.country_id > 0 && 'active'}
                    //                 style={{
                    //                     /* WebkitBoxOrient: 'vertical', */
                    //                     fontSize: '11px',
                    //                     //width: '100px'
                    //                 }}
                    //             >
                    //                 <div style={{ fontWeight: '500', display: 'flex', alignItems: 'center', justifyContent: 'center', color: 'grey' }}>
                    //                     {formatMessage({ id: 'country' })} <CaretDownOutlined style={{ fontSize: '10px', marginLeft: '5px' }}/*  className={state.country_id > 0 && 'active'} */ />
                    //                 </div>
                    //                 {
                    //                     state.country_id > 0
                    //                         ? <div
                    //                             style={{
                    //                                 overflow: 'hidden',
                    //                                 WebkitBoxOrient: 'vertical',
                    //                                 width: '90px',
                    //                                 display: '-webkit-box',
                    //                                 WebkitLineClamp: 2
                    //                             }}
                    //                         >
                    //                             {
                    //                                 Object.values(state.countryList).map((value) => {
                    //                                     if (state.country_id == value.id) {
                    //                                         return value.country
                    //                                     }
                    //                                 })
                    //                             }
                    //                         </div>
                    //                         : formatMessage({ id: 'all' })
                    //                 }
                    //             </div>
                    //             <div /* className={state.country_id > 0 ? 'search-filter-selection-active' : 'search-filter-selection'} */ className='search-filter-selection-active'>
                    //                 <select
                    //                     ref={country}
                    //                     value={state.country_id}
                    //                     onChange={(e) => setState({ country_id: e.target.value, course_level_id: 0, study_field_id: 0, major_id: 0, validOriKey: 'country' })}
                    //                 >
                    //                     <option value={0}>{formatMessage({ id: 'all' })}</option>
                    //                     {
                    //                         Object.values(state.countryList).map((value, key) => {
                    //                             return <option value={value.id} key={key}>
                    //                                 {value.country}
                    //                             </option>
                    //                         })
                    //                     }
                    //                 </select>
                    //             </div>
                    //         </div>
                    //         {/* <CaretDownOutlined style={{ fontSize: '10px', marginLeft: '10px' }} className={state.country_id > 0 && 'active'} /> */}
                    //         <div style={{ fontSize: '20px', width: '1px', right: 0, position: 'absolute', color: '#4a3c3c30' }}>
                    //             |
                    //         </div>
                    //     </div>
                    // },
                    {
                        label: <div
                            className='search-filter'
                            onClick={() => pushUpSelection(region.current)}
                        //style={{ display: 'flex', alignItems: 'center', position: 'relative' }}
                        >
                            <div className='search-filter-content' style={{ textAlign: 'center', borderRight: '1px solid #ddd' }}>
                                <div className={state.region_id > 0 && 'active'}
                                    style={{
                                        /* WebkitBoxOrient: 'vertical', */
                                        fontSize: '11px',
                                        //width: '100px'
                                    }}
                                >
                                    <div style={{ fontWeight: '500', display: 'flex', alignItems: 'center', justifyContent: 'center', color: 'grey' }}>
                                        {formatMessage({ id: 'region' })} <CaretDownOutlined style={{ fontSize: '10px', marginLeft: '5px' }}/*  className={state.country_id > 0 && 'active'} */ />
                                    </div>
                                    {
                                        state.region_id > 0
                                            ? <div
                                                style={{
                                                    overflow: 'hidden',
                                                    WebkitBoxOrient: 'vertical',
                                                    width: '90px',
                                                    display: '-webkit-box',
                                                    WebkitLineClamp: 2
                                                }}
                                            >
                                                {
                                                    Object.values(state.regionList).map((value) => {
                                                        if (state.region_id == value.id) {
                                                            return value.region
                                                        }
                                                    })
                                                }
                                            </div>
                                            : formatMessage({ id: 'all' })
                                    }
                                </div>
                                <div /* className={state.country_id > 0 ? 'search-filter-selection-active' : 'search-filter-selection'} */ className='search-filter-selection-active'>
                                    <select
                                        ref={region}
                                        value={state.region_id}
                                        onChange={(e) => setState({ region_id: e.target.value, course_level_id: 0, study_field_id: 0, major_id: 0, validOriKey: 'region' })}
                                    >
                                        <option value={0}>{formatMessage({ id: 'all' })}</option>
                                        {
                                            Object.values(state.regionList).map((value, key) => {
                                                if (value.country_id == state.country_id) {
                                                    return <option value={value.id} key={key}>
                                                        {value.region}
                                                    </option>
                                                }
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                            {/* <CaretDownOutlined style={{ fontSize: '10px', marginLeft: '10px' }} className={state.country_id > 0 && 'active'} /> */}
                            <div style={{ fontSize: '20px', width: '1px', right: 0, position: 'absolute', color: '#4a3c3c30' }}>
                                |
                            </div>
                        </div>
                    },
                    {
                        label: <div className='search-filter'
                            onClick={() => pushUpSelection(level.current)}
                        //style={{ display: 'flex', alignItems: 'center' }}
                        >
                            <div className='search-filter-content' style={{ textAlign: 'center' }}>
                                <div className={state.course_level_id > 0 && 'active'}
                                    style={{
                                        /* WebkitBoxOrient: 'vertical', */
                                        fontSize: '11px',
                                        //width: '100px'
                                    }}
                                >
                                    <div style={{ fontWeight: '500', display: 'flex', alignItems: 'center', justifyContent: 'center', color: 'grey' }}>
                                        {formatMessage({ id: 'level' })} <CaretDownOutlined style={{ fontSize: '10px', marginLeft: '5px' }}/*  className={state.country_id > 0 && 'active'} */ />
                                    </div>
                                    {
                                        state.course_level_id > 0
                                            ? <div
                                                style={{
                                                    overflow: 'hidden',
                                                    WebkitBoxOrient: 'vertical',
                                                    width: '90px',
                                                    display: '-webkit-box',
                                                    WebkitLineClamp: 2
                                                }}
                                            >
                                                {
                                                    Object.values(state.courseLevelList).map((value) => {
                                                        if (state.course_level_id == value.id) {
                                                            return value.level_name
                                                        }
                                                    })
                                                }
                                            </div>
                                            : formatMessage({ id: 'all' })
                                    }
                                </div>
                                <div /* className={state.country_id > 0 ? 'search-filter-selection-active' : 'search-filter-selection'} */ className='search-filter-selection-active'>
                                    <select
                                        ref={level}
                                        value={state.course_level_id}
                                        onChange={(e) => setState({ course_level_id: e.target.value, study_field_id: 0, major_id: 0, validOriKey: 'level' })}
                                    >
                                        <option value={0}>{formatMessage({ id: 'all' })}</option>
                                        {
                                            Object.values(state.courseLevelList).map((value, key) => {
                                                if (state.validLevel[value.id]) {
                                                    return <option value={value.id} key={key}>
                                                        {value.level_name}
                                                    </option>
                                                }
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                        </div>
                    },
                    {
                        label: <div
                            className='search-filter'
                            onClick={() => pushUpSelection(field.current)}
                        >
                            <div className='search-filter-content' style={{ textAlign: 'center' }}>
                                <div className={state.study_field_id > 0 && 'active'}
                                    style={{
                                        /* WebkitBoxOrient: 'vertical', */
                                        fontSize: '11px',
                                        //width: '100px'
                                    }}
                                >
                                    <div style={{ fontWeight: '500', display: 'flex', alignItems: 'center', justifyContent: 'center', color: 'grey' }}>
                                        {formatMessage({ id: 'field' })} <CaretDownOutlined style={{ fontSize: '10px', marginLeft: '5px' }}/*  className={state.country_id > 0 && 'active'} */ />
                                    </div>
                                    {
                                        state.study_field_id > 0
                                            ? <div
                                                style={{
                                                    overflow: 'hidden',
                                                    WebkitBoxOrient: 'vertical',
                                                    width: '90px',
                                                    display: '-webkit-box',
                                                    WebkitLineClamp: 2
                                                }}
                                            >
                                                {
                                                    Object.values(state.studyFieldList).map((value) => {
                                                        if (state.study_field_id == value.id) {
                                                            return value.main_tag
                                                        }
                                                    })
                                                }
                                            </div>
                                            : formatMessage({ id: 'all' })
                                    }
                                </div>
                                <div /* className={state.country_id > 0 ? 'search-filter-selection-active' : 'search-filter-selection'} */ className='search-filter-selection-active'>
                                    <select
                                        ref={field}
                                        value={state.study_field_id}
                                        onChange={(e) => {
                                            setState({
                                                study_field_id: e.target.value,
                                                major_id: 0,
                                                validOriKey: 'study_field'
                                            })
                                        }}
                                    >
                                        <option value={0}>{formatMessage({ id: 'all' })}</option>
                                        {
                                            Object.values(state.studyFieldList).map((value, key) => {
                                                if (state.validStudyField[value.id]) {
                                                    return <option value={value.id} key={key}>
                                                        {value.main_tag}
                                                    </option>
                                                }
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                        </div>
                    },
                    {
                        label: <div
                            className='search-filter'
                            onClick={() => pushUpSelection(major.current)}
                        >
                            <div className='search-filter-content' style={{ textAlign: 'center' }}>
                                <div className={state.major_id > 0 && 'active'}
                                    style={{
                                        /* WebkitBoxOrient: 'vertical', */
                                        //width: '100px',
                                        fontSize: '11px'
                                    }}
                                >
                                    <div style={{ fontWeight: '500', display: 'flex', alignItems: 'center', justifyContent: 'center', color: 'grey' }}>
                                        {formatMessage({ id: 'major' })} <CaretDownOutlined style={{ fontSize: '10px', marginLeft: '5px' }}/*  className={state.country_id > 0 && 'active'} */ />
                                    </div>
                                    {
                                        state.major_id > 0
                                            ? <div
                                                style={{
                                                    overflow: 'hidden',
                                                    WebkitBoxOrient: 'vertical',
                                                    width: '90px',
                                                    display: '-webkit-box',
                                                    WebkitLineClamp: 2
                                                }}
                                            >
                                                {
                                                    Object.values(state.majorList).map((value) => {
                                                        if (state.major_id == value.id) {
                                                            return value.sub_tag
                                                        }
                                                    })
                                                }
                                            </div>
                                            : formatMessage({ id: 'all' })
                                    }

                                </div>
                                <div /* className={state.country_id > 0 ? 'search-filter-selection-active' : 'search-filter-selection'} */ className='search-filter-selection-active'>
                                    <select
                                        ref={major}
                                        value={state.major_id}
                                        onChange={(e) => setState({ major_id: e.target.value, validOriKey: 'major' })}
                                    >
                                        <option value={0}>{formatMessage({ id: 'all' })}</option>
                                        {
                                            Object.values(state.majorList).map((value, key) => {
                                                if (state.validMajor[value.id]) {
                                                    return <option value={value.id} key={key}>
                                                        {value.sub_tag}
                                                    </option>
                                                }
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                        </div>
                    }
                ]}
            />
            {/* <div ref={subMenu} className="search-subMenu">
                <div className="search-subMenu-child" style={{borderRight:'1px solid grey'}}>
                    <div className="search-subMenu-child-box">
                        {formatMessage({ id: 'country' })} <CaretDownOutlined style={{ fontSize: '10px', marginLeft: '10px' }} className={state.country_id > 0 && 'active'} />
                    </div>

                </div>
                <div className="search-subMenu-child">
                    <div className="search-subMenu-child-box">
                        {formatMessage({ id: 'region' })} <CaretDownOutlined style={{ fontSize: '10px', marginLeft: '10px' }} className={state.region_id > 0 && 'active'} />
                    </div>
                </div>
            </div> */}
            <div className='search-content' ref={courseListHeightDom}>
                {
                    Object.values(state.dataList).map((course, key) => {
                        return (
                            <Card
                                onClick={() => router.push('/viewCourse?course_id=' + course.id)}
                                style={{
                                    width: '100%',
                                    display: 'flex',
                                    boxShadow: ' 0px 0px 10px 0px rgba(0, 0, 0, 0.2)',
                                    marginTop: '10px',
                                    position: 'relative',
                                    borderRadius: '5px'
                                }}
                                cover={
                                    <img
                                        alt="logo"
                                        style={{ height: '150px', width: '150px', borderRadius: '5px' }}
                                        src={/* HOST */  'http://api.edudotcom.my/' + 'auth/image/school/' + course.logo}
                                    />
                                }
                            >
                                {/*  <StarTwoTone
                                    onClick={(e) => {
                                        e.stopPropagation()
                                    }}
                                    twoToneColor="grey"//#ffe000
                                    className='search-content-book_mark'
                                /> */}
                                {/* <StarOutlined className='search-content-book_mark' /> */}
                                <div className='search-content-course_list-content'>
                                    <div className='search-content-course_list-content-title' style={{ WebkitBoxOrient: 'vertical' }}>
                                        {
                                            state.schoolTypeList &&
                                            Object.keys(state.schoolTypeList).map((stype) => {
                                                if (stype == course.school_type) {
                                                    return <Badge text={formatMessage({ id: state.schoolTypeList[stype] })} />
                                                }
                                            })
                                        } {course.course_name}
                                    </div>


                                    <div className='search-content-course_list-content-details'>

                                    </div>
                                    <div className='search-content-course_list-content-location'>
                                        <div
                                            // onClick={(e) => {
                                            //     e.stopPropagation()
                                            //     router.push('/school/viewSchool?school_id=' + course.school_id)
                                            // }}
                                            type="link"
                                            className=' search-content-course_list-content-location-school'
                                        >
                                            {course.school_name}{/*  | {course.school_name_cn} */}
                                        </div>
                                        <div className='search-content-course_list-content-location-studyFields' style={{ marginBottom: '2px' }}>
                                            {
                                                course.sub_tag_list &&
                                                Object.values(course.sub_tag_list.split(",")).map((id, atkey) => {
                                                    if (atkey == 0) {
                                                        return Object.values(state.majorList).map((major) => {
                                                            if (major.id == id) {
                                                                return <React.Fragment>
                                                                    <Badge text={major.sub_tag} style={{ marginLeft: 5, backgroundColor: '#ff5b059c' }} />
                                                                    <Badge text="ETC" style={{ marginLeft: 5, backgroundColor: '#ff5b059c' }} />
                                                                </React.Fragment>
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        </div>

                                        <div style={{ display: 'flex', justifyContent: '', alignItems: 'center', fontSize: '10px', padding: state.currentScreenWidth >= 500 && '5px 0' }}>
                                            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', fontSize: '10px' }}>
                                                <img src={require('@/assets/location_icon.jpeg')} style={{ width: '20px', height: '20px' }} />

                                                <div style={{ marginLeft: '2px' }}>
                                                    {course.country}
                                                    {
                                                        course.country != course.region
                                                            ? state.currentScreenWidth >= 500
                                                                ? <span>, {course.region}</span>
                                                                : <div>{course.region}</div>
                                                            : ''
                                                    }
                                                </div>

                                            </div>
                                            {
                                                screen.height > 568 && screen.width > 320 &&
                                                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', fontSize: '10px', marginLeft: '40px' }}>
                                                    <img src={require('@/assets/duration_icon.jpeg')} style={{ width: '20px', height: '20px' }} />
                                                    <div style={{ marginLeft: '3px' }}>
                                                        {
                                                            course.duration > 0
                                                                ? parseInt(course.duration / 12) > 0
                                                                    ? parseInt(course.duration / 12).toFixed() + (parseInt(course.duration / 12).toFixed() > 1 ? formatMessage({ id: 'years' }) : formatMessage({ id: 'year' })) + ' '
                                                                    : ''
                                                                : formatMessage({ id: 'unknow' })


                                                        }
                                                        {
                                                            course.duration &&
                                                                course.duration % 12 > 0
                                                                ? <div>{course.duration % 12 + (course.duration % 12 > 1 ? formatMessage({ id: 'month\'s' }) : formatMessage({ id: 'month' }))}</div>
                                                                : ''
                                                        }
                                                        {
                                                            /* state.currentScreenWidth >= 500
                                                                ? <span> {formatMessage({ id: 'Duration' })}</span>
                                                                : <div>{formatMessage({ id: 'Duration' })}</div> */
                                                        }
                                                    </div>
                                                </div>
                                            }

                                        </div>
                                        <div style={{ borderTop: '1px solid red', marginTop: '3px' }}></div>
                                        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                            <p style={{ fontSize: '10px', fontStyle: 'oblique', fontWeight: 'bold' }}>{formatMessage({ id: 'Estimated Fees' })}</p>
                                            <div style={{ fontWeight: 'bold', display: 'flex', justifyContent: 'center', alignItems: 'center', fontSize: '12px', color: 'red' }}>
                                                <p style={{ fontSize: '10px', marginRight: '3px' }}>{state.ipCountryCode}</p>
                                                <div>{parseFloat(course.exchanged_tuition_fee).toFixed().replace(/(?=(\B\d{3})+$)/g, ',')}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Card>

                        )
                    })
                }
                {
                    state.dataLoading && <Spin />
                }
                {
                    state.totalItem <= state.pageSize &&
                    <div className='search-content-noContent'>
                        <Empty description={formatMessage({ id: 'oops! no more result' })} />
                    </div>
                }
            </div>
        </div >
    )
}