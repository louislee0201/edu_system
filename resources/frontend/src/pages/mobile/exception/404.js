import React, { useEffect, useState } from "react";
import { Modal, List, Button, WhiteSpace, WingBlank, Icon } from 'antd-mobile';
import router from 'umi/router';
import { formatMessage } from 'umi/locale';

export default (props) => {
    const [state, setOriState] = useState({
        visible: false
    })

    useEffect(() => {
        setState({ visible: props.visible })

    }, [props.visible ]);


    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <Modal
            visible={state.visible}
            transparent
            maskClosable={false}
            footer={[{
                text: 'OK',
                onPress: () => {
                    setState({ visible: false })
                    router.push('/home')
                }
            }]}
        >
            {formatMessage({id:'sorry, the page you visited does not exist'})}
        </Modal>
    )
}