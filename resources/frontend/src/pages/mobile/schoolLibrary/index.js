import React, { useState, useEffect, useRef } from 'react'
import { connect } from 'dva';
import { SearchBar, Menu, Button, Badge } from 'antd-mobile';
import { Card, Modal, Input } from 'antd';
import router from 'umi/router';
import { HOST } from '@/constants/api'
import TopBanner from '@/components/top-banner'
import { formatMessage } from 'umi/locale';
import { CaretDownOutlined, StarTwoTone, HeartTwoTone } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux'
import Loading from '@/components/loading'
import './index.less'
import { baseUrl } from '../../../constants/api';
import { Helmet } from 'react-helmet';

export default () => {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        dataList: [],
        dataList2: [],
        loading: false,
        previewImage: '',
        previewVisible: false,
        previewTitle: '',
        currentScreenHeight: document.documentElement.clientHeight,
        currentScreenWidth: document.documentElement.clientWidth,
        characterList: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],

        country_id: 0,
        region_id: 0,
        //characterKey: ''
        schoolName: ''
    })
    const courseState = useSelector(state => state.course)
    const country = useRef(null)
    const region = useRef(null)
    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }

    window.onresize = function (event) {
        setState({
            currentScreenHeight: document.documentElement.clientHeight,
            currentScreenWidth: document.documentElement.clientWidth
        })
    };

    useEffect(() => {
        setState({ loading: true })
        if (courseState.searchCourseFilter.countryList.length <= 0) {
            dispatch({
                type: 'course/e_getSelection',
                payload: {}
            }).then((value) => {
                requestData()
                if (value) {
                    dispatch({
                        type: 'course/r_updateState',
                        payload: {
                            searchCourseFilter: {
                                ...courseState.searchCourseFilter,
                                ...value
                            }
                        }
                    })
                }
            })
        } else {
            requestData()
        }
    }, []);

    useEffect(() => {
        if (courseState.searchCourseFilter.countryList.length == 1) {
            setState({ country_id: courseState.searchCourseFilter.countryList[0].id })
        }
    }, [courseState.searchCourseFilter.countryList]);

    function requestData(clickPage = 1) {
        setState({ loading: true })
        dispatch({
            type: 'school/e_getSchool',
            payload: {
                isList: true,
                clickPage: 1,
                filterArr: {
                    //active: true
                }
            }
        }).then((value) => {
            setState({
                loading: false,
                ...value,
            })
        })
    }

    function renderComponent() {
        return (
            <React.Fragment>
                <Modal
                    visible={state.previewVisible}
                    title={state.previewTitle}
                    footer={null}
                    onCancel={() => setState({ previewVisible: false })}
                >
                    <img alt="example" style={{ width: '100%' }} src={state.previewImage} />
                </Modal>
                <Loading loading={state.loading} />
            </React.Fragment>
        )
    }

    useEffect(() => {
        if (state.dataList.length > 0) {
            let dataList2 = []
            state.dataList.map((school) => {
                let cAllow = true
                let rAllow = true
                let nAllow = true

                if (state.country_id) {
                    cAllow = false
                    if (school.country == state.country_id) {
                        cAllow = true
                    }
                }
                if (state.region_id) {
                    rAllow = false
                    if (school.region == state.region_id) {
                        rAllow = true
                    }
                }

                if (state.schoolName != '') {
                    nAllow = false
                    if (
                        school.school_name.toUpperCase().indexOf(state.schoolName.toUpperCase()) >= 0 ||
                        school.school_name_cn.toUpperCase().indexOf(state.schoolName.toUpperCase()) >= 0
                    ) {
                        nAllow = true
                    }
                }

                if (cAllow && rAllow && nAllow) {
                    dataList2.push(school)
                }
            })
            //animateValue(dataList2.length)
            setState({ dataList2 })
        }
    }, [state.country_id, state.region_id, state.schoolName, state.loading]);

    function pushUpSelection(elem) {
        if (document.createEvent) {
            var e = document.createEvent("MouseEvents");
            e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            elem[0].dispatchEvent(e);
        } else if (element.fireEvent) {
            elem[0].fireEvent("onmousedown");
        }
    }

    return (
        <React.Fragment>
            <Helmet>
                <meta name="description" content="School" />
                <link rel="canonical" href={baseUrl + "schoolLibrary"} />
            </Helmet>
            <TopBanner
                backBtn={false}
                title={formatMessage({ id: 'school library' })}
                //rightButton={<FilterOutlined />}
                //extraFilter={false}
                subMenuButton={[
                    /*  {
                         label: <div>
                             {formatMessage({ id: 'country' })} <CaretDownOutlined style={{ fontSize: '10px' }} />
                         </div>
                     }, */
                    // {
                    //     label: <div
                    //         className='search-filter'
                    //         onClick={() => pushUpSelection(country.current)}
                    //     //style={{ display: 'flex', alignItems: 'center', position: 'relative' }}
                    //     >
                    //         <div className='search-filter-content' style={{ textAlign: 'center' }}>
                    //             <div className={state.country_id > 0 && 'active'}
                    //                 style={{
                    //                     /* WebkitBoxOrient: 'vertical', */
                    //                     fontSize: '11px',
                    //                     //width: '100px'
                    //                 }}
                    //             >
                    //                 <div style={{ fontWeight: '500', display: 'flex', alignItems: 'center', justifyContent: 'center', color: 'grey' }}>
                    //                     {formatMessage({ id: 'country' })} <CaretDownOutlined style={{ fontSize: '10px', marginLeft: '5px' }}/*  className={state.country_id > 0 && 'active'} */ />
                    //                 </div>
                    //                 {
                    //                     state.country_id > 0
                    //                         ? Object.values(courseState.searchCourseFilter.countryList).map((value) => {
                    //                             if (state.country_id == value.id) {
                    //                                 return value.country
                    //                             }
                    //                         })
                    //                         : formatMessage({ id: 'all' })
                    //                 }
                    //             </div>
                    //             <div /* className={state.country_id > 0 ? 'search-filter-selection-active' : 'search-filter-selection'} */ className='search-filter-selection-active'>
                    //                 <select
                    //                     ref={country}
                    //                     value={state.country_id}
                    //                     onChange={(e) => setState({ country_id: parseInt(e.target.value), region_id: 0 })}
                    //                 >
                    //                     <option value={0}>{formatMessage({ id: 'all' })}</option>
                    //                     {
                    //                         Object.values(courseState.searchCourseFilter.countryList).map((value, key) => {
                    //                             return <option value={value.id} key={key}>
                    //                                 {value.country}
                    //                             </option>
                    //                         })
                    //                     }
                    //                 </select>
                    //             </div>
                    //         </div>
                    //     </div>
                    // },
                    {
                        label: <div
                            className='search-filter'
                            onClick={() => pushUpSelection(region.current)}
                        //style={{ display: 'flex', alignItems: 'center', position: 'relative' }}
                        >
                            <div className='search-filter-content' style={{ textAlign: 'center' }}>
                                <div className={state.region_id > 0 && 'active'}
                                    style={{
                                        /* WebkitBoxOrient: 'vertical', */
                                        fontSize: '11px',
                                        //width: '100px'
                                    }}
                                >
                                    <div style={{ fontWeight: '500', display: 'flex', alignItems: 'center', justifyContent: 'center', color: 'grey' }}>
                                        {formatMessage({ id: 'region' })} <CaretDownOutlined style={{ fontSize: '10px', marginLeft: '5px' }}/*  className={state.region_id > 0 && 'active'} */ />
                                    </div>
                                    {
                                        state.region_id > 0
                                            ? Object.values(courseState.searchCourseFilter.regionList).map((value) => {
                                                if (state.region_id == value.id) {
                                                    return value.region
                                                }
                                            })
                                            : formatMessage({ id: 'all' })
                                    }
                                </div>
                                <div /* className={state.country_id > 0 ? 'search-filter-selection-active' : 'search-filter-selection'} */ className='search-filter-selection-active'>
                                    <select
                                        ref={region}
                                        value={state.region_id}
                                        onChange={(e) => setState({ region_id: parseInt(e.target.value) })}
                                    >
                                        <option value={0}>{formatMessage({ id: 'all' })}</option>
                                        {
                                            Object.values(courseState.searchCourseFilter.regionList).map((value, key) => {
                                                if (state.country_id > 0) {
                                                    if (value.country_id == state.country_id) {
                                                        return <option value={value.id} key={key}>
                                                            {value.region}
                                                        </option>
                                                    }
                                                } else {
                                                    return <option value={value.id} key={key}>
                                                        {value.region}
                                                    </option>
                                                }

                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                        </div>
                    }
                ]}
            />
            {renderComponent()}
            <div className='school_library'>
                <div className='school_library-content'>
                    <Input
                        value={state.schoolName}
                        placeholder={formatMessage({ id: 'search with school name' })}
                        onChange={(e) => setState({ schoolName: e.target.value })}
                    />
                    {
                        Object.values(state.dataList2).map((school, key) => {
                            return (
                                <div className='school_library-content-child' onClick={() => router.push('/viewSchool?school_id=' + school.school_id)}>
                                    <img
                                        alt="logo"
                                        style={{ height: '100%', width: '100%', /* borderRadius: '10px' */ }}
                                        src={HOST/* 'http://api.louis-blog.com/' */ + 'auth/image/school/' + school.picture}
                                    />
                                    {/* <StarTwoTone
                                        onClick={(e) => {
                                            e.stopPropagation()
                                        }}
                                        twoToneColor="grey"//#ffe000
                                        className='school_library-content-book_mark'
                                    /> */}
                                </div>
                            )
                        })
                    }

                </div>


            </div>
        </React.Fragment>
    )
}