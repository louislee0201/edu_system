import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './contactUs.less'
import router from 'umi/router';
import { Card, Divider, Tag } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import { MessageOutlined, MailOutlined, PhoneOutlined, InstagramOutlined, FacebookOutlined } from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({

    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    function setKey(activeKey) {
        if (activeKey == state.activeKey) {
            activeKey = ''
        }
        setState({ activeKey })
    }

    return (
        <div className='desktop-contactUs'>
            <MobileTopBar />
            <div className='desktop-contactUs-img'>
                <img src={require('@/assets/about_us/mobile_other.jpg')} />
            </div>

            <div className='desktop-contactUs-title'>
                {formatMessage({ id: 'contact us' })}
            </div>

            <div className='desktop-contactUs-content'>
                <div className="desktop-other-contactUs-content-left">
                    <div className="desktop-other-contactUs-content-left-icon-info">

                        <div className="desktop-other-contactUs-content-left-icon-info-img">
                            <img src={require('@/assets/contact_us/gps.png')} />
                        </div>

                        <div className="desktop-other-contactUs-content-left-icon-info-child">
                            <div className="desktop-other-contactUs-content-left-icon-info-child-title">
                                {formatMessage({ id: 'address' })}
                            </div>
                            <div className="desktop-other-contactUs-content-left-icon-info-child-content">
                                No.86A, Jalan NB2 1/4, Taman Nusa Bestari 2, 81300 Skudai, Johor, Malaysia
                            </div>
                        </div>

                    </div>

                    <div className="desktop-other-contactUs-content-left-icon-info">
                        <div className="desktop-other-contactUs-content-left-icon-info-img">
                            <PhoneOutlined />
                        </div>

                        <div className="desktop-other-contactUs-content-left-icon-info-child">
                            <div className="desktop-other-contactUs-content-left-icon-info-child-title">
                                {formatMessage({ id: 'contact' })}
                            </div>
                            <div className="desktop-other-contactUs-content-left-icon-info-child-content">
                                +6016-702 3776 <br />
                                {/* (Mr.Rayyan) +6016-757 7846 <br /> */}
                                {/* (Ms.Rin) +6011-5407 1103 */}
                            </div>
                        </div>
                    </div>

                    <div className="desktop-other-contactUs-content-left-icon-info">
                        <div className="desktop-other-contactUs-content-left-icon-info-img">
                            <MailOutlined />
                        </div>

                        <div className="desktop-other-contactUs-content-left-icon-info-child">
                            <div className="desktop-other-contactUs-content-left-icon-info-child-title">
                                {formatMessage({ id: 'email' })}
                            </div>
                            <div className="desktop-other-contactUs-content-left-icon-info-child-content">
                                enquiry@edudotcom.my <br />
                                {/* (Ms. Rayyan) eando.edu@gmail.com */}
                            </div>
                        </div>
                    </div>

                    <div className="desktop-other-contactUs-content-left-icon-info">
                        <div className="desktop-other-contactUs-content-left-icon-info-img">
                            <MessageOutlined />
                        </div>

                        <div className="desktop-other-contactUs-content-left-icon-info-child">
                            <div className="desktop-other-contactUs-content-left-icon-info-child-title">
                                {formatMessage({ id: 'communication' })}
                            </div>
                            <div className="desktop-other-contactUs-content-left-icon-info-child-content">

                                <img
                                    src={require('@/assets/contact_us/instagram.png')}
                                    onClick={() => window.open('https://www.instagram.com/edu_dotcom/', '_blank')}
                                />

                                <img
                                    src={require('@/assets/contact_us/facebook.png')}
                                    onClick={() => window.open('https://www.facebook.com/messages/t/220199178101630', '_blank')}
                                />

                                <img
                                    src={require('@/assets/contact_us/whatsapp.png')}
                                    onClick={() => window.open('https://api.whatsapp.com/send/?phone=%2B60167023776&text&app_absent=0&lang=en', '_blank')}
                                />
                            </div>
                        </div>
                    </div>

                </div>
                <div className="desktop-other-contactUs-content-right" />
            </div>

        </div>
    )
}
