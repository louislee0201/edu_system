import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './faqs.less'
import router from 'umi/router';
import { Card, Divider, Tag } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import { MessageOutlined, MailOutlined, PhoneOutlined, InstagramOutlined, FacebookOutlined } from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({

    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    function setKey(activeKey) {
        if (activeKey == state.activeKey) {
            activeKey = ''
        }
        setState({ activeKey })
    }

    return (
        <div className='desktop-faqs'>
            <MobileTopBar />
            <div className='desktop-faqs-img'>
                <img src={require('@/assets/about_us/mobile_other.jpg')} />
            </div>

            <div className='desktop-faqs-title'>
                {formatMessage({ id: 'FAQs' })}
            </div>

            <div className='desktop-faqs-content'>
                <div className='desktop-other-faqs-content'>
                    <div className='desktop-other-faqs-content-title'>1.	What is EDU DOTCOM?</div>
                        EDU DOTCOM is an higher education consultancy private company which provide platform to both university and students.
                        For more details, please refer to <a onClick={() => router.push('/introduction/aboutUs')}>"About Us"</a> for more details.<br />
                    <br /><br />
                    <div className='desktop-other-faqs-content-title'>2.	What kind of the services provide by EDU DOTCOM and how much for the charges?</div>
                        Absolutely FREE for all the services! For more info,
                        please refer to <a onClick={() => router.push('/ourFreeServices/counselorServices')}>"Our Services"</a> for more details. However, some schools may not authorize EDU DOTCOM as representatives for the recruitment or admission,
                        please check <a onClick={() => router.push('/introduction/ourPartners')}>"Our Partners"</a> if you keen to engage with our services.<br />
                    <br /><br />
                    <div className='desktop-other-faqs-content-title'>3.	How do EDU DOTCOM sustain if the services are all free to the public?</div>
                        We receive financial support from our represent university when they participate in our website or application assist based on the headcount. <br />
                    <br /><br />
                    <div className='desktop-other-faqs-content-title'>4.	Is there any different if I apply through EDU DOTCOM or direct to the university?</div>
                    If the school appointed EDU DOTCOM as representative, the course fees and scholarships that you apply through us is totally same just like you apply directly to university. We have signed an official contract with all our partner schools as a recruitment representative in Malaysia and strictly follow the regulations setup by schools. If you are still in doubt, feel free to contact relevant university for further clarification. <br />
                    <br /><br />
                    <div className='desktop-other-faqs-content-title'>5.	Which university is being partner schools to the EDU DOTCOM?</div>
                    Most but not all. Some schools may authorize EDU DOTCOM to recruit for certain courses only or do not appoint EDU DOTCOM to act as a representative. Under this circumstances, students should contact or forward their enquiry to particular school directly.<br />
                    <br /><br />
                    <div className='desktop-other-faqs-content-title'>6.	Can I apply directly to university?</div>
                    Yes, you can. However, we suggest student to apply with us if the school or course that you are going to apply is under our partner schools listing. We got the exclusive scholarships or offer sometimes. Also, you will get appointed counsellor to assist you regarding of application process, student services, etc for FREE!<br />
                </div>
            </div>

        </div>
    )
}
