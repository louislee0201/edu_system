import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './n4.less'
import router from 'umi/router';
import { Card, Divider, Tag } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    RightOutlined, LeftOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'
import MobileApplyBtn from '@/components/mobile-apply-btn'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({

    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    function setKey(activeKey) {
        if (activeKey == state.activeKey) {
            activeKey = ''
        }
        setState({ activeKey })
    }

    return (
        <div className='desktop-n4'>
            <MobileTopBar />
            <MobileApplyBtn />
            <div className='desktop-n4-img'>
                <img src={require('@/assets/about_us/mobile_provide_courses.jpg')} />
            </div>

            <div className='desktop-n4-title'>
                {formatMessage({ id: 'Japanese Language Online Course (N4)' })}
            </div>

            <div className='desktop-n4-content'>
                <div style={{ fontWeight: 'bold', color: '#debb00', fontSize: '18px' }}>
                    Course Overview
                </div>
                The EDU DOTCOM Online Japanese Course offers a comprehensive program tailored for beginners who have no prior knowledge of the Japanese language. This course provides a solid foundation and requires no fundamental prerequisites, making it accessible to anyone interested in learning Japanese. <br /><br />
                Led by our highly experienced and proficient instructors, you will receive personalized guidance and support throughout your learning journey. Our instructors have a proven track record of successfully cultivating the language skills of numerous students over the years.<br /><br />
                The course utilizes the highly effective "Direct Teaching Method," widely recognized and employed by leading language schools in Japan. This approach ensures a seamless learning experience by eliminating language barriers and maximizing efficiency.<br /><br />
                The curriculum focuses on the four fundamental aspects of language acquisition: listening, speaking, reading, and writing. Engaging activities, interactive exercises, and ample homework assignments are integrated into the course to facilitate continuous improvement and mastery of the language.<br /><br />
                At EDU DOTCOM, we have developed our own teaching materials specifically designed to enhance your learning experience. Join our online Japanese course and embark on a journey to develop your proficiency in this captivating language.
                <br />
                <br />
                By using of “Direct Teaching Method”, which is the most efficiency way of teaching japanese language and adopted by most of the language schools in Japan, you won’t need to worry about language barrier. The curriculum of the course will be taken up with 4 elements which are listening, speaking, reading and writing. Our center comes with self-develop teaching material and have cultivated few hundred students in the past few years.
                <br /><br />
                <div style={{ fontWeight: 'bold', color: '#debb00', fontSize: '18px' }}>
                    Course Features
                </div>



                ★ Students acquire ability to find a student part-time work in Japan<br />
                ★ Students can handle Japanese culture and live in Japan freely<br />
                ★ This course is considered as 3 months Japanese Language Course in Japan.<br />
                ★ Experienced lecturer leads the class.<br />
                ★ Strategic Preparation for JLPT N4 Exam <br />
                ★ Student will have the ability to communicate with Japanese <br /><br />



                <div style={{ fontWeight: 'bold', color: '#debb00', fontSize: '18px' }}>
                    Class Delivered
                </div>
                EDU online platform system.<br /><br />

                <div style={{ fontWeight: 'bold', color: '#debb00', fontSize: '18px' }}>
                    Course Schedule
                </div>

                <table >
                    <tbody>
                        <tr>
                            <td>Duration</td>
                            <td>Course Hours</td>
                            <td>Lesson Days</td>
                        </tr>
                        <tr>
                            <td>1.5 months</td>
                            <td>Estimate 150 hours</td>
                            <td>Anytime</td>
                        </tr>
                    </tbody>
                </table>
                <br />

                <div style={{ fontWeight: 'bold', color: '#debb00', fontSize: '18px' }}>
                    Tuition Fees
                </div>
                RM 1,350<br />
                • Include application fees<br />
                • Include online teaching materials<br />
                • Non-refundable
                <br /><br />

                <div style={{ fontWeight: 'bold', color: '#debb00', fontSize: '18px' }}>
                    Scholarships
                </div>
                ◆ 10% off when you apply N5&N4 course in bundle.<br />
                ◆ 10% off if to the student who apply to study in Japan through EDU DOTCOM services.<br />
            </div>

        </div>
    )
}
