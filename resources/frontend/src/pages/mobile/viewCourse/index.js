import React, { useEffect, useState } from "react";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import { Toast, Menu, Button } from 'antd-mobile';
import { Descriptions, Collapse, Divider, Card } from 'antd';
import TopBanner from '@/components/top-banner'
import { EnvironmentOutlined, HomeOutlined } from '@ant-design/icons';
import DescriptionTable from '@/components/mobile-description-table'
import { HOST } from '@/constants/api'
import router from 'umi/router';
import Loading from '@/components/loading'
import './index.less'
import MobileApplyBtn from '@/components/mobile-apply-btn'
import { Helmet } from "react-helmet";
import { baseUrl } from "../../../constants/api";

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        loading: true,
        course_name: "",
        course_id: 0,
    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    useEffect(() => {
        const course_id = props.location.query.course_id;
        if (course_id) {
            dispatch({
                type: "course/e_getCourse",
                payload: {
                    isList: false,
                    course_id
                }
            }).then(result => {
                if (result) {
                    setState({ ...result, related_id: course_id, loading: false })
                }
            });
        }
    }, []);

    function renderContent() {
        if (!state.loading) {
            return <React.Fragment>
                <TopBanner
                    title={state.course_name}
                />
                <MobileApplyBtn />

                <div className='viewCourse-details'>
                    <div className='viewCourse-details-img'>
                        <img src={state.logo} alt="Course" style={{ width: '100%' }} />
                    </div>

                    <div className='viewCourse-details-content'>
                        <div className='viewCourse-details-content-course_name'>
                            {state.course_name}
                        </div>
                        <div className='viewCourse-details-content-desc'>
                            <div
                                className='viewCourse-details-content-desc-school_name'
                                onClick={() => router.push('/viewSchool?school_id=' + state.school_id)}
                            >
                                <div className='viewCourse-details-content-desc-school_name-icon'>
                                    <img src={require('@/assets/school_icon.jpeg')} style={{ width: '20px', height: '20px' }} />
                                </div>

                                <div className='viewCourse-details-content-desc-school_name-title'>
                                    {state.school_name}
                                </div>
                            </div>
                            <div className='viewCourse-details-content-desc-location'>
                                <div className='viewCourse-details-content-desc-location-icon'>
                                    <img src={require('@/assets/location_icon.jpeg')} style={{ width: '20px', height: '20px' }} />
                                </div>

                                <div className='viewCourse-details-content-desc-location-title'>
                                    {state.region != state.country && state.region + ', '}{state.country}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='viewCourse-description'>
                    <div className='viewCourse-description-courseTitle'>
                        {formatMessage({ id: 'course overview' })}
                    </div>
                    <Divider
                        className='viewCourse-description-title'
                    //orientation="left"
                    >

                    </Divider>
                    <DescriptionTable
                        classMode='course'
                        related_id={state.related_id}
                    />
                </div>

                {
                    // state.courseList.length > 0 &&
                    // <div className='viewCourse-course'>
                    //     <Collapse
                    //         bordered={false}
                    //         expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
                    //         className="site-collapse-custom-collapse"
                    //     >
                    //         <Collapse.Panel
                    //             className="site-collapse-custom-panel"
                    //             header={
                    //                 formatMessage({ id: 'all subjects' })
                    //             }
                    //         >
                    //             <div className='viewCourse-course-list'>
                    //                 {
                    //                     Object.values(state.courseList).map((course, key) => {
                    //                         return (
                    //                             <Card
                    //                                 bodyStyle={{ display: 'none' }}
                    //                                 onClick={() => router.push('/course/viewCourse?course_id=' + course.id)}
                    //                                 hoverable
                    //                                 cover={
                    //                                     <div style={{ height: '150px' }}>
                    //                                         <img
                    //                                             src={HOST + 'auth/image/school_course/' + course.picture}
                    //                                             style={{ width: '100%', height: '100%' }}
                    //                                         />
                    //                                     </div>
                    //                                 }
                    //                                 style={{ width: '40%', flex: '0 0 45%', marginLeft: '3%', marginTop: '3%' }}
                    //                                 key={key}
                    //                             >
                    //                                 {/* <Card.Meta
                    //                                         title={course.course_name}
                    //                                         style={{ textOverflow: 'unset' }}
                    //                                     /> */}
                    //                             </Card>
                    //                         )
                    //                     })
                    //                 }
                    //             </div>

                    //         </Collapse.Panel>
                    //     </Collapse>

                    // </div>
                }


            </React.Fragment>
        }
        return <Loading loading={state.loading} />
    }

    return <>
        <Helmet>
            <meta name="description" content={state.course_name} />
            <link rel="canonical" href={baseUrl + "viewCourse?course_id=" + state.course_id} />
        </Helmet>
        {renderContent()}
    </>
}