import React from 'react';
import { TabBar } from 'antd-mobile';
import router from 'umi/router';
import 'antd-mobile/dist/antd-mobile.css';
import './index.less';
import Mobile from '@/pages/mobile'
import * as DEFAULT from '@/constants/default'
import { formatMessage } from 'umi/locale';

export default function MobileLayout(props) {
    function isTabBarSelect(url) {
        const { location: { pathname } } = props;
        if (pathname == '/' && url == '/home') {
            return true;
        } else {
            return pathname === url;
        }
    }

    const TabBarData = [
        {
            id: 'home',
            name: formatMessage({ id: 'home' }),
            icon: require('@/assets/recycleH5_07.png'),
            selectedicon: require('@/assets/recycleH5_02.png'),
            url: '/home',
        },
        {
            id: 'search course',
            name: formatMessage({ id: 'search course' }),
            icon: require('@/assets/recycleH5_03.png'),
            selectedicon: require('@/assets/recycleH5_06.png'),
            url: '/searchCourse',
        },
        {
            id: 'school library',
            name: formatMessage({ id: 'school library' }),
            icon: require('@/assets/school.png'),
            selectedicon: require('@/assets/school_selected.png'),
            url: '/schoolLibrary',
        },
        {
            id: 'me',
            name: formatMessage({ id: 'me' }),
            icon: require('@/assets/recycleH5_04.png'),
            selectedicon: require('@/assets/recycleH5_05.png'),
            url: '/me',
        }
    ];

    return (
        <div className='mobileLayout-main'>
            <div className='mobileLayout-main-content'>
                {<Mobile {...props} />}
            </div>
            <div className='mobileLayout-main-bottom_button'>
                <TabBar
                    unselectedTintColor="#333"
                    tintColor={DEFAULT.PRIMARY_COLOR}
                //barTintColor="white"
                //tabBarPosition='bottom'
                >
                    {
                        TabBarData.map(t => {
                            const isSelect = isTabBarSelect(t.url);
                            return (
                                <TabBar.Item
                                    title={t.name}
                                    key={t.id}
                                    icon={
                                        <div style={{
                                            width: '22px',
                                            height: '22px',
                                            background: `url(${t.icon}) center center /  21px 21px no-repeat`
                                        }} />
                                    }
                                    selectedIcon={
                                        <div style={{
                                            width: '22px',
                                            height: '22px',
                                            background: `url(${t.selectedicon}) center center /  21px 21px no-repeat`
                                        }} />
                                    }
                                    // badge={1}
                                    onPress={() => {
                                        router.push(t.url);
                                    }}
                                    selected={isSelect}
                                    data-seed="logId"
                                />
                            )
                        })
                    }
                </TabBar>
            </div>
        </div>
    );
}