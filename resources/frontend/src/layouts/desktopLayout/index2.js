import React, { useState } from 'react';
import router from 'umi/router';
import 'antd-mobile/dist/antd-mobile.css';
import Desktop from '@/pages/desktop'
import './index2.less'
import { formatMessage } from 'umi/locale';
import { useDispatch, useSelector } from 'react-redux'
import { Button } from 'antd';
import { UserOutlined, SearchOutlined, HomeOutlined } from '@ant-design/icons';
import { getLocalStorage, saveLocalStorage } from '@/tools'
import { PRIMARY_COLOR } from '@/constants/default'

export default function DesktopLayout(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({

    })

    const configState = useSelector(state => state.config)

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }
    return (
        <div className='desktop'>
            <div className='desktop-top_nav'>
                <div className='desktop-top_nav-logo' onClick={() => router.push('/home')}>
                    <img src={require('@/assets/logo.png')} style={{ height: '60%', marginBottom: '5px' }} />
                </div>

                <div className='desktop-top_nav-menu'>
                    <div
                        onClick={() => router.push('/home')}
                        className='desktop-top_nav-menu-child'
                        type='ghost'
                    >
                        <div className='desktop-top_nav-menu-child-text'>{formatMessage({ id: 'home' })}</div>
                    </div>

                    <div
                        onClick={() => router.push('/newsAndEvents')}
                        className='desktop-top_nav-menu-child'
                        type='ghost'
                    >
                        <div className='desktop-top_nav-menu-child-text'>{formatMessage({ id: 'news and events' })}</div>
                    </div>

                    <div
                        className='desktop-top_nav-menu-child'
                        type='ghost'
                        onClick={() => router.push('/searchCourse')}
                    >
                        <div className='desktop-top_nav-menu-child-text'>{formatMessage({ id: 'search course' })}</div> <SearchOutlined />
                    </div>

                    <div
                        className='desktop-top_nav-menu-child'
                        type='ghost'
                        onClick={() => router.push('/schoolLibrary')}
                    >
                        <div className='desktop-top_nav-menu-child-text'>{formatMessage({ id: 'school library' })}</div> <HomeOutlined />
                    </div>

                    {/* <Button
                        className='desktop-top_nav-menu-child'
                        type='ghost'
                    >
                        {formatMessage({ id: 'about us' })}
                    </Button> */}

                    <div
                        className='desktop-top_nav-menu-child'
                        type='ghost'
                    >
                        <div className='desktop-top_nav-menu-child-text'>{formatMessage({ id: 'contact us' })}</div>
                    </div>

                    {/* <div className='desktop-top_nav-menu-child'>
                        {formatMessage({ id: '' })}
                    </div> */}
                </div>

                <div className='desktop-top_nav-me'>
                    <UserOutlined
                        style={{ color: configState.account_level == 1 ? 'grey' : PRIMARY_COLOR }}
                        onClick={() => {
                            if (configState.account_level == 1) {
                                router.push('/login')
                            } else {
                                //router.push('/personal')
                            }
                        }}
                    />
                    {
                        /* configState.account_level == 1 &&
                        <div
                            //className='my-beauty-status-loginBtn'
                            onClick={() => router.push('/login')}
                        >
                            <img src={require('@/assets/login_icon.jpg')} />
                        </div> */
                    }

                    {
                        configState.account_level == 2 &&
                        <div
                            //className='my-beauty-status-info'3
                            onClick={() => {
                                dispatch({
                                    type: 'config/e_logout',
                                    payload: {}
                                })
                            }}
                        >
                            <img src={require('@/assets/logout_icon.jpg')} />
                            {/* {formatMessage({ id: 'logout' })} */}
                        </div>
                    }
                </div>

            </div>
            <div className='desktop-content'>
                <Desktop {...props} />
            </div>
        </div>
    );
}