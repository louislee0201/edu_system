import React, { useState } from 'react';
import router from 'umi/router';
import 'antd-mobile/dist/antd-mobile.css';
import Desktop from '@/pages/desktop'
import './index.less'
import { formatMessage } from 'umi/locale';
import { useDispatch, useSelector } from 'react-redux'
import { BackTop } from 'antd';
import { UpOutlined, InstagramOutlined, FacebookOutlined, CommentOutlined, DeleteColumnOutlined, ArrowUpOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { getLocalStorage, saveLocalStorage } from '@/tools'
import { PRIMARY_COLOR } from '@/constants/default'
import DesktopApplyForm from '@/components/desktop-apply-form'

export default (props) => {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        showToolsBar: false,
        applyFormVisible: false
    })

    const configState = useSelector(state => state.config)
    const courseState = useSelector(state => state.course)
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    const date = new Date()
    return (
        <React.Fragment>
            <DesktopApplyForm visible={state.applyFormVisible} onClose={() => setState({ applyFormVisible: false })} />
            <div className='desktop-header'>
                <div className='desktop-header_top'>
                    <img src={require('@/assets/logo.png')} onClick={() => router.push('/')} />

                    <div className='desktop-header_top-menu'>
                        {
                            configState.account_level == 2
                                ? <a
                                    onClick={() => {
                                        dispatch({
                                            type: 'config/e_logout',
                                            payload: {}
                                        })
                                    }}
                                >
                                    {formatMessage({ id: 'logout' })}
                                </a>
                                : <a onClick={() => router.push('/login')}>{formatMessage({ id: 'login' })}</a>
                        }

                        <a onClick={() => router.push('/')}>{formatMessage({ id: 'home' })}</a>
                        <a onClick={() => router.push('/other/contactUs')}>{formatMessage({ id: 'contact us' })}</a>
                        <InstagramOutlined
                            onClick={() => window.open('https://www.instagram.com/edu_dotcom/', '_blank')}
                        />
                        <FacebookOutlined
                            onClick={() => window.open('https://www.facebook.com/EDUdotCOM', '_blank')}
                        />
                    </div>
                </div>
                <div className='desktop-header_menu'>
                    <div className='desktop-header_menu-content'>
                        <ul className='desktop-header_menu-content-ul'>
                            <li>
                                <a onClick={() => router.push('/introduction/aboutUs')} >{formatMessage({ id: 'introduction' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/counselorServices')} >{formatMessage({ id: 'Our Free Services' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/provideCourses/n5')}>{formatMessage({ id: 'Provide Courses' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/searchCourse')}>{formatMessage({ id: 'Search Courses' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/schoolLibrary')}>{formatMessage({ id: 'Informations' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/other/contactUs')} >{formatMessage({ id: 'Other' })}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className='desktop-header_dropdown'>
                    <div className='desktop-header_dropdown-content'>
                        <ul className='desktop-header_dropdown-content-ul'>
                            <li>
                                <a onClick={() => router.push('/introduction/aboutUs')} >{formatMessage({ id: 'about us' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/introduction/newsAndEvents')}>{formatMessage({ id: 'newsAndEvents' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/introduction/ourPartners')}>{formatMessage({ id: 'our partners' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/introduction/testimonial')}>{formatMessage({ id: 'Testimonial' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/introduction/privatePolicies')} >{formatMessage({ id: 'Private Policies' })}</a>
                            </li>
                        </ul>

                        <ul className='desktop-header_dropdown-content-ul'>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/counselorServices')}>{formatMessage({ id: 'Counselor Services' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/applicationServices')}>{formatMessage({ id: 'Application Services' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/visaGuidance')}>{formatMessage({ id: 'Visa Guidance' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/accommadation')}>{formatMessage({ id: 'Accommadation' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/arrivalGuidance')}>{formatMessage({ id: 'Arrival Guidance' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/studentGuardian')}>{formatMessage({ id: 'Student Guardian' })}</a>
                            </li>
                        </ul>

                        <ul className='desktop-header_dropdown-content-ul'>
                            <li>
                                <a onClick={() => router.push('/provideCourses/n5')}>{formatMessage({ id: 'Japanese Language Online Course (N5)' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/provideCourses/n4')}>{formatMessage({ id: 'Japanese Language Online Course (N4)' })}</a>
                            </li>
                        </ul>

                        <ul className='desktop-header_dropdown-content-ul'>
                            <li>
                                <a onClick={() => {
                                    dispatch({
                                        type: 'course/r_updateState',
                                        payload: {
                                            searchCourseFilter: {
                                                ...courseState.searchCourseFilter,
                                                country_id: 3,
                                                dataList: []
                                            }
                                        }
                                    })
                                    router.push('/searchCourse')
                                }}>{formatMessage({ id: 'Courses In Japan' })}</a>
                            </li>
                            {/* <li>
                                <a onClick={() => {
                                    dispatch({
                                        type: 'course/r_updateState',
                                        payload: {
                                            searchCourseFilter: {
                                                ...courseState.searchCourseFilter,
                                                country_id: 5,
                                                dataList: []
                                            }
                                        }
                                    })
                                    router.push('/searchCourse')
                                }}>{formatMessage({ id: 'Courses In Singapore' })}</a>
                            </li> */}
                        </ul>

                        <ul className='desktop-header_dropdown-content-ul'>
                            <li>
                                <a onClick={() => router.push('/schoolLibrary')} >{formatMessage({ id: 'school library' })}</a>
                            </li>
                            <li>
                                <a href="#" /* onClick={() => router.push('/')} */>
                                    {formatMessage({ id: 'Personality Test' })}
                                    <div>{formatMessage({ id: '(coming soon)' })}</div>
                                </a>
                            </li>
                            <li>
                                <a href="#" /* onClick={() => router.push('/')} */>
                                    {formatMessage({ id: 'Discover Study Country' })}
                                    <div>{formatMessage({ id: '(coming soon)' })}</div>
                                </a>
                            </li>
                            <li>
                                <a href="#" /* onClick={() => router.push('/')} */>
                                    {formatMessage({ id: 'Occupation Profile' })}
                                    <div>{formatMessage({ id: '(coming soon)' })}</div>
                                </a>
                            </li>
                            <li>
                                <a href="#" /* onClick={() => router.push('/')} */>
                                    {formatMessage({ id: 'Explore Studies' })}
                                    <div>{formatMessage({ id: '(coming soon)' })}</div>
                                </a>
                            </li>
                        </ul>

                        <ul className='desktop-header_dropdown-content-ul'>
                            <li>
                                <a onClick={() => router.push('/other/contactUs')}>{formatMessage({ id: 'contact us' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/other/faqs')} >{formatMessage({ id: 'FAQs' })}</a>
                            </li>
                            <li>
                                <a onClick={() => setState({ applyFormVisible: true })}>{formatMessage({ id: 'Apply With US' })}</a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div >
            <div /* className='desktop-content' */>
                <Desktop {...props} />
            </div>
            <div className='desktop-footer'>
                <div className='desktop-footer-content'>
                    <div className='desktop-footer-content-title'>
                        <ul className='desktop-footer-content-title-ul'>
                            <li>
                                <a onClick={() => router.push('/introduction/aboutUs')} >{formatMessage({ id: 'introduction' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/counselorServices')} >{formatMessage({ id: 'Our Free Services' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/provideCourses/n5')}>{formatMessage({ id: 'Provide Courses' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/searchCourse')}>{formatMessage({ id: 'Search Courses' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/schoolLibrary')}>{formatMessage({ id: 'Informations' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/other/contactUs')} >{formatMessage({ id: 'Other' })}</a>
                            </li>
                        </ul>
                    </div>
                    <div className='desktop-footer-content-child'>
                        <ul className='desktop-footer-content-child-ul'>
                            <li>
                                <a onClick={() => router.push('/introduction/aboutUs')} >{formatMessage({ id: 'about us' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/introduction/newsAndEvents')}>{formatMessage({ id: 'newsAndEvents' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/introduction/ourPartners')}>{formatMessage({ id: 'our partners' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/introduction/testimonial')}>{formatMessage({ id: 'Testimonial' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/introduction/privatePolicies')} >{formatMessage({ id: 'Private Policies' })}</a>
                            </li>
                        </ul>

                        <ul className='desktop-footer-content-child-ul'>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/counselorServices')}>{formatMessage({ id: 'Counselor Services' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/applicationServices')}>{formatMessage({ id: 'Application Services' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/visaGuidance')}>{formatMessage({ id: 'Visa Guidance' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/accommadation')}>{formatMessage({ id: 'Accommadation' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/arrivalGuidance')}>{formatMessage({ id: 'Arrival Guidance' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/ourFreeServices/studentGuardian')}>{formatMessage({ id: 'Student Guardian' })}</a>
                            </li>
                        </ul>

                        <ul className='desktop-footer-content-child-ul'>
                            <li>
                                <a onClick={() => router.push('/provideCourses/n5')}>{formatMessage({ id: 'Japanese Language Online Course (N5)' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/provideCourses/n4')}>{formatMessage({ id: 'Japanese Language Online Course (N4)' })}</a>
                            </li>
                        </ul>

                        <ul className='desktop-footer-content-child-ul'>
                            <li>
                                <a onClick={() => {
                                    dispatch({
                                        type: 'course/r_updateState',
                                        payload: {
                                            searchCourseFilter: {
                                                ...courseState.searchCourseFilter,
                                                country_id: 3
                                            }
                                        }
                                    })
                                    router.push('/searchCourse')
                                }}>{formatMessage({ id: 'Courses In Japan' })}</a>
                            </li>
                            {/* <li>
                                <a onClick={() => {
                                    dispatch({
                                        type: 'course/r_updateState',
                                        payload: {
                                            searchCourseFilter: {
                                                ...courseState.searchCourseFilter,
                                                country_id: 5
                                            }
                                        }
                                    })
                                    router.push('/searchCourse')
                                }}>{formatMessage({ id: 'Courses In Singapore' })}</a>
                            </li> */}
                        </ul>

                        <ul className='desktop-footer-content-child-ul'>
                            <li>
                                <a onClick={() => router.push('/schoolLibrary')} >{formatMessage({ id: 'school library' })}</a>
                            </li>
                            <li>
                                <a href="#" /* onClick={() => router.push('/')} */>
                                    {formatMessage({ id: 'Personality Test' })}
                                    <div>{formatMessage({ id: '(coming soon)' })}</div>
                                </a>
                            </li>
                            <li>
                                <a href="#" /* onClick={() => router.push('/')} */>
                                    {formatMessage({ id: 'Discover Study Country' })}
                                    <div>{formatMessage({ id: '(coming soon)' })}</div>
                                </a>
                            </li>
                            <li>
                                <a href="#" /* onClick={() => router.push('/')} */>
                                    {formatMessage({ id: 'Occupation Profile' })}
                                    <div>{formatMessage({ id: '(coming soon)' })}</div>
                                </a>
                            </li>
                            <li>
                                <a href="#" /* onClick={() => router.push('/')} */>
                                    {formatMessage({ id: 'Explore Studies' })}
                                    <div>{formatMessage({ id: '(coming soon)' })}</div>
                                </a>
                            </li>
                        </ul>

                        <ul className='desktop-footer-content-child-ul'>
                            <li>
                                <a onClick={() => router.push('/other/contactUs')}>{formatMessage({ id: 'contact us' })}</a>
                            </li>
                            <li>
                                <a onClick={() => router.push('/other/faqs')} >{formatMessage({ id: 'FAQs' })}</a>
                            </li>
                            <li>
                                <a onClick={() => setState({ applyFormVisible: true })}>{formatMessage({ id: 'Apply With US' })}</a>
                            </li>
                        </ul>

                    </div>

                </div>
            </div>

            <div className='desktop-lesson'>
                <div className='desktop-lesson-content'>
                    <div className='desktop-lesson-content-school_name'>
                        <a href="#" onClick={() => router.push('/')} >EDU DOTCOM</a>
                    </div>

                    <div className='desktop-lesson-content-copyright'>
                        <div>
                            Copyright © {date.getFullYear()} Edu Dotcom Sdn. Bhd.All rights reverved.
                        </div>
                        <div>
                            For more information, please e-mail us at enquiry@edudotcom.my
                        </div>
                    </div>
                </div>
            </div>
            <BackTop>
                <div
                    className='desktop-backToTop'
                >
                    <ArrowUpOutlined />
                </div>
            </BackTop>
            {/* <div className='desktop-tools ant-back-top' style={{
                height: state.showToolsBar ? '90px' : '40px'
            }}>
                {
                    state.showToolsBar &&
                    <div className='desktop-tools-list'>
                        <div className='desktop-tools-list-menu' onClick={() => window.open('https://api.whatsapp.com/send/?phone=%2B60167023776&text&app_absent=0&lang=en', '_blank')}>
                            <img
                                src={require('@/assets/contact_us/whatsapp.png')}
                            />
                        </div>
                        <div className='desktop-tools-list-menu' onClick={() => window.open('https://www.facebook.com/messages/t/220199178101630', '_blank')}>
                            <img
                                src={require('@/assets/contact_us/facebook_messenger.png')}
                            />
                        </div>
                    </div>
                }

                <div
                    className='desktop-tools-main'
                    onClick={() => setState({ showToolsBar: !state.showToolsBar })}
                >
                    {
                        state.showToolsBar
                            ? <CloseCircleOutlined style={{ fontSize: '25px' }} />
                            : <CommentOutlined style={{ fontSize: '25px' }} />
                    }
                </div>
            </div> */}

        </React.Fragment >
    );
}