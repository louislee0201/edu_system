import { API_GET_SCHOOL } from '@/constants/api'
import proxyRequest from '@/utils/request';

export async function api_getSchool(params) {
    return proxyRequest.post(API_GET_SCHOOL, params);
}
