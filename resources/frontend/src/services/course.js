import { API_GET_COURSE } from '@/constants/api'
import proxyRequest from '@/utils/request';

export async function api_getCourse(params) {
    return proxyRequest.post(API_GET_COURSE, params);
}
