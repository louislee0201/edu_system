import proxyRequest from '@/utils/request';

export async function request(params, operate) {
    return proxyRequest[operate.method](operate.constant, params);
}
