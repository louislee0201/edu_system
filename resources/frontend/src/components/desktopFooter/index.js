import React, { useEffect, useState } from "react";
import { useDispatch } from 'react-redux'
import './index.less'
import { Carousel, Button } from 'antd';
import { formatMessage } from 'umi/locale';
import { InstagramOutlined, FacebookOutlined, MailOutlined, PhoneOutlined, HomeOutlined } from '@ant-design/icons';
import { PRIMARY_COLOR } from '@/constants/default'
import router from 'umi/router';
import { isWindows } from "mobile-device-detect";

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        loading: false
    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }


    const date = new Date()
    return (
        <div className='desktop-footer'>
            <div className='desktop-footer-logo'>
                <img src={require('@/assets/edu_white logo.png')} style={{ height: 'auto', width: '15%' }} />
            </div>

            <div className='desktop-footer-desc'>
                <div className='desktop-footer-desc-child'>
                    <div className='desktop-footer-desc-child-title'>
                        {formatMessage({ id: 'follow us' })}
                    </div>
                    <div className='desktop-footer-desc-child-content'>
                        <div className='desktop-footer-desc-child-content-icon'>
                            <Button
                                onClick={() => window.open('https://www.instagram.com/edu_dotcom/', '_blank')}
                                type='ghost'
                                className='desktop-footer-desc-child-content-icon-text'
                            >
                                <InstagramOutlined /> <div> Instagram</div>
                            </Button>
                        </div>
                        <div className='desktop-footer-desc-child-content-icon'>
                            <Button
                                onClick={() => window.open('https://www.facebook.com/EDUdotCOM', '_blank')}
                                type='ghost'
                                className='desktop-footer-desc-child-content-icon-text'
                            >
                                <FacebookOutlined /> <div> Facebook</div>
                            </Button>
                        </div>
                    </div>

                </div>

                <div className='desktop-footer-desc-child'>
                    <div className='desktop-footer-desc-child-title'>
                        {formatMessage({ id: 'contact us' })}
                    </div>
                    <div className='desktop-footer-desc-child-content'>
                        <div className='desktop-footer-desc-child-content-icon'>
                            <Button
                                type='ghost'
                                className='desktop-footer-desc-child-content-icon-text'
                            >
                                <PhoneOutlined /> <div> +607-5113776 / +6016-7023776</div>
                            </Button>
                        </div>
                        <div className='desktop-footer-desc-child-content-icon'>
                            <Button
                                type='ghost'
                                className='desktop-footer-desc-child-content-icon-text'
                            >
                                <MailOutlined /> <div> enquiry@edudotcom.my</div>
                            </Button>
                        </div>
                        <div className='desktop-footer-desc-child-content-icon'>
                            <Button
                                type='ghost'
                                className='desktop-footer-desc-child-content-icon-text'
                            >
                                <HomeOutlined /> <div> NO.86A, JLN NB2 1/4, TMN NUSA BESTARI 2 81300 SKUDAI, JB, MY</div>
                            </Button>
                        </div>
                    </div>

                </div>
            </div>

            <div className='desktop-footer-copy_right'>
                © {date.getFullYear()} Edu Dotcom Sdn. Bhd.
            </div>
        </div>

    )

}
