import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './index.less'
import router from 'umi/router';
import { Card, Divider, Tag } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    RightOutlined, LeftOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import MobileTopBar from '@/components/mobile-top-bar'
import DesktopApplyForm from '@/components/desktop-apply-form'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        applyFormVisible: false
    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <React.Fragment>
            <DesktopApplyForm visible={state.applyFormVisible} onClose={() => setState({ applyFormVisible: false })} />
            <div className='mobile-apply_form' onClick={() => setState({ applyFormVisible: true })}>

                <div className='mobile-apply_form-btn'>
                    {formatMessage({ id: 'enquiry - apply' })}
                </div>
            </div>
        </React.Fragment>
    )
}
