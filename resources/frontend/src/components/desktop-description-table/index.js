import React, { useEffect, useState, useRef } from "react";
import {
    Spin,
    Button,
    Popconfirm,
    Collapse,
    Tabs,
    InputNumber,
    Card,
    Carousel
} from "antd";
import { } from 'antd-mobile';
import { CaretRightOutlined, LeftOutlined, RightOutlined } from '@ant-design/icons';
import { formatMessage } from "umi/locale"
import { useDispatch } from 'react-redux'
import './index.less'
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
export default (props) => {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        visibleModal: false,
        descriptionList: [],
        loading: false
    })

    function setState(values, callback = () => { }) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))

        callback({ ...state, ...values })
    }

    useEffect(() => {
        setState({ loading: true })
        dispatch({
            type: "description/e_getDescriptionType",
            payload: {}
        }).then(result => {
            if (result) {
                setState({ ...result })
                dispatch({
                    type: "description/e_getDescriptionList",
                    payload: {
                        classMode: props.classMode,
                        related_id: props.related_id
                    }
                }).then(result => {
                    if (result) {
                        setState({ ...result, loading: false })
                        //props.setState({ loading: false })
                    }
                })
            }
        })
    }, [])

    function renderCollapseData(lists, mode = '') {
        let element = []
        let noCollapse = []

        Object.values(lists).map((list, listKey) => {
            Object.keys(list).map((description_type, description_key) => {
                const description_data = list[description_type]
                let content = []
                if (state.descriptionTypeList[description_type] == 'only picture') {
                    let imageList = []
                    let style = {
                        width: '100%'
                    }
                    if (description_data.data.img.length > 1) {
                        style = {
                            width: '49%',
                            marginBottom: '2%'
                        }
                    }
                    Object.values(description_data.data.img).map((pic, picKey) => {
                        let imageUrl
                        if (pic.indexOf('base64') >= 0) {
                            imageUrl = pic
                        } else {
                            imageUrl = 'https://api.edudotcom.my/auth/image/description/' + pic
                        }

                        imageList.push(
                            <img
                                className='onlyPicture'
                                style={{ ...style }}
                                src={imageUrl}
                                //alt="avatar"
                                key={picKey}
                            //style={{ width: 'auto', height: '100%' }}
                            />
                        )
                    })

                    let text = []
                    if (description_data.data.content) {
                        let datas = description_data.data.content.split('\n')
                        for (let i = 0; i < datas.length; i++) {
                            if (datas[i] != '') {
                                text.push(
                                    <div>{datas[i]}</div>
                                )
                            } else {
                                text.push(
                                    <div style={{ height: '10px' }} />
                                )
                            }
                        }

                    }

                    content.push([
                        <div style={{ display: 'flex', justifyContent: 'space-between', flexFlow: 'row wrap' }}>
                            {imageList}
                        </div>,
                        text
                    ])
                } else if (state.descriptionTypeList[description_type] == 'only text') {
                    let datas = description_data.data.split('\n')
                    for (let i = 0; i < datas.length; i++) {
                        if (datas[i] != '') {
                            content.push(
                                <div>{datas[i]}</div>
                            )
                        } else {
                            content.push(
                                <div style={{ height: '10px' }} />
                            )
                        }

                    }
                } else if (state.descriptionTypeList[description_type] == 'optional') {

                    content.push(
                        <Tabs
                            type="card"
                            id={description_key}
                            onTouchStart={() => {
                                setState({ showSlideTabs: false })
                            }}
                        >
                            {renderCollapseData(description_data.data, 'optional')}
                        </Tabs>
                    )

                } else if (state.descriptionTypeList[description_type] == 'picture and text') {
                    const textList = description_data.data.content
                    Object.values(description_data.data.img).map((pic, picKey) => {
                        let image = ''
                        if (pic.indexOf('base64') >= 0) {
                            image = pic
                        } else {
                            image = window.location.protocol + '//' + window.location.hostname +
                                (window.location.port > 0 ? ':8000' : '') +
                                '/auth/image/description/' + pic
                        }

                        content.push(
                            <div style={{ marginRight: '5px', marginBottom: '5px', /* maxHeight: '130px', */ display: 'flex' }}>
                                <img src={image} alt="avatar" key={picKey} style={{ width: '30%', height: '30%', marginRight: '5px' }} />
                                <div style={{ width: '100%', textAlign: 'center', marginTop: 'auto', marginBottom: 'auto', padding: '0 10px' }}>
                                    {textList[picKey]}
                                </div>
                            </div>
                        )
                    })
                } else if (state.descriptionTypeList[description_type] == 'video') {
                    let videos = []
                    Object.values(description_data.data).map((video, videoKey) => {
                        videos.push(
                            <div style={{ width: '49%' }} key={videoKey}>
                                <iframe
                                    width="100%"
                                    height="235px"
                                    src={video}
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen
                                />
                            </div>
                        )
                    })

                    content.push(
                        <div style={{ flexWrap: 'wrap', paddingLeft: '0', display: 'flex', justifyContent: 'space-between' }}>
                            {videos}
                        </div>
                    )

                } else if (state.descriptionTypeList[description_type] == 'pdf') {
                    let card = []
                    Object.values(description_data.data).map((ob, obKey) => {
                        let time = ob.img.split(".")[0]
                        let imageUrl = ''
                        if (Number.isInteger(parseInt(time))) {
                            imageUrl = 'https://api.edudotcom.my/auth/image/description/' + ob.img
                        } else {
                            imageUrl = ob.img
                        }
                        
                        card.push(
                            <div style={{ width: '50%', textAlign: 'center' }}>
                                <Card
                                    onClick={(e) => {
                                        e.preventDefault()
                                        window.open(
                                            'https://api.edudotcom.my/auth/pdf/description/' + ob.pdf,
                                            '_blank'
                                        )
                                    }}
                                    key={obKey}
                                    style={{
                                        marginRight: '8%',
                                        marginLeft: '8%',
                                        marginTop: '10px',
                                        width: '250px',
                                        display: 'inline-block'
                                    }}
                                    cover={< img alt="example" src={imageUrl} style={{ width: 'auto' }
                                    } />}
                                >
                                    <Card.Meta title={ob.content} />
                                </Card >
                            </div>
                        )
                    })

                    content.push(
                        <div style={{ display: 'flex', flexFlow: 'row wrap' }}>
                            {card}
                        </div>
                    )

                } else if (state.descriptionTypeList[description_type] == 'new component') {
                    content = renderCollapseData(description_data.data)
                }
                if (mode == 'optional') {
                    noCollapse.push(
                        <Tabs.TabPane tab={description_data.title} key={listKey}>
                            {content}
                        </Tabs.TabPane>
                    )
                }
                if (description_data.title == null) {
                    noCollapse.push(content)
                } else {
                    element.push(
                        <Collapse.Panel
                            className="site-collapse-custom-panel"
                            header={<span style={{ fontWeight: 'bold' }}>{description_data.title}</span>} key={listKey}
                        >
                            {content}
                        </Collapse.Panel>
                    )
                }

            })

        })
        if (mode == 'optional') {
            return noCollapse
        }
        return [
            noCollapse,
            element.length > 0 && <Collapse
                bordered={false}
                expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
                className="site-collapse-custom-collapse"
            >
                {element}
            </Collapse>
        ]
    }


    function renderCollapse(description) {
        const list = JSON.parse(description.content)
        return renderCollapseData(list)
    }

    if (state.loading) {
        return <Spin />
    }

    return (
        <div className='mobile-desc'>
            <Collapse
                bordered={false}
                expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
                className="site-collapse-custom-collapse"
            >
                {
                    Object.values(state.descriptionList).map((description) => {
                        return (
                            <Collapse.Panel
                                className="site-collapse-custom-panel"
                                header={<span style={{ fontWeight: 'bold' }}>{description.title}</span>} key={description.id}
                            >
                                {renderCollapse(description)}
                            </Collapse.Panel>
                        )
                    })
                }
            </Collapse>
        </div>

    )
}