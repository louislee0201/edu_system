import React, { useState } from 'react';
import router from 'umi/router';
import Desktop from '@/pages/desktop'
import './index.less'
import { formatMessage } from 'umi/locale';
import { Input, Modal, Radio, message } from 'antd';
import { useDispatch, useSelector } from 'react-redux'
import DesktopTitleMenu from '@/components/desktop-title-menu'

export default (props) => {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        form: '',
        name: '',
        email: '',
        contact: '',
        remark: ''
    })
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }


    return (
        <Modal
            className='desktop-applyform'
            visible={props.visible}
            onOk={() => {
                if (typeof props.info !== 'undefined' && (
                    !props.info.school_contact_me ||
                    (props.info.school_contact_me && !props.info.contact_me)
                )) {
                    props.onClose()
                    return
                }

                if (
                    state.form == '' ||
                    state.name == '' ||
                    state.email == '' ||
                    state.contact == ''
                ) {
                    message.error(formatMessage({ id: 'please provide the infomation under the form' }));
                }
                else {
                    dispatch({
                        type: "home/e_submitApplyForm",
                        payload: {
                            name: state.name,
                            email: state.email,
                            contact: state.contact,
                            remark: state.remark,
                            form: state.form,
                            link: window.location.href
                        }
                    }).then(result => {
                        if (result) {
                            message.success(formatMessage({ id: 'successfully submit' }) + '!');
                            props.onClose()
                        }
                    });
                }
            }}
            onCancel={() => props.onClose()}
        >
            {
                typeof props.info !== 'undefined' && props.info.school_contact_me == 0
                    ? <React.Fragment>
                        EDU DOTCOM do not represent <span>{props.info.school_name}.</span><br /><br />
                        All enquiries shall contact directly to <span style={{ borderBottom: '1px solid grey', fontWeight: 'bold' }}>{props.info.email}</span>
                    </React.Fragment>
                    : typeof props.info !== 'undefined' && props.info.contact_me == 0 && props.info.school_contact_me == 1
                        ? <React.Fragment>
                            EDU DOTCOM was appointed by <span style={{}}>{props.info.school_name}</span> as representative for certain courses only. This course is not under authorization for our center.
                            <br /><br />
                            All enquiries regarding on this course shall contact directly to <span style={{ borderBottom: '1px solid grey', fontWeight: 'bold' }}>{props.info.email}</span>
                        </React.Fragment>
                        : <React.Fragment >
                            <div className='desktop-applyform-radio_title'>
                                {formatMessage({ id: 'please select type' })}:
                            </div>
                            <Radio.Group onChange={(e) => setState({ form: e.target.value })} value={state.form}>
                                <Radio value="apply with us">{formatMessage({ id: 'Apply With US' })}</Radio>
                                <Radio value="enquiry">{formatMessage({ id: 'enquiry' })}</Radio>
                            </Radio.Group>
                            <Input onChange={(e) => setState({ name: e.target.value })} value={state.name} placeholder={formatMessage({ id: 'name' })} />
                            <Input type="email" onChange={(e) => setState({ email: e.target.value })} value={state.email} placeholder={formatMessage({ id: 'email' })} />
                            <Input onChange={(e) => setState({ contact: e.target.value })} value={state.contact} placeholder={formatMessage({ id: 'contact' })} />

                            <Input.TextArea onChange={(e) => setState({ remark: e.target.value })} value={state.remark} placeholder={formatMessage({ id: 'remark' }) + formatMessage({ id: 'optional' })} />
                        </React.Fragment>
            }

        </Modal >
    )

}