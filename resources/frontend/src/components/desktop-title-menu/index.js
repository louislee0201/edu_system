import React, { useState } from 'react';
import router from 'umi/router';
import 'antd-mobile/dist/antd-mobile.css';
import Desktop from '@/pages/desktop'
import './index.less'
import { formatMessage } from 'umi/locale';
import { useDispatch, useSelector } from 'react-redux'
import { Divider } from 'antd';
import { UserOutlined, InstagramOutlined, FacebookOutlined } from '@ant-design/icons';
import { getLocalStorage, saveLocalStorage } from '@/tools'
import { PRIMARY_COLOR } from '@/constants/default'

export default (props) => {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({

    })
    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    return (
        <div className='desktop-title_menu'>
            <div className='desktop-title_menu-title'>
                {props.title}
            </div>
            <div className='desktop-title_menu-list'>
                {
                    Object.values(props.menu).map((m, k) => {
                        return <div
                            className={
                                window.location.pathname.indexOf(m.url) >= 0
                                    ? 'desktop-title_menu-list-menu active'
                                    : 'desktop-title_menu-list-menu'
                            }
                            key={k}
                            onClick={() => router.push(m.url)}
                        >
                            {m.label}
                        </div>
                    })
                }
            </div>
        </div>

    );
}