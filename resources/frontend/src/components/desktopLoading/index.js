import React, { useEffect, useState } from "react";
import './index.less'

export default function (props) {

    if (props.loading) {
        return <div className='desktop_loading'>
            <div className='desktop_loading-png'>
                <img src={require('@/assets/loading_logo.png')} style={{ width: '100%', height: '100%' }} />
            </div>
            <div className='desktop_loading-gif'>
                <img src={require('@/assets/loading_logo.gif')} style={{ width: '100%', height: '100%' }} />
            </div>
        </div>
    }

    return (
        props.children && !props.loading ? props.children : null
    )

}
