import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import './index.less'
import router from 'umi/router';
import { Card, Divider, Drawer } from 'antd';
import { Carousel } from 'antd-mobile';
import { formatMessage } from 'umi/locale';
import {
    UnorderedListOutlined, BulbOutlined, DownOutlined, UpOutlined, InfoCircleOutlined,
    BookOutlined, EllipsisOutlined, SafetyCertificateOutlined, TeamOutlined
} from '@ant-design/icons';
import Loading from '@/components/loading'
import { PRIMARY_COLOR } from '@/constants/default'
import DesktopApplyForm from '@/components/desktop-apply-form'

export default function (props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        drawerVisible: false,
        activeKey: '',
        activeSubKey: '',
        applyFormVisible: false
    })

    function setState(value) {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
    }

    const courseState = useSelector(state => state.course)
    const configState = useSelector(state => state.config)

    useEffect(() => {
        let activeKey = ''
        if (window.location.pathname.indexOf('/introduction') > -1) {
            activeKey = 'introduction'
        } else if (window.location.pathname.indexOf('/ourFreeServices') > -1) {
            activeKey = 'Our Free Services'
        } else if (window.location.pathname.indexOf('/eduCourse') > -1) {
            activeKey = 'Provide Course'
        } else if (window.location.pathname.indexOf('/searchCourses') > -1) {
            activeKey = 'Search Courses'
        } else if (window.location.pathname.indexOf('/information') > -1) {
            activeKey = 'Information'
        } else if (window.location.pathname.indexOf('/other') > -1) {
            activeKey = 'Other'
        }
        setState({ activeKey })
    }, []);

    return (
        <div className='mobile-homePage-top_nav'>
            <div className='mobile-homePage-top_nav-img' onClick={() => router.push('/home')}>
                <img src={require('@/assets/edu_white logo.png')} />
            </div>
            <Drawer
                className="mobile-homePage-top_nav-drawer"
                style={{ zIndex: '9999' }}
                placement="right"
                onClose={() => setState({ drawerVisible: false })}
                visible={state.drawerVisible}
            >
                <div className="mobile-homePage-top_nav-drawer-main_part">
                    <div className="mobile-homePage-top_nav-drawer-main_part-img">
                        <img src={require('@/assets/edu_white logo.png')} />
                    </div>
                    <div className="mobile-homePage-top_nav-drawer-main_part-login">
                        {
                            configState.account_level == 1 &&
                            <div
                                className='mobile-homePage-top_nav-drawer-main_part-login-un'
                            >
                                {formatMessage({ id: 'welcome to' })} EDU DOTCOM
                        </div>
                        }

                        {
                            configState.account_level == 2 &&
                            <div
                                className='mobile-homePage-top_nav-drawer-main_part-login-ed'
                            >
                                <div>
                                    {formatMessage({ id: 'hi' })}, {configState.name} !
                            </div>
                                <div>
                                    {configState.email}
                                </div>
                            </div>

                        }
                    </div>
                </div>

                <div className="mobile-homePage-top_nav-drawer-login_btn">
                    <div
                        className="mobile-homePage-top_nav-drawer-login_btn-btn"
                        style={{ background: '#f1c40f', color: 'white' }}
                        onClick={() => router.push('/login?mode=register')}
                    >
                        {formatMessage({ id: 'register' })}
                    </div>
                    <div className="mobile-homePage-top_nav-drawer-login_btn-btn" onClick={() => router.push('/login')}>
                        {formatMessage({ id: 'login' })}
                    </div>
                </div>

                <div className="mobile-homePage-top_nav-drawer-menu_list">
                    <div
                        className={state.activeKey == 'introduction' ? "mobile-homePage-top_nav-drawer-menu_list-child active" : "mobile-homePage-top_nav-drawer-menu_list-child"}
                        onClick={() => setState({ activeKey: state.activeKey != 'introduction' ? 'introduction' : '' })}
                    >

                        <div className="mobile-homePage-top_nav-drawer-menu_list-child-box">
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-icon">
                                <TeamOutlined />
                            </div>
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-title">
                                {formatMessage({ id: 'introduction' })}
                            </div>
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-dropdown">
                                {state.activeKey == 'introduction' ? <UpOutlined /> : <DownOutlined />}
                            </div>
                        </div>
                        {
                            state.activeKey == 'introduction' &&
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list">
                                <div
                                    className={
                                        window.location.pathname.indexOf('/introduction/aboutUs') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div
                                        className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title"
                                        onClick={() => router.push('/introduction/aboutUs')}
                                    >
                                        {formatMessage({ id: 'about us' })}
                                    </div>
                                </div>

                                <div
                                    className={
                                        window.location.pathname.indexOf('/introduction/newsAndEvents') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div
                                        className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title"
                                        onClick={() => router.push('/introduction/newsAndEvents')}
                                    >
                                        {formatMessage({ id: 'newsAndEvents' })}
                                    </div>
                                </div>

                                <div
                                    className={
                                        window.location.pathname.indexOf('/introduction/ourPartners') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div
                                        className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title"
                                        onClick={() => router.push('/introduction/ourPartners')}
                                    >
                                        {formatMessage({ id: 'our partners' })}
                                    </div>
                                </div>

                                <div
                                    className={
                                        window.location.pathname.indexOf('/introduction/testimonial') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div
                                        className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title"
                                        onClick={() => router.push('/introduction/testimonial')}
                                    >
                                        {formatMessage({ id: 'Testimonial' })}
                                    </div>
                                </div>

                                <div
                                    className={
                                        window.location.pathname.indexOf('/introduction/privatePolicies') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div
                                        className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title"
                                        onClick={() => router.push('/introduction/privatePolicies')}
                                    >
                                        {formatMessage({ id: 'Private Policies' })}
                                    </div>
                                </div>
                            </div>
                        }
                    </div>

                    <div className={state.activeKey == 'Our Free Services' ? "mobile-homePage-top_nav-drawer-menu_list-child active" : "mobile-homePage-top_nav-drawer-menu_list-child"} onClick={() => setState({ activeKey: state.activeKey != 'Our Free Services' ? 'Our Free Services' : '' })}>
                        <div className="mobile-homePage-top_nav-drawer-menu_list-child-box">
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-icon">
                                <SafetyCertificateOutlined />
                            </div>
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-title">
                                {formatMessage({ id: 'Our Free Services' })}
                            </div>
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-dropdown">
                                {state.activeKey == 'Our Free Services' ? <UpOutlined /> : <DownOutlined />}
                            </div>
                        </div>
                        {
                            state.activeKey == 'Our Free Services' &&
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list">
                                <div
                                    onClick={() => router.push('/ourFreeServices/counselorServices')}
                                    className={
                                        window.location.pathname.indexOf('/ourFreeServices/counselorServices') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Counselor Services' })}
                                    </div>
                                </div>

                                <div
                                    onClick={() => router.push('/ourFreeServices/applicationServices')}
                                    className={
                                        window.location.pathname.indexOf('/ourFreeServices/applicationServices') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Application Services' })}
                                    </div>
                                </div>

                                <div
                                    onClick={() => router.push('/ourFreeServices/visaGuidance')}
                                    className={
                                        window.location.pathname.indexOf('/ourFreeServices/visaGuidance') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Visa Guidance' })}
                                    </div>
                                </div>

                                <div
                                    onClick={() => router.push('/ourFreeServices/accommadation')}
                                    className={
                                        window.location.pathname.indexOf('/ourFreeServices/accommadation') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Accommadation' })}
                                    </div>
                                </div>

                                <div
                                    onClick={() => router.push('/ourFreeServices/arrivalGuidance')}
                                    className={
                                        window.location.pathname.indexOf('/ourFreeServices/arrivalGuidance') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Arrival Guidance' })}
                                    </div>
                                </div>

                                <div
                                    onClick={() => router.push('/ourFreeServices/studentGuardian')}
                                    className={
                                        window.location.pathname.indexOf('/ourFreeServices/studentGuardian') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Student Guardian' })}
                                    </div>
                                </div>
                            </div>
                        }
                    </div>

                    <div className={state.activeKey == 'Provide Courses' ? "mobile-homePage-top_nav-drawer-menu_list-child active" : "mobile-homePage-top_nav-drawer-menu_list-child"} onClick={() => setState({ activeKey: state.activeKey != 'Provide Courses' ? 'Provide Courses' : '' })}>
                        <div className="mobile-homePage-top_nav-drawer-menu_list-child-box">
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-icon">
                                <BulbOutlined />
                            </div>
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-title">
                                {formatMessage({ id: 'Provide Courses' })}
                            </div>
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-dropdown">
                                {state.activeKey == 'Provide Courses' ? <UpOutlined /> : <DownOutlined />}
                            </div>
                        </div>
                        {
                            state.activeKey == 'Provide Courses' &&
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list">
                                <div
                                    onClick={() => router.push('/provideCourses/n5')}
                                    className={
                                        window.location.pathname.indexOf('/provideCourses/n5') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Japanese Language Online Course (N5)' })}
                                    </div>
                                </div>

                                <div
                                    onClick={() => router.push('/provideCourses/n4')}
                                    className={
                                        window.location.pathname.indexOf('/provideCourses/n4') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Japanese Language Online Course (N4)' })}
                                    </div>
                                </div>
                            </div>
                        }
                    </div>

                    <div className={state.activeKey == 'Search Courses' ? "mobile-homePage-top_nav-drawer-menu_list-child active" : "mobile-homePage-top_nav-drawer-menu_list-child"} onClick={() => setState({ activeKey: state.activeKey != 'Search Courses' ? 'Search Courses' : '' })}>
                        <div className="mobile-homePage-top_nav-drawer-menu_list-child-box">
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-icon">
                                <BookOutlined />
                            </div>
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-title">
                                {formatMessage({ id: 'Search Courses' })}
                            </div>
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-dropdown">
                                {state.activeKey == 'Search Courses' ? <UpOutlined /> : <DownOutlined />}
                            </div>
                        </div>
                        {
                            state.activeKey == 'Search Courses' &&
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list">
                                <div
                                    className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child"
                                    onClick={() => {
                                        dispatch({
                                            type: 'course/r_updateState',
                                            payload: {
                                                searchCourseFilter: {
                                                    ...courseState.searchCourseFilter,
                                                    country_id: 3,
                                                    dataList: []
                                                }
                                            }
                                        })
                                        router.push('/searchCourse')
                                    }}
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Study in Japan' })}
                                    </div>
                                </div>

                                {/* <div
                                    className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child"
                                    onClick={() => {
                                        dispatch({
                                            type: 'course/r_updateState',
                                            payload: {
                                                searchCourseFilter: {
                                                    ...courseState.searchCourseFilter,
                                                    country_id: 5,
                                                    dataList: []
                                                }
                                            }
                                        })
                                        router.push('/searchCourse')
                                    }}
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Study in Singapore' })}
                                    </div>
                                </div> */}
                            </div>
                        }
                    </div>

                    <div className={state.activeKey == 'Informations' ? "mobile-homePage-top_nav-drawer-menu_list-child active" : "mobile-homePage-top_nav-drawer-menu_list-child"} onClick={() => setState({ activeKey: state.activeKey != 'Informations' ? 'Informations' : '' })}>

                        <div className="mobile-homePage-top_nav-drawer-menu_list-child-box">
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-icon">
                                <InfoCircleOutlined />
                            </div>
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-title">
                                {formatMessage({ id: 'Informations' })}
                            </div>
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-dropdown">
                                {state.activeKey == 'Informations' ? <UpOutlined /> : <DownOutlined />}
                            </div>
                        </div>
                        {
                            state.activeKey == 'Informations' &&
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list">
                                <div
                                    className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child"
                                    onClick={() => router.push('/schoolLibrary')}
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'school library' })}
                                    </div>
                                </div>

                                <div
                                    className={
                                        window.location.pathname.indexOf('/introduction/personalityTest') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Personality Test' })}
                                        <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title-tp">
                                            {formatMessage({ id: '(coming soon)' })}
                                        </div>
                                    </div>
                                </div>

                                <div
                                    className={
                                        window.location.pathname.indexOf('/introduction/discoverStudyCountry') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Discover Study Country' })}
                                        <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title-tp">
                                            {formatMessage({ id: '(coming soon)' })}
                                        </div>
                                    </div>
                                </div>

                                <div
                                    className={
                                        window.location.pathname.indexOf('/introduction/occupationProfile') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Occupation Profile' })}
                                        <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title-tp">
                                            {formatMessage({ id: '(coming soon)' })}
                                        </div>
                                    </div>
                                </div>

                                <div
                                    className={
                                        window.location.pathname.indexOf('/introduction/exploreStudies') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Explore Studies' })}
                                        <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title-tp">
                                            {formatMessage({ id: '(coming soon)' })}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }
                    </div>

                    <DesktopApplyForm visible={state.applyFormVisible} onClose={() => setState({ applyFormVisible: false })} />

                    <div className={state.activeKey == 'Other' ? "mobile-homePage-top_nav-drawer-menu_list-child active" : "mobile-homePage-top_nav-drawer-menu_list-child"} onClick={() => setState({ activeKey: state.activeKey != 'Other' ? 'Other' : '' })}>
                        <div className="mobile-homePage-top_nav-drawer-menu_list-child-box">
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-icon">
                                <EllipsisOutlined />
                            </div>
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-title">
                                {formatMessage({ id: 'Other' })}
                            </div>
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-dropdown">
                                {state.activeKey == 'Other' ? <UpOutlined /> : <DownOutlined />}
                            </div>
                        </div>
                        {
                            state.activeKey == 'Other' &&
                            <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list">
                                <div
                                    onClick={() => router.push('/other/contactUs')}
                                    className={
                                        window.location.pathname.indexOf('/other/contactUs') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'contact us' })}
                                    </div>
                                </div>

                                <div
                                    onClick={() => router.push('/other/faqs')}
                                    className={
                                        window.location.pathname.indexOf('/other/faqs') >= 0
                                            ? 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child active'
                                            : 'mobile-homePage-top_nav-drawer-menu_list-child-content_list-child'
                                    }
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'FAQs' })}
                                    </div>
                                </div>

                                <div
                                    className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child"
                                    onClick={() => setState({ applyFormVisible: true })}
                                >
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-icon">

                                    </div>
                                    <div className="mobile-homePage-top_nav-drawer-menu_list-child-content_list-child-title">
                                        {formatMessage({ id: 'Apply With US' })}
                                    </div>
                                </div>
                            </div>
                        }
                    </div>
                </div>
            </Drawer>
            {/*  <TreeSelect
            showSearch
            style={{ width: '70%' }}
            //value={this.state.value}
            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
            placeholder="Please select"
            allowClear
            treeDefaultExpandAll
        >
            <TreeSelect.TreeNode value="parent 1" title="parent 1">
                <TreeSelect.TreeNode value="parent 1-0" title="parent 1-0">
                    <TreeSelect.TreeNode value="leaf1" title="my leaf" />
                    <TreeSelect.TreeNode value="leaf2" title="your leaf" />
                </TreeSelect.TreeNode>
                <TreeSelect.TreeNode value="parent 1-1" title="parent 1-1">
                    <TreeSelect.TreeNode value="sss" title={<b style={{ color: '#08c' }}>sss</b>} />
                </TreeSelect.TreeNode>
            </TreeSelect.TreeNode>
        </TreeSelect> */}
            <div className='mobile-homePage-top_nav-icon' onClick={() => setState({ drawerVisible: true })}>
                <UnorderedListOutlined />
            </div>

        </div >
    )
}
