import { LoadingOutlined } from '@ant-design/icons';
import './index.less'
import { Spin } from 'antd';
import { PRIMARY_COLOR } from '@/constants/default'
import { formatMessage } from "umi/locale";

export default (props) => {
    if (props.loading) {
        return (
            <div className='loading_page' onClick={(e) => e.preventDefault}>
                <div className='loading_page-content'>
                    <div className='loading_page-content-icon'>
                        <Spin
                            size='large'
                            style={{ color: PRIMARY_COLOR }}
                            indicator={
                                <LoadingOutlined type='loading' color='#4d4d4d' spin />
                            }
                        />
                    </div>
                    <div className='loading_page-content-msg'>
                        {formatMessage({ id: 'loading...' })}
                    </div>
                </div>
            </div>
        )
    }

    return null
}