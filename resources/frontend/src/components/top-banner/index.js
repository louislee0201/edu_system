import React, { Component } from 'react'
import { SearchBar, Modal, Menu, Icon } from 'antd-mobile';
import router from 'umi/router';
import './index.less'
import { FilterOutlined } from '@ant-design/icons';
import { formatMessage } from 'umi/locale';

export default (props) => {
    const width = props.extraFilter ? 80 : 100
    const extraFilter = props.extraFilter ? props.extraFilter : null
    return (
        <React.Fragment>
            <div className='top_banner'>
                <div className='top_banner-content'>
                    {
                        (typeof props.backBtn === 'undefined' || props.backBtn) &&
                        <div
                            className='top_banner-content-icon'
                            onClick={() => router.goBack()}
                        >
                            <Icon size='md' type='left' />
                        </div>
                    }

                    {
                        props.leftButton &&
                        <div className='top_banner-content-left' >
                            <div className='top_banner-left-filter_icon'>
                                {props.leftButton}
                            </div>
                        </div>
                    }



                    <div className='top_banner-content-title'>
                        {props.title}
                    </div>

                    <div className='top_banner-content-filter' >
                        <div className='top_banner-content-filter_icon'>
                            {props.rightButton}
                        </div>
                    </div>
                </div>
                {
                    props.subMenuButton &&
                    <div className='top_banner-content2'>
                        <div className='top_banner-content2-menus'>
                            {
                                Object.values(props.subMenuButton).map((value, key) => {
                                    return (
                                        <div
                                            key={key}
                                            style={{ width: (100 / props.subMenuButton.length) + '%', padding: '5px' }}
                                            className='top_banner-content2-menus_child'
                                        >
                                            {value.label}
                                        </div>
                                    )
                                })
                            }
                            {
                                extraFilter
                            }
                        </div>
                    </div>
                }

            </div>

            <div className={props.subMenuButton ? 'top_box-big' : 'top_box-small'} />
        </React.Fragment>
    )
}