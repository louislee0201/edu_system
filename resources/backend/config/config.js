const plugins = [
  ['umi-plugin-antd-icon-config', {}],
  ['umi-plugin-react',
    {
      antd: true,
      dva: true,
      locale: {
        // default false
        enable: true,
        // default zh-CN
        default: 'zh-CN',
        // default true, when it is true, will use `navigator.language` overwrite default
        baseNavigator: true,
      }
    },
  ],
  
];

export default 
{
  base: '/admin/',//admin/
  publicPath: '/admin/',//admin/
  outputPath: '../../public/admin',
  plugins,
  hash: true,
  cssLoaderOptions:
  {
    localIdentName:'[local]'
  },
  /* routes: [
    {
      path: '/login',
      component: '../pages/login', // 指定以下页面的布局
    }
  ] */
};
