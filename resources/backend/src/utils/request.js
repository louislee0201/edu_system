import { message } from 'antd'
import { getUserToken, removeUser, reSetUserToken } from '@/tools/user'
import NProgress from 'nprogress';
import { formatMessage } from 'umi/locale'
import { HOST, ROUTE } from '@/constants/api'

const API_STATUS = {
    200: 'Success',
    500: 'Internal Error',
    601: 'Api Key Invalid',
    602: 'Token Expired'
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }

    const error = new Error(response.statusText);
    error.response = response;
    throw error;
}

function checkLocalDev() {
    let url = window.location.href
    let arr = url.split("/");
    return arr[0] + "//" + arr[2]
}

export default async function request(url, method, payload) {
    payload.api_token = getUserToken()
    let newUrl = url.indexOf('auth') >= 0 ? url : ROUTE + url
    const response = await fetch(HOST + newUrl,
        {
            method,
            body: JSON.stringify(payload),
            headers: {
                'Content-Type': 'application/json'
            }
        });
    checkStatus(response);
    return await response.json().then(result => {
        return responResult(result)
    });

    function responResult(result) {
        console.log(result)
        if (result.code == 200) {
            return {
                success: true,
                data: (typeof result.data !== 'undefined' && result.data !== null && result.data != '')
                    ? result.data : ''
            }
        }

        message.error(formatMessage({ id: result.error }));
        if (result.code == 602) {
            removeUser()
            location.reload()
        }
        return {
            success: false,
            data: result.error
        }
    }

}