import React, { Component } from 'react'
import { connect } from 'dva';
import { formatMessage } from 'umi/locale';
import router from 'umi/router'
import './index.scss';
import { list } from '@/components/authorized/list';
import Authorized from '@/components/authorized'
import { Form, Input, Button, Checkbox, Avatar, Layout, Menu } from 'antd';
import { getUserRole } from '@/tools/user'

const { Header, Sider, Content } = Layout;

@connect(({ loading, user }) => ({ loading, user }))
class SiderBar extends Component {
    goTo(url) {
        router.push(url)
    }

    render() {
        const routeRules = list()
        if (this.props.user.role) {
            return (
                <Sider style={{ minHeight: '100vh' }} collapsible collapsed={this.props.collapsed}>

                    <Button type="link" className="logoBtn" onClick={this.goTo.bind(this, '/home')}>
                        <div className="logo">
                            EDU
                        </div>
                    </Button>


                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                        {
                            Object.keys(routeRules).map((listKey, key) => {
                                return (
                                    Object.values(routeRules[listKey].rules).map((rule, rulesKey) => {
                                        if (Authorized({ authority: rule.role })) {
                                            return (
                                                <Menu.SubMenu
                                                    key={key}
                                                    title={
                                                        <span>
                                                            {
                                                                routeRules[listKey].icon
                                                            }
                                                            <span>
                                                                {formatMessage({ id: listKey })}
                                                            </span>
                                                        </span>
                                                    }
                                                >
                                                    {
                                                        Object.values(rule.routes).map((subValue, subKey) => {
                                                            return (
                                                                <Menu.Item
                                                                    key={key + '.' + subKey}
                                                                    onClick={this.goTo.bind(this, subValue.url)}
                                                                >
                                                                    {formatMessage({ id: subValue.titleName })}
                                                                </Menu.Item>
                                                            )
                                                        })
                                                    }
                                                </Menu.SubMenu>
                                            )
                                            
                                        }
                                    })
                                )
                            })
                        }
                    </Menu>
                </Sider>
            );
        }
        else {
            return null
        }
    }
}

export default SiderBar