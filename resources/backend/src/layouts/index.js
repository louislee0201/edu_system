import React, { Component } from 'react'
import SiderBar from './siderBar';
import HeaderBar from './headerBar';
import ContentBar from './contentBar';
import './index.scss';
import { Layout, Spin } from 'antd';
import { connect } from 'dva';
import Login from '@/pages/login'

@connect(({ user }) => ({ user }))
class AdminLayouts extends Component {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    checkRoleStatus() {
        if (this.props.user.role != '') {
            return (
                <React.Fragment>
                    <SiderBar
                        collapsed={this.state.collapsed}
                    />

                    <Layout className="site-layout">

                        <HeaderBar
                            collapsed={this.state.collapsed}
                            toggle={this.toggle}
                        />

                        <ContentBar
                            children={this.props.children}
                        />

                    </Layout>
                </React.Fragment>
            )
        }
        return (
            <Login />
        )
    }

    render() {
        if (this.props.user.statusCheck) {
            return (
                <Layout>
                    {this.checkRoleStatus()}
                </Layout>
            );
        }
        return (<Spin style={{ height: '100%', justifyContent: 'center', display: 'flex', alignItems: 'center' }} />)
    }
}

export default AdminLayouts