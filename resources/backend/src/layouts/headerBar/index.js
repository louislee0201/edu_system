import React, { Component } from 'react'
import {connect} from 'dva';
import { formatMessage } from 'umi/locale';
import { SYSTEM_LANGUAGE } from '@/constants/default'
import './index.scss';

import { Form, Input, Button, Checkbox, Avatar, Layout, Menu, message, Dropdown } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  ControlTwoTone,
} from '@ant-design/icons';

const { Header, Sider, Content } = Layout;

@connect(({user}) => ({user}))
class headerBar extends Component
{
    menuList(F_changeLanguage,nameTitleLanguage,F_logout)
    {
        return(
            <Menu>
                <Menu.Item onClick={F_changeLanguage}>
                    {nameTitleLanguage}
                </Menu.Item>
                <Menu.Item onClick={F_logout}>
                    {formatMessage({ id: 'logout' })}
                </Menu.Item>
            </Menu>
        )
    }

    changeLanguage = () =>
    {
        const lang = this.props.user.language == SYSTEM_LANGUAGE.zh_CN ? SYSTEM_LANGUAGE.en_US : SYSTEM_LANGUAGE.zh_CN
        this.props.dispatch({
            type: 'user/e_changeLanguage',
            payload: lang,
        })
    }

    logout()
    {
        this.props.dispatch({
            type:'user/e_logout',
            payload:{}
        })
    }

    render()
    {
        const nameTitleLanguage = this.props.user.language == SYSTEM_LANGUAGE.zh_CN ? 'English' : '中文'
        const List = this.menuList.bind(this,this.changeLanguage.bind(this),nameTitleLanguage,this.logout.bind(this))
        
        return (
            <Header className="site-layout-background" style={{ padding: 0 }}>
                {
                    React.createElement(this.props.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                        className: 'trigger',
                        onClick: this.props.toggle,
                    })
                }
                <Dropdown overlay={List} placement="bottomRight">
                    <ControlTwoTone />
                </Dropdown>
            </Header>
        );
    }
}

export default headerBar