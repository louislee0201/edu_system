import { api_updateCourse, api_getCourseList, api_getRequirementAttr, api_submitRequirement, api_updateRequirement } from '@/services/course'

export default
    {
        namespace: 'course',
        state:
        {
        },
        reducers:
        {
            r_updateState(state, { payload }) {
                return { ...state, ...payload }
            }
        },
        effects:
        {
            * e_getCourseList({ payload }, { put, call }) {
                const { success, data } = yield call(api_getCourseList, payload);
                if (success) {
                    return data
                }
                return false
            },

            * e_updateCourse({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateCourse, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_getRequirementAttr({ payload }, { put, call }) {
                const { success, data } = yield call(api_getRequirementAttr, payload);
                if (success) {
                    return data
                }
                return false
            },
            *e_submitRequirement({ payload }, { put, call }) {
                const { success, data } = yield call(api_submitRequirement, payload);
                if (success) {
                    return data
                }
                return false
            },
            *e_updateRequirement({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateRequirement, payload);
                if (success) {
                    return data
                }
                return false
            }
        },
    }