import { api_updateSchool, api_getSchoolList, api_editSchoolCourseArrangementTitle } from '@/services/school'

export default
    {
        namespace: 'school',
        state:
        {
        },
        reducers:
        {
            r_updateState(state, { payload }) {
                return { ...state, ...payload }
            }
        },
        effects:
        {
            * e_getSchoolList({ payload }, { put, call }) {
                const { success, data } = yield call(api_getSchoolList, payload);
                if (success) {
                    return data
                }
                return false
            },

            * e_updateSchool({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateSchool, payload);
                if (success) {
                    return data
                }
                return false
            },

            * e_editSchoolCourseArrangementTitle({ payload }, { put, call }) {
                const { success, data } = yield call(api_editSchoolCourseArrangementTitle, payload);
                if (success) {
                    return data
                }
                return false
            },
        },
    }