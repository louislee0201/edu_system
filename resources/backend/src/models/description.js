import { api_updateDescription, api_getDescriptionType, api_getDescriptionList, api_getDescriptionData } from '@/services/description'

export default
    {
        namespace: 'description',
        state:
        {
        },
        reducers:
        {
            r_updateState(state, { payload }) {
                return { ...state, ...payload }
            }
        },
        effects:
        {
            * e_updateDescription({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateDescription, payload);
                if (success) {
                    return data
                }
                return false
            },

            * e_getDescriptionType({ payload }, { put, call }) {
                const { success, data } = yield call(api_getDescriptionType, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_getDescriptionList({ payload }, { put, call }) {
                const { success, data } = yield call(api_getDescriptionList, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_getDescriptionData({ payload }, { put, call }) {
                const { success, data } = yield call(api_getDescriptionData, payload);
                if (success) {
                    return data
                }
                return false
            }
        },
    }