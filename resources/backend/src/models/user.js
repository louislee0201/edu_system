import { message } from 'antd';
import { api_login, api_logout, api_checkLogin } from '@/services/user'
import { removeUser, saveUser, getUserLanguage, getUserRole, getUserToken } from '@/tools/user'
import router from 'umi/router'
import { DEFAULT_SETTING, STORAGE_KEY } from '@/constants/default'
import { setLocale } from 'umi/locale'
export default
    {
        namespace: 'user',
        state:
        {
            language: '',
            role: '',
            statusCheck: false
        },
        reducers:
        {
            r_updateState(state, { payload }) {
                return { ...state, ...payload }
            }
        },
        effects:
        {
            * e_login({ payload }, { put, call }) {
                const { success, data } = yield call(api_login, payload, 'POST');
                if (success) {
                    saveUser(payload.rememberMe, {
                        name: STORAGE_KEY.user,
                        data,
                        saveTime: 30
                    })

                    yield put({
                        type: 'r_updateState',
                        payload: {
                            role: data.role
                        }
                    });

                    router.push('/home')
                }
                return false
            },

            * e_logout({ payload }, { put, call }) {
                yield call(api_logout, {}, 'POST');
                removeUser()
                location.reload()
            },

            * e_checkLogin({ payload }, { put, call }) {
                const { success, data } = yield call(api_checkLogin, {}, 'POST');
                if (success) {
                    return true
                }
                return false
            },

            * e_changeLanguage({ payload }, { put, call }) {
                setLocale(payload, false)
                yield put({
                    type: 'r_updateState',
                    payload: {
                        language: payload
                    }
                });
            }
        },
        subscriptions:
        {
            setup({ dispatch, history }) {
                //giving default language (zh-CN)
                const language = getUserLanguage()
                if (!language) {
                    setLocale(DEFAULT_SETTING.language, false)

                    dispatch({
                        type: 'r_updateState',
                        payload:
                        {
                            language: DEFAULT_SETTING.language
                        }
                    })
                }
                else {
                    dispatch({
                        type: 'r_updateState',
                        payload:
                        {
                            language
                        }
                    })
                }

                //check user token
                const role = getUserRole();
                if (role) {
                    dispatch({
                        type: 'e_checkLogin',
                        payload: {}
                    }).then((result) => {
                        if (result) {
                            dispatch({
                                type: 'r_updateState',
                                payload:
                                {
                                    role,
                                    statusCheck: true
                                }
                            })

                            history.listen(({ pathname }) => {
                                if (pathname == '/login' || pathname == '/') {
                                    router.push('/home')
                                }
                            })
                        }
                        else
                        {
                            dispatch({
                                type: 'r_updateState',
                                payload:
                                {
                                    statusCheck: true
                                }
                            })
                        }
                    })
                }
                else
                {
                    dispatch({
                        type: 'r_updateState',
                        payload:
                        {
                            statusCheck: true
                        }
                    })
                }
            },
        },
    }