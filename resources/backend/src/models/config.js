import {
    api_getCountry, api_updateCountry, api_getCourseLevel, api_updateCourseLevel, api_searchSchool, api_getQualificationList, api_updateQualificationList, api_updateQualificationGrade,
    api_updateQualificationSubject, api_getLanguageList, api_updateLanguage, api_updateProficiency, api_getProficiencyList, api_getProficiencyQualificationSubject, api_addProfiencyGrade,
    api_updateProficiencyGrade, api_getRegionList, api_updateRegion, api_getTag, api_updateMainTagList, api_updateSubTagList, api_editCourseSubTagList
} from '@/services/config'

export default
    {
        namespace: 'config',
        state:
        {
        },
        reducers:
        {
            r_updateState(state, { payload }) {
                return { ...state, ...payload }
            }
        },
        effects:
        {
            *e_editCourseSubTagList({ payload }, { put, call }) {
                const { success, data } = yield call(api_editCourseSubTagList, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_updateSubTagList({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateSubTagList, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_updateMainTagList({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateMainTagList, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_getTag({ payload }, { put, call }) {
                const { success, data } = yield call(api_getTag, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_getCountry({ payload }, { put, call }) {
                const { success, data } = yield call(api_getCountry, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_updateCountry({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateCountry, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_getCourseLevelList({ payload }, { put, call }) {
                const { success, data } = yield call(api_getCourseLevel, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_updateCourseLevel({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateCourseLevel, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_searchSchool({ payload }, { put, call }) {
                const { success, data } = yield call(api_searchSchool, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_getQualificationList({ payload }, { put, call }) {
                const { success, data } = yield call(api_getQualificationList, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_updateQualificationList({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateQualificationList, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_updateQualificationSubject({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateQualificationSubject, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_updateQualificationGrade({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateQualificationGrade, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_getLanguageList({ payload }, { put, call }) {
                const { success, data } = yield call(api_getLanguageList, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_updateLanguage({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateLanguage, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_getProficiencyList({ payload }, { put, call }) {
                const { success, data } = yield call(api_getProficiencyList, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_updateProficiency({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateProficiency, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_getProficiencyQualificationSubject({ payload }, { put, call }) {
                const { success, data } = yield call(api_getProficiencyQualificationSubject, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_addProfiencyGrade({ payload }, { put, call }) {
                const { success, data } = yield call(api_addProfiencyGrade, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_updateProficiencyGrade({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateProficiencyGrade, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_getRegionList({ payload }, { put, call }) {
                const { success, data } = yield call(api_getRegionList, payload);
                if (success) {
                    return data
                }
                return false
            },
            * e_updateRegion({ payload }, { put, call }) {
                const { success, data } = yield call(api_updateRegion, payload);
                if (success) {
                    return data
                }
                return false
            },
        },
    }