import { } from '@/services/qualification'

export default
    {
        namespace: 'qualification',
        state:
        {
        },
        reducers:
        {
            r_updateState(state, { payload }) {
                return { ...state, ...payload }
            }
        },
        effects:
        {
            
        },
    }