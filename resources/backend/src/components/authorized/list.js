import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    OrderedListOutlined,
    ContainerOutlined,
    BookOutlined
} from '@ant-design/icons';

export function list() {
    return {
        school: {
            icon: <UserOutlined />,
            rules: [
                {
                    role: ['admin', 'superb'],
                    routes: [
                        {
                            url: '/school/schoolList',
                            titleName: 'school list'
                        }
                    ]
                }
            ]
        },
        course: {
            icon: <BookOutlined />,
            rules: [
                {
                    role: ['admin', 'superb', 'school'],
                    routes: [
                        {
                            url: '/course/courseList',
                            titleName: 'course list'
                        }
                    ]
                }
            ]
        },
        config: {
            icon: <OrderedListOutlined />,
            rules: [
                {
                    role: ['admin', 'superb'],
                    routes: [
                        {
                            url: '/config/qualificationList',
                            titleName: 'qualification list'
                        },
                        {
                            url: '/config/proficiencyList',
                            titleName: 'proficiency list'
                        },
                        {
                            url: '/config/tag',
                            titleName: 'main tag list'
                        },
                    ]
                }
            ]
        },
    }
}