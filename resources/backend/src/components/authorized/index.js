import React from 'react';
import { getUserRole } from '@/tools/user'
import { list } from '@/components/authorized/list';
import noPermission from './noPermission'

export default function Authorized(info) {
    const role = getUserRole()
    if (info.isLink && info.authority && role) {
        const lists = list()
        const currentLink = window.location.pathname.split("/")
        const titleRoute = currentLink[1]
        const contentRoute = currentLink[2]
        if (lists[titleRoute]) {
            lists[titleRoute].rules.forEach(rule => {
                if (rule.role.includes(role)) {
                    rule.routes.forEach(route => {
                        if (route.url.indexOf(titleRoute + '/' + contentRoute) >= 0) {
                            return true
                        }
                    });
                }
            });
        }
        else if (titleRoute == 'home') {
            return true
        }

        return noPermission
    }
    else if (info.authority && role) {
        if (info.authority.includes(role)) {
            if (info.children) {
                return info.children
            }
            return true
        }
    }
    return false
}