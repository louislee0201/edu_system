import React, { useEffect, useState } from "react";
import {
    Modal,
    Input,
    Button,
    Popconfirm,
    Collapse,
    message,
    InputNumber,
    Radio,
    Tag
} from "antd";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'

export default function GradeModal(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        buttonLoading: false,
        grade_id: 0,
        type: 2,
        grade: '',
        score: '',
        cgpa: '',
    })

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }
    useEffect(() => {
        if (props.modalMode == 'edit') {
            setState({
                ...props.editData
            })
        }
    }, [props.visible]);

    function updateGrade(payload) {
        let submit = true
        props.dataList.forEach(element => {
            if (element.grade.toUpperCase() == state.grade.toUpperCase() && submit && state.grade_id != element.id) {
                message.error(formatMessage({ id: 'repeat grade, please try another grade' }), 1)
                submit = false
                return
            }
        });

        if (submit) {
            setState({
                buttonLoading: true
            })
            dispatch({
                type: props.dispatchType,
                payload: {
                    ...payload,
                    scoreType: state.type,
                    score: state.score,
                    cgpa: state.cgpa,
                    grade: state.grade,
                    grade_id: state.grade_id,
                    qualification_id: props.qualification_id,
                    proficiency_id: props.proficiency_id
                }
            }).then((value) => {
                props.onClose({
                    ...value,
                    visible: false
                })

                setState({
                    score: '',
                    cgpa: '',
                    grade: '',
                    grade_id: 0,
                    buttonLoading: false
                })

                message.success(formatMessage({ id: 'success update' }), 1)
            })
        }
    }

    return (
        <Modal
            title={props.modalMode == 'add' ? formatMessage({ id: 'add new grade' }) : formatMessage({ id: 'setting qualification grade' })}
            visible={props.visible}
            footer={[
                <Button
                    key="submit"
                    loading={state.buttonLoading}
                    type="primary"
                    onClick={() => {
                        updateGrade({
                            action: props.modalMode
                        })
                    }}
                >
                    OK
                </Button>
            ]}
            onCancel={() => {
                props.onClose({
                    visible: false
                })
                setState({
                    score: '',
                    cgpa: '',
                    grade: '',
                    grade_id: 0
                })
            }}
        >
            <Input style={{ marginBottom: '5px' }} placeholder={formatMessage({ id: 'subject_name' })} onChange={e => setState({ grade: e.target.value.toUpperCase() })} value={state.grade} />

            <div style={{ display: 'flex', alignItems: 'center' }}>
                <Radio.Group
                    onChange={(e) => { setState({ type: e.target.value }) }}
                    value={state.type}
                >
                    {
                        Object.keys(props.scoreTypeList).map((key, arrKey) => {
                            return (
                                <Radio disabled={key == 1 ? true : false} key={arrKey} value={parseInt(key)}>{formatMessage({ id: props.scoreTypeList[key] })}</Radio>
                            )
                        })
                    }
                </Radio.Group>
            </div>

            <div style={{ display: 'flex', alignItems: 'center', marginBottom: '5px' }}>
                <div style={{ width: '50px' }}>
                    {
                        formatMessage({ id: 'score' }) + ': '
                    }
                </div>
                <InputNumber
                    value={state.score}
                    onChange={e => {
                        if (e > 0 || e === null) {
                            setState({ score: e === null ? '' : e })
                        }
                        else {
                            if (e <= 0 && typeof e == 'number') {
                                message.error(formatMessage({ id: 'cgpa or score must more than 0' }), 1)
                            }

                        }
                    }}
                />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
                <div style={{ width: '50px' }}>
                    CGPA:
                </div>
                <InputNumber
                    value={state.cgpa}
                    onChange={e => {
                        if (e > 0 || e === null) {
                            setState({ cgpa: e === null ? '' : e })
                        }
                        else {
                            if (e <= 0 && typeof e == 'number') {
                                message.error(formatMessage({ id: 'cgpa or score must more than 0' }), 1)
                            }

                        }
                    }}
                />
            </div>
        </Modal>
    )
}