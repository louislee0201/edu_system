import React, { Component } from 'react';
import { Table, Input, Button, Tag, Pagination, Popconfirm } from 'antd';
import router from 'umi/router'
import Highlighter from 'react-highlight-words';
import { SearchOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';

class DataTable extends Component {
    state = {
        searchText: '',
        searchedColumn: '',
    };

    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={formatMessage({ id: 'search' }) + ' ' + formatMessage({ id: `${dataIndex}` })}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    {
                        formatMessage({ id: 'search' })
                    }
                </Button>
                <Button onClick={() => this.handleReset(clearFilters, dataIndex)} size="small" style={{ width: 90 }}>
                    {
                        formatMessage({ id: 'reset' })
                    }
                </Button>
            </div>
        ),
        filterIcon: filtered =>
            <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select());
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text.toString()}
                />
            ) : (
                    text
                ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm()
    };

    handleReset = (clearFilters, dataIndex) => {
        clearFilters()
        let newFilterArr = this.props.filterArr
        newFilterArr[dataIndex] = ''
        this.props.setState({
            filterArr: newFilterArr
        })
        this.props.requestData();
    };


    onChangePage(clickPage, sizePage) {
        this.props.requestData(clickPage)
    }

    filterAttr(pagination, filters, sorter) {
        Object.keys(filters).map((key) => {
            let newKey = key == this.props.key ? 'id' : key
            let newFilterArr = this.props.filterArr
            newFilterArr[newKey] = typeof filters[key][0] !== 'undefined' ? filters[key] : ''
            this.props.setState({
                filterArr: newFilterArr
            }, () => {
                this.props.requestData()
            })
        })
    }

    render() {
        return (
            <div>
                <Table
                    {...(this.props.rowSelection && { rowSelection: { ...this.props.rowSelection } })}
                    {...(this.props.scroll && { scroll: { ...this.props.scroll } })}
                    onPressEnter={this.filterAttr.bind(this)}
                    onChange={this.filterAttr.bind(this)}
                    columns={
                        Object.values(this.props.columnsList).map((value) => {
                            if (value.search) {
                                return { ...value, ...this.getColumnSearchProps(value.dataIndex), title: formatMessage({ id: value.title }) }
                            }
                            return { ...value, title: formatMessage({ id: value.title }) }
                        })
                    }
                    dataSource={this.props.dataList}
                    pagination={false}
                    loading={this.props.loading}
                />

                <Pagination
                    defaultPageSize={this.props.pagination ? this.props.pagination : 10}
                    style={{ float: 'right', marginTop: '10px' }}
                    current={this.props.currentPage}
                    total={this.props.totalItem}
                    onChange={this.onChangePage.bind(this)}
                />

            </div>
        );

    }
}

export default DataTable