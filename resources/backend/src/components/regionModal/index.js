import React, { useEffect, useState } from "react";
import {
    Modal,
    Input,
    Button,
    Popconfirm,
    Collapse,
    message,
    Descriptions,
    Radio,
    Tag
} from "antd";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import { QuestionCircleOutlined } from '@ant-design/icons';

export default function RegionModal(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        buttonLoading: false,
        mode: '',
        region_id: 0,
        region: ''
    })

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }

    useEffect(() => {
        // dispatch({
        //     type: 'config/e_getCountry',
        //     payload: {}
        // }).then((value) => {
        //     props.setState({
        //         ...value
        //     })
        // })
    }, []);

    function update(payload) {
        dispatch({
            type: 'config/e_updateRegion',
            payload: {
                ...state,
                ...payload,
                country_id: props.country_id
            }
        }).then((value) => {
            props.setState({
                ...value,
            })
            setState({
                mode: '',
                region_id: 0,
                region: ''
            })
            message.success(formatMessage({ id: 'success update' }), 1)
        })
    }
    console.log(props)
    return (
        <Modal
            title={formatMessage({ id: 'setting region' })}
            visible={props.visible}
            onOk={() => props.onClose({ visible: false })}
            onCancel={() => props.onClose({ visible: false })}
            footer={[
                <Button key="back" style={{ backgroundColor: 'pink', color: 'white' }} onClick={() => setState({ mode: 'add', region_id: 0, region: '' })}>{formatMessage({ id: 'add region' })}</Button>,
                <Button key="submit" type="primary" onClick={() => props.onClose({ visible: false })}>
                    OK
                </Button>,
            ]}
        >
            {
                Object.values(props.regionList).map((value, key) => {
                    return (
                        <div key={key} style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <div>
                                {
                                    value.id == state.region_id
                                        ?
                                        <div style={{ display: 'flex', alignItems: 'center' }}>
                                            {
                                                (key + 1) + '. '
                                            }
                                            <Input value={state.region} onChange={(e) => setState({ region: e.target.value })} />
                                        </div>
                                        : (key + 1) + '. ' + value.region
                                }
                            </div>

                            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Button
                                    type="link"
                                    onClick={() => {
                                        if (state.mode == 'edit' && state.region_id == value.id) {
                                            update({ action: 'edit' })
                                        }
                                        else {
                                            setState({ mode: 'edit', region_id: value.id, region: value.region })
                                        }
                                    }}
                                >
                                    {
                                        state.mode == 'edit' && state.region_id == value.id
                                            ? formatMessage({ id: 'save' })
                                            : formatMessage({ id: 'edit' })
                                    }
                                </Button>


                                <Popconfirm
                                    key={'activation'}
                                    title={formatMessage({ id: 'Are you sure?' })}
                                    onConfirm={() => update({ action: 'activation', region_id: value.id })}
                                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                >
                                    <Tag color={'red'}>
                                        {
                                            value.active
                                                ? formatMessage({ id: 'hide' }).toUpperCase()
                                                : formatMessage({ id: 'show' }).toUpperCase()
                                        }
                                    </Tag>
                                </Popconfirm>

                                <Popconfirm
                                    key={'delete'}
                                    title={formatMessage({ id: 'Are you sure?' })}
                                    onConfirm={() => update({ action: 'delete', region_id: value.id })}
                                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                >
                                    <Tag color={'red'}>
                                        {formatMessage({ id: 'delete' }).toUpperCase()}
                                    </Tag>
                                </Popconfirm>
                            </div>
                        </div>
                    )
                })
            }

            {
                state.mode == 'add' &&
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <Input value={state.region} onChange={(e) => setState({ region: e.target.value })} />
                    </div>

                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Button
                            type="link"
                            onClick={() => {
                                update({ action: 'add' })
                            }}
                        >
                            {
                                formatMessage({ id: 'save' })
                            }
                        </Button>

                        <Popconfirm
                            key={'cancel'}
                            title={formatMessage({ id: 'Are you sure?' })}
                            onConfirm={() => setState({ mode: '' })}
                            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                        >
                            <Tag color={'red'}>
                                {formatMessage({ id: 'cancel' }).toUpperCase()}
                            </Tag>
                        </Popconfirm>
                    </div>
                </div>
            }

        </Modal>
    )
}