import React, { useEffect, useState } from "react";
import {
    Modal,
    Input,
    Button,
    Popconfirm,
    Collapse,
    message,
    Descriptions,
    Radio,
    Tag
} from "antd";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import { QuestionCircleOutlined } from '@ant-design/icons';

export default function LanguageModal(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        buttonLoading: false,
        mode: '',
        language_id: 0,
        language: ''
    })

    useEffect(() => {
        if (props.languageList.length <= 0) {
            dispatch({
                type: 'config/e_getLanguageList',
                payload: {}
            }).then((result) => {
                props.setState({
                    ...result
                })
            })
        }
    }, []);

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }

    function updateLanguage(payload) {
        dispatch({
            type: 'config/e_updateLanguage',
            payload: {
                ...state,
                ...payload
            }
        }).then((value) => {
            props.setState({
                ...value,
            })
            setState({
                mode: '',
                language_id: 0,
                language: ''
            })
            message.success(formatMessage({ id: 'success update' }), 1)
        })
    }

    return (
        <Modal
            title={formatMessage({ id: 'setting language' })}
            visible={props.visible}
            onOk={() => props.onClose(false)}
            onCancel={() => props.onClose(false)}
            footer={[
                <Button key="back" style={{ backgroundColor: 'pink', color: 'white' }} onClick={() => setState({ mode: 'add', language_id: 0, language: '' })}>{formatMessage({ id: 'add language' })}</Button>,
                <Button key="submit" type="primary" onClick={() => props.onClose(false)}>
                    OK
                </Button>,
            ]}
        >
            {
                Object.values(props.languageList).map((value, key) => {
                    return (
                        <div key={key} style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <div>
                                {
                                    value.id == state.language_id
                                        ?
                                        <div style={{ display: 'flex', alignItems: 'center' }}>
                                            {
                                                (key + 1) + '. '
                                            }
                                            <Input value={state.language} onChange={(e) => setState({ language: e.target.value })} />
                                        </div>
                                        : (key + 1) + '. ' + value.language
                                }
                            </div>

                            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Button
                                    type="link"
                                    onClick={() => {
                                        if (state.mode == 'edit' && state.language_id == value.id) {
                                            updateLanguage({ action: 'edit' })
                                        }
                                        else {
                                            setState({ mode: 'edit', language_id: value.id, language: value.language })
                                        }
                                    }}
                                >
                                    {
                                        state.mode == 'edit' && state.language_id == value.id
                                            ? formatMessage({ id: 'save' })
                                            : formatMessage({ id: 'edit' })
                                    }
                                </Button>


                                <Popconfirm
                                    key={'activation'}
                                    title={formatMessage({ id: 'Are you sure?' })}
                                    onConfirm={() => updateLanguage({ action: 'activation', language_id: value.id })}
                                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                >
                                    <Tag color={'red'}>
                                        {
                                            value.active
                                                ? formatMessage({ id: 'hide' }).toUpperCase()
                                                : formatMessage({ id: 'show' }).toUpperCase()
                                        }
                                    </Tag>
                                </Popconfirm>

                                <Popconfirm
                                    key={'delete'}
                                    title={formatMessage({ id: 'Are you sure?' })}
                                    onConfirm={() => updateLanguage({ action: 'delete', language_id: value.id })}
                                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                >
                                    <Tag color={'red'}>
                                        {formatMessage({ id: 'delete' }).toUpperCase()}
                                    </Tag>
                                </Popconfirm>
                            </div>
                        </div>
                    )
                })
            }

            {
                state.mode == 'add' &&
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <Input value={state.language} onChange={(e) => setState({ language: e.target.value })} />
                    </div>

                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Button
                            type="link"
                            onClick={() => {
                                updateLanguage({ action: 'add' })
                            }}
                        >
                            {
                                formatMessage({ id: 'save' })
                            }
                        </Button>

                        <Popconfirm
                            key={'cancel'}
                            title={formatMessage({ id: 'Are you sure?' })}
                            onConfirm={() => setState({ mode: '' })}
                            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                        >
                            <Tag color={'red'}>
                                {formatMessage({ id: 'cancel' }).toUpperCase()}
                            </Tag>
                        </Popconfirm>
                    </div>
                </div>
            }

        </Modal>
    )
}