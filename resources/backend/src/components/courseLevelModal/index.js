import React, { useEffect, useState } from "react";
import {
    Modal,
    Input,
    Button,
    Popconfirm,
    Collapse,
    message,
    Descriptions,
    Radio,
    Tag
} from "antd";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import { QuestionCircleOutlined } from '@ant-design/icons';

export default function CourseLevelModal(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        buttonLoading: false,
        mode: '',
        level_id: 0,
        level: ''
    })

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }

    useEffect(() => {
        dispatch({
            type: 'config/e_getCourseLevelList',
            payload: {}
        }).then((value) => {
            props.setState({
                ...value
            })
        })
    }, []);

    function updateCourseLevel(payload) {
        dispatch({
            type: 'config/e_updateCourseLevel',
            payload: {
                ...state,
                ...payload
            }
        }).then((value) => {
            props.setState({
                ...value,
            })
            setState({
                mode: '',
                level_id: 0,
                level: ''
            })
            message.success(formatMessage({ id: 'success update' }), 1)
        })
    }

    return (
        <Modal
            title={formatMessage({ id: 'setting level' })}
            visible={props.visible}
            onOk={() => props.setState({ visible: false })}
            onCancel={() => props.setState({ visible: false })}
            footer={[
                <Button key="back" style={{ backgroundColor: 'pink', color: 'white' }} onClick={() => setState({ mode: 'add', level_id: 0, level: '' })}>{formatMessage({ id: 'add level' })}</Button>,
                <Button key="submit" type="primary" onClick={() => props.setState({ visible: false })}>
                    OK
                </Button>,
            ]}
        >
            {
                Object.values(props.courseLevelList).map((value, key) => {
                    return (
                        <div key={key} style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <div>
                                {
                                    value.id == state.level_id
                                        ?
                                        <div style={{ display: 'flex', alignItems: 'center' }}>
                                            {
                                                (key + 1) + '. '
                                            }
                                            <Input value={state.level} onChange={(e) => setState({ level: e.target.value })} />
                                        </div>
                                        : (key + 1) + '. ' + value.level_name
                                }
                            </div>

                            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Button
                                    type="link"
                                    onClick={() => {
                                        if (state.mode == 'edit' && state.level_id == value.id) {
                                            updateCourseLevel({ action: 'edit' })
                                        }
                                        else {
                                            setState({ mode: 'edit', level_id: value.id, level: value.level_name })
                                        }
                                    }}
                                >
                                    {
                                        state.mode == 'edit' && state.level_id == value.id
                                            ? formatMessage({ id: 'save' })
                                            : formatMessage({ id: 'edit' })
                                    }
                                </Button>


                                <Popconfirm
                                    key={'activation'}
                                    title={formatMessage({ id: 'Are you sure?' })}
                                    onConfirm={() => updateCourseLevel({ action: 'activation', level_id: value.id })}
                                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                >
                                    <Tag color={'red'}>
                                        {
                                            value.active
                                                ? formatMessage({ id: 'hide' }).toUpperCase()
                                                : formatMessage({ id: 'show' }).toUpperCase()
                                        }
                                    </Tag>
                                </Popconfirm>

                                <Popconfirm
                                    key={'delete'}
                                    title={formatMessage({ id: 'Are you sure?' })}
                                    onConfirm={() => updateCourseLevel({ action: 'delete', level_id: value.id })}
                                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                >
                                    <Tag color={'red'}>
                                        {formatMessage({ id: 'delete' }).toUpperCase()}
                                    </Tag>
                                </Popconfirm>
                            </div>
                        </div>
                    )
                })
            }

            {
                state.mode == 'add' &&
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <Input value={state.level} onChange={(e) => setState({ level: e.target.value })} />
                    </div>

                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Button
                            type="link"
                            onClick={() => {
                                updateCourseLevel({ action: 'add' })
                            }}
                        >
                            {
                                formatMessage({ id: 'save' })
                            }
                        </Button>

                        <Popconfirm
                            key={'cancel'}
                            title={formatMessage({ id: 'Are you sure?' })}
                            onConfirm={() => setState({ mode: '' })}
                            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                        >
                            <Tag color={'red'}>
                                {formatMessage({ id: 'cancel' }).toUpperCase()}
                            </Tag>
                        </Popconfirm>
                    </div>
                </div>
            }

        </Modal>
    )
}