import React, { useEffect, useState } from "react";
import {
    Modal,
    Input,
    Button,
    Popconfirm,
    Collapse,
    Spin,
    Descriptions,
    Radio,
    Tabs
} from "antd";
import { formatMessage } from "umi/locale";

export default function DataView(props) {
    const [state, setOriState] = useState({
        editMode: false,
        buttonLoading: false,
    })

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }

    function editMode(key, loadingIconkey) {
        if (state.editMode) {
            setState({
                buttonLoading: true
            })
            props.updateData(() => {
                setState({
                    buttonLoading: false,
                    editMode: false
                })
            })
        }
        else {
            setState({
                editMode: true
            })
        }
    }

    return (
        <React.Fragment>
            <Descriptions column={1} bordered>
                {
                    Object.values(props.title).map((data, key) => {
                        return (
                            <Descriptions.Item
                                key={key}
                                label={formatMessage({ id: data.label })}
                            >
                                {
                                    state.editMode
                                        ? data.edit_element
                                            ? data.edit_element
                                            :
                                            <Input
                                                disabled={data.disabled}
                                                value={props[data.key]}
                                                onChange={(e) => props.setState({ [data.key]: e.target.value })}
                                            />
                                        :
                                        <div className="data_label">
                                            {
                                                data.default_element
                                                    ? data.default_element
                                                    : props[data.key]
                                            }
                                        </div>

                                }
                            </Descriptions.Item>
                        )
                    })
                }
            </Descriptions>
            <div style={{ textAlign: 'center', marginTop: '5px' }}>
                <Button
                    loading={state.buttonLoading}
                    onClick={() => editMode()}
                    type="primary"
                >
                    {formatMessage({ id: state.editMode ? "save" : "edit" })}
                </Button>
            </div>
        </React.Fragment>
    )
}