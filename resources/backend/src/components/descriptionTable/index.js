import React, { useEffect, useState } from "react";
import {
    Menu,
    Button,
    Popconfirm,
    Collapse,
    Card,
    InputNumber,
    Dropdown,
    Spin
} from "antd";
import { formatMessage } from "umi/locale"
import { useDispatch } from 'react-redux'
import DataTable from '@/components/dataTable'
import DescriptionModal from './descriptionModal/index2'
import { DownOutlined, SettingOutlined, DeleteOutlined, QuestionCircleOutlined, OrderedListOutlined } from '@ant-design/icons'
import { router } from "dva";

export default function Description(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        visibleModal: false,
        loading: false,
        editData: {},
        descriptionList: [],

        setSortKey: '',
        sortKey: '',
        mode: '',
        rangeButton: -1
    })

    function setState(values, callback = () => { }) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))

        callback({ ...state, ...values })
    }

    useEffect(() => {
        if (props.related_id > 0) {
            setState({ loading: true })
            dispatch({
                type: "description/e_getDescriptionType",
                payload: {}
            }).then(result => {
                if (result) {
                    setState({ ...result })
                    dispatch({
                        type: "description/e_getDescriptionList",
                        payload: {
                            classMode: props.classMode,
                            related_id: props.related_id
                        }
                    }).then(result2 => {
                        if (result2) {
                            setState({ ...result, ...result2, loading: false })
                        }
                    })
                }
            })
        }

    }, [props.related_id])

    function renderCollapseData(lists) {
        let element = []
        let noCollapse = []
        Object.values(lists).map((list, listKey) => {
            Object.keys(list).map((description_type, description_key) => {
                const description_data = list[description_type]
                let content = []
                if (state.descriptionTypeList[description_type] == 'only picture') {
                    let imageList = []
                    Object.values(description_data.data.img).map((pic, picKey) => {
                        let imageUrl
                        if (pic.indexOf('base64') >= 0) {
                            imageUrl = pic
                        } else {
                            imageUrl = window.location.protocol + '//' + window.location.hostname +
                                (window.location.port > 0 ? ':8000' : '') +
                                '/auth/image/description/' + pic
                        }

                        imageList.push(
                            <img src={imageUrl} alt="avatar" key={picKey} style={{ width: '100px', height: '100px', marginRight: '5px', marginBottom: '5px' }} />
                        )
                    })

                    let text = []
                    if (description_data.data.content) {
                        let datas = description_data.data.content.split('\n')
                        for (let i = 0; i < datas.length; i++) {
                            if (datas[i] != '') {
                                text.push(
                                    <div>{datas[i]}</div>
                                )
                            } else {
                                text.push(
                                    <div style={{ height: '10px' }} />
                                )
                            }
                        }

                    }
                    content.push([
                        imageList,
                        text
                    ])
                } else if (state.descriptionTypeList[description_type] == 'only text') {
                    let datas = description_data.data.split('\n')
                    for (let i = 0; i < datas.length; i++) {
                        if (datas[i] != '') {
                            content.push(
                                <div>{datas[i]}</div>
                            )
                        } else {
                            content.push(
                                <div style={{ height: '10px' }} />
                            )
                        }

                    }
                } else if (state.descriptionTypeList[description_type] == 'picture and text') {
                    const textList = description_data.data.content
                    Object.values(description_data.data.img).map((pic, picKey) => {
                        let image
                        if (pic.indexOf('base64') >= 0) {
                            image = pic
                        } else {
                            image = window.location.protocol + '//' + window.location.hostname +
                                (window.location.port > 0 ? ':8000' : '') +
                                '/auth/image/description/' + pic
                        }

                        content.push(
                            <div style={{ marginRight: '5px', marginBottom: '5px', display: 'flex' }}>
                                <img src={image} alt="avatar" key={picKey} style={{ width: '335px', height: '235px', marginRight: '5px' }} />
                                <div style={{ display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                    {textList[picKey]}
                                </div>
                            </div>
                        )
                    })

                } else if (state.descriptionTypeList[description_type] == 'pdf') {
                    let card = []
                    Object.values(description_data.data).map((ob, obKey) => {
                        let time = ob.img.split(".")[0]
                        let imageUrl = ''
                        if (Number.isInteger(parseInt(time))) {
                            imageUrl = window.location.protocol + '//' + window.location.hostname +
                                (window.location.port > 0 ? ':8000' : '') +
                                '/auth/image/description/' + ob.img
                        } else {
                            imageUrl = ob.img
                        }
                        card.push(
                            <Card
                                onClick={() => {
                                    window.open(
                                        window.location.protocol + '//' + window.location.hostname +
                                        (window.location.port > 0 ? ':8000' : '') +
                                        '/auth/pdf/description/' + ob.pdf,
                                        '_blank'
                                    )
                                }}
                                hoverable
                                key={obKey}
                                style={{ marginLeft: '10px', marginTop: '10px' }}
                                cover={< img alt="example" src={imageUrl} />}
                            >
                                <Card.Meta title={ob.content} />
                            </Card >
                        )
                    })

                    content.push(
                        <div style={{ flexWrap: 'wrap', paddingLeft: '0', display: 'flex' }}>
                            {card}
                        </div>
                    )

                } else if (state.descriptionTypeList[description_type] == 'video') {
                    let videos = []
                    Object.values(description_data.data).map((video, videoKey) => {
                        videos.push(
                            <div style={{ marginLeft: '10px', marginTop: '10px', flex: '0 0 32%' }} key={videoKey}>
                                <iframe
                                    width="250"
                                    height="250"
                                    src={video}
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen
                                />
                            </div>
                        )
                    })

                    content.push(
                        <div style={{ flexWrap: 'wrap', paddingLeft: '0', display: 'flex' }}>
                            {videos}
                        </div>
                    )

                } else if (state.descriptionTypeList[description_type] == 'new component' || state.descriptionTypeList[description_type] == 'optional') {
                    content = renderCollapseData(description_data.data)
                }
                if (description_data.title == null) {
                    noCollapse.push(content)
                } else {
                    element.push(
                        <Collapse.Panel
                            header={description_data.title}
                            key={listKey}
                        >
                            {content}
                        </Collapse.Panel>
                    )
                }
            })
        })

        return [noCollapse, element.length > 0 && <Collapse>{element}</Collapse>]
    }


    function renderCollapse(description) {
        const list = JSON.parse(description.content)
        return renderCollapseData(list)
    }

    function loadDropDown(description) {
        let content = ''
        if (state.descriptionList.length > 1) {//include itself so will become 2 value
            if (state.rangeButton != description.range_key) {
                content = <OrderedListOutlined
                    onClick={(event) => {
                        event.stopPropagation()
                        setState({ rangeButton: description.range_key })
                    }}
                />
            } else {
                content = <div style={{ display: 'inline-block' }}>
                    <Dropdown
                        onClick={(event) => {
                            event.stopPropagation()
                            event.preventDefault()
                        }}
                        trigger={['click']}
                        overlay={renderMuenu()}
                    >
                        <Button>
                            {formatMessage({ id: 'select the key to be replaced' })} <DownOutlined />
                        </Button>
                    </Dropdown>
                </div>
            }
        }

        function renderMuenu() {
            return (
                <Menu
                    onClick={({ domEvent, key }) => {
                        domEvent.stopPropagation()
                        setState({ loading: true })
                        dispatch({
                            type: "description/e_updateDescription",
                            payload: {
                                description_id: description.id,
                                change_key: key,
                                action: 'edit',
                                related_id: props.related_id,
                                classMode: props.classMode
                            }
                        }).then(result => {
                            if (result) {
                                setState({
                                    ...result,
                                    loading: false,
                                    rangeButton: -1
                                })
                            }
                        })
                    }}
                >
                    {
                        Object.values(state.descriptionList).map((value) => {
                            if (value.range_key != description.range_key) {
                                return (
                                    <Menu.Item key={value.range_key}>{value.range_key}</Menu.Item>
                                )
                            }
                        })
                    }

                </Menu>
            )
        }
        return content

    }
    function settingModal(description) {
        const rangeButton = loadDropDown(description)

        return [
            <div style={{ color: 'red', marginRight: '5px', fontWeight: 'bold', display: 'inline-block' }}>
                {formatMessage({ id: 'sort' })}: {description.range_key}
            </div>,
            rangeButton,
            <SettingOutlined
                style={{ width: '50px' }}
                onClick={event => {
                    event.stopPropagation()
                    let lists = JSON.parse(description.content)
                    let temMainData = []
                    let temData = []
                    renderEditData(lists, -1)
                    function renderEditData(lists, parent) {
                        let recordChild = []
                        Object.values(lists).map((list) => {
                            Object.keys(list).map((description_type) => {
                                const data = list[description_type]
                                if (state.descriptionTypeList[description_type] != 'new component' && state.descriptionTypeList[description_type] != 'optional') {
                                    let pAt = data.data
                                    if (state.descriptionTypeList[description_type] == 'picture and text') {
                                        pAt = []
                                        data.data.img.forEach((pic, picKey) => {
                                            pAt.push({
                                                img: pic,
                                                content: data.data.content[picKey]
                                            })
                                        })
                                    }
                                    temData.push(pAt)
                                    temMainData.push({
                                        description_type: parseInt(description_type),
                                        title: data.title,
                                        parent,
                                        edit: false,
                                        radio: false,
                                        done: true,
                                        data: temData.length - 1
                                    })
                                    recordChild.push(temMainData.length - 1)
                                } else {
                                    temMainData.push({
                                        description_type: parseInt(description_type),
                                        title: data.title,
                                        parent,
                                        edit: false,
                                        radio: false,
                                        done: true,
                                        data: []
                                    })
                                    recordChild.push(temMainData.length - 1)
                                    let currentKey = temMainData.length - 1
                                    let child = renderEditData(data.data, temMainData.length - 1)
                                    temMainData[currentKey].data = child
                                }
                            })
                            
                        })
                        return recordChild
                    }
                    setState({
                        editData: {
                            mainTitle: description.title,
                            temMainData,
                            temData,
                            description_id: description.id
                        },
                        mode: 'edit',
                        visibleModal: true
                    })
                }}
            />,
            <Popconfirm
                key={'delete'}
                title={formatMessage({ id: 'Are you sure?' })}
                onConfirm={(event) => {
                    event.stopPropagation()
                    setState({ loading: true })
                    dispatch({
                        type: "description/e_updateDescription",
                        payload: {
                            description_id: description.id,
                            action: 'delete',
                            related_id: props.related_id,
                            classMode: props.classMode
                        }
                    }).then(result => {
                        if (result) {
                            setState({ ...result, loading: false })
                        }
                    })
                }}
                icon={< QuestionCircleOutlined style={{ color: 'red' }} />}
            >
                <DeleteOutlined
                    onClick={event => {
                        // If you don't want click extra trigger collapse, you can prevent this:
                        event.stopPropagation()
                    }}
                />
            </Popconfirm >
        ]
    }

    return (
        <div>
            <Button
                style={{ marginRight: '5px', marginBottom: '10px' }}
                type="primary"
                onClick={() => {
                    setState({
                        visibleModal: true,
                        mode: 'add'
                    })
                }}
            >
                {
                    formatMessage({ id: 'add new description' })
                }
            </Button>
            <Spin spinning={state.loading} />
            <Collapse
            //onChange={(e) => getDescriptionData(e, 'info')}
            >
                {
                    Object.values(state.descriptionList).map((description) => {
                        return (
                            <Collapse.Panel
                                extra={settingModal(description)}
                                header={description.title} key={description.id}
                            >
                                {renderCollapse(description)}
                            </Collapse.Panel>
                        )
                    })
                }
            </Collapse>
            <DescriptionModal
                {...({
                    ...state,
                    classMode: props.classMode,
                    visible: state.visibleModal,
                    related_id: props.related_id,
                    setState,

                })}
                onClose={(result) => {
                    setState({
                        visibleModal: result.visible
                    })
                }}
            />
        </div>
    )
}