import React, { useEffect, useState } from "react";
import {
    Input,
    Button,
    Popconfirm,
    Upload,
    Card,
    Divider,
    Radio,
    Modal,
    Collapse,
    Dropdown,
    Menu,
    Spin,
    Icon
} from "antd";
import { formatMessage } from "umi/locale"
import { useDispatch } from 'react-redux'
import { PlusOutlined, SettingOutlined, DeleteOutlined, QuestionCircleOutlined, OrderedListOutlined, DownOutlined, CloseOutlined } from '@ant-design/icons';
import { getBase64 } from '@/tools'

export default function DescriptionModal(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        pictureModalVisible: false,
        pictureContent: '',
        mainTitle: '',
        previewImage: '',
        previewVisible: false,
        previewTitle: '',

        temMainData: [],
        temData: [],
        loading: false,
        allowRenderRadio: true,

        picTemDataKey: 0,
        temDataKey: 0,
        rangeButton: -1,
        buttonLoading: false,
        description_id: 0,
        videoLink: '',
        pdfModalVisible: false,
        pdfEditKey: -1
    })

    useEffect(() => {
        setState({ ...props.editData })
    }, [props.editData]);

    useEffect(() => {
        setState({ loading: true })
        dispatch({
            type: "description/e_getDescriptionType",
            payload: {}
        }).then(result => {
            if (result) {
                setState({ ...result, loading: false })
            }
        });
    }, []);

    function setState(values, callback = () => { }) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))

        callback({ ...state, ...values })
    }

    function handlePreview(file) {
        setState({
            previewImage: file.url || (props.descriptionTypeList[state.descriptionType] == 'picture and text' ? state.pictureAndText[file.binaryUrlKey].img : state.binaryUrlList[file.binaryUrlKey]),
            previewVisible: true,
            previewTitle: file.name || file.url.substring(file.url.lastIndexOf('/') + 1),
        });
    };

    const uploadButton = (
        <div>
            <PlusOutlined />
            <div style={{ marginTop: 8 }}>{formatMessage({ id: 'upload image' })}</div>
        </div>
    )

    function checkPictureList(dataContent) {
        let pictureList = []
        if (state.temData[dataContent.data]) {
            let imageList = props.descriptionTypeList[dataContent.description_type] == 'picture and text' ? state.temData[dataContent.data] : state.temData[dataContent.data].img
            imageList = imageList ? imageList : []
            imageList.forEach((pic, picKey) => {
                let imageUrl = ''
                if (props.descriptionTypeList[dataContent.description_type] == 'picture and text') {
                    let time = pic.img.split(".")[0]
                    if (Number.isInteger(parseInt(time))) {
                        imageUrl = window.location.protocol + '//' + window.location.hostname +
                            (window.location.port > 0 ? ':8000' : '') +
                            '/auth/image/description/' + pic.img
                    } else {
                        imageUrl = pic.img
                    }
                    pictureList.push({
                        uid: '-' + picKey + 1,
                        name: 'image',
                        status: 'done',
                        url: imageUrl,
                        content: pic.content
                    })
                } else {
                    let time = pic.split(".")[0]

                    if (Number.isInteger(parseInt(time))) {
                        imageUrl = window.location.protocol + '//' + window.location.hostname +
                            (window.location.port > 0 ? ':8000' : '') +
                            '/auth/image/description/' + pic
                    } else {
                        imageUrl = pic
                    }
                    pictureList.push({
                        uid: '-' + picKey + 1,
                        name: 'image',
                        status: 'done',
                        url: imageUrl
                    })
                }
            })
        }
        return pictureList
    }

    function renderUploadPic(dataContent, mainKey) {
        let pictureList = checkPictureList(dataContent)
        return (
            <div style={{ marginTop: '10px', padding: '0 10px' }}>
                <Input
                    onChange={(e) => {
                        let temMainData = state.temMainData
                        temMainData[mainKey].title = e.target.value
                        setState({ temMainData })
                    }}
                    value={dataContent.title}
                    placeholder={formatMessage({ id: 'content title' })}
                    style={{ marginBottom: '10px' }}
                />

                <Upload
                    accept={'.jpeg,.png,.jpg'}
                    listType="picture-card"
                    fileList={pictureList}
                    onPreview={handlePreview}
                    onChange={(e) => {
                        let fileList = e.fileList
                        if (props.descriptionTypeList[dataContent.description_type] == 'picture and text') {
                            if (fileList.length > pictureList.length) {
                                getBase64(fileList[fileList.length - 1].originFileObj, callbackSaveBinaryUrl)

                                function callbackSaveBinaryUrl(result) {
                                    let temData = state.temData
                                    let temMainData = state.temMainData
                                    let temDataKey = 0
                                    let picTemDataKey = 0

                                    if (dataContent.data == -1) {
                                        temData.push([
                                            {
                                                img: result,
                                                content: ''
                                            }
                                        ])
                                        temMainData[mainKey].data = temData.length - 1
                                        temDataKey = temData.length - 1
                                    } else {
                                        temData[dataContent.data].push({
                                            img: result,
                                            content: ''
                                        })
                                        temDataKey = dataContent.data
                                        picTemDataKey = temData[dataContent.data].length - 1
                                    }

                                    setState({
                                        temData,
                                        temMainData,
                                        pictureModalVisible: true,
                                        picTemDataKey,
                                        temDataKey
                                    })
                                }
                            } else if (fileList.length < pictureList.length) {
                                let newPictureList = []
                                fileList.forEach((file, key) => {
                                    newPictureList.push({
                                        img: file.url,
                                        content: file.content
                                    })
                                })
                                let temData = state.temData
                                temData[dataContent.data] = newPictureList
                                setState({ temData })
                            }
                        } else {
                            let newPictureList = []
                            fileList.forEach((file, key) => {
                                if (file.originFileObj) {
                                    getBase64(file.originFileObj, callbackSaveBinaryUrl)
                                    function callbackSaveBinaryUrl(result) {
                                        newPictureList.push(result)
                                        if (key + 1 == fileList.length) {
                                            let temData = state.temData
                                            let temMainData = state.temMainData
                                            if (dataContent.data == -1) {
                                                temData.push({
                                                    img: newPictureList,
                                                    content: ''
                                                })
                                                temMainData[mainKey].data = temData.length - 1
                                            } else {
                                                temData[dataContent.data].img = newPictureList
                                            }

                                            setState({ temData, temMainData })
                                        }
                                    }
                                } else {
                                    newPictureList.push(file.url)
                                    if (key + 1 == fileList.length) {
                                        let temData = state.temData
                                        let temMainData = state.temMainData
                                        if (dataContent.data == -1) {
                                            temData.push({
                                                img: newPictureList,
                                                content: ''
                                            })
                                            temMainData[mainKey].data = temData.length - 1
                                        } else {
                                            temData[dataContent.data].img = newPictureList
                                        }
                                        setState({ temData, temMainData })
                                    }
                                }
                            })

                            if (fileList.length == 0) {
                                let temData = state.temData
                                temData[dataContent.data] = []
                                setState({ temData })
                            }
                        }

                    }}
                >
                    {uploadButton}
                </Upload>

                {
                    props.descriptionTypeList[dataContent.description_type] == 'only picture' &&
                    <Input.TextArea
                        onChange={(e) => {
                            let temData = state.temData
                            let temMainData = state.temMainData
                            if (dataContent.data == -1) {
                                temData.push({
                                    img: [],
                                    content: e.target.value
                                })
                                temMainData[mainKey].data = temData.length - 1
                            } else {
                                temData[dataContent.data].content = e.target.value
                            }
                            setState({ temData, temMainData })
                        }}
                        value={state.temData[dataContent.data] ? state.temData[dataContent.data].content : ''}
                        placeholder={formatMessage({ id: 'content title' })}
                        style={{ marginBottom: '10px' }}
                    />
                }

                { renderSubmidCancelButton(dataContent, mainKey)}
            </div >
        )
    }

    function renderTextArea(dataContent, mainKey) {
        return (
            <div style={{ marginTop: '10px', padding: '0 10px' }}>
                <Input
                    onChange={(e) => {
                        let temMainData = state.temMainData
                        temMainData[mainKey].title = e.target.value
                        setState({ temMainData })
                    }}
                    value={dataContent.title}
                    placeholder={formatMessage({ id: 'content title' })}
                    style={{ marginBottom: '10px' }
                    }
                />

                <Input.TextArea
                    value={state.temData[dataContent.data]}
                    onChange={(e) => {
                        let temData = state.temData
                        let temMainData = state.temMainData
                        if (dataContent.data == -1) {
                            temData.push(e.target.value)
                            temMainData[mainKey].data = temData.length - 1
                        } else {
                            temData[dataContent.data] = e.target.value
                        }
                        setState({ temData, temMainData })
                    }}
                    style={{ marginTop: '10px' }}
                />

                {renderSubmidCancelButton(dataContent, mainKey)}
            </div>
        )
    }

    function renderSubmidCancelButton(dataContent, mainKey) {
        return (
            <div>
                <Button
                    key="done"
                    type="primary"
                    style={{ backgroundColor: 'green' }}
                    onClick={() => {
                        let temMainData = state.temMainData
                        let allowRenderRadio = state.allowRenderRadio
                        temMainData[mainKey].edit = false
                        temMainData[mainKey].radio = false
                        if (temMainData[mainKey].parent > -1) {
                            temMainData[temMainData[mainKey].parent].edit = true
                            temMainData[temMainData[mainKey].parent].radio = true
                        } else {
                            temMainData[mainKey].done = true
                            allowRenderRadio = true
                        }
                        setState({ temMainData, allowRenderRadio })
                    }}
                >
                    {formatMessage({ id: 'done' })}
                </Button>
                <Button
                    key="cancel"
                    type="danger"
                    style={{ marginTop: '10px', marginLeft: '5px' }}
                    onClick={() => {
                        let temMainData = state.temMainData
                        let allowRenderRadio = true
                        if (temMainData[mainKey].parent > -1) {
                            temMainData[temMainData[mainKey].parent].data.splice(temMainData[temMainData[mainKey].parent].data.indexOf(mainKey), 1)
                            temMainData[temMainData[mainKey].parent].edit = true
                            temMainData[temMainData[mainKey].parent].radio = true
                            if (temMainData[temMainData[mainKey].parent].parent == -1) {
                                temMainData[temMainData[mainKey].parent].done = false
                            }
                        }
                        temMainData.splice(mainKey, 1)
                        temMainData.forEach(element => {
                            if (element.parent == -1 && !element.done) {
                                allowRenderRadio = false
                            }
                        })
                        setState({
                            temMainData,
                            allowRenderRadio
                        })
                    }}
                >
                    {formatMessage({ id: 'cancel' })}
                </Button>
            </div >
        )
    }

    function renderSubmidtedData(value) {
        let content = []
        if (props.descriptionTypeList[value.description_type] == 'only picture') {
            let imageList = []
            Object.values(value.data.img).map((pic, picKey) => {
                let time = pic.split(".")[0]
                let imageUrl = ''
                if (Number.isInteger(parseInt(time))) {
                    imageUrl = window.location.protocol + '//' + window.location.hostname +
                        (window.location.port > 0 ? ':8000' : '') +
                        '/auth/image/description/' + pic
                } else {
                    imageUrl = pic
                }
                imageList.push(
                    <img src={imageUrl} alt="avatar" key={picKey} style={{ width: '100px', height: '100px', marginRight: '5px', marginBottom: '5px' }} />
                )
            })


            let text = []
            if (value.data.content) {
                let datas = value.data.content.split('\n')
                for (let i = 0; i < datas.length; i++) {
                    if (datas[i] != '') {
                        text.push(
                            <div>{datas[i]}</div>
                        )
                    } else {
                        text.push(
                            <div style={{ height: '10px' }} />
                        )
                    }
                }
            }

            content.push([
                imageList,
                text
            ])
        } else if (props.descriptionTypeList[value.description_type] == 'only text') {
            if (value.data) {
                let datas = value.data.split('\n')
                for (let i = 0; i < datas.length; i++) {
                    if (datas[i] != '') {
                        content.push(
                            <div>{datas[i]}</div>
                        )
                    } else {
                        content.push(
                            <div style={{ height: '10px' }} />
                        )
                    }
                }
            }

        } else if (props.descriptionTypeList[value.description_type] == 'picture and text') {
            Object.values(value.data).map((pic, picKey) => {
                let time = pic.img.split(".")[0]
                let imageUrl = ''
                if (Number.isInteger(parseInt(time))) {
                    imageUrl = window.location.protocol + '//' + window.location.hostname +
                        (window.location.port > 0 ? ':8000' : '') +
                        '/auth/image/description/' + pic.img
                } else {
                    imageUrl = pic.img
                }
                content.push(
                    <div style={{ marginRight: '5px', marginBottom: '5px', maxHeight: '130px', display: 'flex' }}>
                        <img src={imageUrl} alt="avatar" key={picKey} style={{ width: '100px', height: '100px', marginRight: '5px' }} />
                        <div style={{ display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                            {pic.content}
                        </div>
                    </div>
                )
            })
        } else if (props.descriptionTypeList[value.description_type] == 'video') {
            let videos = []
            Object.values(value.data).map((video, videoKey) => {
                videos.push(
                    <div style={{ marginLeft: '10px', marginTop: '10px', flex: '0 0 32%' }} key={videoKey}>
                        <iframe
                            width="250"
                            height="250"
                            src={video}
                            frameBorder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen
                        />
                    </div>
                )
            })
            content.push(
                <div style={{ flexWrap: 'wrap', paddingLeft: '0', display: 'flex' }}>
                    {videos}
                </div>
            )
        } else if (props.descriptionTypeList[value.description_type] == 'pdf') {
            /* let card = []
            Object.values(value.data).map((ob, obKey) => {
                let time = ob.img.split(".")[0]
                let imageUrl = ''
                if (Number.isInteger(parseInt(time))) {
                    imageUrl = window.location.protocol + '//' + window.location.hostname +
                        (window.location.port > 0 ? ':8000' : '') +
                        '/auth/image/description/' + ob.img
                } else {
                    imageUrl = ob.img
                }
                card.push(
                    <Card
                        //onClick={() => console.log(123)}
                        hoverable
                        key={obKey}
                        style={{ marginLeft: '10px', marginTop: '10px' }}
                        cover={<img alt="example" src={imageUrl} />}
                    >
                        <Card.Meta title={ob.content} />
                    </Card>
                )
            })

            content.push(
                <div style={{ flexWrap: 'wrap', paddingLeft: '0', display: 'flex' }}>
                    {card}
                </div>
            ) */

            let card = []
            Object.values(value.data).map((ob, obKey) => {
                let time = ob.img.split(".")[0]
                let imageUrl = ''
                if (Number.isInteger(parseInt(time))) {
                    imageUrl = /* window.location.protocol + '//' + window.location.hostname +
                                (window.location.port > 0 ? ':8000' : '') + */
                        'http://edudotcom.my/auth/image/description/' + ob.img
                } else {
                    imageUrl = ob.img
                }
                card.push(
                    <Card
                        onClick={(e) => {
                            e.preventDefault()
                            window.open(
                                window.location.protocol + '//' + window.location.hostname +
                                (window.location.port > 0 ? ':8000' : '') +
                                '/auth/pdf/description/' + ob.pdf,
                                '_blank'
                            )
                        }}
                        key={obKey}
                        style={{
                            marginRight: '8%',
                            marginLeft: '8%',
                            marginTop: '10px',
                            width: '250px',
                            display: 'inline-block'
                        }}
                        cover={< img alt="example" src={imageUrl} style={{ width: 'auto' }
                        } />}
                    >
                        <Card.Meta title={ob.content} />
                    </Card >
                )
            })

            content.push(
                card
            )

        } else if (
            props.descriptionTypeList[value.description_type] == 'new component' ||
            props.descriptionTypeList[value.description_type] == 'optional'
        ) {
            content.push(renderOption(value.data, 'done'))
        }
        return content
    }

    function loadDropDown(mainKey) {
        let content = ''
        let broKey = []
        const temMainData = state.temMainData
        if (temMainData[mainKey].parent > -1) {
            broKey = temMainData[temMainData[mainKey].parent].data
        } else {
            temMainData.forEach((value, key) => {
                if (value.parent == -1) {
                    broKey.push(key)
                }
            })
        }
        if (broKey.length > 1) {//include itself so will become 2 value
            if (state.rangeButton != mainKey) {
                content = <OrderedListOutlined
                    onClick={(event) => {
                        event.stopPropagation()
                        setState({ rangeButton: mainKey })
                    }}
                />
            } else {
                content = <div style={{ display: 'inline-block' }}>
                    <Dropdown
                        onClick={(event) => {
                            event.stopPropagation()
                            event.preventDefault()
                        }}
                        trigger={['click']}
                        overlay={renderMuenu()}
                    >
                        <Button>
                            {formatMessage({ id: 'select the key to be replaced' })} <DownOutlined />
                        </Button>
                    </Dropdown>
                </div>
            }
        }

        function renderMuenu() {
            return (
                <Menu
                    onClick={({ domEvent, key }) => {
                        domEvent.stopPropagation()
                        // let temMainData = state.temMainData
                        // let oldData = temMainData[state.rangeButton]
                        // console.log(oldData)
                        // if (
                        //     props.descriptionTypeList[oldData.description_type] == 'new component' ||
                        //     props.descriptionTypeList[oldData.description_type] == 'optional'
                        // ) {
                        //     oldData.data.forEach(element => {
                        //         temMainData[element].parent = key //changing parent key of child
                        //     })
                        // }

                        // if (props.descriptionTypeList[temMainData[key].description_type] == 'new component' ||
                        //     props.descriptionTypeList[temMainData[key].description_type] == 'optional'
                        // ) {
                        //     temMainData[key].data.forEach(element => {
                        //         temMainData[element].parent = state.rangeButton
                        //     })
                        // }

                        // temMainData[state.rangeButton] = temMainData[key]
                        // temMainData[key] = oldData

                        // setState({ rangeButton: -1, temMainData })
                        let temMainData = state.temMainData
                        let oldData = temMainData[state.rangeButton]
                        if (oldData.parent > -1) {
                            let parent = temMainData[temMainData[state.rangeButton].parent]
                            let childArr = []
                            parent.data.forEach((childKey, pstKey) => {
                                if (childKey == key) {
                                    if (pstKey > parent.data.indexOf(state.rangeButton)) {
                                        childArr.push(parseInt(key))
                                        childArr.push(state.rangeButton)
                                    } else {
                                        childArr.push(state.rangeButton)
                                        childArr.push(parseInt(key))
                                    }

                                } else if (childKey != state.rangeButton) {
                                    childArr.push(childKey)
                                }
                            })
                            temMainData[temMainData[state.rangeButton].parent].data = childArr
                        } else if (oldData.parent == -1) {
                            let recordFather = {}
                            let broList = []
                            temMainData.forEach((ob, obKey) => {
                                if (ob.parent == -1) {
                                    if (obKey == key) {
                                        if (obKey > state.rangeButton) {
                                            if (props.descriptionTypeList[temMainData[key].description_type] == 'new component' || props.descriptionTypeList[temMainData[key].description_type] == 'optional') {
                                                recordFather[parseInt(key)] = broList.length
                                            }
                                            broList.push(temMainData[key])

                                            if (props.descriptionTypeList[temMainData[state.rangeButton].description_type] == 'new component' || props.descriptionTypeList[temMainData[state.rangeButton].description_type] == 'optional') {
                                                recordFather[parseInt(state.rangeButton)] = broList.length
                                            }
                                            broList.push(temMainData[state.rangeButton])
                                        } else {
                                            if (props.descriptionTypeList[temMainData[state.rangeButton].description_type] == 'new component' || props.descriptionTypeList[temMainData[state.rangeButton].description_type] == 'optional') {
                                                recordFather[parseInt(state.rangeButton)] = broList.length
                                            }
                                            broList.push(temMainData[state.rangeButton])

                                            if (props.descriptionTypeList[temMainData[key].description_type] == 'new component' || props.descriptionTypeList[temMainData[key].description_type] == 'optional') {
                                                recordFather[parseInt(key)] = broList.length
                                            }
                                            broList.push(temMainData[key])
                                        }

                                    } else if (obKey != state.rangeButton) {
                                        if (props.descriptionTypeList[ob.description_type] == 'new component' || props.descriptionTypeList[ob.description_type] == 'optional') {
                                            recordFather[obKey] = broList.length
                                        }
                                        broList.push(ob)
                                    }
                                } else {
                                    console.log(recordFather)
                                    if (typeof recordFather[ob.parent] !== 'undefined') {
                                        broList[recordFather[ob.parent]].data[broList[recordFather[ob.parent]].data.indexOf(obKey)] = broList.length
                                        ob.parent = recordFather[ob.parent]
                                    }
                                    broList.push(ob)
                                }
                            })
                            temMainData = broList
                        }
                        console.log(temMainData)
                        /* temMainData.forEach((ob, obKey) => {
                            if (props.descriptionTypeList[ob.description_type] == 'new component' || props.descriptionTypeList[ob.description_type] == 'optional') {
                                ob.data.forEach(child => {
                                    temMainData[child].parent = obKey
                                })
                            }
                        }) */
                        setState({ rangeButton: -1, temMainData })
                    }}
                >
                    {
                        Object.values(broKey).map((value) => {
                            if (value != mainKey) {
                                return (
                                    <Menu.Item key={value}>{value}</Menu.Item>
                                )
                            }
                        })
                    }

                </Menu>
            )
        }
        return content

    }
    function settingModal(mainKey) {
        const rangeButton = loadDropDown(mainKey)

        return [
            <div style={{ color: 'red', marginRight: '5px', fontWeight: 'bold', display: 'inline-block' }}>
                {formatMessage({ id: 'sort' })}: {mainKey}
            </div>,
            rangeButton,
            <SettingOutlined
                style={{ width: '50px' }}
                onClick={event => {
                    event.stopPropagation()
                    let temMainData = state.temMainData
                    temMainData[mainKey].edit = true
                    temMainData[mainKey].done = false
                    if (props.descriptionTypeList[temMainData[mainKey].description_type] == 'new component' || props.descriptionTypeList[temMainData[mainKey].description_type] == 'optional') {
                        temMainData[mainKey].radio = true
                    }

                    if (temMainData[mainKey].parent > -1) {
                        temMainData[temMainData[mainKey].parent].radio = false
                    }
                    setState({ temMainData, allowRenderRadio: false })
                }}
            />,
            <Popconfirm
                key={'delete'}
                title={formatMessage({ id: 'Are you sure?' })}
                onConfirm={(event) => {
                    event.stopPropagation()
                    let temMainData = state.temMainData
                    let temData = state.temData
                    if (temMainData[mainKey].parent > -1) {
                        temMainData[temMainData[mainKey].parent].data.splice(temMainData[temMainData[mainKey].parent].data.indexOf(mainKey), 1)
                    }
                    //temData[temMainData[mainKey].data] = null
                    temMainData[mainKey] = {}
                    setState({ temMainData, temData })
                }}
                icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
            >
                <DeleteOutlined
                    onClick={event => {
                        // If you don't want click extra trigger collapse, you can prevent this:
                        event.stopPropagation()
                    }}
                />
            </Popconfirm>
        ]
    }

    function renderVideo(dataContent, mainKey) {
        return (
            <div style={{ marginTop: '10px', padding: '0 10px' }}>
                <Input
                    onChange={(e) => {
                        let temMainData = state.temMainData
                        temMainData[mainKey].title = e.target.value
                        setState({ temMainData })
                    }}
                    value={dataContent.title}
                    placeholder={formatMessage({ id: 'content title' })}
                    style={{ marginBottom: '10px' }
                    }
                />

                <div style={{ display: 'flex' }}>
                    <Input
                        value={state.videoLink}
                        onChange={(e) => {
                            setState({ videoLink: e.target.value })
                        }}
                        style={{ width: '400px' }}
                    />

                    <Button
                        key="submit"
                        type="primary"
                        disabled={state.videoLink.indexOf('http')}
                        onClick={() => {
                            let temData = state.temData
                            let temMainData = state.temMainData
                            if (dataContent.data == -1) {
                                temData.push([state.videoLink])
                                temMainData[mainKey].data = temData.length - 1
                            } else {
                                temData[dataContent.data].push(state.videoLink)
                            }
                            setState({
                                temData,
                                temMainData,
                                videoLink: ''
                            })
                        }}
                    >
                        OK
                    </Button>
                </div>

                <div style={{ flexWrap: 'wrap', paddingLeft: '0', display: 'flex' }}>
                    {
                        state.temData[dataContent.data] &&
                        Object.values(state.temData[dataContent.data]).map((video, videoKey) => {
                            return (
                                <div
                                    style={{
                                        marginLeft: '10px',
                                        marginTop: '10px',
                                        flex: '0 0 32%',
                                        position: 'relative'
                                    }}
                                    key={videoKey}
                                >
                                    <div
                                        onClick={() => {
                                            let temData = state.temData
                                            temData[dataContent.data].splice(videoKey, 1)
                                            setState({ temData })
                                        }}
                                        style={{
                                            position: 'absolute',
                                            backgroundColor: '#eae6e6e6',
                                            borderRadius: '50%',
                                            width: '20px',
                                            textAlign: 'center',
                                            right: '-3%',
                                            top: 0,
                                            marginTop: '-10px'
                                        }}
                                    >
                                        <CloseOutlined />
                                    </div>
                                    <iframe
                                        width="100%"
                                        height="100%"
                                        src={video}
                                        frameBorder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowFullScreen
                                    />
                                </div>
                            )
                        })
                    }
                </div>

                { renderSubmidCancelButton(dataContent, mainKey)}
            </div >
        )
    }

    function checkPDFList(dataContent, mode) {
        let list = []
        if (state.temData[dataContent.data]) {
            if (mode == 'pdf') {
                state.temData[dataContent.data].forEach((ob, obKey) => {
                    let pdfTime = ob.pdf.split(".")[0]
                    let pdfUrl = ''
                    if (Number.isInteger(parseInt(pdfTime))) {
                        pdfUrl = window.location.protocol + '//' + window.location.hostname +
                            (window.location.port > 0 ? ':8000' : '') +
                            '/auth/image/description/' + ob.pdf
                    } else {
                        pdfUrl = ob.pdf
                    }

                    list.push({
                        uid: obKey,
                        name: ob.content,
                        status: 'done',
                        url: pdfUrl,
                        content: ob.content
                    })
                })
            } else {
                let validKey = state.pdfEditKey > -1 ? state.pdfEditKey : state.temData[dataContent.data].length - 1
                if (validKey >= 0) {
                    let img = state.temData[dataContent.data][validKey].img
                    if (img == '') {
                        return list
                    }
                    let pic = ''
                    let time = img.split(".")[0]
                    if (Number.isInteger(parseInt(time))) {
                        pic = window.location.protocol + '//' + window.location.hostname +
                            (window.location.port > 0 ? ':8000' : '') +
                            '/auth/image/description/' + img
                    } else {
                        pic = img
                    }
                    list.push({
                        uid: 0,
                        name: state.temData[dataContent.data][validKey].content,
                        status: 'done',
                        content: state.temData[dataContent.data][validKey].content,
                        url: pic
                    })
                }
            }

        }
        return list
    }

    function renderPDF(dataContent, mainKey) {
        let imageList = checkPDFList(dataContent, 'img')
        let validKey = state.pdfEditKey > -1
            ? state.pdfEditKey
            : state.temData[dataContent.data]
                ? state.temData[dataContent.data].length - 1
                : -1
        return (
            <div style={{ marginTop: '10px', padding: '0 10px' }}>
                <Input
                    onChange={(e) => {
                        let temMainData = state.temMainData
                        temMainData[mainKey].title = e.target.value
                        setState({ temMainData })
                    }}
                    value={dataContent.title}
                    placeholder={formatMessage({ id: 'content title' })}
                    style={{ marginBottom: '10px' }
                    }
                />

                <Upload.Dragger
                    onPreview={(e) => {
                        setState({ pdfModalVisible: true, pdfEditKey: e.uid })
                    }}
                    onChange={(ob) => {
                        let temData = state.temData
                        if (ob.file.status == 'removed') {
                            temData[dataContent.data].splice(ob.file.uid, 1)
                            setState({ temData })
                        } else {
                            getBase64(ob.file.originFileObj, (result) => {
                                let temMainData = state.temMainData
                                if (dataContent.data == -1) {
                                    temData.push([{
                                        pdf: result,
                                        content: '',
                                        img: ''
                                    }])
                                    temMainData[mainKey].data = temData.length - 1
                                } else {
                                    temData[dataContent.data].push({
                                        pdf: result,
                                        content: '',
                                        img: ''
                                    })
                                }
                                setState({ temData, temMainData, pdfModalVisible: true })
                            })
                        }

                    }}
                    accept=".pdf"
                    fileList={checkPDFList(dataContent, 'pdf')}
                >
                    <p className="ant-upload-drag-icon">
                        <Icon type="inbox" />
                    </p>
                    <p className="ant-upload-text">{formatMessage({ id: 'Click or drag file to this area to upload' })}</p>
                </Upload.Dragger>

                <Modal
                    title={formatMessage({ id: 'please provide picture content' })}
                    visible={state.pdfModalVisible}
                    onCancel={() => {
                        let temData = state.temData
                        if (temData[dataContent.data]) {
                            temData[dataContent.data].splice(validKey, 1)
                            setState({
                                pdfModalVisible: false,
                                temData,
                                pdfEditKey: -1
                            })
                        }

                    }}
                    footer={[
                        <Button
                            key="submit"
                            type="primary"
                            disabled={
                                state.temData[dataContent.data] && validKey > -1
                                    ? state.temData[dataContent.data][validKey].content == '' || state.temData[dataContent.data][validKey].img == ''
                                    : false
                            }
                            onClick={() => {
                                setState({
                                    pdfModalVisible: false,
                                    pdfEditKey: -1
                                })
                            }}
                        >
                            OK
                        </Button>,
                    ]}
                >
                    <Input
                        onChange={(e) => {
                            let temData = state.temData
                            if (temData[dataContent.data]) {
                                temData[dataContent.data][validKey].content = e.target.value
                                setState({ temData })
                            }
                        }}
                        value={
                            state.temData[dataContent.data] && validKey > -1
                                ? state.temData[dataContent.data][validKey].content
                                : ''
                        }
                        placeholder={formatMessage({ id: 'title' })}
                        style={{ marginBottom: '10px' }}
                    />

                    <Upload
                        accept={'.jpeg,.png,.jpg'}
                        listType="picture-card"
                        fileList={
                            checkPDFList(dataContent, 'img')
                        }
                        onPreview={handlePreview}
                        onChange={(e) => {
                            let fileList = e.fileList
                            let temData = state.temData
                            if (temData[dataContent.data]) {
                                if (fileList.length == 0) {
                                    temData[dataContent.data][validKey].img = ''
                                    setState({ temData })
                                } else {
                                    getBase64(fileList[fileList.length - 1].originFileObj, (result) => {
                                        temData[dataContent.data][validKey].img = result
                                        setState({ temData })
                                    })
                                }
                            }
                        }}
                    >
                        {imageList.length == 0 && uploadButton}
                    </Upload>

                    {
                        state.pdfEditKey > -1 &&
                        <Upload.Dragger
                            showUploadList={false}
                            onChange={(ob) => {
                                let temData = state.temData
                                getBase64(ob.file.originFileObj, (result) => {
                                    temData[dataContent.data][validKey].pdf = result
                                    setState({ temData })
                                })
                            }}
                            accept=".pdf"
                        >
                            <p className="ant-upload-drag-icon">
                                <Icon type="inbox" />
                            </p>
                            <p className="ant-upload-text">
                                {
                                    formatMessage({ id: 'Click or drag file to this area to upload' }) +
                                    ' ( ' + formatMessage({ id: 'for replace PDF file only' }) + ' )'

                                }
                            </p>
                        </Upload.Dragger>
                    }


                </Modal>

                { renderSubmidCancelButton(dataContent, mainKey)}
            </div >
        )
    }
    function renderOption(loopData = state.temMainData, mode = '') {
        let completeContent = []
        let editContent = []
        Object.values(loopData).map((value, key) => {
            if (value.parent == -1 || mode == 'done') {
                if (
                    value.done || mode == 'done'
                ) {
                    let title = ''
                    if (value.title) {
                        title = value.title
                    } else if (Number.isInteger(value)) {
                        if (state.temMainData[value].title) {
                            title = state.temMainData[value].title
                        } else {
                            title = formatMessage({ id: props.descriptionTypeList[state.temMainData[value].description_type] })
                        }
                    } else {
                        title = formatMessage({ id: props.descriptionTypeList[value.description_type] })
                    }
                    completeContent.push(
                        <Collapse.Panel
                            {...(
                                value.done
                                    ? { extra: settingModal(key) }
                                    : Number.isInteger(value) && state.temMainData[state.temMainData[value].parent].edit
                                        ? { extra: settingModal(value) }
                                        : {}
                            )}
                            key={key}
                            header={title}
                        >
                            {
                                props.descriptionTypeList[value.description_type] == 'new component' || props.descriptionTypeList[value.description_type] == 'optional'
                                    ? renderSubmidtedData(value)
                                    : Number.isInteger(value)
                                        ? renderSubmidtedData({
                                            ...state.temMainData[value],
                                            data: Array.isArray(state.temMainData[value].data) ? state.temMainData[value].data : state.temData[state.temMainData[value].data]

                                        })
                                        : renderSubmidtedData({
                                            ...value,
                                            data: state.temData[value.data]
                                        })
                            }
                        </Collapse.Panel>
                    )
                } else if (value.edit) {
                    if (
                        props.descriptionTypeList[value.description_type] == 'only picture' ||
                        props.descriptionTypeList[value.description_type] == 'picture and text'
                    ) {
                        editContent.push(renderUploadPic(value, key))
                    } else if (props.descriptionTypeList[value.description_type] == 'only text') {
                        editContent.push(renderTextArea(value, key))
                    } else if (props.descriptionTypeList[value.description_type] == 'video') {
                        editContent.push(renderVideo(value, key))
                    } else if (props.descriptionTypeList[value.description_type] == 'pdf') {
                        editContent.push(renderPDF(value, key))
                    } else if (
                        props.descriptionTypeList[value.description_type] == 'new component' ||
                        props.descriptionTypeList[value.description_type] == 'optional'
                    ) {
                        editContent.push(renderRadio(value, key))
                    }
                } else if (
                    props.descriptionTypeList[value.description_type] == 'new component' ||
                    props.descriptionTypeList[value.description_type] == 'optional'
                ) {
                    editContent.push([
                        props.descriptionTypeList[value.description_type] == 'new component' &&
                        <div style={{ marginTop: '10px', padding: '0 10px' }}>
                            <Input
                                onChange={(e) => {
                                    let temMainData = state.temMainData
                                    temMainData[key].title = e.target.value
                                    setState({ temMainData })
                                }}
                                value={state.temMainData[key].title}
                                placeholder={formatMessage({ id: 'component title' })}
                            />
                        </div>,
                        renderOption(value.data)
                    ])
                }
            } else if (Number.isInteger(value)) {
                if (state.temMainData[value].done) {
                    let title = formatMessage({ id: props.descriptionTypeList[state.temMainData[value].description_type] })
                    if (state.temMainData[value].title) {
                        title = state.temMainData[value].title
                    }
                    editContent.push(
                        <Collapse>
                            <Collapse.Panel
                                {...(state.temMainData[state.temMainData[value].parent].edit
                                    ? {
                                        extra: settingModal(value)
                                    }
                                    : {}
                                )}
                                key={key}
                                header={title}
                            >
                                {
                                    props.descriptionTypeList[state.temMainData[value].description_type] == 'new component' || props.descriptionTypeList[state.temMainData[value].description_type] == 'optional'
                                        ? renderSubmidtedData(state.temMainData[value])
                                        : renderSubmidtedData({
                                            ...state.temMainData[value],
                                            data: Array.isArray(state.temMainData[value].data) ? state.temMainData[value].data : state.temData[state.temMainData[value].data]

                                        })
                                }
                            </Collapse.Panel>
                        </Collapse>
                    )
                } else if (state.temMainData[value].edit || state.temMainData[value].data.length > 0) {
                    if (
                        props.descriptionTypeList[state.temMainData[value].description_type] == 'only picture' ||
                        props.descriptionTypeList[state.temMainData[value].description_type] == 'picture and text'
                    ) {
                        editContent.push(renderUploadPic(state.temMainData[value], value))
                    } else if (props.descriptionTypeList[state.temMainData[value].description_type] == 'only text') {
                        editContent.push(renderTextArea(state.temMainData[value], value))
                    } else if (
                        props.descriptionTypeList[state.temMainData[value].description_type] == 'new component' ||
                        props.descriptionTypeList[state.temMainData[value].description_type] == 'optional'
                    ) {
                        editContent.push([
                            renderRadio(state.temMainData[value], value)
                        ])
                    }
                } else {
                    let title = formatMessage({ id: props.descriptionTypeList[state.temMainData[value].description_type] })
                    if (state.temMainData[value].title) {
                        title = state.temMainData[value].title
                    }
                    editContent.push(
                        <Collapse style={{ marginTop: '10px' }}>
                            <Collapse.Panel
                                extra={settingModal(value)}
                                key={value}
                                header={title}
                            >
                                {
                                    renderSubmidtedData({
                                        ...state.temMainData[value],
                                        data: state.temData[state.temMainData[value].data]
                                    })
                                }
                            </Collapse.Panel>
                        </Collapse>
                    )
                }

            }
        })

        return [
            completeContent.length > 0 && <Collapse
                style={{ marginTop: '10px' }}
            >
                {completeContent}
            </Collapse>,
            editContent
        ]
    }

    function renderRadio(dataContent, mainKey) {
        let status = ''
        if (Array.isArray(dataContent.data)) {
            status = 'done'
            Object.values(dataContent.data).map((value) => {
                if (state.temMainData[value].data < 0 || state.temMainData[value].edit || Array.isArray(state.temMainData[value].data)) {
                    status = ''
                }
            })
        }

        return (
            <div
                style={{
                    padding: '0 15px', marginTop: '10px'
                }}
            >
                {
                    props.descriptionTypeList[dataContent.description_type] == 'new component' &&
                    <div style={{ textAlign: 'center' }}>
                        <Input
                            onChange={(e) => {
                                let temMainData = state.temMainData
                                temMainData[mainKey].title = e.target.value
                                setState({ temMainData })
                            }}
                            value={dataContent.title}
                            placeholder={formatMessage({ id: 'component title' })}
                            style={{ marginBottom: '10px' }}
                        />
                    </div>
                }

                {
                    Array.isArray(dataContent.data) &&
                    renderOption(
                        dataContent.data,
                        status
                    )
                }

                {
                    dataContent.radio && <React.Fragment>
                        <Divider
                            orientation="left"
                            style={{ margin: 'unset' }}
                        >
                            {formatMessage({ id: 'please select type' })}
                        </Divider>
                        <Radio.Group
                            onChange={(e) => {
                                let tem = {
                                    description_type: e.target.value,
                                    title: '',
                                    data: -1,
                                    edit: true,
                                    parent: mainKey,
                                    radio: props.descriptionTypeList[e.target.value] == 'new component' || props.descriptionTypeList[e.target.value] == 'optional' ? true : false
                                }
                                let temMainData = state.temMainData
                                temMainData.push(tem)
                                temMainData[mainKey].edit = false
                                temMainData[mainKey].radio = false
                                if (temMainData[mainKey].data == -1) {
                                    temMainData[mainKey].data = [temMainData.length - 1]
                                } else {
                                    temMainData[mainKey].data.push(temMainData.length - 1)
                                }
                                setState({ temMainData })
                            }}
                            style={{ display: 'flex', flexWrap: 'wrap', paddingLeft: '0' }}
                        >
                            {
                                Object.keys(props.descriptionTypeList).map((key, arrKey) => {
                                    if (props.descriptionTypeList[dataContent.description_type] == 'optional') {
                                        if (dataContent.description_type <= parseInt(key)) {
                                            return
                                        }
                                    }
                                    return (
                                        <Radio
                                            style={{ flex: '0 0 40%' }}
                                            key={arrKey}
                                            value={parseInt(key)}
                                        >
                                            {formatMessage({ id: props.descriptionTypeList[key] })}
                                        </Radio>
                                    )
                                })
                            }
                        </Radio.Group>

                        {
                            dataContent.data.length > 0 &&
                            <Button
                                key="done"
                                type="primary"
                                style={{ backgroundColor: 'green' }}
                                onClick={() => {
                                    let temMainData = state.temMainData
                                    let allowRenderRadio = state.allowRenderRadio
                                    temMainData[mainKey].edit = false
                                    temMainData[mainKey].radio = false
                                    temMainData[mainKey].done = true
                                    if (temMainData[mainKey].parent > -1) {
                                        temMainData[temMainData[mainKey].parent].edit = true
                                        temMainData[temMainData[mainKey].parent].radio = true
                                    } else {
                                        temMainData[mainKey].done = true
                                        allowRenderRadio = true
                                    }
                                    setState({ temMainData, allowRenderRadio })
                                }}
                            >
                                {formatMessage({ id: 'done' })}
                            </Button>
                        }

                        <Button
                            key="cancel"
                            type="danger"
                            style={{ marginTop: '10px' }}
                            onClick={() => {
                                let temMainData = state.temMainData
                                let allowRenderRadio = true
                                if (dataContent.parent > -1) {
                                    temMainData[dataContent.parent].data.splice(temMainData[dataContent.parent].data.indexOf(mainKey), 1)
                                    temMainData[dataContent.parent].edit = true
                                    temMainData[dataContent.parent].radio = true
                                }
                                temMainData.splice(mainKey, 1)
                                temMainData.forEach(element => {
                                    if (element.parent == -1 && !element.done) {
                                        allowRenderRadio = false
                                    }
                                })
                                setState({
                                    allowRenderRadio,
                                    temMainData
                                })
                            }}
                        >
                            {formatMessage({ id: 'cancel' })}
                        </Button>
                    </React.Fragment>
                }

            </div >
        )
    }

    function checkInvalidData() {
        const temMainData = state.temMainData
        let disabled = false
        temMainData.forEach(element => {
            if (element.parent == -1 && !element.done) {
                disabled = true
            }
        })
        if (temMainData.length <= 0 || state.mainTitle == '') {
            disabled = true
        }
        return disabled
    }

    return (
        <Modal
            width={'70%'}
            title={props.mode == 'add' ? formatMessage({ id: 'add new description' }) : formatMessage({ id: 'setting description' })}
            visible={props.visible}
            maskClosable={false}
            onCancel={() => {
                setState({
                    rangeButton: -1,
                    pictureModalVisible: false,
                    pictureContent: '',
                    mainTitle: '',
                    previewImage: '',
                    previewVisible: false,
                    previewTitle: '',

                    temMainData: [],
                    temData: [],
                    loading: false,
                    allowRenderRadio: true
                })
                props.onClose({ visible: false })
            }}
            footer={[
                <Button
                    loading={state.buttonLoading}
                    key="submit"
                    type="primary"
                    disabled={checkInvalidData()}
                    onClick={() => {
                        setState({
                            //buttonLoading: true
                        })
                        let unData = []
                        Object.values(state.temMainData).map((data) => {
                            if (data.parent == -1) {
                                unData.push(data)
                            }
                        })
                        let finalData = loopCheck(unData)

                        function loopCheck(data) {
                            let submitedData = []
                            data.forEach(value => {
                                if (props.descriptionTypeList[value.description_type] == 'picture and text') {
                                    let pic = []
                                    let text = []
                                    state.temData[value.data].forEach(content => {
                                        pic.push(content.img)
                                        text.push(content.content)
                                    })
                                    submitedData.push({
                                        [value.description_type]: {
                                            title: value.title,
                                            data: {
                                                img: pic,
                                                content: text
                                            }
                                        }
                                    })
                                } else if (props.descriptionTypeList[value.description_type] != 'new component' && props.descriptionTypeList[value.description_type] != 'optional') {
                                    let contentData = state.temData[value.data]
                                    if (props.descriptionTypeList[value.description_type] == 'only picture') {
                                        contentData = {
                                            img: Object.values(state.temData[value.data].img).map((pic) => {
                                                if (pic.indexOf('base64') >= 0) {
                                                    return pic
                                                } else {
                                                    let image = pic.split('/')
                                                    return image[image.length - 1]
                                                }
                                            }),
                                            content: state.temData[value.data].content
                                        }
                                    } else if (props.descriptionTypeList[value.description_type] == 'only text') {
                                        contentData = ''
                                        if (state.temData[value.data]) {
                                            let datas = state.temData[value.data].split('\n')
                                            for (let i = 0; i < datas.length; i++) {
                                                if (datas[i] != '') {
                                                    if (i + 1 == datas.length) {
                                                        contentData += datas[i]
                                                    }
                                                    else {
                                                        contentData += datas[i] + "\\n"
                                                    }

                                                } else {
                                                    if (i + 1 != datas.length) {
                                                        contentData += "\\n"
                                                    }
                                                }
                                            }
                                        }

                                    }

                                    submitedData.push({
                                        [value.description_type]: {
                                            title: value.title,
                                            data: contentData
                                        }
                                    })
                                } else {
                                    submitedData.push({
                                        [value.description_type]: {
                                            title: value.title,
                                            data: loopCheck(
                                                Object.values(value.data).map((childKey) => {
                                                    return state.temMainData[childKey]
                                                })
                                            )
                                        }
                                    })
                                }
                            })
                            return submitedData
                        }

                        dispatch({
                            type: "description/e_updateDescription",
                            payload: {
                                descriptionList: finalData,
                                action: props.mode,
                                title: state.mainTitle,
                                description_id: state.description_id,
                                related_id: props.related_id,
                                classMode: props.classMode,
                            }
                        }).then(result => {
                            if (result) {
                                setState({
                                    rangeButton: -1,
                                    pictureModalVisible: false,
                                    pictureContent: '',
                                    mainTitle: '',
                                    previewImage: '',
                                    previewVisible: false,
                                    previewTitle: '',

                                    temMainData: [],
                                    temData: [],
                                    loading: false,
                                    allowRenderRadio: true
                                })
                                props.setState({ ...result })
                                props.onClose({ visible: false })
                            }
                            setState({
                                buttonLoading: false
                            })
                        })
                    }}
                >
                    OK
                </Button >,
            ]}
        >
            <Modal
                title={formatMessage({ id: 'please provide picture content' })}
                visible={state.pictureModalVisible}
                onCancel={() => {
                    let temData = state.temData
                    temData[state.temDataKey].splice(state.picTemDataKey, 1)
                    setState({
                        pictureModalVisible: false,
                        temData,
                        picTemDataKey: 0,
                        temDataKey: 0
                    })
                }}
                footer={[
                    <Button
                        key="submit"
                        type="primary"
                        disabled={state.pictureContent == ''}
                        onClick={() => {
                            let temData = state.temData
                            temData[state.temDataKey][state.picTemDataKey].content = state.pictureContent
                            setState({
                                pictureModalVisible: false,
                                temData,
                                picTemDataKey: 0,
                                temDataKey: 0,
                                pictureContent: ''
                            })
                        }}
                    >
                        OK
                    </Button>,
                ]}
            >
                <Input value={state.pictureContent} onChange={e => setState({ pictureContent: e.target.value })} />
            </Modal>

            <Input
                onChange={(e) => setState({ mainTitle: e.target.value })}
                value={state.mainTitle}
                placeholder={formatMessage({ id: 'main title' })}
            />

            <Modal
                visible={state.previewVisible}
                title={state.previewTitle}
                footer={null}
                onCancel={() => setState({ previewVisible: false })}
            >
                <img alt="example" style={{ width: '100%' }} src={state.previewImage} />
            </Modal>
            <Spin spinning={state.loading} />

            {
                props.descriptionTypeList &&
                <React.Fragment>
                    {
                        state.temMainData.length > 0 && Object.values(renderOption()).map((value) => {
                            return value
                        })
                    }

                    {
                        state.allowRenderRadio &&
                        <React.Fragment>
                            <Divider
                                orientation="left"
                                style={{ margin: 'unset' }}
                            >
                                {formatMessage({ id: 'please select type' })}
                            </Divider>
                            <Radio.Group
                                onChange={(e) => {
                                    let tem = {
                                        description_type: e.target.value,
                                        title: '',
                                        data: -1,
                                        parent: -1,
                                        edit: true,
                                        done: false,
                                        radio: props.descriptionTypeList[e.target.value] == 'new component' || props.descriptionTypeList[e.target.value] == 'optional' ? true : false
                                    }
                                    let temMainData = state.temMainData
                                    temMainData.push(tem)
                                    setState({ temMainData, allowRenderRadio: false })
                                }}
                                style={{ display: 'flex', flexWrap: 'wrap', paddingLeft: '0' }}
                            >
                                {
                                    Object.keys(props.descriptionTypeList).map((key, arrKey) => {
                                        return (
                                            <Radio
                                                style={{ flex: '0 0 40%' }}
                                                key={arrKey}
                                                value={parseInt(key)}
                                            >
                                                {formatMessage({ id: props.descriptionTypeList[key] })}
                                            </Radio>
                                        )
                                    })
                                }
                            </Radio.Group>
                        </React.Fragment>
                    }

                </React.Fragment>
            }
        </Modal >
    )
}