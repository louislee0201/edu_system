import React, { useEffect, useState } from "react";
import {
    Input,
    Button,
    Popconfirm,
    Upload,
    message,
    Divider,
    Radio,
    Modal,
    Collapse,
    InputNumber,
    Menu,
    Dropdown
} from "antd";
import { formatMessage } from "umi/locale"
import { useDispatch } from 'react-redux'
import { PlusOutlined, SettingOutlined, DeleteOutlined, QuestionCircleOutlined, OrderedListOutlined, DownOutlined } from '@ant-design/icons';
import { getBase64 } from '@/tools'

export default function DescriptionModal(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        buttonLoading: false,
        visibleModal: false,
        loading: false,

        descriptionType: 0,
        pictureList: [],
        previewImage: '',
        previewVisible: false,
        previewTitle: '',

        mainTitle: '',

        binaryUrlList: [],
        textArea: '',

        pictureAndText: [],
        pictureModalVisible: false,
        pictureContent: '',
        submitedData: {},

        descriptionTypeSub: 0,
        pictureListSub: [],
        mainTitleSub: '',
        binaryUrlListSub: [],
        textAreaSub: '',
        pictureAndTextSub: [],
        submitedDataSub: {},

        setSortKey: '',
        setSortKeySub: '',
        sortKey: '',
        sortKeySub: '',

        settingInfoKey: 0
    })

    useEffect(() => {
        setState({ ...props.editData })
    }, [props.editData]);

    useEffect(() => {
        dispatch({
            type: "description/e_getDescriptionType",
            payload: {}
        }).then(result => {
            if (result) {
                setState({ ...result })
            }
        });
    }, []);

    function setState(values, callback = () => { }) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))

        callback({ ...state, ...values })
    }

    function handlePreview(file) {
        setState({
            previewImage: file.url || (props.descriptionTypeList[state.descriptionType] == 'picture and text' ? state.pictureAndText[file.binaryUrlKey].img : state.binaryUrlList[file.binaryUrlKey]),
            previewVisible: true,
            previewTitle: file.name || file.url.substring(file.url.lastIndexOf('/') + 1),
        });
    };

    const uploadButton = (
        <div>
            <PlusOutlined />
            <div style={{ marginTop: 8 }}>{formatMessage({ id: 'upload image' })}</div>
        </div>
    );

    function renderUploadPic(renderType) {
        let dTypeKey = renderType != '' ? 'Sub' : ''
        return (
            <div style={{ marginTop: '10px' }}>
                <Upload
                    accept={'.jpeg,.png,.jpg'}
                    listType="picture-card"
                    fileList={state['pictureList' + dTypeKey]}
                    onPreview={handlePreview}
                    onChange={(e) => {
                        let fileList = e.fileList
                        if (props.descriptionTypeList[state['descriptionType' + dTypeKey]] == 'picture and text') {
                            if (fileList.length > (state['pictureAndText' + dTypeKey]).length) {
                                fileList[fileList.length - 1].binaryUrlKey = fileList.length - 1
                                setState({
                                    pictureModalVisible: true,
                                    ['pictureList' + dTypeKey]: fileList
                                })
                            } else {
                                let oldPictureList = ['pictureAndText' + dTypeKey]
                                oldPictureList.forEach((file, key) => {
                                    if (file.uid == e.file.uid) {
                                        oldPictureList.splice(key, 1)
                                    }
                                });
                                setState({
                                    ['pictureAndText' + dTypeKey]: oldPictureList,
                                    ['pictureList' + dTypeKey]: fileList
                                })
                            }
                        } else {
                            let binaryUrlList = []
                            fileList.forEach((file, key) => {
                                getBase64(file.originFileObj, callbackSaveBinaryUrl)

                                function callbackSaveBinaryUrl(result) {
                                    binaryUrlList[key] = result
                                    fileList[key].binaryUrlKey = key
                                    if (key + 1 == fileList.length) {
                                        setState({
                                            ['pictureList' + dTypeKey]: fileList,
                                            ['binaryUrlList' + dTypeKey]: binaryUrlList
                                        })
                                    }
                                }
                            });

                            if (fileList.length == 0) {
                                setState({
                                    ['binaryUrlList' + dTypeKey]: [],
                                    ['pictureList' + dTypeKey]: []
                                })
                            }
                        }

                    }}
                >
                    {state['pictureList' + dTypeKey].length >= 8 ? null : uploadButton}
                </Upload>
                <Modal
                    visible={state.previewVisible}
                    title={state.previewTitle}
                    footer={null}
                    onCancel={() => setState({ previewVisible: false })}
                >
                    <img alt="example" style={{ width: '100%' }} src={state.previewImage} />
                </Modal>
            </div >
        )
    }

    function renderTextArea(renderType) {
        let value = renderType != '' ? 'textAreaSub' : 'textArea'
        return (
            <Input.TextArea value={state[value]} onChange={(e) => setState({ [value]: e.target.value })} style={{ marginTop: '10px' }} />
        )
    }

    function renderNewComponent() {
        return (
            <div style={{ marginTop: '10px', /*border: '1px solid red',*/ padding: '10px 25px' }}>
                <Input
                    onChange={(e) => setState({ mainTitleSub: e.target.value })}
                    value={state.mainTitleSub}
                    placeholder={formatMessage({ id: 'sub main title' })}
                />
                {

                    Object.keys(state.submitedDataSub).length > 0 &&
                    <Collapse style={{ marginTop: '10px' }}>
                        {
                            Object.keys(state.submitedDataSub).map((rangeKey) => {
                                let value = state.submitedDataSub[rangeKey]
                                return (
                                    Object.keys(value).map((description_type, arrKey) => {
                                        let content
                                        if (props.descriptionTypeList[description_type] == 'only picture') {
                                            content = []
                                            Object.values(value[description_type]).map((pic, picKey) => {
                                                content.push(
                                                    <img src={pic} alt="avatar" key={picKey} style={{ width: '100px', height: '100px', marginRight: '5px', marginBottom: '5px' }} />
                                                )
                                            })
                                        } else if (props.descriptionTypeList[description_type] == 'only text') {
                                            content = value[description_type]
                                        } else if (props.descriptionTypeList[description_type] == 'picture and text') {
                                            content = []
                                            Object.values(value[description_type]).map((data, dataKey) => {
                                                content.push(
                                                    <div style={{ marginRight: '5px', marginBottom: '5px', maxHeight: '130px', display: 'flex' }}>
                                                        <img src={data.img} alt="avatar" key={dataKey} style={{ width: '100px', height: '100px', marginRight: '5px' }} />
                                                        <div style={{ display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                                            {
                                                                data.content
                                                            }
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                        return (
                                            <Collapse.Panel
                                                key={rangeKey}
                                                header={formatMessage({ id: props.descriptionTypeList[description_type] })}
                                                extra={settingModal(rangeKey, 'Sub')}
                                            >
                                                {
                                                    content
                                                }
                                            </Collapse.Panel>

                                        )
                                    })

                                )
                            })
                        }
                    </Collapse>
                }
                {
                    state.descriptionTypeSub <= 0 &&
                    <React.Fragment>
                        <Divider
                            orientation="left"
                            style={{ margin: 'unset' }}
                        >
                            {formatMessage({ id: 'please select type' })}
                        </Divider>
                        <Radio.Group
                            onChange={(e) => {
                                setState({
                                    descriptionTypeSub: e.target.value,
                                    binaryUrlListSub: [],
                                    pictureListSub: [],
                                    pictureContent: '',
                                    textAreaSub: '',
                                })
                            }}
                            style={{ display: 'flex', flexWrap: 'wrap', paddingLeft: '0' }}
                        >
                            {
                                Object.keys(props.descriptionTypeList).map((key, arrKey) => {
                                    if (props.descriptionTypeList[key] != 'new component') {
                                        return (
                                            <Radio
                                                style={{ flex: '0 0 40%' }}
                                                key={arrKey}
                                                value={parseInt(key)}
                                            >
                                                {formatMessage({ id: props.descriptionTypeList[key] })}
                                            </Radio>
                                        )
                                    }

                                })
                            }
                        </Radio.Group>
                    </React.Fragment>
                }
                {
                    renderOption('sub')
                }
            </div>
        )
    }

    function dataURLtoFile(dataurl) {
        let filename = new Date().getTime()
        // 获取到base64编码
        const arr = dataurl.split(',')
        // 将base64编码转为字符串
        const bstr = window.atob(arr[1])
        const fileType = arr[0].split(':')[1].split(';')[0]
        let n = bstr.length
        const u8arr = new Uint8Array(n) // 创建初始化为0的，包含length个元素的无符号整型数组
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n)
        }
        return new File([u8arr], filename, {
            type: fileType
        })
    }

    function renderOption(renderType = '') {
        let options
        let type = renderType != '' ? 'Sub' : ''
        if (props.descriptionTypeList[state['descriptionType' + type]] == 'only picture' || props.descriptionTypeList[state['descriptionType' + type]] == 'picture and text') {
            options = renderUploadPic(renderType)
        } else if (props.descriptionTypeList[state['descriptionType' + type]] == 'only text') {
            options = renderTextArea(renderType)
        } else if (props.descriptionTypeList[state.descriptionType] == 'new component' && renderType == '') {
            options = renderNewComponent(renderType)
        }

        const doneButton = <Button
            key="done"
            type="primary"
            style={{ backgroundColor: type == '' ? 'green' : 'darkblue', marginTop: '10px' }}
            onClick={() => {
                let oldSubmitedData = state['submitedData' + type]
                let finalValue

                if (props.descriptionTypeList[state.descriptionType] == 'only picture') {
                    finalValue = state.binaryUrlList
                } else if (props.descriptionTypeList[state.descriptionType] == 'only text') {
                    finalValue = state.textArea
                } else if (props.descriptionTypeList[state.descriptionType] == 'picture and text') {
                    finalValue = state.pictureAndText
                } else if (props.descriptionTypeList[state.descriptionType] == 'new component') {

                    if (type == '') {
                        if (Object.keys(state.submitedDataSub).length <= 0 || state.mainTitleSub == '') {
                            message.error(formatMessage({ id: 'please provide info' }), 1)
                            return
                        }

                        finalValue = {
                            [state.mainTitleSub]: state.submitedDataSub
                        }

                        setState({
                            pictureListSub: [],
                            mainTitleSub: '',
                            binaryUrlListSub: [],
                            textAreaSub: '',
                            pictureAndTextSub: [],
                            submitedDataSub: {}
                        })
                    } else {
                        oldSubmitedData = state.submitedDataSub
                        if (props.descriptionTypeList[state.descriptionTypeSub] == 'only picture') {
                            finalValue = state.binaryUrlListSub
                        } else if (props.descriptionTypeList[state.descriptionTypeSub] == 'only text') {
                            finalValue = state.textAreaSub
                        } else if (props.descriptionTypeList[state.descriptionTypeSub] == 'picture and text') {
                            finalValue = state.pictureAndTextSub
                        }
                    }
                }

                if (Object.keys(finalValue).length <= 0) {
                    message.error(formatMessage({ id: 'please provide info' }), 1)
                    return
                }

                let newSubmitedData = {}
                Object.keys(oldSubmitedData).map((jsonKey, jsonArrKey) => {
                    newSubmitedData[jsonArrKey] = oldSubmitedData[jsonKey]
                })

                newSubmitedData[Object.keys(newSubmitedData).length] = {
                    [state['descriptionType' + type]]: finalValue
                }

                setState({
                    ['descriptionType' + type]: 0,
                    ['submitedData' + type]: newSubmitedData,
                    ['pictureAndText' + type]: [],
                    ['pictureList' + type]: [],

                })
            }}
        >
            {formatMessage({ id: 'done' })}
        </Button>

        const cancelButton = <Button
            key="cancel"
            type="danger"
            style={{ marginTop: '10px', marginLeft: '5px' }}
            onClick={() => {
                setState({
                    ['pictureList' + type]: [],
                    ['binaryUrlList' + type]: [],
                    ['textArea' + type]: '',
                    ['pictureAndText' + type]: [],
                    ['descriptionType' + type]: 0,
                    ['setSortKey' + type]: '',
                    ...(
                        type == ''
                            ? {
                                pictureListSub: [],
                                pictureAndTextSub: [],
                                binaryUrlListSub: [],
                                descriptionTypeSub: 0,
                                textAreaSub: '',
                                mainTitleSub: '',
                                submitedDataSub: {},
                                setSortKeySub: ''
                            }
                            : {}
                    )
                })

            }}
        >
            {formatMessage({ id: 'cancel' })}
        </Button>

        return (
            [options, state['descriptionType' + type] > 0 && [doneButton, cancelButton]]
        )
    }

    useEffect(() => {
        if (state.sortKey > 0) {

        }
    }, [state.sortKey]);

    function reRangeSubmidData(dTypeKey, rangeKey) {
        let oldSubmitedData = state['submitedData' + dTypeKey]
        let recordData = oldSubmitedData[rangeKey] //要换的data
        if (typeof oldSubmitedData[String(state.sortKey)] != 'undefined') {
            oldSubmitedData[rangeKey] = oldSubmitedData[String(state.sortKey)]
            oldSubmitedData[String(state.sortKey)] = recordData
        } else {
            oldSubmitedData[String(state.sortKey)] = recordData
            delete oldSubmitedData[rangeKey]
        }

        let newSubmitedData = {}
        Object.keys(oldSubmitedData).map((jsonKey, jsonArrKey) => {
            newSubmitedData[jsonArrKey] = oldSubmitedData[jsonKey]
        })

        setState({
            ['submitedData' + dTypeKey]: newSubmitedData,
            ['setSortKey' + dTypeKey]: ''
        })
    }

    function loadDropDown(rangeKey, dTypeKey) {
        if (dTypeKey == 'setComponent') {
            dTypeKey = ''
        }
        let oldSubmitedData = state['submitedData' + dTypeKey]
        return (
            <Menu
                onClick={({ domEvent, key }) => {
                    domEvent.stopPropagation()
                    setState({
                        sortKey: key
                    })
                }}
            >
                {
                    Object.keys(oldSubmitedData).map((range_key, arrKey) => {
                        if (rangeKey != range_key) {
                            return (
                                <Menu.Item key={range_key}>
                                    {
                                        range_key
                                    }
                                </Menu.Item>
                            )
                        }
                    })
                }
            </Menu>
        )
    }

    function settingModal(rangeKey, dTypeKey = '') {
        const rangeButton = state['setSortKey' + (dTypeKey == 'Sub' ? dTypeKey : '')] !== rangeKey
            ? <OrderedListOutlined
                onClick={(event) => {
                    event.stopPropagation()
                    if (dTypeKey == 'setComponent') {
                        dTypeKey = ''
                    }

                    setState({
                        ['setSortKey' + dTypeKey]: rangeKey
                    })
                }}
            />
            : <div style={{ display: 'inline-block' }}>
                <Dropdown onClick={(event) => event.stopPropagation()} overlay={loadDropDown(rangeKey, dTypeKey)}>
                    <Button>
                        {formatMessage({ id: 'select the key to be replaced' })} <DownOutlined />
                    </Button>
                </Dropdown>

                <Button
                    key="submid"
                    type="primary"
                    style={{ marginLeft: '5px' }}
                    onClick={(event) => {
                        event.stopPropagation()
                        if (state.sortKey < 0) {
                            message.error(formatMessage({ id: 'please provide info' }), 1)
                            return
                        }
                        if (dTypeKey == 'setComponent') {
                            dTypeKey = ''
                        }
                        reRangeSubmidData(dTypeKey, rangeKey)

                    }}
                >
                    OK
                </Button>

            </div>

        return [
            <div style={{ color: 'red', marginRight: '5px', fontWeight: 'bold', display: 'inline-block' }}>
                {formatMessage({ id: 'sort' })}: {rangeKey}
            </div>,
            rangeButton,
            <SettingOutlined
                style={{ width: '50px' }}
                onClick={event => {
                    event.stopPropagation()
                    if (dTypeKey == 'setComponent') {
                        let mainTitleSub = ''
                        let descriptionType = 0
                        let oldSubmitedData = state.submitedData
                        let finalData = {}

                        Object.keys(oldSubmitedData[rangeKey]).map((description_type) => {
                            descriptionType = description_type
                            Object.keys(oldSubmitedData[rangeKey][description_type]).map((title) => {
                                mainTitleSub = title
                                finalData = oldSubmitedData[rangeKey][description_type][title]

                            })
                        })
                        delete oldSubmitedData[rangeKey]
                        setState({
                            mainTitleSub,
                            descriptionType: parseInt(descriptionType),
                            submitedDataSub: finalData,
                            submitedData: oldSubmitedData
                        })
                    } else {
                        if (state['descriptionType' + dTypeKey] > 0) {
                            message.error(formatMessage({ id: 'please save or remove current data' }), 1)
                            return
                        }
                        let oldSubmitedData = state['submitedData' + dTypeKey]
                        Object.keys(oldSubmitedData[rangeKey]).map((key, arrkey) => {
                            if (props.descriptionTypeList[key] == 'only text') {
                                setState({
                                    ['textArea' + dTypeKey]: oldSubmitedData[rangeKey][key],
                                    ['descriptionType' + dTypeKey]: key
                                })
                            } else if (props.descriptionTypeList[key] == 'only picture') {
                                let newPictureList = []
                                oldSubmitedData[rangeKey][key].forEach((image, imageKey) => {
                                    let imageFile = dataURLtoFile(image)
                                    imageFile.originFileObj = imageFile
                                    imageFile.originFileObj.uid = imageFile.name
                                    imageFile.binaryUrlKey = imageKey
                                    imageFile.percent = 100
                                    imageFile.status = 'done'
                                    imageFile.thumbUrl = image
                                    imageFile.uid = imageFile.name
                                    newPictureList.push(imageFile)
                                })

                                setState({
                                    ['binaryUrlList' + dTypeKey]: oldSubmitedData[rangeKey][key],
                                    ['descriptionType' + dTypeKey]: key,
                                    ['pictureList' + dTypeKey]: newPictureList
                                })
                            } else if (props.descriptionTypeList[key] == 'picture and text') {
                                let newPictureList = []
                                oldSubmitedData[rangeKey][key].forEach((data, imageKey) => {
                                    let imageFile = dataURLtoFile(data.img)
                                    imageFile.originFileObj = imageFile
                                    imageFile.originFileObj.uid = imageFile.name
                                    imageFile.binaryUrlKey = imageKey
                                    imageFile.percent = 100
                                    imageFile.status = 'done'
                                    imageFile.thumbUrl = data.img
                                    imageFile.uid = imageFile.name
                                    newPictureList.push(imageFile)
                                })

                                setState({
                                    ['pictureAndText' + dTypeKey]: oldSubmitedData[rangeKey][key],
                                    ['descriptionType' + dTypeKey]: key,
                                    ['pictureList' + dTypeKey]: newPictureList
                                })
                            }
                        })
                        delete oldSubmitedData[rangeKey]
                        setState({ ['submitedData' + dTypeKey]: oldSubmitedData })
                    }
                }}
            />,
            <Popconfirm
                key={'delete'}
                title={formatMessage({ id: 'Are you sure?' })}
                onConfirm={(event) => {
                    event.stopPropagation()
                    if (dTypeKey == 'setComponent') {
                        dTypeKey = ''
                    }
                    let oldSubmitedData = state['submitedData' + dTypeKey]
                    delete oldSubmitedData[rangeKey]
                    setState({
                        ['submitedData' + dTypeKey]: oldSubmitedData,
                        ['setSortKey' + dTypeKey]: oldSubmitedData,
                    })
                }}
                icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
            >
                <DeleteOutlined
                    onClick={event => {
                        // If you don't want click extra trigger collapse, you can prevent this:
                        event.stopPropagation()
                    }}
                />
            </Popconfirm>
        ]
    }

    function renderSubmidtedData(value, description_type) {
        let content
        let componentTitle = ''
        if (props.descriptionTypeList[description_type] == 'only picture') {
            content = []
            Object.values(value[description_type]).map((pic, picKey) => {
                content.push(
                    <img src={pic} alt="avatar" key={picKey} style={{ width: '100px', height: '100px', marginRight: '5px', marginBottom: '5px' }} />
                )
            })
        } else if (props.descriptionTypeList[description_type] == 'only text') {
            content = []
            let datas = value[description_type].split('\n')
            datas.forEach(data => {
                content.push(
                    <div>{data}</div>
                )
            })

        } else if (props.descriptionTypeList[description_type] == 'picture and text') {
            content = []
            Object.values(value[description_type]).map((data, dataKey) => {
                content.push(
                    <div style={{ marginRight: '5px', marginBottom: '5px', maxHeight: '130px', display: 'flex' }}>
                        <img src={data.img} alt="avatar" key={dataKey} style={{ width: '100px', height: '100px', marginRight: '5px' }} />
                        <div style={{ display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                            {
                                data.content
                            }
                        </div>
                    </div>
                )
            })
        } else if (props.descriptionTypeList[description_type] == 'new component') {
            let subContent = []
            Object.keys(value[description_type]).map((subTitle) => {
                componentTitle = subTitle
                Object.keys(value[description_type][subTitle]).map((data, dataKey) => {
                    Object.keys(value[description_type][subTitle][data]).map((description_type_sub) => {
                        let subValue = value[description_type][subTitle][data]
                        subContent.push(
                            <Collapse.Panel
                                header={formatMessage({ id: props.descriptionTypeList[description_type_sub] })}
                                key={dataKey}
                            >
                                {renderSubmidtedData(subValue, description_type_sub)}
                            </Collapse.Panel>
                        )
                    })

                })
            })

            content = <Collapse>
                {subContent}
            </Collapse>
        }

        return componentTitle != '' ? { content, componentTitle } : content
    }

    return (
        <Modal
            width={'50%'}
            title={props.mode == 'add' ? formatMessage({ id: 'add new description' }) : formatMessage({ id: 'setting description' })}
            visible={props.visible}
            onCancel={() => {
                setState({
                    submitedData: {},
                    mainTitle: '',
                    descriptionType: 0,
                    descriptionTypeSub: 0
                })
                props.onClose({ visible: false })
            }}
            footer={[
                <Button
                    loading={state.buttonLoading}
                    key="submit"
                    type="primary"
                    disabled={Object.keys(state.submitedData).length <= 0 || state.mainTitle == '' || state.descriptionType > 0 || state.descriptionTypeSub > 0}
                    onClick={() => {
                        setState({
                            buttonLoading: true
                        })
                        dispatch({
                            type: "description/e_updateDescription",
                            payload: {
                                descriptionList: state.submitedData,
                                action: props.mode,
                                title: state.mainTitle,
                                related_id: props.related_id,
                                classMode: props.classMode,
                                settingInfoKey: state.settingInfoKey
                            }
                        }).then(result => {
                            if (result) {
                                setState({
                                    submitedData: {},
                                    mainTitle: ''
                                })
                                props.setState({ ...result })
                                props.onClose({ visible: false })
                            }
                            setState({
                                buttonLoading: false
                            })
                        })
                    }}
                >
                    OK
                </Button>,
            ]}
        >
            <Modal
                title={formatMessage({ id: 'please provide picture content' })}
                visible={state.pictureModalVisible}
                onCancel={() => {
                    let dTypeKey = state.descriptionTypeSub > 0 ? 'Sub' : ''
                    let oldPictureList = state['pictureList' + dTypeKey]
                    oldPictureList.splice(state['pictureList' + dTypeKey].length - 1, 1)
                    setState({
                        pictureModalVisible: false,
                        ['pictureList' + dTypeKey]: oldPictureList
                    })
                }}
                footer={[
                    <Button
                        key="submit"
                        type="primary"
                        disabled={state.pictureContent == ''}
                        onClick={() => {
                            let dTypeKey = state.descriptionTypeSub > 0 ? 'Sub' : ''
                            getBase64(state['pictureList' + dTypeKey][state['pictureList' + dTypeKey].length - 1].originFileObj, callbackSaveBinaryUrl)

                            function callbackSaveBinaryUrl(result) {
                                let olPictureAndText = state['pictureAndText' + dTypeKey]
                                olPictureAndText[state['pictureList' + dTypeKey].length - 1] = {
                                    img: result,
                                    content: state.pictureContent,
                                    uid: state['pictureList' + dTypeKey][state['pictureList' + dTypeKey].length - 1].uid
                                }

                                setState({
                                    pictureModalVisible: false,
                                    ['pictureAndText' + dTypeKey]: olPictureAndText,
                                    pictureContent: '',
                                    ['binaryUrlList' + dTypeKey]: []
                                })
                            }
                        }}
                    >
                        OK
                    </Button>,
                ]}
            >
                <Input value={state.pictureContent} onChange={e => setState({ pictureContent: e.target.value })} />
            </Modal>

            <Input
                onChange={(e) => setState({ mainTitle: e.target.value })}
                value={state.mainTitle}
                placeholder={formatMessage({ id: 'main title' })}
            />

            {
                Object.keys(state.submitedData).length > 0 &&
                <Collapse style={{ marginTop: '10px' }}>
                    {
                        Object.keys(state.submitedData).map((rangeKey) => {
                            let value = state.submitedData[rangeKey]
                            return (
                                Object.keys(value).map((description_type, arrKey) => {
                                    let result = renderSubmidtedData(value, description_type)

                                    return (
                                        <Collapse.Panel
                                            key={rangeKey}
                                            header={
                                                formatMessage({ id: props.descriptionTypeList[description_type] }) +
                                                (
                                                    props.descriptionTypeList[description_type] == 'new component'
                                                        ? ' [ ' + result.componentTitle + ' ]'
                                                        : ''
                                                )
                                            }
                                            extra={
                                                settingModal(
                                                    rangeKey,
                                                    props.descriptionTypeList[description_type] == 'new component'
                                                        ? 'setComponent'
                                                        : ''
                                                )
                                            }
                                        >
                                            {
                                                props.descriptionTypeList[description_type] == 'new component'
                                                    ? result.content
                                                    : result
                                            }
                                        </Collapse.Panel>

                                    )
                                })

                            )
                        })
                    }
                </Collapse >
            }

            {
                props.descriptionTypeList &&
                <React.Fragment>
                    {
                        state.descriptionType <= 0 &&
                        <React.Fragment>
                            <Divider
                                orientation="left"
                                style={{ margin: 'unset' }}
                            >
                                {formatMessage({ id: 'please select type' })}
                            </Divider>
                            <Radio.Group
                                onChange={(e) => {
                                    setState({
                                        descriptionType: e.target.value,
                                        binaryUrlList: [],
                                        pictureList: [],
                                        pictureContent: '',
                                        textArea: '',
                                    })
                                }}
                                style={{ display: 'flex', flexWrap: 'wrap', paddingLeft: '0' }}
                            >
                                {
                                    Object.keys(props.descriptionTypeList).map((key, arrKey) => {
                                        return (
                                            <Radio
                                                style={{ flex: '0 0 40%' }}
                                                key={arrKey}
                                                value={parseInt(key)}
                                            >
                                                {formatMessage({ id: props.descriptionTypeList[key] })}
                                            </Radio>
                                        )
                                    })
                                }
                            </Radio.Group>
                        </React.Fragment>
                    }
                    {
                        renderOption()
                    }
                </React.Fragment>
            }
        </Modal >
    )
}