import React, { useState, useMemo } from 'react';
import { Checkbox, Spin } from 'antd'

export default function InfinityTable(props) {
    const [state, setOriState] = useState({
        selectedRow: [],
        data: [],
        selectedKey: {},
        titleChecked: false,

        selectedKeyMultiple: [],
        titleCheckedMultiple: [],
        titleMultipleColor: []
    })

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }

    function selected(data, boolean, key = '') {
        if (key >= 0) {
            let oldMultipleChecked = state.titleCheckedMultiple
            // if (oldMultipleChecked.length <= 0) {
            //     props.rowSelection.forEach(element => {
            //         oldMultipleChecked.push(false)
            //     });
            // }

            if (boolean) {
                let newData = typeof state.selectedRow[key] === 'undefined' ? [] : state.selectedRow[key]
                let newKey = typeof state.selectedKeyMultiple[key] === 'undefined' ? {} : state.selectedKeyMultiple[key]
                newData.push(data)
                newKey[data.id] = true

                let oldData = state.selectedRow
                oldData[key] = newData

                let oldKey = state.selectedKeyMultiple
                oldKey[key] = newKey

                oldMultipleChecked[key] = newData.length === props.dataSource.length
                setState({
                    titleCheckedMultiple: oldMultipleChecked,
                    selectedRow: oldData,
                    selectedKeyMultiple: oldKey
                })
                props.rowSelection[key].onSelect(data, boolean, newData)
            }
            else {
                let newKey = state.selectedKeyMultiple
                let oldData = state.selectedRow
                let newData = []
                console.log(oldData, oldData[key])
                Object.values(oldData[key]).map((value, arrykey) => {
                    if (value.id != data.id) {
                        newData.push(value)
                    }
                    else {
                        newKey[key][data.id] = false
                    }
                })

                oldData[key] = newData

                oldMultipleChecked[key] = false
                setState({
                    titleCheckedMultiple: oldMultipleChecked,
                    selectedRow: oldData,
                    selectedKeyMultiple: newKey
                })
                props.rowSelection[key].onSelect(data, boolean, newData)
            }
        }
        else {
            let oldData = state.selectedRow
            let oldKey = state.selectedKey
            if (boolean) {
                oldData.push(data)
                oldKey[data.id] = true
                setState({
                    titleChecked: oldData.length === props.dataSource.length,
                    selectedRow: oldData,
                    selectedKey: oldKey
                })
                props.rowSelection.onSelect(data, boolean, oldData)
            }
            else {
                let newData = []
                let newKey = state.selectedKey
                Object.values(oldData).map((value, key) => {
                    if (value.id != data.id) {
                        newData.push(value)
                    }
                    else {
                        newKey[data.id] = false
                    }
                })
                setState({
                    titleChecked: false,
                    selectedRow: newData,
                    selectedKey: newKey
                })
                props.rowSelection.onSelect(data, boolean, newData)
            }
        }
    }

    function selectedAll(boolean, key = '') {
        const dataRecourse = props.dataSource
        let replaceKey = {}
        Object.values(props.dataSource).map((dataValue, dataKey) => {
            replaceKey[dataValue.id] = boolean
        })
        if (key >= 0) {
            let oldselectedMultpletKey = state.selectedKeyMultiple
            oldselectedMultpletKey[key] = replaceKey

            let oldTitleCheckedMultiple = state.titleCheckedMultiple
            oldTitleCheckedMultiple[key] = boolean

            let oldSelectedRowKey = state.selectedRow


            if (boolean) {
                oldSelectedRowKey[key] = dataRecourse
                setState({
                    titleCheckedMultiple: oldTitleCheckedMultiple,
                    selectedRow: oldSelectedRowKey,
                    selectedKeyMultiple: oldselectedMultpletKey
                })
                props.rowSelection[key].onSelectAll(boolean, dataRecourse, dataRecourse)
            }
            else {
                oldSelectedRowKey[key] = []
                setState({
                    titleCheckedMultiple: oldTitleCheckedMultiple,
                    selectedRow: oldSelectedRowKey,
                    selectedKeyMultiple: oldselectedMultpletKey
                })
                props.rowSelection[key].onSelectAll(boolean, [], dataRecourse)
            }
        }
        else {
            if (boolean) {
                setState({
                    titleChecked: true,
                    selectedRow: dataRecourse,
                    selectedKey: defaultFalseKey
                })
                props.rowSelection.onSelectAll(boolean, dataRecourse, dataRecourse)
            }
            else {
                setState({
                    selectedRow: [],
                    selectedKey: defaultFalseKey,
                    titleChecked: false
                })
                props.rowSelection.onSelectAll(boolean, [], dataRecourse)
            }
        }
    }

    useMemo(() => {
        if (!state.dataSource && !props.loading) {
            let data = []
            let newKey = {}
            Object.values(props.dataSource).map((dataValue, dataKey) => {
                newKey[dataValue.id] = false
                data[dataKey] = []
                return (
                    Object.values(props.columns).map((titleValue, titleKey) => {
                        data[dataKey].push(
                            <td>
                                {
                                    titleValue.render ? titleValue.render(dataValue[titleValue.dataIndex], dataValue) : dataValue[titleValue.dataIndex]
                                }
                            </td>
                        )
                    })
                )
            })

            setState({
                data,
                selectedKey: newKey
            })
        }
    }, [props.loading]);

    function loadCheckbox(dataValue, key = '') {
        if (Array.isArray(props.rowSelection)) {
            let checked = false
            if (state.selectedKeyMultiple[key]) {
                if (state.selectedKeyMultiple[key][dataValue.id]) {
                    checked = true
                }
            }

            return (
                <input
                    type="checkbox"
                    onChange={(e) => selected(dataValue, e.target.checked, key)}
                    checked={checked}
                />
            )
        }

        return (
            <input
                type="checkbox"
                onChange={(e) => selected(dataValue, e.target.checked)}
                checked={state.selectedKey[dataValue.id] || false}
            />
        )
    }

    return (
        <div style={{ overflowY: 'scroll', height: '400px', overflowX: 'scroll' }}>
            <Spin spinning={props.loading}>
                {
                    props.columns && props.dataSource &&
                    <table style={{ width: '100%' }}>
                        <tbody>
                            <tr>

                                {
                                    props.rowSelection
                                        &&
                                        Array.isArray(props.rowSelection)
                                        ?
                                        Object.values(props.rowSelection).map((value, key) => {
                                            return (
                                                <td key={key + 1} style={{ textAlign: 'center', width: '60px', position: 'sticky', top: 0, backgroundColor: '#fafafa', height: '50px', color: 'black' }}>
                                                    {
                                                        props.defaultTitleCheckboxValue
                                                            ? props.defaultTitleCheckboxValue[key]
                                                            : <Checkbox checked={state.titleCheckedMultiple[key] == 'undefined' ? false : state.titleCheckedMultiple[key]} onChange={(e) => selectedAll(e.target.checked, key)} />
                                                    }
                                                </td>
                                            )
                                        })
                                        :
                                        <td td style={{ textAlign: 'center', width: '60px', position: 'sticky', top: 0, backgroundColor: '#fafafa', height: '50px', color: 'black' }}>
                                            <Checkbox checked={state.titleChecked} onChange={(e) => selectedAll(e.target.checked)} />
                                        </td>

                                }

                                {
                                    Object.values(props.columns).map((value, key) => {
                                        return (
                                            <td key={key} style={{ position: 'sticky', top: 0, backgroundColor: '#fafafa', height: '50px', color: 'black' }} >
                                                {
                                                    value.title
                                                }
                                            </td>
                                        )
                                    })
                                }
                            </tr>

                            {
                                Object.values(props.dataSource).map((dataValue, dataKey) => {
                                    return (
                                        <tr key={dataKey} style={{ borderTop: '1px solid #e8e8e8', height: '100px' }}>
                                            {
                                                props.rowSelection
                                                    &&
                                                    Array.isArray(props.rowSelection)
                                                    ?
                                                    Object.values(props.rowSelection).map((value, key) => {
                                                        return (
                                                            <td style={{ textAlign: 'center' }}>
                                                                {
                                                                    loadCheckbox(dataValue, key)
                                                                }

                                                            </td>
                                                        )
                                                    })
                                                    :
                                                    <td style={{ textAlign: 'center' }}>
                                                        {
                                                            loadCheckbox(dataValue)
                                                        }

                                                    </td>
                                            }

                                            {
                                                state.data[dataKey]
                                            }
                                        </tr>
                                    )
                                })
                            }

                        </tbody>
                    </table>
                }
            </Spin>

        </div >
    )
}