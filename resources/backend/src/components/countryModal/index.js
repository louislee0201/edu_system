import React, { useEffect, useState } from "react";
import {
    Modal,
    Input,
    Button,
    Popconfirm,
    Collapse,
    message,
    Descriptions,
    Radio,
    Tag
} from "antd";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import { QuestionCircleOutlined } from '@ant-design/icons';

export default function CountryModal(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        buttonLoading: false,
        mode: '',
        country_id: 0,
        country: ''
    })

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }

    useEffect(() => {
        dispatch({
            type: 'config/e_getCountry',
            payload: {}
        }).then((value) => {
            props.setState({
                ...value
            })
        })
    }, []);

    function updateCountry(payload) {
        dispatch({
            type: 'config/e_updateCountry',
            payload: {
                ...state,
                ...payload
            }
        }).then((value) => {
            props.setState({
                ...value,
            })
            setState({
                mode: '',
                country_id: 0,
                country: ''
            })
            message.success(formatMessage({ id: 'success update' }), 1)
        })
    }

    return (
        <Modal
            title={formatMessage({ id: 'setting country' })}
            visible={props.visible}
            onOk={() => props.onClose({ visible: false })}
            onCancel={() => props.onClose({ visible: false })}
            footer={[
                <Button key="back" style={{ backgroundColor: 'pink', color: 'white' }} onClick={() => setState({ mode: 'add', country_id: 0, country: '' })}>{formatMessage({ id: 'add country' })}</Button>,
                <Button key="submit" type="primary" onClick={() => props.onClose({ visible: false })}>
                    OK
                </Button>,
            ]}
        >
            {
                Object.values(props.countryList).map((value, key) => {
                    return (
                        <div key={key} style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <div>
                                {
                                    value.id == state.country_id
                                        ?
                                        <div style={{ display: 'flex', alignItems: 'center' }}>
                                            {
                                                (key + 1) + '. '
                                            }
                                            <Input value={state.country} onChange={(e) => setState({ country: e.target.value })} />
                                        </div>
                                        : (key + 1) + '. ' + value.country
                                }
                            </div>

                            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                <Button
                                    type="link"
                                    onClick={() => {
                                        if (state.mode == 'edit' && state.country_id == value.id) {
                                            updateCountry({ action: 'edit' })
                                        }
                                        else {
                                            setState({ mode: 'edit', country_id: value.id, country: value.country })
                                        }
                                    }}
                                >
                                    {
                                        state.mode == 'edit' && state.country_id == value.id
                                            ? formatMessage({ id: 'save' })
                                            : formatMessage({ id: 'edit' })
                                    }
                                </Button>


                                <Popconfirm
                                    key={'activation'}
                                    title={formatMessage({ id: 'Are you sure?' })}
                                    onConfirm={() => updateCountry({ action: 'activation', country_id: value.id })}
                                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                >
                                    <Tag color={'red'}>
                                        {
                                            value.active
                                                ? formatMessage({ id: 'hide' }).toUpperCase()
                                                : formatMessage({ id: 'show' }).toUpperCase()
                                        }
                                    </Tag>
                                </Popconfirm>

                                <Popconfirm
                                    key={'delete'}
                                    title={formatMessage({ id: 'Are you sure?' })}
                                    onConfirm={() => updateCountry({ action: 'delete', country_id: value.id })}
                                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                >
                                    <Tag color={'red'}>
                                        {formatMessage({ id: 'delete' }).toUpperCase()}
                                    </Tag>
                                </Popconfirm>
                            </div>
                        </div>
                    )
                })
            }

            {
                state.mode == 'add' &&
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <Input value={state.country} onChange={(e) => setState({ country: e.target.value })} />
                    </div>

                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Button
                            type="link"
                            onClick={() => {
                                updateCountry({ action: 'add' })
                            }}
                        >
                            {
                                formatMessage({ id: 'save' })
                            }
                        </Button>

                        <Popconfirm
                            key={'cancel'}
                            title={formatMessage({ id: 'Are you sure?' })}
                            onConfirm={() => setState({ mode: '' })}
                            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                        >
                            <Tag color={'red'}>
                                {formatMessage({ id: 'cancel' }).toUpperCase()}
                            </Tag>
                        </Popconfirm>
                    </div>
                </div>
            }

        </Modal>
    )
}