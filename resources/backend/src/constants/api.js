export const HOST = window.location.hostname.indexOf('louis-blog') >= 0 || window.location.hostname.indexOf('edudotcom') >= 0
    ? window.location.protocol + '//' + window.location.hostname.replace('www', 'api') + '/'
    : "http://" + window.location.hostname + ":8000/"
export const IMAGE_URL = "image/";
export const ROUTE = 'admin_api'

export const API_USER_LOGIN = "auth/login";
export const API_USER_LOGOUT = "auth/logout";
export const API_CHECK_LOGIN = 'auth/loginStatus'

export const API_GET_COUNTRY = '/getCountry'
export const API_UPDATE_COUNTRY = '/updateCountry'
export const API_GET_COURSE_LEVEL = '/getCourseLevelList'
export const API_UPDATE_COURSE_LEVEL = '/updateCourseLevel'
export const API_SEARCH_SCHOOL = '/getSchoolList'
export const API_GET_QUALIFICATION = '/getQualification'
export const API_UPDATE_QUALIFICATION = '/editQualification'
export const API_UPDATE_QUALIFICATION_SUBJECT = '/updateQualificationSubject'
export const API_UPDATE_QUALIFICATION_GRADE = '/updateQualificationGrade'
export const API_GET_LANGUAGE_LIST = '/getLanguage'
export const API_UPDATE_LANGUAGE = '/updateLanguage'
export const API_GET_PROFICIENCY_LIST = '/getProficiency'
export const API_UPDATE_PROFICIENCY = '/updateProficiency'
export const API_GET_PROFICIENCY_QUALIFICATION_SUBJECT = '/getQualificationSubject'
export const API_ADD_PROFICIENCY_GRADE = '/addProfiencyGrade'
export const API_UPDATE_PROFICIENCY_GRADE = '/editProficiencyGrade'
export const API_GET_TAG = '/getTag'
export const API_UPDATE_MAIN_TAG_LIST = '/updateMainTagList'
export const API_UPDATE_SUB_TAG_LIST = '/updateSubTagList'
export const API_EDIT_COURSE_SUB_TAG_LIST = '/editCourseSubTagList'

export const API_GET_SCHOOL_LIST = "/getSchool";
export const API_UPDATE_SCHOOL = "/updateSchool";
export const API_EDIT_SCHOOL_COURSE_ARRANGEMENT_TITLE = '/editSchoolCourseArrangementTitle'

export const API_GET_REGION_LIST = '/getReionList'
export const API_UPDATE_REGION = '/updateRegion'

export const API_GET_COURSE_LIST = '/getCourse'
export const API_UPDATE_COURSE = "/updateCourse";
export const API_GET_REQUIREMENT_ATTR = '/getRequirementAttr'
export const API_SUBMIT_REQUIREMENT = '/submitRequirement'
export const API_UPDATE_REQUIREMENT = '/updateRequirement'

export const API_GET_DESCRIPTION_TYPE = '/getDescriptionType'
export const API_UPDATE_DESCRIPTION = '/updateDescription'
export const API_GET_DESCRIPTION_LIST = '/getDescriptionList'
export const API_GET_DESCRIPTION_DATA = '/getDescriptionData'
