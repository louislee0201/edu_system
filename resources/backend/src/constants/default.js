export const DEFAULT_SETTING = 
{
    language:'en-US'
}

export const SYSTEM_LANGUAGE = 
{
    zh_CN:'zh-CN',
    en_US:'en-US'
}

export const STORAGE_KEY =
{
    user:'user_info',
    language:'umi_locale'
}