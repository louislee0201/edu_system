import React, { useState, useEffect } from 'react';
import DataTable from '@/components/dataTable'
import { Button, Tag, Popconfirm } from 'antd';
import router from 'umi/router'
import Authorized from '@/components/authorized'
import { formatMessage } from 'umi/locale';
import { useDispatch } from 'react-redux'
import { QuestionCircleOutlined } from '@ant-design/icons';

export default function courseList(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        totalItem: 0,
        currentPage: 0,
        loading: true,
        dataList: [],
        filterArr: {},
        columnsList: [],
        pageLoad: false,
        courseLevelList: []
    });

    useEffect(() => {
        requestData()
    }, []);

    useEffect(() => {
        if (state.pageLoad) {
            setState({
                columnsList: [
                    {
                        title: 'course_name',
                        dataIndex: 'course_name',//follow by this key show data
                        key: 'course_name',
                        search: true,

                    },
                    {
                        title: 'school_name',
                        dataIndex: 'school_name',//follow by this key show data
                        key: 'school_name',
                        search: true,
                    },
                    {
                        title: 'school_name_cn',
                        dataIndex: 'school_name_cn',//follow by this key show data
                        key: 'school_name_cn',
                        search: true,
                    },
                    {
                        title: 'course_level',
                        dataIndex: 'course_level',//follow by this key show data
                        key: 'course_level',
                        filters: loadCourseLevel(),
                        filterMultiple: false,
                        render: (tags, listValue) => (
                            <span>
                                {
                                    state.courseLevelList &&
                                    Object.values(state.courseLevelList).map((value) => {
                                        if (value.id == listValue.course_level) {
                                            return value.level_name
                                        }
                                    })
                                }
                            </span>
                        ),
                    },
                    {
                        title: 'actions',
                        dataIndex: 'actions',
                        fixed: 'right',
                        render: (tags, listValue) => (
                            <span>
                                <Authorized authority={['admin', 'superb']}>
                                    <Popconfirm
                                        key={'delete'}
                                        title={formatMessage({ id: 'Are you sure?' })}
                                        onConfirm={() => handleUpdate({ course_id: listValue.course_id, action: 'delete' })}
                                        icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                    >
                                        <Tag color={'red'}>
                                            {formatMessage({ id: 'delete' }).toUpperCase()}
                                        </Tag>
                                    </Popconfirm>

                                    <Popconfirm
                                        key={'school_status'}
                                        title={formatMessage({ id: 'Are you sure?' })}
                                        onConfirm={() => handleUpdate({ course_id: listValue.course_id, action: 'activation' })}
                                        icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                    >
                                        {
                                            listValue.active
                                                ?
                                                <Tag color={'orange'}>
                                                    {formatMessage({ id: 'hide info' }).toUpperCase()}
                                                </Tag>
                                                :
                                                <Tag color={'orange'}>
                                                    {formatMessage({ id: 'show info' }).toUpperCase()}
                                                </Tag>
                                        }
                                    </Popconfirm>

                                </Authorized>


                                <Tag
                                    key={'view'}
                                    color={'geekblue'}
                                    onClick={() => goTo('/course/viewCourse?course_id=' + listValue.course_id)}
                                >
                                    {formatMessage({ id: 'view' }).toUpperCase()}
                                </Tag>
                            </span>
                        ),
                    }
                ]
            })
        }
    }, [state.pageLoad])

    function loadCourseLevel() {
        let List = []
        state.courseLevelList.forEach(value => {
            List.push({
                text: value.level_name,
                value: value.id
            })
        });
        return List
    }

    function setState(value, callback = '') {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
        if (callback != '') {
            callback()
        }
    }

    function requestData(clickPage = 1) {
        setState({
            loading: true
        })
        dispatch({
            type: 'course/e_getCourseList',
            payload: {
                isList: true,
                clickPage,
                filterArr: state.filterArr
            }
        }).then((value) => {
            setState({
                ...value,
                loading: false,
                currentPage: value.totalItem > 0 ? clickPage : 0,
                pageLoad: !state.pageLoad ? true : false
            })
        })
    }

    function goTo(url) {
        router.push(url)
    }

    function handleUpdate(data) {
        setState({
            loading: true
        })

        dispatch({
            type: 'course/e_updateCourse',
            payload: {
                ...data,
                clickPage: state.clickPage,
                isList: true,
                filterArr: state.filterArr
            }
        }).then((value) => {
            setState({
                ...value,
                loading: false
            })
        })
    }

    return (
        <div>
            <Button
                style={{ marginRight: '5px', marginBottom: '10px' }}
                type="primary"
                onClick={() => goTo('/course/addNewCourse')}
            >
                {
                    formatMessage({ id: 'add new course' })
                }
            </Button>

            <DataTable goTo={goTo} requestData={requestData} {...state} key={'course_id'} setState={setState} />
        </div>
    );
}