import React, { useEffect, useState } from "react";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import DataView from '@/components/dataView'
import { getBase64 } from '@/tools'
import { Tabs, InputNumber, Radio, Button, Popconfirm, Tag, Modal, Cascader, Checkbox } from "antd";
import DataTable from '@/components/dataTable'
import RequirementModal from './requirementModal2'
import Authorized from '@/components/authorized'
import { QuestionCircleOutlined } from '@ant-design/icons';
import DescriptionTable from '@/components/descriptionTable'

export default function ViewCourse(props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        requirementModalVisible: false,
        editData: {},
        qualificationInfoList: {},
        dataList: [],
        loading: false,
        classMode: 'course',
        modalMode: 'add',
        addTagModalVisible: false,
        mainTagsList: [],
        subTagsList: []
    })

    useEffect(() => {
        const course_id = props.location.query.course_id;
        if (course_id) {
            dispatch({
                type: "course/e_getCourseList",
                payload: {
                    isList: false,
                    course_id
                }
            }).then(result => {
                if (result) {
                    setState({ ...result, course_id })
                }
            });
        }
    }, []);

    function loadTitle() {
        return (
            [
                {
                    label: "school_name",
                    key: "school_name",
                    disabled: true
                },
                {
                    label: "course_name",
                    key: "course_name"
                },
                {
                    label: "course_level",
                    key: "course_level",
                    default_element: state.courseLevelList &&
                        Object.values(state.courseLevelList).map((value) => {
                            if (value.id == state.course_level) {
                                return value.level_name
                            }
                        }),
                    edit_element: state.courseLevelList &&
                        <Radio.Group value={state.course_level} onChange={(e) => setState({ course_level: e.target.value })}>
                            {
                                Object.values(state.courseLevelList).map((value, key) => {
                                    if (value.active) {
                                        return (
                                            <Radio key={key} value={value.id}>{value.level_name}</Radio>
                                        )
                                    }
                                })
                            }
                        </Radio.Group>
                },
                {
                    label: "secondary_complete",
                    key: "secondary_complete",
                    default_element: formatMessage({ id: state.secondary_complete ? 'yes' : 'no' }),
                    edit_element: <Radio.Group value={state.secondary_complete} onChange={(e) => setState({ secondary_complete: e.target.value })}>
                        <Radio value={1}>{formatMessage({ id: 'yes' })}</Radio>
                        <Radio value={0}>{formatMessage({ id: 'no' })}</Radio>
                    </Radio.Group>
                },
                {
                    label: "duration ( Month )",
                    key: "duration",
                    default_element: (
                        parseInt(state.duration / 12) > 0
                            ? parseInt(state.duration / 12).toFixed() + (parseInt(state.duration / 12).toFixed() > 1 ? formatMessage({ id: 'years' }) : formatMessage({ id: 'year' })) + ' '
                            : ''
                    ) + (
                            state.duration % 12 > 0
                                ? state.duration % 12 + (state.duration % 12 > 1 ? formatMessage({ id: 'month\'s' }) : formatMessage({ id: 'month' }))
                                : ''
                        ),
                    edit_element: <InputNumber onChange={(e) => {
                        if (Number.isInteger(e)) {
                            setState({ duration: parseInt(e.toFixed()) })
                        }
                    }} value={state.duration} style={{ width: '100%' }} min={0} />
                },
                {
                    label: "currency_type",
                    key: "currency_type",
                    default_element: state.currencyTypeList && Object.keys(state.currencyTypeList).map((key) => {
                        if (key == state.currency_type) {
                            return state.currencyTypeList[key]
                        }
                    }),
                    edit_element: <Radio.Group value={parseInt(state.currency_type)} onChange={(e) => setState({ currency_type: e.target.value })}>
                        {
                            state.currencyTypeList &&
                            Object.keys(state.currencyTypeList).map((key) => {
                                return <Radio value={parseInt(key)}>{state.currencyTypeList[key]}</Radio>
                            })
                        }
                    </Radio.Group>
                },
                {
                    label: "tuition_fee",
                    key: "tuition_fee",
                    default_element: state.tuition_fee,
                    edit_element: <InputNumber onChange={(e) => {
                        setState({ tuition_fee: e })
                    }} value={state.tuition_fee} style={{ width: '100%' }} min={0} />
                },
                {
                    label: "contact_me",
                    key: "contact_me",
                    default_element: formatMessage({ id: state.contact_me ? 'yes' : 'no' }),
                    edit_element: <div>
                        {formatMessage({ id: state.contact_me ? 'yes' : 'no' })} <Checkbox checked={state.contact_me} onChange={(e) => setState({ contact_me: e.target.checked })} />
                    </div>
                }

                /*  {
                     label: "picture",
                     key: "picture",
                     default_element: state.picture != '' ? <img src={state.picture} style={{ width: '120px', height: '120px', border: '1px solid' }} /> : '',
                     edit_element: <Upload
                         accept='.png,.jpg'
                         name="avatar"
                         listType="picture-card"
                         className="avatar-uploader"
                         showUploadList={false}
                         onChange={(info) => {
                             if (info.file.status === 'done') {
                                 getBase64(info.file.originFileObj, imageUrl => setState({ picture: imageUrl })
                                 );
                             }
                         }}
                     >
                         {
                             state.picture ?
                                 <img src={state.picture} alt="avatar" style={{ width: '100%' }} />
                                 :
                                 <div>
                                     <div className="ant-upload-text">{formatMessage({ id: 'upload image' })}</div>
                                 </div>
                         }
                     </Upload>
                 }, */
            ]
        )
    }

    function setState(values, callback = () => { }) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))

        callback({ ...state, ...values })
    }

    function updateData(callback = () => { }) {
        dispatch({
            type: 'course/e_updateCourse',
            payload: {
                isList: false,
                ...state,
                action: 'edit'
            }
        }).then((result) => {
            if (result) {
                setState({
                    ...result
                })
                callback()
            }
        })
    }

    function handleUpdate(data) {
        setState({
            loading: true
        })

        dispatch({
            type: 'course/e_updateRequirement',
            payload: {
                ...data,
                course_id: state.course_id
            }
        }).then((value) => {
            setState({
                ...value,
                loading: false
            })
        })
    }

    function renderSubCascader() {
        let list = []
        Object.values(state.subTagsList).map((st, stKey) => {
            if (st.main_tag_id == state.main_tag_id && state.subTagList.indexOf(st.id) < 0) {
                list.push({
                    value: st.id,
                    label: st.sub_tag,
                })
            }

        })
        return list
    }
    console.log(state.editData)
    return (
        <Tabs type="card">
            <Tabs.TabPane tab={formatMessage({ id: 'data' })} key="attr">
                <DataView {...state} title={loadTitle()} setState={setState} updateData={updateData} />
            </Tabs.TabPane>
            <Tabs.TabPane tab={formatMessage({ id: 'requirement' })} key="requirement">

                <Button
                    style={{ marginRight: '5px', marginBottom: '10px' }}
                    type="primary"
                    onClick={() => {
                        setState({
                            requirementModalVisible: true,
                            editData: {
                                mode: 'add',
                                editKey: 0
                            }
                        })
                    }}
                >
                    {
                        formatMessage({ id: 'add new requirement' })
                    }
                </Button>

                <RequirementModal
                    onClose={(result) => {
                        setState({
                            requirementModalVisible: result.visible,
                            ...result.value
                        })
                    }}
                    setState={setState}
                    {...({
                        ...state,
                        visible: state.requirementModalVisible
                    })}
                />

                <DataTable /*requestData={requestData}*/
                    {...({
                        ...state,
                        columnsList: [
                            {
                                title: 'qualification_name',
                                dataIndex: 'qualification_name',//follow by this key show data
                                key: 'qualification_name',
                                width: '20%'
                            },
                            {
                                title: 'description',
                                dataIndex: 'description',//follow by this key show data
                                key: 'description'
                            },
                            {
                                title: 'actions',
                                dataIndex: 'actions',
                                fixed: 'right',
                                render: (tags, listValue) => (
                                    <span>
                                        <Authorized authority={['admin', 'superb', 'school']}>
                                            <Popconfirm
                                                key={'delete'}
                                                title={formatMessage({ id: 'Are you sure?' })}
                                                onConfirm={() => handleUpdate({ qualification_id: listValue.qualification_id, action: 'delete' })}
                                                icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                            >
                                                <Tag color={'red'}>
                                                    {formatMessage({ id: 'delete' }).toUpperCase()}
                                                </Tag>
                                            </Popconfirm>

                                            <Tag color={'blue'}
                                                onClick={() => {
                                                    setState({
                                                        editData: {
                                                            editKey: listValue.requirementId,
                                                            mode: 'edit'
                                                        },
                                                        requirementModalVisible: true
                                                    })
                                                }}
                                            >
                                                {formatMessage({ id: 'edit' }).toUpperCase()}
                                            </Tag>

                                            <Popconfirm
                                                key={'activation'}
                                                title={formatMessage({ id: 'Are you sure?' })}
                                                onConfirm={() => handleUpdate({ qualification_id: listValue.qualification_id, action: 'activation' })}
                                                icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                            >
                                                {
                                                    listValue.active
                                                        ?
                                                        <Tag color={'orange'}>
                                                            {formatMessage({ id: 'hide info' }).toUpperCase()}
                                                        </Tag>
                                                        :
                                                        <Tag color={'orange'}>
                                                            {formatMessage({ id: 'show info' }).toUpperCase()}
                                                        </Tag>
                                                }
                                            </Popconfirm>

                                        </Authorized>
                                    </span>
                                ),
                            }
                        ]
                    })}
                    key={'requirement_id'}
                    setState={setState}
                />
            </Tabs.TabPane>
            <Tabs.TabPane tab={formatMessage({ id: 'description' })} key="description">
                <DescriptionTable
                    {...({
                        ...state,
                        related_id: state.course_id
                    })}
                />
            </Tabs.TabPane>

            <Tabs.TabPane tab={formatMessage({ id: 'major list' })} key="majorList">

                <Button
                    style={{ marginRight: '5px', marginBottom: '10px' }}
                    type="primary"
                    onClick={() =>
                        setState({
                            addTagModalVisible: true,
                            modalMode: 'add'
                        })
                    }
                >
                    {
                        formatMessage({ id: 'add sub tag' })
                    }
                </Button>

                <Modal
                    okButtonProps={{ disabled: state.sub_tag_id == 0 || state.main_tag_id == 0 }}
                    title={formatMessage({ id: state.modalMode == 'add' ? 'add study field and major' : 'setting study field and major' })}
                    visible={state.addTagModalVisible}
                    onOk={() => {
                        setState({
                            loading: true
                        })
                        dispatch({
                            type: 'config/e_editCourseSubTagList',
                            payload: {
                                action: state.modalMode,
                                course_id: state.course_id,
                                sub_tag_id: state.sub_tag_id
                            }
                        }).then((value) => {
                            setState({
                                ...value,
                                loading: false,
                                addTagModalVisible: false,
                                main_tag_id: 0,
                                sub_tag_id: 0
                            })
                        })
                    }}
                    onCancel={() => setState({ addTagModalVisible: false, main_tag_id: 0, sub_tag_id: 0 })}
                >
                    <Cascader
                        value={[state.main_tag_id]}
                        style={{ width: '100%' }}
                        options={
                            Object.values(state.mainTagsList).map((mt, mtKey) => {
                                return {
                                    value: mt.id,
                                    label: mt.main_tag,
                                }
                            })
                        }
                        onChange={(e) => setState({ main_tag_id: e[0], sub_tag_id: 0 })}
                        placeholder={formatMessage({ id: 'please select study field' })}
                    />
                    {
                        state.main_tag_id > 0 &&
                        <Cascader
                            value={[state.sub_tag_id]}
                            style={{ width: '100%' }}
                            options={renderSubCascader()}
                            onChange={(e) => setState({ sub_tag_id: e[0] })}
                            placeholder={formatMessage({ id: 'please select major' })}
                        />

                    }

                    {/* <Input value={state.sub_tag} onChange={(e) => setState({ sub_tag: e.target.value })}></Input> */}
                </Modal>

                <DataTable
                    columnsList={
                        [
                            {
                                title: 'sub_tag',
                                dataIndex: 'sub_tag',//follow by this key show data
                                key: 'sub_tag',
                                render: (tags, listValue) => (
                                    <span>
                                        {
                                            Object.values(state.subTagsList).map((st, stKey) => {
                                                if (st.id == listValue.sub_tag_id) {
                                                    return st.sub_tag
                                                }
                                            })
                                        }
                                    </span>
                                ),
                            },
                            {
                                title: 'main_tag',
                                dataIndex: 'main_tag',//follow by this key show data
                                key: 'main_tag',
                                render: (tags, listValue) => (
                                    <span>
                                        {
                                            Object.values(state.mainTagsList).map((st) => {
                                                if (st.id == listValue.main_tag_id) {
                                                    return st.main_tag
                                                }
                                            })

                                        }
                                    </span>
                                ),
                            },
                            {
                                title: 'actions',
                                dataIndex: 'actions',
                                fixed: 'right',
                                render: (tags, listValue) => (
                                    <span>
                                        <Authorized authority={['admin', 'superb']}>
                                            <Popconfirm
                                                key={'delete'}
                                                title={formatMessage({ id: 'Are you sure?' })}
                                                onConfirm={() => {
                                                    setState({
                                                        loading: true
                                                    })

                                                    dispatch({
                                                        type: 'config/e_editCourseSubTagList',
                                                        payload: {
                                                            sub_tag_id: listValue.sub_tag_id,
                                                            action: 'delete',
                                                            course_id: state.course_id,
                                                            school_major_id: listValue.id
                                                        }
                                                    }).then((value) => {
                                                        setState({
                                                            ...value,
                                                            loading: false
                                                        })
                                                    })
                                                }}
                                                icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                            >
                                                <Tag color={'red'}>
                                                    {formatMessage({ id: 'delete' }).toUpperCase()}
                                                </Tag>
                                            </Popconfirm>

                                        </Authorized>

                                        {/* <Tag
                                            key={'view'}
                                            color={'geekblue'}
                                            onClick={() =>
                                                setState({
                                                    addSubTagModalVisible: true,
                                                    modalMode: 'edit',
                                                    sub_tag: listValue.sub_tag,
                                                    sub_tag_id: listValue.id
                                                })
                                            }
                                        >
                                            {formatMessage({ id: 'edit' }).toUpperCase()}
                                        </Tag> */}
                                    </span>
                                ),
                            }
                        ]

                    }
                    dataList={state.subTagList}
                    key={'id'}
                    setState={setState}
                />
            </Tabs.TabPane>
        </Tabs>

    )
}