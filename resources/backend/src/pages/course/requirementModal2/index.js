import React, { useEffect, useState } from "react";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import { Radio, InputNumber, Divider, Button, Spin, Popconfirm, Modal, Drawer, Cascader, Select, Checkbox, Tag } from "antd";
import { QuestionCircleOutlined, PlusOutlined, CloseOutlined, DeleteOutlined } from '@ant-design/icons';
import './index.less'
export default (props) => {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        loading: false,
        mode: 'add',
        editKey: 0,
        qualification_id: 0,
        no_specific_requirement: 0,
        stepOneDone: false,
        addInsteadOrIncludedDrawerVisible: false,
        addInsteadOrIncludedDrawerMode: '',
        addInsteadOrIncludedDrawerKey: -1,
        drawerMode: '',
        qualificationGradeList: [],
        qualificationSubjectList: [],
        qualificationList: [],
        gradeTypeList: [],
        insteadGradeTypeList: [],


        submitedGrade: {},
        submitedIncludedAndInstead: [],
        submitedIncludedAndInsteadExtra: [],
        submitedWithoutSubject: [],

        includedSubjectId: 0,
        includedGradeId: 0,

        insteadFirstQualificatinId: 0,
        insteadFirstSubjectId: 0,
        insteadFirstGradeId: 0,
        insteadFirstGradeTypeId: 0,

        insteadSecondQualificatinId: 0,
        insteadSecondSubjectId: 0,
        insteadSecondGradeId: 0,
        insteadSecondGradeTypeId: 0,

        insteadQuantity: 1,
        gradeTypeId: 0,
    })

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }

    useEffect(() => {
        setState({ loading: true })
        dispatch({
            type: 'course/e_getRequirementAttr',
            payload: {
                course_id: props.course_id
            }
        }).then((value) => {
            if (value) {
                setState({
                    ...value,
                    loading: false
                })
            }
            //setState({ loading: false })
        })
    }, []);

    useEffect(() => {
        setState(props.editData)
    }, [props.editData]);

    useEffect(() => {
        if (state.editKey) {
            let requirementConditions = state.requirementConditions
            let requirementIncludedList = requirementConditions.requirementIncludedList
            let requirementIncludedInsteadList = requirementConditions.requirementIncludedInsteadList
            let requirementGradeList = requirementConditions.requirementGradeList
            let requirementList = requirementConditions.requirementList

            let submitedGrade = {}
            requirementGradeList.map((rg) => {
                if (rg.school_course_requirement_id == state.editKey) {
                    submitedGrade[rg.qualification_grade_id] = rg.qualification_grade_quantity
                }
            })

            let qualification_id = 0
            let no_specific_requirement = 0
            let gradeTypeId = 0
            requirementList.map((r) => {
                if (r.id == state.editKey) {
                    qualification_id = r.qualification_id
                    no_specific_requirement = r.no_specific_requirement
                    gradeTypeId = r.grade_type
                }
            })

            let submitedIncludedAndInstead = []
            let submitedWithoutSubject = []
            let submitedIncludedAndInsteadExtra = []

            requirementIncludedList.map((ri) => {
                if (ri.school_course_requirement_id == state.editKey) {
                    let record
                    requirementIncludedInsteadList.map((rii) => {
                        if (rii.school_course_requirement_included_id == ri.id) {
                            record = rii
                        }
                    })
                    if (ri.case_handling == 0) {
                        submitedWithoutSubject.push(ri.qualification_subject_id)
                    } else if (ri.case_handling == 3) {
                        submitedIncludedAndInsteadExtra.push({
                            insteadQuantity: ri.instead_quantity,
                            includedSubjectId: ri.qualification_subject_id,
                            includedGradeId: ri.qualification_grade_id,
                            insteadFirstQualificatinId: record && record.instead_qualification_id ? record.instead_qualification_id : 0,
                            insteadFirstSubjectId: record && record.instead_qualification_subject_id ? record.instead_qualification_subject_id : 0,
                            insteadFirstGradeId: record && record.instead_qualification_grade_id ? record.instead_qualification_grade_id : 0,
                            insteadFirstGradeTypeId: record && record.instead_grade_type ? record.instead_grade_type : 0,

                            insteadSecondQualificatinId: record && record.second_instead_qualification_id ? record.second_instead_qualification_id : 0,
                            insteadSecondSubjectId: record && record.second_instead_qualification_subject_id ? record.second_instead_qualification_subject_id : 0,
                            insteadSecondGradeId: record && record.second_instead_qualification_grade_id ? record.second_instead_qualification_grade_id : 0,
                            insteadSecondGradeTypeId: record && record.second_instead_grade_type ? record.second_instead_grade_type : 0
                        })
                    } else {
                        submitedIncludedAndInstead.push({
                            insteadQuantity: ri.instead_quantity,
                            includedSubjectId: ri.qualification_subject_id,
                            includedGradeId: ri.qualification_grade_id,
                            insteadFirstQualificatinId: record && record.instead_qualification_id ? record.instead_qualification_id : 0,
                            insteadFirstSubjectId: record && record.instead_qualification_subject_id ? record.instead_qualification_subject_id : 0,
                            insteadFirstGradeId: record && record.instead_qualification_grade_id ? record.instead_qualification_grade_id : 0,
                            insteadFirstGradeTypeId: record && record.instead_grade_type ? record.instead_grade_type : 0,

                            insteadSecondQualificatinId: record && record.second_instead_qualification_id ? record.second_instead_qualification_id : 0,
                            insteadSecondSubjectId: record && record.second_instead_qualification_subject_id ? record.second_instead_qualification_subject_id : 0,
                            insteadSecondGradeId: record && record.second_instead_qualification_grade_id ? record.second_instead_qualification_grade_id : 0,
                            insteadSecondGradeTypeId: record && record.second_instead_grade_type ? record.second_instead_grade_type : 0
                        })
                    }
                }
            })

            setState({
                submitedGrade,
                submitedIncludedAndInstead,
                submitedWithoutSubject,
                submitedIncludedAndInsteadExtra,
                stepOneDone: true,
                qualification_id,
                no_specific_requirement,
                gradeTypeId
            })

        }
    }, [state.editKey]);

    useEffect(() => {
        setState({ requirementConditions: props.requirementConditions })
    }, [props.requirementConditions]);


    function analysisDescription(r, requirementGradeList, requirementIncludedList, requirementIncludedInsteadList) {
        let qualificationScList = {}
        Object.values(state.qualificationList).map((q) => {
            qualificationScList[q.id] = q.qualification_name
        })

        let gradeScList = {}
        Object.values(state.qualificationGradeList).map((g) => {
            gradeScList[g.id] = g.grade
        })

        let subjectScList = {}
        Object.values(state.qualificationSubjectList).map((s) => {
            subjectScList[s.id] = s.subject_name
        })

        let gradeContent = []
        Object.values(requirementGradeList).map((rg) => {
            if (rg.school_course_requirement_id == r.id) {
                gradeContent.push(
                    <div style={{ width: '45px', padding: '5px' }}>
                        {rg.qualification_grade_quantity} <span style={{ color: 'red', fontWeight: 'bold' }}>{gradeScList[rg.qualification_grade_id]}</span>
                    </div>
                )
            }
        })

        let includedContent = []
        Object.values(requirementIncludedList).map((ri, rik) => {
            if (ri.school_course_requirement_id == r.id) {
                if (ri.case_handling == 0) {
                    includedContent.push(
                        <div>
                            • {formatMessage({ id: 'not included' })} {subjectScList[ri.qualification_subject_id]}
                        </div>
                    )
                } else {
                    includedContent.push(
                        <div>
                            • {formatMessage({ id: 'Designated subjects' })} {subjectScList[ri.qualification_subject_id]}
                        </div>
                    )

                    Object.values(requirementIncludedInsteadList).map((rii) => {
                        if (ri.id == rii.school_course_requirement_included_id) {
                            if (rii.instead_qualification_id) {
                                includedContent.push(
                                    <div>
                                        一 {formatMessage({ id: 'or replaced by' })} ({qualificationScList[rii.instead_qualification_id]}) {
                                            subjectScList[rii.instead_qualification_subject_id]
                                        } {gradeScList[rii.instead_qualification_grade_id]}
                                    </div>
                                )
                            }

                            if (rii.second_instead_qualification_id) {
                                includedContent.push(
                                    <div>
                                        一 {formatMessage({ id: 'or replaced by' })} ({qualificationScList[rii.second_instead_qualification_id]}) {
                                            subjectScList[rii.second_instead_qualification_subject_id]
                                        } {gradeScList[rii.second_instead_qualification_grade_id]}
                                    </div>
                                )
                            }
                        }
                    })
                }

            }
        })
        return [
            <div style={{ display: 'flex' }}>
                {gradeContent}
            </div>,
            includedContent
        ]
    }

    useEffect(() => {
        if (state.requirementConditions) {
            let requirementConditions = state.requirementConditions

            let requirementList = requirementConditions.requirementList
            let requirementIncludedList = requirementConditions.requirementIncludedList
            let requirementIncludedInsteadList = requirementConditions.requirementIncludedInsteadList
            let requirementGradeList = requirementConditions.requirementGradeList

            let dataList = []
            requirementList.map((r) => {
                dataList.push({
                    requirementId: r.id,
                    qualification_id: r.qualification_id,
                    qualification_name: Object.values(state.qualificationList).map((q) => {
                        if (q.id == r.qualification_id) {
                            return q.qualification_name
                        }
                    }),
                    description: analysisDescription(r, requirementGradeList, requirementIncludedList, requirementIncludedInsteadList),

                })
            })

            props.setState({ dataList })
        }
    }, [state.requirementConditions]);

    function checkRemainingSubmitedGrade() {
        let submitedGrade = { ...state.submitedGrade }
        state.submitedIncludedAndInstead.forEach(r => {
            submitedGrade[r.includedGradeId]--
        });

        let gradeList = []
        Object.keys(submitedGrade).map((g) => {
            if (submitedGrade[g] > 0) {
                gradeList.push(g)
            }
        })
        return gradeList
    }
    function renderComponent() {
        return <React.Fragment>
            <Drawer
                width={640}
                placement='right'
                closable={false}
                onClose={() => setState({ addInsteadOrIncludedDrawerVisible: false })}
                visible={state.addInsteadOrIncludedDrawerVisible}
                key='right'
            >
                <div style={{ display: 'flex', fontSize: '20px' }}>
                    <Divider orientation="left" style={{ margin: 0, marginBottom: '5px' }}>
                        {formatMessage({ id: 'select appointed subject' })}
                    </Divider>

                    <Popconfirm
                        placement="bottomRight"
                        title={formatMessage({ id: 'Are you sure?' })}
                        onConfirm={() => {
                            setState({
                                includedSubjectId: 0,
                                includedGradeId: 0
                            })
                        }}
                        okText="Yes"
                        cancelText="No"
                    >
                        <Button
                            disabled={!state.includedSubjectId && !state.includedGradeId}
                            type='link'
                            style={{
                                fontSize: '20px',
                                padding: '0'
                            }}
                        >
                            <DeleteOutlined />
                        </Button>
                    </Popconfirm>
                </div>
                <Select
                    value={
                        state.includedSubjectId ? state.includedSubjectId : ''
                    }
                    style={{ width: '100%' }}
                    onChange={(e) => {
                        setState({ includedSubjectId: e })
                    }}
                >
                    {
                        Object.values(state.qualificationSubjectList).map((s, sk) => {
                            if (s.qualification_id == state.qualification_id) {
                                return <Select.Option key={sk} value={s.id}>{s.subject_name}</Select.Option>
                            }
                        })
                    }
                </Select>

                <Select
                    stlye={{ marginTop: '5px' }}
                    value={state.includedGradeId ? state.includedGradeId : ''}
                    style={{ width: '100%' }}
                    onChange={(e) => {
                        setState({ includedGradeId: e })
                    }}
                >
                    {
                        state.drawerMode != 'extra' &&
                        checkRemainingSubmitedGrade().map((sg, sgk) => {
                            return Object.values(state.qualificationGradeList).map((g) => {
                                if (g.id == sg) {
                                    return <Select.Option key={sgk} value={sg}>
                                        <div style={{ display: 'flex' }}>
                                            <div style={{ marginRight: '10px', width: '20px' }}>
                                                {g.grade}
                                            </div>
                                            {
                                                g.score != null &&
                                                <div style={{ marginRight: '10px' }}>
                                                    ({formatMessage({ id: 'score' }) + ': ' + g.score})
                                                        </div>
                                            }
                                            {
                                                g.cgpa != null &&
                                                <div>
                                                    (CGPA: {g.cgpa})
                                                </div>
                                            }

                                        </div>
                                    </Select.Option>
                                }
                            })
                        })
                    }

                    {
                        state.drawerMode == 'extra' &&
                        Object.values(state.qualificationGradeList).map((g, gk) => {
                            if (g.qualification_id == state.qualification_id) {
                                return <Select.Option key={gk} value={g.id}>
                                    <div style={{ display: 'flex' }}>
                                        <div style={{ marginRight: '10px', width: '20px' }}>
                                            {g.grade}
                                        </div>
                                        {
                                            g.score != null &&
                                            <div style={{ marginRight: '10px' }}>
                                                ({formatMessage({ id: 'score' }) + ': ' + g.score})
                                                    </div>
                                        }
                                        {
                                            g.cgpa != null &&
                                            <div>
                                                (CGPA: {g.cgpa})
                                            </div>
                                        }

                                    </div>
                                </Select.Option>
                            }
                        })
                    }
                </Select>

                {
                    state.includedGradeId > 0 && state.includedSubjectId > 0 &&
                    <React.Fragment>
                        <div style={{ display: 'flex', marginTop: '5px' }}>
                            <div style={{ fontWeight: 'bold', marginRight: '5px' }}>
                                {formatMessage({ id: 'allowed' })}:
                            </div>

                            <div style={{ display: 'flex', alignItems: 'center', border: '1px solid', flexFlow: 'row wrap' }}>
                                {checkValidIncludedAndInsteadGrade(state.gradeTypeId, state.includedGradeId, state.qualification_id)}
                            </div>
                        </div>
                        <Divider style={{ margin: '20px 0', backgroundColor: '#1890ff' }} />
                        <div style={{ display: 'flex', fontSize: '20px' }}>
                            <Divider orientation="left" style={{ fontSize: '20px', margin: 0, marginBottom: '5px' }}>
                                {formatMessage({ id: 'or replaced by' })}
                            </Divider>

                            <Popconfirm
                                placement="bottomRight"
                                title={formatMessage({ id: 'Are you sure?' })}
                                onConfirm={() => {
                                    setState({
                                        insteadFirstQualificatinId: 0,
                                        insteadFirstSubjectId: 0,
                                        insteadFirstGradeId: 0,
                                        insteadFirstGradeTypeId: 0
                                    })
                                }}
                                okText="Yes"
                                cancelText="No"
                            >
                                <Button
                                    disabled={!state.insteadFirstQualificatinId && !state.insteadFirstSubjectId && !state.insteadFirstGradeId}
                                    type='link'
                                    style={{
                                        fontSize: '20px',
                                        padding: '0'
                                    }}
                                >
                                    <DeleteOutlined />
                                </Button>
                            </Popconfirm>
                        </div>

                        <Select
                            value={
                                state.insteadFirstQualificatinId ? state.insteadFirstQualificatinId : ''
                            }
                            style={{ width: '100%' }}
                            onChange={(e) => {
                                setState({
                                    insteadFirstQualificatinId: e,
                                    insteadFirstSubjectId: 0,
                                    insteadFirstGradeId: 0,
                                })
                            }}
                        >
                            {
                                Object.values(state.qualificationList).map((q, qk) => {
                                    return <Select.Option key={qk} value={q.id}>{q.qualification_name}</Select.Option>
                                })
                            }
                        </Select>

                        <Select
                            value={
                                state.insteadFirstSubjectId ? state.insteadFirstSubjectId : ''
                            }
                            style={{ width: '100%' }}
                            onChange={(e) => {
                                setState({ insteadFirstSubjectId: e })
                            }}
                        >
                            {
                                Object.values(state.qualificationSubjectList).map((s, sk) => {
                                    if (s.qualification_id == state.insteadFirstQualificatinId) {
                                        return <Select.Option key={sk} value={s.id}>{s.subject_name}</Select.Option>
                                    }
                                })
                            }
                        </Select>

                        <Select
                            stlye={{ marginTop: '5px' }}
                            value={state.insteadFirstGradeId ? state.insteadFirstGradeId : ''}
                            style={{ width: '100%' }}
                            onChange={(e) => {
                                setState({ insteadFirstGradeId: e })
                            }}
                        >
                            {
                                Object.values(state.qualificationGradeList).map((g, gk) => {
                                    if (g.qualification_id == state.insteadFirstQualificatinId) {
                                        return <Select.Option key={gk} value={g.id}>
                                            <div style={{ display: 'flex' }}>
                                                <div style={{ marginRight: '10px', width: '20px' }}>
                                                    {g.grade}
                                                </div>
                                                {
                                                    g.score != null &&
                                                    <div style={{ marginRight: '10px' }}>
                                                        ({formatMessage({ id: 'score' }) + ': ' + g.score})
                                                        </div>
                                                }
                                                {
                                                    g.cgpa != null &&
                                                    <div>
                                                        (CGPA: {g.cgpa})
                                                </div>
                                                }

                                            </div>
                                        </Select.Option>
                                    }
                                })
                            }
                        </Select>

                        <Radio.Group
                            onChange={(e) => setState({ insteadFirstGradeTypeId: e.target.value })}
                            style={{ display: 'flex', flexWrap: 'wrap', paddingLeft: '0' }}
                            value={state.insteadFirstGradeTypeId}
                        >
                            {
                                Object.keys(state.insteadGradeTypeList).map((key, arrKey) => {
                                    return (
                                        <Radio
                                            style={{ flex: '0 0 40%' }}
                                            key={arrKey}
                                            value={parseInt(key)}
                                        >
                                            {formatMessage({ id: state.insteadGradeTypeList[key] })}
                                        </Radio>
                                    )
                                })
                            }
                        </Radio.Group>

                        {
                            state.insteadFirstQualificatinId > 0 &&
                            state.insteadFirstSubjectId > 0 &&
                            state.insteadFirstGradeId > 0 &&
                            state.insteadFirstGradeTypeId > 0 &&
                            <React.Fragment>
                                <div style={{ display: 'flex', marginTop: '5px' }}>
                                    <div style={{ fontWeight: 'bold', marginRight: '5px' }}>
                                        {formatMessage({ id: 'allowed' })}:
                                    </div>

                                    <div style={{ display: 'flex', alignItems: 'center', border: '1px solid', flexFlow: 'row wrap' }}>
                                        {
                                            checkValidIncludedAndInsteadGrade(
                                                state.insteadFirstGradeTypeId,
                                                state.insteadFirstGradeId,
                                                state.insteadFirstQualificatinId,
                                                state.insteadGradeTypeList
                                            )
                                        }
                                    </div>
                                </div>
                                <Divider style={{ margin: '20px 0', backgroundColor: '#1890ff' }} />

                                <div style={{ display: 'flex', fontSize: '20px' }}>
                                    <Divider orientation="left" style={{ fontSize: '20px', margin: 0, marginBottom: '5px' }}>
                                        {formatMessage({ id: 'or replaced by' })}
                                    </Divider>

                                    <Popconfirm
                                        placement="bottomRight"
                                        title={formatMessage({ id: 'Are you sure?' })}
                                        onConfirm={() => {
                                            setState({
                                                insteadSecondQualificatinId: 0,
                                                insteadSecondSubjectId: 0,
                                                insteadSecondGradeId: 0,
                                                insteadSecondGradeTypeId: 0
                                            })
                                        }}
                                        okText="Yes"
                                        cancelText="No"
                                    >
                                        <Button
                                            disabled={!state.insteadSecondQualificatinId && !state.insteadSecondSubjectId && !state.insteadSecondGradeId}
                                            type='link'
                                            style={{
                                                fontSize: '20px',
                                                padding: '0'
                                            }}
                                        >
                                            <DeleteOutlined />
                                        </Button>
                                    </Popconfirm>
                                </div>
                                <Select
                                    value={
                                        state.insteadSecondQualificatinId ? state.insteadSecondQualificatinId : ''
                                    }
                                    style={{ width: '100%' }}
                                    onChange={(e) => {
                                        setState({
                                            insteadSecondQualificatinId: e,
                                            insteadSecondSubjectId: 0,
                                            insteadSecondGradeId: 0,
                                        })
                                    }}
                                >
                                    {
                                        Object.values(state.qualificationList).map((q, qk) => {
                                            return <Select.Option key={qk} value={q.id}>{q.qualification_name}</Select.Option>
                                        })
                                    }
                                </Select>

                                <Select
                                    value={
                                        state.insteadSecondSubjectId ? state.insteadSecondSubjectId : ''
                                    }
                                    style={{ width: '100%' }}
                                    onChange={(e) => {
                                        setState({ insteadSecondSubjectId: e })
                                    }}
                                >
                                    {
                                        Object.values(state.qualificationSubjectList).map((s, sk) => {
                                            if (s.qualification_id == state.insteadSecondQualificatinId) {
                                                return <Select.Option key={sk} value={s.id}>{s.subject_name}</Select.Option>
                                            }
                                        })
                                    }
                                </Select>

                                <Select
                                    stlye={{ marginTop: '5px' }}
                                    value={state.insteadSecondGradeId ? state.insteadSecondGradeId : ''}
                                    style={{ width: '100%' }}
                                    onChange={(e) => {
                                        setState({ insteadSecondGradeId: e })
                                    }}
                                >
                                    {
                                        Object.values(state.qualificationGradeList).map((g, gk) => {
                                            if (g.qualification_id == state.insteadSecondQualificatinId) {
                                                return <Select.Option key={gk} value={g.id}>
                                                    <div style={{ display: 'flex' }}>
                                                        <div style={{ marginRight: '10px', width: '20px' }}>
                                                            {g.grade}
                                                        </div>
                                                        {
                                                            g.score != null &&
                                                            <div style={{ marginRight: '10px' }}>
                                                                ({formatMessage({ id: 'score' }) + ': ' + g.score})
                                                        </div>
                                                        }
                                                        {
                                                            g.cgpa != null &&
                                                            <div>
                                                                (CGPA: {g.cgpa})
                                                        </div>
                                                        }

                                                    </div>
                                                </Select.Option>
                                            }
                                        })
                                    }
                                </Select>

                                <Radio.Group
                                    onChange={(e) => setState({ insteadSecondGradeTypeId: e.target.value })}
                                    style={{ display: 'flex', flexWrap: 'wrap', paddingLeft: '0' }}
                                    value={state.insteadSecondGradeTypeId}
                                >
                                    {
                                        Object.keys(state.insteadGradeTypeList).map((key, arrKey) => {
                                            return (
                                                <Radio
                                                    style={{ flex: '0 0 40%' }}
                                                    key={arrKey}
                                                    value={parseInt(key)}
                                                >
                                                    {formatMessage({ id: state.insteadGradeTypeList[key] })}
                                                </Radio>
                                            )
                                        })
                                    }
                                </Radio.Group>
                                {
                                    state.insteadSecondQualificatinId > 0 &&
                                    state.insteadSecondSubjectId > 0 &&
                                    state.insteadSecondGradeTypeId > 0 &&
                                    state.insteadSecondGradeId > 0 &&
                                    <div style={{ display: 'flex', marginTop: '5px' }}>
                                        <div style={{ fontWeight: 'bold', marginRight: '5px' }}>
                                            {formatMessage({ id: 'allowed' })}:
                                    </div>

                                        <div style={{ display: 'flex', alignItems: 'center', border: '1px solid', flexFlow: 'row wrap' }}>
                                            {
                                                checkValidIncludedAndInsteadGrade(
                                                    state.insteadSecondGradeTypeId,
                                                    state.insteadSecondGradeId,
                                                    state.insteadSecondQualificatinId,
                                                    state.insteadGradeTypeList
                                                )
                                            }
                                        </div>
                                    </div>
                                }

                            </React.Fragment>
                        }

                        {
                            state.insteadFirstQualificatinId > 0 &&
                            state.insteadFirstSubjectId > 0 &&
                            state.insteadFirstGradeId > 0 &&
                            <React.Fragment>
                                <Divider style={{ margin: '20px 0', backgroundColor: '#1890ff' }} />
                                <div style={{ display: 'flex', alignItems: 'center', marginTop: '5px' }}>
                                    <InputNumber
                                        value={state.insteadQuantity}
                                        onChange={(e) => {
                                            let count = 2 + (
                                                state.insteadSecondQualificatinId > 0 && state.insteadSecondSubjectId > 0 && state.insteadSecondGradeId > 0
                                                    ? 1
                                                    : 0
                                            )
                                            if (e > 0 && e <= count) {

                                                setState({ insteadQuantity: parseInt(e) })
                                            }
                                        }}
                                    />

                                    <div style={{ border: '1px solid red', color: 'red', padding: '15px 15px', marginLeft: '8px' }}>
                                        {
                                            formatMessage(
                                                {
                                                    id: 'student must achieve (insteadQuantity) out of (submitedIncludedAndInstead) in above conditions'
                                                },
                                                {
                                                    insteadQuantity: state.insteadQuantity,
                                                    submitedIncludedAndInstead: 2 + (
                                                        state.insteadSecondQualificatinId > 0 && state.insteadSecondSubjectId > 0 && state.insteadSecondGradeId > 0
                                                            ? 1
                                                            : 0
                                                    )
                                                }
                                            )
                                        }
                                    </div>

                                </div>

                            </React.Fragment>
                        }

                        <Button
                            disabled={checkIncludedInstead()}
                            type='primary'
                            style={{ marginTop: '30px', marginRight: '10px' }}
                            onClick={() => {
                                let submitedIncludedAndInstead = state.submitedIncludedAndInstead
                                let dataKeyList = 'submitedIncludedAndInstead'
                                if (state.drawerMode == 'extra') {
                                    submitedIncludedAndInstead = state.submitedIncludedAndInsteadExtra
                                    dataKeyList = 'submitedIncludedAndInsteadExtra'
                                }

                                let data = {}
                                data.includedSubjectId = state.includedSubjectId
                                data.includedGradeId = state.includedGradeId

                                data.insteadFirstQualificatinId = state.insteadFirstQualificatinId
                                data.insteadFirstSubjectId = state.insteadFirstSubjectId
                                data.insteadFirstGradeId = state.insteadFirstGradeId
                                data.insteadFirstGradeTypeId = state.insteadFirstGradeTypeId

                                data.insteadSecondQualificatinId = state.insteadSecondQualificatinId
                                data.insteadSecondSubjectId = state.insteadSecondSubjectId
                                data.insteadSecondGradeId = state.insteadSecondGradeId
                                data.insteadSecondGradeTypeId = state.insteadSecondGradeTypeId

                                data.insteadQuantity = state.insteadQuantity
                                submitedIncludedAndInstead.push(data)
                                setState({
                                    [dataKeyList]: submitedIncludedAndInstead,
                                    addInsteadOrIncludedDrawerVisible: false,
                                    includedSubjectId: 0,
                                    includedGradeId: 0,
                                    insteadFirstQualificatinId: 0,
                                    insteadFirstSubjectId: 0,
                                    insteadFirstGradeId: 0,
                                    insteadFirstGradeTypeId: 0,
                                    insteadSecondQualificatinId: 0,
                                    insteadSecondSubjectId: 0,
                                    insteadSecondGradeId: 0,
                                    insteadSecondGradeTypeId: 0,
                                    insteadQuantity: 1
                                })
                            }}
                        >
                            {formatMessage({ id: 'submit' })}
                        </Button>
                    </React.Fragment>
                }
                <Button
                    style={{ marginTop: '30px' }}
                    onClick={() => {
                        setState({
                            addInsteadOrIncludedDrawerVisible: false,
                            includedSubjectId: 0,
                            includedGradeId: 0,
                            insteadFirstQualificatinId: 0,
                            insteadFirstSubjectId: 0,
                            insteadFirstGradeId: 0,
                            insteadFirstGradeTypeId: 0,
                            insteadSecondQualificatinId: 0,
                            insteadSecondSubjectId: 0,
                            insteadSecondGradeId: 0,
                            insteadSecondGradeTypeId: 0,
                            insteadQuantity: 1
                        })
                    }}
                >
                    {formatMessage({ id: 'cancel' })}
                </Button>
            </Drawer>
        </React.Fragment >
    }

    function checkIncludedInstead() {
        let disabled = true
        if (state.includedSubjectId && state.includedGradeId) {
            disabled = false
        }

        if (
            state.insteadFirstQualificatinId ||
            state.insteadFirstSubjectId ||
            state.insteadFirstGradeId ||
            state.insteadFirstGradeTypeId
        ) {
            disabled = true
            if (
                state.insteadFirstQualificatinId &&
                state.insteadFirstSubjectId &&
                state.insteadFirstGradeId &&
                state.insteadFirstGradeTypeId
            ) {
                disabled = false
            }
        }

        if (
            state.insteadSecondQualificatinId ||
            state.insteadSecondSubjectId ||
            state.insteadSecondGradeId ||
            state.insteadSecondGradeTypeId
        ) {
            disabled = true
            if (
                state.insteadSecondQualificatinId &&
                state.insteadSecondSubjectId &&
                state.insteadSecondGradeId &&
                state.insteadSecondGradeTypeId
            ) {
                disabled = false
            }
        }

        return disabled
    }

    function updateSubmitedGrade(value, key) {
        let submitedGrade = state.submitedGrade
        if (parseInt(value) >= 0) {
            submitedGrade[key] = parseInt(value)
            setState({ submitedGrade })
        }
    }

    function checkStepOneSave() {
        let allow = true
        Object.keys(state.submitedGrade).map((g) => {
            if (state.submitedGrade[g] > 0) {
                allow = false
            }
        })
        return allow || Object.keys(state.submitedGrade).length <= 0 || state.gradeTypeId <= 0
    }

    function checkValidIncludedAndInsteadGrade(related_grade_type_id, related_grade_id, related_qualification_id, related_grade_type_list = state.gradeTypeList, related_grade_list = state.qualificationGradeList) {
        let score = []
        let cgpa = []
        let ownScore
        let ownCGPA
        let scoreValid = false

        related_grade_list.forEach(g => {
            if (related_grade_id == g.id) {
                ownScore = parseFloat(g.score)
                ownCGPA = parseFloat(g.cgpa)
            }
        })

        related_grade_list.forEach(g => {
            g.score = parseFloat(g.score)
            g.cgpa = parseFloat(g.cgpa)
            if (
                (
                    related_grade_type_list[related_grade_type_id] == "grade score more than" ||
                    related_grade_type_list[related_grade_type_id] == "score more than"
                ) &&
                ownScore <= g.score &&
                g.qualification_id == related_qualification_id
            ) {
                score.push(
                    <div style={{ width: '50px', textAlign: 'center', borderRight: '1px solid grey' }}>
                        {g.grade}
                    </div>
                )
                scoreValid = true
            } else if (
                (
                    related_grade_type_list[related_grade_type_id] == "grade score less than" ||
                    related_grade_type_list[related_grade_type_id] == "score less than"
                ) &&
                ownScore >= g.score &&
                g.qualification_id == related_qualification_id
            ) {
                score.push(
                    <div style={{ width: '50px', textAlign: 'center', borderRight: '1px solid grey' }}>
                        {g.grade}
                    </div>
                )
                scoreValid = true
            } else if (
                (
                    related_grade_type_list[related_grade_type_id] == "grade cgpa more than" ||
                    related_grade_type_list[related_grade_type_id] == "cgpa more than"
                ) &&
                ownCGPA <= g.cgpa &&
                g.qualification_id == related_qualification_id
            ) {

                cgpa.push(
                    <div style={{ width: '50px', textAlign: 'center', borderRight: '1px solid grey' }}>
                        {g.grade}
                    </div>
                )
            } else if (
                (
                    related_grade_type_list[related_grade_type_id] == "grade cgpa less than" ||
                    related_grade_type_list[related_grade_type_id] == "cgpa less than"
                ) &&
                ownCGPA >= g.cgpa &&
                g.qualification_id == related_qualification_id
            ) {
                cgpa.push(
                    <div style={{ width: '50px', textAlign: 'center', borderRight: '1px solid grey' }}>
                        {g.grade}
                    </div>
                )
            }
        })

        if (scoreValid) {
            return score
        }
        return cgpa
    }

    function calculateSelectedGradeScoreCGPA(related_id = state.gradeTypeId) {
        let cgpa = 0
        let score = 0
        Object.keys(state.submitedGrade).map((sg) => {
            if (state.submitedGrade[sg] > 0) {
                state.qualificationGradeList.forEach(g => {
                    if (sg == g.id) {
                        cgpa += parseFloat(g.cgpa) * state.submitedGrade[sg]
                        score += parseFloat(g.score) * state.submitedGrade[sg]
                    }
                })
            }
        })
        if (
            state.gradeTypeList[related_id] == 'grade score more than' ||
            state.gradeTypeList[related_id] == 'score more than' ||
            state.gradeTypeList[related_id] == 'score less than' ||
            state.gradeTypeList[related_id] == 'grade score less than'
        ) {
            return <React.Fragment>
                {formatMessage({ id: state.gradeTypeList[related_id] })} <span style={{ color: 'red', fontWeight: 'bold' }}>{score}</span> {formatMessage({ id: 'score' })}
            </React.Fragment>
        } else {
            return <React.Fragment>
                {formatMessage({ id: state.gradeTypeList[related_id] })} <span style={{ color: 'red', fontWeight: 'bold' }}>{cgpa}</span> {formatMessage({ id: 'cgpa' })}
            </React.Fragment>
        }
    }
    console.log(state)
    return (
        <div>
            <Modal
                maskClosable={false}
                title={formatMessage({ id: state.mode == 'add' ? 'add new requirement' : 'setting requirement' })}
                visible={props.visible}
                okButtonProps={{
                    disabled: state.qualification_id <= 0 || (checkStepOneSave() && state.no_specific_requirement <= 0)
                }}
                onOk={() => {
                    setState({ loading: true })
                    dispatch({
                        type: 'course/e_submitRequirement',
                        payload: {
                            submitedGrade: state.submitedGrade,
                            submitedIncludedAndInstead: state.submitedIncludedAndInstead,
                            submitedIncludedAndInsteadExtra: state.submitedIncludedAndInsteadExtra,
                            submitedWithoutSubject: state.submitedWithoutSubject,
                            gradeTypeId: state.gradeTypeId,
                            qualification_id: state.qualification_id,
                            no_specific_requirement: state.no_specific_requirement,
                            action: state.mode,
                            course_id: props.course_id,
                            editKey: state.editKey
                        }
                    }).then((value) => {
                        if (value) {
                            setState({
                                ...value,
                                loading: false,
                                qualification_id: 0,
                                no_specific_requirement: 0,
                                stepOneDone: false,
                                addInsteadOrIncludedDrawerVisible: false,
                                addInsteadOrIncludedDrawerMode: '',
                                addInsteadOrIncludedDrawerKey: -1,
                                drawerMode: '',
                                submitedGrade: {},
                                submitedIncludedAndInstead: [],
                                submitedIncludedAndInsteadExtra: [],
                                submitedWithoutSubject: [],

                                includedSubjectId: 0,
                                includedGradeId: 0,

                                insteadFirstQualificatinId: 0,
                                insteadFirstSubjectId: 0,
                                insteadFirstGradeId: 0,
                                insteadFirstGradeTypeId: 0,

                                insteadSecondQualificatinId: 0,
                                insteadSecondSubjectId: 0,
                                insteadSecondGradeId: 0,
                                insteadSecondGradeTypeId: 0,

                                insteadQuantity: 1,
                                gradeTypeId: 0,
                            })
                            props.onClose({
                                visible: false
                            })
                        }
                        //setState({ loading: false })
                    })
                }}
                onCancel={() => {
                    setState({
                        qualification_id: 0,
                        no_specific_requirement: 0,
                        stepOneDone: false,
                        addInsteadOrIncludedDrawerVisible: false,
                        addInsteadOrIncludedDrawerMode: '',
                        addInsteadOrIncludedDrawerKey: -1,
                        drawerMode: '',
                        submitedGrade: {},
                        submitedIncludedAndInstead: [],
                        submitedIncludedAndInsteadExtra: [],
                        submitedWithoutSubject: [],

                        includedSubjectId: 0,
                        includedGradeId: 0,

                        insteadFirstQualificatinId: 0,
                        insteadFirstSubjectId: 0,
                        insteadFirstGradeId: 0,
                        insteadFirstGradeTypeId: 0,

                        insteadSecondQualificatinId: 0,
                        insteadSecondSubjectId: 0,
                        insteadSecondGradeId: 0,
                        insteadSecondGradeTypeId: 0,

                        insteadQuantity: 1,
                        gradeTypeId: 0,
                        editKey: 0
                    })
                    props.onClose({
                        visible: false
                    })
                }}
            >
                <div
                    style={
                        state.stepOneDone
                            ? {
                                filter: 'blur(1px)',
                                pointerEvents: 'none'
                            }
                            : {}
                    }
                >
                    <Divider orientation="left" style={{ fontSize: '20px', margin: 0, marginBottom: '5px' }}>
                        {formatMessage({ id: 'step 1' })}
                    </Divider>
                    <Cascader
                        value={[state.qualification_id]}
                        style={{ width: '100%', marginBottom: '5px' }}
                        options={
                            Object.values(state.qualificationList).map((value, key) => {
                                return {
                                    value: value.id,
                                    label: value.qualification_name,
                                }
                            })
                        }
                        onChange={(e) => {
                            setState({
                                qualification_id: e[0]
                            })
                        }}
                        placeholder={formatMessage({ id: 'please select qualification' })}
                    />
                    <Checkbox checked={state.no_specific_requirement} onChange={(e) => setState({ no_specific_requirement: e.target.checked })}>{formatMessage({ id: 'no specific requirement' })}</Checkbox>
                    {
                        !state.no_specific_requirement && state.qualification_id > 0 &&
                        <div style={{ display: 'flex', flexFlow: 'row wrap' }}>
                            {
                                Object.values(state.qualificationGradeList).map((g, gk) => {
                                    if (g.qualification_id == state.qualification_id) {
                                        return <div
                                            style={{
                                                flex: '0 0 48%',
                                                display: 'flex',
                                                //justifyContent: 'space-between',
                                                alignItems: 'center',
                                                marginRight: '11px'
                                            }}
                                        >
                                            <div
                                                style={{ display: 'flex', alignItems: 'center' }}
                                            >
                                                <InputNumber
                                                    value={
                                                        state.submitedGrade[g.id] ? state.submitedGrade[g.id] : 0
                                                    }
                                                    style={{ width: '70px', marginRight: '5px' }}
                                                    onChange={(e) => updateSubmitedGrade(e, g.id)}
                                                />
                                                <div
                                                    style={{
                                                        color: 'red',
                                                        width: '25px',
                                                    }}
                                                >
                                                    {g.grade}
                                                </div>
                                            </div>
                                            <div style={{ display: 'flex' }}>
                                                {
                                                    g.score != null &&
                                                    <div style={{ marginRight: '10px' }}>
                                                        {'(' + formatMessage({ id: 'score' }) + ': ' + g.score + ')'}
                                                    </div>
                                                }
                                                {
                                                    g.cgpa != null &&
                                                    <div>
                                                        {'(CGPA: ' + g.cgpa + ')'}
                                                    </div>
                                                }

                                            </div>

                                        </div>
                                    }
                                })
                            }
                        </div>
                    }

                    {
                        state.qualification_id > 0 && !state.no_specific_requirement &&
                        <React.Fragment>
                            <Divider
                                orientation="left"
                                style={{ margin: 'unset', marginTop: '7px' }}
                            >
                                {formatMessage({ id: 'please select grade type' })}
                            </Divider>

                            <Radio.Group
                                onChange={(e) => setState({ gradeTypeId: e.target.value })}
                                style={{ display: 'flex', flexWrap: 'wrap', paddingLeft: '0' }}
                                value={state.gradeTypeId}
                            >
                                {
                                    Object.keys(state.gradeTypeList).map((key, arrKey) => {
                                        return (
                                            <Radio
                                                style={{ flex: '0 0 40%' }}
                                                key={arrKey}
                                                value={parseInt(key)}
                                            >
                                                {formatMessage({ id: state.gradeTypeList[key] })}
                                            </Radio>
                                        )
                                    })
                                }
                            </Radio.Group>
                        </React.Fragment>
                    }


                    {
                        !state.stepOneDone && !state.no_specific_requirement &&
                        <Button
                            disabled={checkStepOneSave()}
                            style={{ width: '100%', marginTop: '5px' }}
                            type="primary"
                            onClick={() => {
                                setState({ stepOneDone: !state.stepOneDone })
                            }}
                        >
                            {formatMessage({ id: 'save' })}
                        </Button>
                    }
                </div>

                {
                    state.stepOneDone &&
                    <React.Fragment>
                        <Popconfirm
                            placement="top"
                            title={formatMessage({ id: 'by editing step 1 may clear all data in step 2. are you sure to edit in step 1 ?' })}
                            onConfirm={() => {
                                setState({ submitedIncludedAndInstead: [], stepOneDone: false })
                            }}
                            okText="Yes"
                            cancelText="No"
                        >
                            <Button
                                style={{ width: '100%', marginTop: '5px' }}
                                type="primary"
                            >
                                {formatMessage({ id: 'edit step 1' })}
                            </Button>
                        </Popconfirm>

                        <Divider orientation="left" style={{ fontSize: '20px', margin: 0, marginBottom: '5px' }}>
                            {formatMessage({ id: 'step 2' })}
                        </Divider>

                        <div style={{ display: 'flex' }}>
                            <div style={{ fontWeight: 'bold', marginRight: '5px' }}>
                                {formatMessage({ id: 'selected' })}:
                            </div>

                            <div style={{ display: 'flex', padding: '0px 10px', alignItems: 'center', border: '1px solid', flexFlow: 'row wrap' }}>
                                {
                                    Object.keys(state.submitedGrade).map((sg, sgk) => {
                                        if (state.submitedGrade[sg] > 0) {
                                            return Object.values(state.qualificationGradeList).map((g, gk) => {
                                                if (sg == g.id) {
                                                    return <div style={{ width: '60px', /* flex: '1', */ /* textAlign: 'center' */ }} key={sgk}>
                                                        {state.submitedGrade[sg]} <span style={{ color: 'red', fontWeight: 'bold' }}>{g.grade}</span>
                                                    </div>
                                                }
                                            })
                                        }

                                    })
                                }
                            </div>
                        </div>

                        <div style={{ display: 'flex', marginTop: '5px' }}>
                            <div style={{ fontWeight: 'bold', marginRight: '5px' }}>
                                {formatMessage({ id: 'conditions' })}:
                            </div>

                            <div style={{ display: 'flex', padding: '0px 10px', alignItems: 'center', border: '1px solid', flexFlow: 'row wrap' }}>
                                <div>
                                    {calculateSelectedGradeScoreCGPA()}
                                </div>
                            </div>
                        </div>

                        <Divider style={{ textAlign: 'center', fontWeight: 'bold', fontSize: '17px' }}>
                            {formatMessage({ id: 'add for without calculation subject' })}
                        </Divider>

                        <Select
                            value={''}
                            style={{ width: '100%' }}
                            onChange={(e) => {
                                let submitedWithoutSubject = state.submitedWithoutSubject
                                submitedWithoutSubject.push(e)
                                setState({ submitedWithoutSubject })
                            }}
                        >
                            {
                                Object.values(state.qualificationSubjectList).map((s, sk) => {
                                    if (s.qualification_id == state.qualification_id && state.submitedWithoutSubject.indexOf(s.id) < 0) {
                                        return <Select.Option key={sk} value={s.id}>
                                            {s.subject_name}
                                        </Select.Option>
                                    }
                                })
                            }
                        </Select>

                        {
                            state.submitedWithoutSubject.length > 0 &&
                            <React.Fragment>
                                <div style={{ color: 'red' }}>{formatMessage({ id: 'click for delete' })}</div>
                                <div style={{ display: 'flex', alignItems: 'center', flexFlow: 'row wrap' }}>
                                    {
                                        Object.values(state.submitedWithoutSubject).map((ss, ssk) => {
                                            return Object.values(state.qualificationSubjectList).map((s, sk) => {
                                                if (s.id == ss) {
                                                    return <div
                                                        key={ssk}
                                                        style={{
                                                            marginTop: '5px',
                                                            padding: '10px',
                                                            border: '1px solid',
                                                            marginRight: '5px',
                                                            cursor: 'pointer'
                                                        }}
                                                        onClick={() => {
                                                            let oldSubmitedWithoutSubject = state.submitedWithoutSubject
                                                            delete oldSubmitedWithoutSubject[ssk]
                                                            let submitedWithoutSubject = []
                                                            oldSubmitedWithoutSubject.forEach(r => {
                                                                submitedWithoutSubject.push(r)
                                                            });
                                                            setState({ submitedWithoutSubject })
                                                        }}
                                                    >
                                                        {s.subject_name}
                                                    </div>
                                                }
                                            })
                                        })
                                    }
                                </div>
                            </React.Fragment>
                        }



                        <Divider style={{ textAlign: 'center', fontWeight: 'bold', fontSize: '17px' }}>
                            {formatMessage({ id: 'add for designated subjects and alternative subjects' })}
                        </Divider>

                        <div style={{}}>
                            {
                                Object.values(state.submitedIncludedAndInstead).map((value, k) => {
                                    return <div key={k} style={{ marginTop: '5px' }}>
                                        {k + 1}. <span style={{ padding: '2px 5px', backgroundColor: '#0000002b' }}>
                                            {formatMessage({ id: 'Designated subjects' })}
                                        </span> ({
                                            Object.values(state.qualificationList).map((ql, qlk) => {
                                                if (state.qualification_id == ql.id) {
                                                    return ql.qualification_name
                                                }
                                            })
                                        }) {
                                            Object.values(state.qualificationSubjectList).map((s, sk) => {
                                                if (value.includedSubjectId == s.id) {
                                                    return s.subject_name
                                                }
                                            })
                                        } <span style={{ color: 'red', fontWeight: 'bold' }}>
                                            {
                                                Object.values(state.qualificationGradeList).map((g, gk) => {
                                                    if (value.includedGradeId == g.id) {
                                                        return g.grade
                                                    }
                                                })
                                            }
                                        </span> <Popconfirm
                                            key={'delete'}
                                            title={formatMessage({ id: 'Are you sure?' })}
                                            onConfirm={() => {
                                                let oldSubmitedIncludedAndInstead = state.submitedIncludedAndInstead
                                                delete oldSubmitedIncludedAndInstead[k]
                                                let submitedIncludedAndInstead = []
                                                oldSubmitedIncludedAndInstead.map((r) => {
                                                    submitedIncludedAndInstead.push(r)
                                                })
                                                setState({ submitedIncludedAndInstead })
                                            }}
                                            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                        >
                                            <Tag color={'red'}>
                                                {formatMessage({ id: 'delete' }).toUpperCase()}
                                            </Tag>
                                        </Popconfirm>
                                        <Tag color={'blue'} onClick={() => {
                                            let oldSubmitedIncludedAndInstead = state.submitedIncludedAndInstead
                                            delete oldSubmitedIncludedAndInstead[k]
                                            let submitedIncludedAndInstead = []
                                            oldSubmitedIncludedAndInstead.map((r) => {
                                                submitedIncludedAndInstead.push(r)
                                            })
                                            setState({
                                                submitedIncludedAndInstead,
                                                drawerMode: 'normal',
                                                addInsteadOrIncludedDrawerKey: k,
                                                addInsteadOrIncludedDrawerMode: 'edit',
                                                addInsteadOrIncludedDrawerVisible: true,
                                                includedSubjectId: value.includedSubjectId,
                                                includedGradeId: value.includedGradeId,
                                                insteadFirstQualificatinId: value.insteadFirstQualificatinId,
                                                insteadFirstSubjectId: value.insteadFirstSubjectId,
                                                insteadFirstGradeId: value.insteadFirstGradeId,
                                                insteadFirstGradeTypeId: value.insteadFirstGradeTypeId,
                                                insteadSecondQualificatinId: value.insteadSecondQualificatinId,
                                                insteadSecondSubjectId: value.insteadSecondSubjectId,
                                                insteadSecondGradeId: value.insteadSecondGradeId,
                                                insteadSecondGradeTypeId: value.insteadSecondGradeTypeId,
                                                insteadQuantity: value.insteadQuantity
                                            })
                                        }}>
                                            {formatMessage({ id: 'edit' }).toUpperCase()}
                                        </Tag>
                                        <div>
                                            ({
                                                formatMessage(
                                                    {
                                                        id: 'conditions included (insteadQuantity) requirement'
                                                    },
                                                    {
                                                        insteadQuantity: value.insteadQuantity
                                                    }
                                                )
                                            })
                                        </div>

                                        {
                                            value.insteadFirstQualificatinId > 0 &&
                                            <div style={{ marginLeft: '10px', marginTop: '5px' }}>
                                                ——<span style={{ padding: '2px 5px', backgroundColor: '#0000002b' }}>
                                                    {formatMessage({ id: 'or replaced by' })}
                                                </span> ({
                                                    Object.values(state.qualificationList).map((ql, qlk) => {
                                                        if (value.insteadFirstQualificatinId == ql.id) {
                                                            return ql.qualification_name
                                                        }
                                                    })
                                                }) {
                                                    Object.values(state.qualificationSubjectList).map((s, sk) => {
                                                        if (value.insteadFirstSubjectId == s.id) {
                                                            return s.subject_name
                                                        }
                                                    })
                                                } <span style={{ color: 'red', fontWeight: 'bold' }}>
                                                    {
                                                        Object.values(state.qualificationGradeList).map((g, gk) => {
                                                            if (value.insteadFirstGradeId == g.id) {
                                                                return g.grade
                                                            }
                                                        })
                                                    }
                                                </span>
                                            </div>
                                        }

                                        {
                                            value.insteadSecondQualificatinId > 0 &&
                                            <div style={{ marginLeft: '10px', marginTop: '5px' }}>
                                                ——<span style={{ padding: '2px 5px', backgroundColor: '#0000002b' }}>
                                                    {formatMessage({ id: 'or replaced by' })}
                                                </span> ({
                                                    Object.values(state.qualificationList).map((ql, qlk) => {
                                                        if (value.insteadSecondQualificatinId == ql.id) {
                                                            return ql.qualification_name
                                                        }
                                                    })
                                                }) {
                                                    Object.values(state.qualificationSubjectList).map((s, sk) => {
                                                        if (value.insteadSecondSubjectId == s.id) {
                                                            return s.subject_name
                                                        }
                                                    })
                                                } <span style={{ color: 'red', fontWeight: 'bold' }}>
                                                    {
                                                        Object.values(state.qualificationGradeList).map((g, gk) => {
                                                            if (value.insteadSecondGradeId == g.id) {
                                                                return g.grade
                                                            }
                                                        })
                                                    }
                                                </span>
                                            </div>
                                        }
                                    </div>
                                })
                            }
                        </div>

                        <Button
                            type="primary"
                            onClick={() => {
                                setState({
                                    addInsteadOrIncludedDrawerVisible: true,
                                    addInsteadOrIncludedDrawerMode: 'add',
                                    drawerMode: 'normal'
                                })
                            }}
                            shape="circle"
                            style={{ marginTop: '10px' }}
                        >
                            <PlusOutlined />
                        </Button>

                        <Divider orientation="left" style={{ fontSize: '20px', margin: 0, marginBottom: '5px' }}>
                            {formatMessage({ id: 'advantage option' })}
                        </Divider>

                        <div>
                            {
                                Object.values(state.submitedIncludedAndInsteadExtra).map((value, k) => {
                                    return <div key={k} style={{ marginTop: '5px' }}>
                                        {k + 1}. <span style={{ padding: '2px 5px', backgroundColor: '#0000002b' }}>
                                            {formatMessage({ id: 'Designated subjects' })}
                                        </span> ({
                                            Object.values(state.qualificationList).map((ql, qlk) => {
                                                if (state.qualification_id == ql.id) {
                                                    return ql.qualification_name
                                                }
                                            })
                                        }) {
                                            Object.values(state.qualificationSubjectList).map((s, sk) => {
                                                if (value.includedSubjectId == s.id) {
                                                    return s.subject_name
                                                }
                                            })
                                        } <span style={{ color: 'red', fontWeight: 'bold' }}>
                                            {
                                                Object.values(state.qualificationGradeList).map((g, gk) => {
                                                    if (value.includedGradeId == g.id) {
                                                        return g.grade
                                                    }
                                                })
                                            }
                                        </span> <Popconfirm
                                            key={'delete'}
                                            title={formatMessage({ id: 'Are you sure?' })}
                                            onConfirm={() => {
                                                let oldsubmitedIncludedAndInsteadExtra = state.submitedIncludedAndInsteadExtra
                                                delete oldsubmitedIncludedAndInsteadExtra[k]
                                                let submitedIncludedAndInsteadExtra = []
                                                oldsubmitedIncludedAndInsteadExtra.map((r) => {
                                                    submitedIncludedAndInsteadExtra.push(r)
                                                })
                                                setState({ submitedIncludedAndInsteadExtra })
                                            }}
                                            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                        >
                                            <Tag color={'red'}>
                                                {formatMessage({ id: 'delete' }).toUpperCase()}
                                            </Tag>
                                        </Popconfirm>
                                        <Tag color={'blue'} onClick={() => {
                                            let oldsubmitedIncludedAndInsteadExtra = state.submitedIncludedAndInsteadExtra
                                            delete oldsubmitedIncludedAndInsteadExtra[k]
                                            let submitedIncludedAndInsteadExtra = []
                                            oldsubmitedIncludedAndInsteadExtra.map((r) => {
                                                submitedIncludedAndInsteadExtra.push(r)
                                            })
                                            setState({
                                                submitedIncludedAndInsteadExtra,
                                                drawerMode: 'extra',
                                                addInsteadOrIncludedDrawerKey: k,
                                                addInsteadOrIncludedDrawerMode: 'edit',
                                                addInsteadOrIncludedDrawerVisible: true,
                                                includedSubjectId: value.includedSubjectId,
                                                includedGradeId: value.includedGradeId,
                                                insteadFirstQualificatinId: value.insteadFirstQualificatinId,
                                                insteadFirstSubjectId: value.insteadFirstSubjectId,
                                                insteadFirstGradeId: value.insteadFirstGradeId,
                                                insteadFirstGradeTypeId: value.insteadFirstGradeTypeId,
                                                insteadSecondQualificatinId: value.insteadSecondQualificatinId,
                                                insteadSecondSubjectId: value.insteadSecondSubjectId,
                                                insteadSecondGradeId: value.insteadSecondGradeId,
                                                insteadSecondGradeTypeId: value.insteadSecondGradeTypeId,
                                                insteadQuantity: value.insteadQuantity
                                            })
                                        }}>
                                            {formatMessage({ id: 'edit' }).toUpperCase()}
                                        </Tag>
                                        <div>
                                            ({
                                                formatMessage(
                                                    {
                                                        id: 'conditions included (insteadQuantity) requirement'
                                                    },
                                                    {
                                                        insteadQuantity: value.insteadQuantity
                                                    }
                                                )
                                            })
                                        </div>

                                        {
                                            value.insteadFirstQualificatinId > 0 &&
                                            <div style={{ marginLeft: '10px', marginTop: '5px' }}>
                                                ——<span style={{ padding: '2px 5px', backgroundColor: '#0000002b' }}>
                                                    {formatMessage({ id: 'or replaced by' })}
                                                </span> ({
                                                    Object.values(state.qualificationList).map((ql, qlk) => {
                                                        if (value.insteadFirstQualificatinId == ql.id) {
                                                            return ql.qualification_name
                                                        }
                                                    })
                                                }) {
                                                    Object.values(state.qualificationSubjectList).map((s, sk) => {
                                                        if (value.insteadFirstSubjectId == s.id) {
                                                            return s.subject_name
                                                        }
                                                    })
                                                } <span style={{ color: 'red', fontWeight: 'bold' }}>
                                                    {
                                                        Object.values(state.qualificationGradeList).map((g, gk) => {
                                                            if (value.insteadFirstGradeId == g.id) {
                                                                return g.grade
                                                            }
                                                        })
                                                    }
                                                </span>
                                            </div>
                                        }

                                        {
                                            value.insteadSecondQualificatinId > 0 &&
                                            <div style={{ marginLeft: '10px', marginTop: '5px' }}>
                                                ——<span style={{ padding: '2px 5px', backgroundColor: '#0000002b' }}>
                                                    {formatMessage({ id: 'or replaced by' })}
                                                </span> ({
                                                    Object.values(state.qualificationList).map((ql, qlk) => {
                                                        if (value.insteadSecondQualificatinId == ql.id) {
                                                            return ql.qualification_name
                                                        }
                                                    })
                                                }) {
                                                    Object.values(state.qualificationSubjectList).map((s, sk) => {
                                                        if (value.insteadSecondSubjectId == s.id) {
                                                            return s.subject_name
                                                        }
                                                    })
                                                } <span style={{ color: 'red', fontWeight: 'bold' }}>
                                                    {
                                                        Object.values(state.qualificationGradeList).map((g, gk) => {
                                                            if (value.insteadSecondGradeId == g.id) {
                                                                return g.grade
                                                            }
                                                        })
                                                    }
                                                </span>
                                            </div>
                                        }
                                    </div>
                                })
                            }
                        </div>

                        <Button
                            type="primary"
                            onClick={() => {
                                setState({
                                    addInsteadOrIncludedDrawerVisible: true,
                                    addInsteadOrIncludedDrawerMode: 'add',
                                    drawerMode: 'extra'
                                })
                            }}
                            shape="circle"
                            style={{ marginTop: '10px' }}
                        >
                            <PlusOutlined />
                        </Button>

                    </React.Fragment>
                }
                <div style={{ textAlign: 'center' }}>
                    <Spin spinning={state.loading} />
                </div>
            </Modal>
            {renderComponent()}
        </div>
    )
}