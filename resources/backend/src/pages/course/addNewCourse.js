import React, { Component } from 'react'
import { connect } from 'dva';
import router from 'umi/router'
import { Form, Input, Button, Divider, AutoComplete, message, Upload, Radio, InputNumber } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { formatMessage } from 'umi/locale';
import CourseLevelModal from '@/components/courseLevelModal'

@connect(({ }) => ({}))
class addNewCourse extends Component {
    state = {
        loading: false,
        iconLoading: false,
        visible: false,
        courseLevelList: [],
        schoolList: [],
        searchResult: []
    };

    getBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    submitWithPic = async (values) => {
        if (!values.picture.file.url && !values.picture.file.preview) {
            await this.getBase64(values.picture.file.originFileObj).then((result) => {
                values.picture = result
                this.request(values)
                return result
            });
        }
    };

    submit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.state.searchResult.forEach(school => {
                    if (values.school_name == school.school_name) {
                        values.school_id = school.id
                    }
                    else if (values.school_name == school.school_name_cn) {
                        values.school_id = school.id
                    }
                });

                if (values.school_id) {
                    this.setState({
                        loading: !this.state.loading,
                        iconLoading: !this.state.iconLoading
                    })
                    if (typeof values.picture !== 'undefined') {
                        this.submitWithPic(values)
                    }
                    else {
                        this.request(values)
                    }
                }
                else {
                    message.error(formatMessage({ id: 'please select school' }), 1)
                }
            }
        });
    }

    request(values) {
        this.props.dispatch({
            type: 'course/e_updateCourse',
            payload: {
                ...values,
                action: 'add'
            }
        }).then((course_id) => {
            if (course_id) {
                router.push('/course/viewCourse?course_id=' + course_id)
            }
            this.setState({
                loading: !this.state.loading,
                iconLoading: !this.state.iconLoading
            })
        })
    }

    searchSchool(value) {
        this.props.dispatch({
            type: 'config/e_searchSchool',
            payload: {
                searchContent: value
            }
        }).then((result) => {
            if (result) {
                let newSchoolList = []
                result.searchResult.forEach(school => {
                    if (school.school_name.toUpperCase().indexOf(value.toUpperCase()) >= 0) {
                        newSchoolList.push(school.school_name)
                    } else {
                        newSchoolList.push(school.school_name_cn)
                    }
                });
                this.setState({
                    schoolList: newSchoolList,
                    searchResult: result.searchResult
                })
            }
        })
    }

    loadForm() {
        return [
            {
                label: 'school_name',
                key: 'school_name',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'school_name_warning' })
                }],
                element: <AutoComplete
                    dataSource={this.state.schoolList}
                    style={{ width: '100%' }}
                    onSearch={this.searchSchool.bind(this)}
                />
            },
            {
                label: 'course_name',
                key: 'course_name',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'course_name_warning' })
                }],
                element: <Input />
            },
            {
                label: 'course_level',
                key: 'course_level',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'course_level_warning' })
                }],
                element: <Radio.Group>
                    {
                        Object.values(this.state.courseLevelList).map((value, key) => {
                            if (value.active) {
                                return (
                                    <Radio key={key} value={value.id}>{value.level_name}</Radio>
                                )
                            }
                        })
                    }
                </Radio.Group>,
                rightElement: <React.Fragment>
                    <Button style={{ float: 'right' }} type="link" onClick={() => this.setState({ visible: true })}>
                        {formatMessage({ id: 'setting level' })}
                    </Button>

                    <CourseLevelModal {...this.state} setState={this.setState.bind(this)} />
                </React.Fragment>
            },
            {
                label: 'secondary_complete',
                key: 'secondary_complete',
                element: <Radio.Group defaultValue={1}>
                    <Radio value={1}>{formatMessage({ id: 'yes' })}</Radio>
                    <Radio value={0}>{formatMessage({ id: 'no' })}</Radio>
                </Radio.Group>
            },
            {
                label: 'contact_me',
                key: 'contact_me',
                element: <Radio.Group defaultValue={1}>
                    <Radio value={1}>{formatMessage({ id: 'yes' })}</Radio>
                    <Radio value={0}>{formatMessage({ id: 'no' })}</Radio>
                </Radio.Group>
            },
            {
                label: 'duration ( Month )',
                key: 'duration',
                element: <InputNumber precision={0.1} min={0} />
            },
            {
                label: 'currency_type',
                key: 'currency_type',
                element: <Radio.Group defaultValue={0}>
                    {
                        this.state.currencyTypeList &&
                        Object.keys(this.state.currencyTypeList).map((key) => {
                            return <Radio value={key}>{this.state.currencyTypeList[key]}</Radio>
                        })
                    }
                </Radio.Group>
            },
            {
                label: 'tuition_fee',
                key: 'tuition_fee',
                element: <InputNumber style={{ width: '100%' }} min={0} />
            },
            {
                label: 'picture',
                key: 'picture',
                element: <Upload
                    fileList={[]}
                    accept=".png,.jpg"
                    showUploadList={false}
                >
                    <Button>
                        <UploadOutlined /> {formatMessage({ id: 'upload image' })} <span style={{ color: 'red' }}> ( {formatMessage({ id: 'only accept jpg or png' })} )</span>
                    </Button>
                </Upload>,
            },
        ]
    }

    render() {
        const { getFieldDecorator } = this.props.form
        return (
            <div>
                <Form className="" labelCol={{ span: 9 }} wrapperCol={{ span: 8 }}>
                    <Divider orientation="left">{formatMessage({ id: 'add new course' })}</Divider>

                    {
                        Object.values(this.loadForm()).map((value, key) => {
                            return (
                                <Form.Item key={key} label={formatMessage({ id: value.label })}>
                                    {
                                        value.rules
                                            ?
                                            getFieldDecorator(value.key, {
                                                rules: value.rules
                                            })
                                                (
                                                    value.element
                                                )
                                            :
                                            getFieldDecorator(value.key)
                                                (
                                                    value.element
                                                )
                                    }

                                    {
                                        value.rightElement &&
                                        value.rightElement
                                    }
                                </Form.Item>
                            )
                        })
                    }

                </Form>

                <div style={{ textAlign: 'center' }}>
                    <Button type="primary" loading={this.state.loading} onClick={this.submit.bind(this)}>
                        {formatMessage({ id: 'submit' })}
                    </Button>
                </div>
            </div>
        );
    }
}

const WrappedaddNewCourse = Form.create()(addNewCourse);
export default WrappedaddNewCourse