import React, { useEffect, useState } from "react";
import { Drawer, Button, Popover, notification, Select, Cascader, message, Radio, Divider, InputNumber } from 'antd';
import { formatMessage } from "umi/locale";
import DataTable from '@/components/dataTable'
import { QuestionCircleOutlined, LoadingOutlined } from '@ant-design/icons';
import InfinityTable from './table'
import { useDispatch } from 'react-redux'

export default function MultipleSubjectRandom(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        insteadQualificationId: 0,
        insteadQualificationSubjectId: 0,
        insteadGradeId: 0,
        insteadGradeType: 0,
        insteadProficiencyId: 0,

        insteadQualificationSubjectList: [],
        insteadQualificationGradeList: [],
        insteadProficiencyInfo: {},

        readingScore: 0,
        writingScore: 0,
        listeningScore: 0,
        speakingScore: 0,
    })

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }

    const openNotificationWithIcon = (description) => {
        notification.open({
            message: formatMessage({ id: 'tips' }),
            description,
            icon: <QuestionCircleOutlined style={{ color: 'red' }} />
        });
    };

    const selectionIgnore = {
        onSelect: (record, selected, selectedRows) => {
            props.setState({
                loading: true
            })
            let newSelectedRows = {}
            selectedRows.forEach(element => {
                newSelectedRows[element.id] = element
            });

            setTimeout(() => {
                props.setState({
                    selectedUnSubmitSubjectList: newSelectedRows,
                    loading: false
                })
            }, 100);
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
            // props.setState({
            //     selectedUnSubmitSubjectList: selectedRows
            // })
        },
    };

    function submitAndCondition(subject_id, grade_id) {
        let oldSubjectFixGrade = props.subjectFixGrade
        if (typeof oldSubjectFixGrade[grade_id] === 'undefined' || oldSubjectFixGrade[subject_id] != grade_id) {

            if (grade_id > 0) {
                oldSubjectFixGrade[subject_id] = grade_id
            }
            else {
                delete oldSubjectFixGrade[subject_id]
            }
            props.setState({
                subjectFixGrade: oldSubjectFixGrade,
                loading: true
            })

            setTimeout(() => {
                props.setState({
                    loading: false
                })
            }, 100);
        }
    }

    function renderAncConditionSelection(listValue) {
        let renderedKey = {}
        Object.values(props.subjectFixGrade).map((fixValue, fixArrayKey) => {
            renderedKey[fixValue] = typeof renderedKey[fixValue] == 'undefined' ? 1 : renderedKey[fixValue] + 1
        })
        return (
            <Select
                defaultValue={0}
                style={{ width: 120 }}
            >
                <Select.Option onClick={() => submitAndCondition(listValue.id, 0)} value={0}>Empty</Select.Option>
                {
                    Object.keys(props.submitedGrade).map((key, arrKey) => {
                        return (
                            Object.values(props.qualificationGradeList).map((value, valueArrayKey) => {
                                if (key == value.id && (renderedKey[key] < props.submitedGrade[key] || typeof renderedKey[key] == 'undefined')) {
                                    return (
                                        <Select.Option key={valueArrayKey} onClick={() => submitAndCondition(listValue.id, value.id)} value={value.id}>{value.grade}</Select.Option>
                                    )
                                }
                            })

                        )
                    })
                }
            </Select>
        )
    }

    function updateInsteadValue(value, key) {
        if (value > 0 && state[key] != value) {
            setState({
                [key]: value
            })
        } else {
            setState({
                [key]: 0
            })
        }

        props.setState({
            loading: true
        })

        setTimeout(() => {
            props.setState({
                loading: false
            })
        }, 100);
    }

    function loadSelectionInsteadList(listValue) {
        let list = []
        if (props.qualification_id == state.insteadQualificationId) {
            Object.values(state.insteadQualificationSubjectList).map((value, key) => {
                if (value.id != listValue.id) {
                    list.push({
                        value: value.id,
                        label: value.subject_name,
                    })
                }

            })
        } else {
            Object.values(state.insteadQualificationSubjectList).map((value, key) => {
                list.push({
                    value: value.id,
                    label: value.subject_name,
                })
            })
        }

        return list
    }

    function renderInsteadQualification(listValue) {
        let qualification = <Cascader
            {...(state.insteadQualificationId > 0 ? { defaultValue: [state.insteadQualificationId] } : {})}
            style={{ width: '100%' }}
            options={
                Object.values(props.qualificationList).map((value, key) => {
                    return {
                        value: value.id,
                        label: value.qualification_name,
                    }
                })
            }
            onChange={(e) => {
                if (e[0] > 0 && state.insteadQualificationId != e[0]) {
                    props.setState({ loading: true })
                    dispatch({
                        type: 'course/e_getRequirementAttr',
                        payload: {
                            qualification_id: e[0]
                        }
                    }).then((value) => {
                        if (value) {
                            setState({
                                insteadQualificationSubjectList: value.qualificationSubjectList,
                                insteadQualificationGradeList: value.qualificationGradeList,
                                insteadQualificationId: e[0]

                            })
                        }
                        props.setState({ loading: false })
                    })
                } else {
                    updateInsteadValue(0, 'insteadQualificationId')
                }
            }}
            placeholder={formatMessage({ id: 'please select qualification' })}
        />

        let proficiency = <Cascader
            {...(state.insteadProficiencyId > 0 ? { defaultValue: [state.insteadProficiencyId] } : {})}
            style={{ width: '100%' }}
            options={
                Object.values(props.proficiencyList).map((value, key) => {
                    return {
                        value: value.id,
                        label: value.proficiency_name,
                    }
                })
            }
            onChange={(e) => {
                if (e[0] > 0 && state.insteadProficiencyId != e[0]) {
                    props.setState({ loading: true })
                    dispatch({
                        type: 'course/e_getRequirementAttr',
                        payload: {
                            qualification_id: state.insteadGradeId,
                            proficiency_id: e[0],
                            is_proficiency: listValue.is_proficiency
                        }
                    }).then((value) => {
                        if (value) {
                            setState({
                                insteadProficiencyInfo: value.proficiencyInfo,
                                insteadProficiencyId: e[0],
                                readingScore: parseFloat(value.proficiencyInfo.reading_score_start),
                                speakingScore: parseFloat(value.proficiencyInfo.speaking_score_start),
                                writingScore: parseFloat(value.proficiencyInfo.writing_score_start),
                                listeningScore: parseFloat(value.proficiencyInfo.listening_score_start)
                            })
                        }
                        props.setState({ loading: false })
                    })
                } else {
                    updateInsteadValue(0, 'insteadProficiencyId')
                }
            }}
            placeholder={formatMessage({ id: 'please select proficiency' })}
        />
        if (state.insteadProficiencyId == 0 && state.insteadQualificationId == 0 && listValue.is_proficiency == true) {
            return [qualification, <div style={{ textAlign: 'center' }}>{formatMessage({ id: 'or' })}</div>, proficiency]
        } else if (state.insteadProficiencyId > 0 && listValue.is_proficiency == true) {
            return proficiency
        } else {
            return qualification
        }
    }

    function updateScore(e, key) {
        e = e == '' || e == null ? 1 : e
        if (e >= 0) {
            setState({
                [key]: e
            })
            props.setState({
                loading: true
            })
            setTimeout(() => {
                props.setState({
                    loading: false
                })
            }, 100);
        }
    }

    console.log(state)

    const renderOrCondition = (listValue) => (
        <div style={{ width: '300px' }}>
            {
                renderInsteadQualification(listValue)
            }
            {
                state.insteadQualificationSubjectList.length > 0 && state.insteadQualificationId > 0 &&
                <Cascader
                    style={{ width: '100%' }}
                    options={loadSelectionInsteadList(listValue)}
                    onChange={(e) => updateInsteadValue(e[0], 'insteadQualificationSubjectId')}
                    placeholder={formatMessage({ id: 'please select subject' })}
                />
            }

            {
                ((state.insteadQualificationGradeList.length > 0 && (state.insteadQualificationId > 0 || state.insteadProficiencyId > 0)) || Object.keys(state.insteadProficiencyInfo).length > 0) &&
                <React.Fragment>
                    <Divider
                        orientation="left"
                        style={{ margin: 'unset', marginTop: '7px' }}
                    >
                        {formatMessage({ id: 'please select grade' })}
                    </Divider>

                    {
                        state.insteadQualificationGradeList.length > 0 &&
                        <Radio.Group
                            onChange={(e) => updateInsteadValue(e.target.value, 'insteadGradeId')}
                            style={{ display: 'flex', flexWrap: 'wrap', paddingLeft: '0' }}
                        >
                            {
                                Object.values(state.insteadQualificationGradeList).map((value, key) => {
                                    return (
                                        <Radio
                                            style={{ flex: '0 0 40%' }}
                                            key={key}
                                            value={value.id}
                                        >
                                            {value.grade + ' ( ' + (value.score !== null ? value.score : value.cgpa) + ' )'}
                                        </Radio>
                                    )
                                })
                            }
                        </Radio.Group>
                    }

                    {
                        Object.keys(state.insteadProficiencyInfo).length > 0 &&
                        <div>
                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                <div style={{ display: 'flex' }}>
                                    <div style={{ width: '120px' }}>
                                        {formatMessage({ id: 'Reading Score' })}:
                                    </div>
                                    <InputNumber
                                        value={state.readingScore}
                                        min={parseFloat(state.insteadProficiencyInfo.reading_score_start)}
                                        max={parseFloat(state.insteadProficiencyInfo.reading_score_end)}
                                        onChange={(e) => updateScore(e, 'readingScore')}
                                    />
                                </div>
                                <div style={{ textAlign: 'right', color: 'red' }}>
                                    {
                                        parseFloat(state.insteadProficiencyInfo.reading_score_start) + ' ~ ' + parseFloat(state.insteadProficiencyInfo.reading_score_end)
                                    }
                                </div>
                            </div>

                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                <div style={{ display: 'flex' }}>
                                    <div style={{ width: '120px' }}>
                                        {formatMessage({ id: 'Speaking Score' })}:
                                    </div>
                                    <InputNumber
                                        value={state.speakingScore}
                                        min={parseFloat(state.insteadProficiencyInfo.speaking_score_start)}
                                        max={parseFloat(state.insteadProficiencyInfo.speaking_score_end)}
                                        onChange={(e) => updateScore(e, 'speakingScore')}
                                    />
                                </div>
                                <div style={{ textAlign: 'right', color: 'red' }}>
                                    {
                                        parseFloat(state.insteadProficiencyInfo.speaking_score_start) + ' ~ ' + parseFloat(state.insteadProficiencyInfo.speaking_score_end)
                                    }
                                </div>
                            </div>

                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                <div style={{ display: 'flex' }}>
                                    <div style={{ width: '120px' }}>
                                        {formatMessage({ id: 'Writing Score' })}:
                                    </div>
                                    <InputNumber
                                        value={state.writingScore}
                                        min={parseFloat(state.insteadProficiencyInfo.writing_score_start)}
                                        max={parseFloat(state.insteadProficiencyInfo.writing_score_end)}
                                        onChange={(e) => updateScore(e, 'writingScore')}
                                    />
                                </div>
                                <div style={{ textAlign: 'right', color: 'red' }}>
                                    {
                                        parseFloat(state.insteadProficiencyInfo.writing_score_start) + ' ~ ' + parseFloat(state.insteadProficiencyInfo.writing_score_end)
                                    }
                                </div>
                            </div>

                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                <div style={{ display: 'flex' }}>
                                    <div style={{ width: '120px' }}>
                                        {formatMessage({ id: 'Listening Score' })}:
                                    </div>
                                    <InputNumber
                                        value={state.listeningScore}
                                        min={parseFloat(state.insteadProficiencyInfo.listening_score_start)}
                                        max={parseFloat(state.insteadProficiencyInfo.listening_score_end)}
                                        onChange={(e) => updateScore(e, 'listeningScore')}
                                    />
                                </div>
                                <div style={{ textAlign: 'right', color: 'red' }}>
                                    {
                                        parseFloat(state.insteadProficiencyInfo.listening_score_start) + ' ~ ' + parseFloat(state.insteadProficiencyInfo.listening_score_end)
                                    }
                                </div>
                            </div>
                        </div>
                    }

                    <Divider
                        orientation="left"
                        style={{ margin: 'unset', marginTop: '7px' }}
                    >
                        {formatMessage({ id: 'please select grade type' })}
                    </Divider>
                    <Radio.Group
                        onChange={(e) => updateInsteadValue(e.target.value, 'insteadGradeType')}
                        style={{ display: 'flex', flexWrap: 'wrap', paddingLeft: '0' }}
                    >
                        {
                            Object.keys(props.gradeTypeList).map((key, arrKey) => {
                                return (
                                    <Radio
                                        style={{ flex: '0 0 40%' }}
                                        key={arrKey}
                                        value={parseInt(key)}
                                    >
                                        {formatMessage({ id: props.gradeTypeList[key] })}
                                    </Radio>
                                )
                            })
                        }
                    </Radio.Group>

                    {
                        (state.listeningScore + state.writingScore + state.speakingScore + state.readingScore) > 0 && state.insteadGradeType > 0 &&
                        <div>
                            <div style={{ color: 'red' }}>
                                {formatMessage({ id: 'selected' })} :
                            </div>
                            {
                                formatMessage({ id: props.gradeTypeList[state.insteadGradeType] }) + ' ' + (state.listeningScore + state.writingScore + state.speakingScore + state.readingScore) +
                                ' ( ' + formatMessage({ id: 'total score' }) + ' )'
                            }
                        </div>
                    }

                    {
                        state.insteadGradeId > 0 && state.insteadGradeType > 0 &&
                        <React.Fragment>
                            <div style={{ color: 'red' }}>
                                {formatMessage({ id: 'selected' })} :
                            </div>
                            <div style={{ display: 'flex', flexWrap: 'wrap', paddingLeft: '0' }} >
                                {
                                    Object.values(state.insteadQualificationGradeList).map((main, mainKey) => {
                                        if (main.id == state.insteadGradeId) {
                                            if (props.gradeTypeList[state.insteadGradeType] == 'more than') {
                                                return (
                                                    Object.values(state.insteadQualificationGradeList).map((sub, subKey) => {
                                                        if (parseFloat(sub.score) >= parseFloat(main.score) || parseFloat(sub.cgpa) >= parseFloat(main.cgpa)) {
                                                            return (
                                                                <div key={subKey} style={{ flex: '0 0 40%' }}>
                                                                    {sub.grade + ' ( ' + (sub.score !== null ? sub.score : sub.cgpa) + ' )'}
                                                                </div>
                                                            )
                                                        }

                                                    })
                                                )
                                            }
                                            else if (props.gradeTypeList[state.insteadGradeType] == 'less than') {
                                                return (
                                                    Object.values(state.insteadQualificationGradeList).map((sub, subKey) => {
                                                        if (parseFloat(sub.score) <= parseFloat(main.score) || parseFloat(sub.cgpa) <= parseFloat(main.cgpa)) {
                                                            return (
                                                                <div key={subKey} style={{ flex: '0 0 40%' }}>
                                                                    {sub.grade + ' ( ' + (sub.score !== null ? sub.score : sub.cgpa) + ' )'}
                                                                </div>
                                                            )
                                                        }

                                                    })
                                                )
                                            }
                                        }

                                    })
                                }

                            </div>
                        </React.Fragment>
                    }
                </React.Fragment>
            }

            <Button
                key="submit"
                type="link"
                disabled={
                    (
                        (state.insteadQualificationSubjectId > 0 && state.insteadQualificationId > 0 && state.insteadGradeId > 0) ||
                        state.insteadProficiencyId > 0
                    ) && state.insteadGradeType > 0
                        ? false : true
                }
                onClick={() => {
                    let oldiInsteadList = props.insteadList
                    if (typeof oldiInsteadList[listValue.id] === 'undefined') {
                        oldiInsteadList[listValue.id] = {}
                    }
                    oldiInsteadList[listValue.id][state.insteadQualificationId > 0 ? 'Q' + state.insteadQualificationId : 'P' + state.insteadProficiencyId] = {
                        insteadQualificationId: state.insteadQualificationId,
                        insteadProficiencyId: state.insteadProficiencyId,
                        insteadQualificationSubjectId: state.insteadQualificationSubjectId,
                        insteadGradeId: state.insteadGradeId,
                        insteadGradeType: state.insteadGradeType,
                        readingScore: state.readingScore,
                        writingScore: state.writingScore,
                        listeningScore: state.listeningScore,
                        speakingScore: state.speakingScore
                    }
                    props.setState({
                        insteadList: oldiInsteadList,
                        loading: true
                    })

                    setState({
                        insteadQualificationId: 0,
                        insteadQualificationSubjectId: 0,
                        insteadGradeId: 0,
                        insteadGradeType: 0,
                        insteadProficiencyId: 0,
                        insteadProficiencyInfo: {},
                        insteadQualificationSubjectList: [],
                        insteadQualificationGradeList: [],
                        readingScore: 0,
                        writingScore: 0,
                        listeningScore: 0,
                        speakingScore: 0,
                    })

                    message.success(formatMessage({ id: 'saved' }), 1)

                    setTimeout(() => {
                        props.setState({
                            loading: false
                        })
                    }, 100);
                }}
            >
                OK
            </Button>

        </div >
    )

    function loadTableColumns() {
        return [
            {
                title: formatMessage({ id: 'include' }),
                dataIndex: 'include',//follow by this key show data
                key: 'include',
                render: (tag, listValue) => (
                    <span>
                        {
                            typeof props.selectedUnSubmitSubjectList[listValue.id] !== 'undefined' ? '' : renderAncConditionSelection(listValue)

                        }
                    </span>
                )
            },
            {
                title: formatMessage({ id: 'instead' }),
                dataIndex: 'instead',//follow by this key show data
                key: 'instead',
                render: (tag, listValue) => (
                    <span>
                        {
                            typeof props.selectedUnSubmitSubjectList[listValue.id] !== 'undefined'
                                ? ''
                                : <React.Fragment>
                                    <Popover
                                        placement="rightTop"
                                        content={renderOrCondition(listValue)}
                                        trigger="click"
                                        onVisibleChange={(e) => {
                                            if (!e && (state.insteadQualificationId > 0 || state.insteadProficiencyId > 0)) {
                                                props.setState({
                                                    loading: true
                                                })
                                                console.log
                                                setState({
                                                    insteadQualificationId: 0,
                                                    insteadProficiencyId: 0,
                                                    insteadQualificationSubjectId: 0,
                                                    insteadGradeId: 0,
                                                    insteadGradeType: 0,
                                                    insteadProficiencyInfo: {},
                                                    insteadQualificationSubjectList: [],
                                                    insteadQualificationGradeList: [],
                                                    readingScore: 0,
                                                    writingScore: 0,
                                                    listeningScore: 0,
                                                    speakingScore: 0
                                                })
                                                setTimeout(() => {
                                                    props.setState({
                                                        loading: false
                                                    })
                                                }, 100);
                                            }
                                        }}
                                    >
                                        <Button>{formatMessage({ id: 'instead' })}</Button>
                                    </Popover>
                                    {
                                        typeof props.insteadList[listValue.id] !== 'undefined' &&
                                        <div style={{ textAlign: 'center' }}>{formatMessage({ id: 'selected' }) + ' ' + Object.keys(props.insteadList[listValue.id]).length}</div>
                                    }
                                </React.Fragment>

                        }
                    </span>

                )
            },
            {
                title: formatMessage({ id: 'subject_name' }),
                dataIndex: 'subject_name',//follow by this key show data
                key: 'subject_name'
            }
        ]
    }

    return (
        <div>
            <Drawer
                title={formatMessage({ id: 'add condition' })}
                placement="right"
                closable={false}
                onClose={props.onClose}
                visible={props.visible}
                width={720}
            >

                <InfinityTable
                    loading={props.loading}
                    columns={loadTableColumns()}
                    rowSelection={[selectionIgnore]}
                    dataSource={props.qualificationSubjectList}
                    submitedGrade={props.submitedGrade}

                    defaultTitleCheckboxValue={
                        [
                            <Button
                                type='link'
                                style={{ fontSize: '20px' }}
                                onClick={() => openNotificationWithIcon(formatMessage({ id: 'if selected will not include in condition' }))}
                            >
                                <QuestionCircleOutlined style={{ color: 'red' }} />
                            </Button>
                        ]
                    }
                />
            </Drawer>
        </div>
    );
}