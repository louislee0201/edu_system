import React, { useEffect, useState } from "react";
import {
    Modal,
    Input,
    Button,
    InputNumber,
    notification,
    message,
    Spin,
    Radio,
    Cascader,
    Divider,
    Checkbox
} from "antd";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import { QuestionCircleOutlined } from '@ant-design/icons';
import MultipleSubjectRandomModal from './multipleSubjectRandomModal'

export default function requirementModal(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        buttonLoading: false,
        mode: '',
        gradeType: 0,
        qualification_id: 0,
        loading: false,

        multipleSubjectRandomModalVisible: false,
        animationmultipleSubjectRandomModalVisible: false,

        submitedGrade: {},
        selectedUnSubmitSubjectList: {},
        subjectFixGrade: {},

        insteadList: {},
        scoreValue: 0,

        noSpecificRequirement: false
    })

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }

    useEffect(() => {
        if (props.editData.mode == 'edit' && props.visible) {
            setState(props.editData)
        }
    }, [props.visible]);

    useEffect(() => {
        setState({ loading: true })
        dispatch({
            type: 'course/e_getRequirementAttr',
            payload: {
                qualification_id: state.qualification_id
            }
        }).then((value) => {
            if (value) {
                setState(value)
            }
            setState({ loading: false })
        })
    }, [state.qualification_id]);

    function updateGradeValue(grade_id, value, scoreValue) {
        let oldSubmitedGrade = state.submitedGrade
        let oldScoreValue = state.scoreValue
        if (typeof oldSubmitedGrade[grade_id] === 'undefined' || oldSubmitedGrade[grade_id] != value) {
            if (typeof oldSubmitedGrade[grade_id] !== 'undefined') {
                oldScoreValue -= oldSubmitedGrade[grade_id] * parseFloat(scoreValue)
            }
            if (value === null || value == '') {
                delete oldSubmitedGrade[grade_id]
            } else {
                oldScoreValue += parseFloat(scoreValue) * value
                oldSubmitedGrade[grade_id] = value
            }
            setState({
                submitedGrade: oldSubmitedGrade,
                scoreValue: oldScoreValue
            })

        }
    }

    const openNotificationWithIcon = (description) => {
        notification.open({
            message: formatMessage({ id: 'tips' }),
            description,
            icon: <QuestionCircleOutlined style={{ color: 'red' }} />
        });
    };

    function submit() {
        setState({ loading: true })
        dispatch({
            type: 'course/e_submitRequirement',
            payload: {
                ...state,
                course_id: props.course_id
            }
        }).then((value) => {
            if (value) {
                setState({
                    gradeType: 0,
                    qualification_id: 0,

                    submitedGrade: {},
                    selectedUnSubmitSubjectList: {},
                    subjectFixGrade: {},

                    insteadList: {},
                    scoreValue: 0,

                    gradeTypeList: [],
                    //qualificationList: [],
                    qualificationSubjectList: [],
                    qualificationGradeList: [],
                })
                props.onClose({
                    visible: false,
                    value
                })
            }
            setState({ loading: false })

        })
    }

    return (
        <Modal
            title={state.mode == 'edit' ? formatMessage({ id: 'setting requirement' }) : formatMessage({ id: 'add new requirement' })}
            visible={props.visible}
            onCancel={() => {
                setState({
                    gradeType: 0,
                    qualification_id: 0,
                    noSpecificRequirement: false,
                    submitedGrade: {},
                    selectedUnSubmitSubjectList: {},
                    subjectFixGrade: {},

                    insteadList: {},
                    scoreValue: 0,

                    gradeTypeList: [],
                    //qualificationList: [],
                    qualificationSubjectList: [],
                    qualificationGradeList: [],
                })
                props.onClose({ visible: false })
            }}
            footer={[
                <Button
                    disabled={(Object.keys(state.submitedGrade).length <= 0 || state.qualification_id <= 0 || state.gradeType <= 0) && state.noSpecificRequirement === false}
                    key="submit"
                    type="primary"
                    onClick={submit}
                >
                    {formatMessage({ id: 'submit' })}
                </Button>,
            ]}
        >
            <Cascader
                style={{ width: '100%' }}
                options={
                    state.qualificationList &&
                    Object.values(state.qualificationList).map((value, key) => {
                        return {
                            value: value.id,
                            label: value.qualification_name,
                        }
                    })
                }
                onChange={(e) => {
                    setState({
                        qualification_id: e[0],
                        gradeType: 0,

                        submitedGrade: {},
                        selectedUnSubmitSubjectList: {},
                        subjectFixGrade: {},

                        insteadList: {},
                        scoreValue: 0,

                        gradeTypeList: [],
                        //qualificationList: [],
                        qualificationSubjectList: [],
                        qualificationGradeList: [],
                    })
                }}
                placeholder={formatMessage({ id: 'please select qualification' })}
            />

            <div style={{ textAlign: 'center' }}>
                <Spin spinning={state.loading} />
            </div>


            {
                !state.loading && state.qualification_id > 0 &&
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>

                    <Checkbox onChange={(e) => setState({ noSpecificRequirement: e.target.checked })}>{formatMessage({ id: 'no specific requirement' })}</Checkbox>

                    {
                        !state.noSpecificRequirement &&
                        <React.Fragment>
                            <Button
                                type="link"
                                onClick={() => {
                                    setState({
                                        multipleSubjectRandomModalVisible: true,
                                        animationmultipleSubjectRandomModalVisible: true,
                                        subjectFixGrade: {},
                                        selectedUnSubmitSubjectList: {},
                                        insteadList: {}
                                    })
                                }}
                            >
                                {formatMessage({ id: 'advantage option' })}
                            </Button>
                            <Button
                                type='link'
                                style={{ fontSize: '20px' }}
                                onClick={() => openNotificationWithIcon(formatMessage({ id: 'please enter the level that meets the subject conditions' }))}
                            >
                                <QuestionCircleOutlined style={{ color: 'red' }} />
                            </Button>
                        </React.Fragment>
                    }
                </div>
            }

            {
                !state.noSpecificRequirement && state.qualificationSubjectList && state.qualificationSubjectList.length > 0 &&
                <React.Fragment>
                    {
                        state.animationmultipleSubjectRandomModalVisible &&
                        <MultipleSubjectRandomModal
                            setState={setState}
                            {...({
                                ...state,
                                visible: state.multipleSubjectRandomModalVisible
                            })}
                            onClose={() => {
                                setState({
                                    multipleSubjectRandomModalVisible: false,
                                    loading: true
                                })

                                setTimeout(() => {
                                    setState({
                                        animationmultipleSubjectRandomModalVisible: false,
                                        loading: false
                                    })
                                }, 1000);
                            }}
                        />
                    }

                    <div style={{ display: 'flex', flexWrap: 'wrap', paddingLeft: '0' }}>
                        {
                            Object.values(state.qualificationGradeList).map((value, key) => {
                                return (
                                    <div key={key} style={{ flex: '0 0 50%', display: 'flex', justifyContent: 'space-between' }}>
                                        <span>{key + 1}. {value.grade} {value.cgpa >= 0 && value.cgpa !== null && '( ' + value.cgpa + ' )'} {value.score >= 0 && value.score !== null && '( ' + value.score + ' )'}</span>
                                        <InputNumber
                                            style={{ marginRight: '40px' }}
                                            size="small"
                                            min={0}
                                            defaultValue={0}
                                            precision={0.0}
                                            onChange={(e) => updateGradeValue(value.id, e, value.score !== null ? value.score : value.cgpa)}
                                        />
                                    </div>
                                )
                            })
                        }
                    </div>
                    {
                        Object.keys(state.submitedGrade).length > 0 &&
                        <React.Fragment>
                            <Divider
                                orientation="left"
                                style={{ margin: 'unset', marginTop: '7px' }}
                            >
                                {formatMessage({ id: 'please select grade type' })}
                            </Divider>
                            <Radio.Group
                                onChange={(e) => setState({ gradeType: e.target.value })}
                                style={{ display: 'flex', flexWrap: 'wrap', paddingLeft: '0' }}
                            >
                                {
                                    Object.keys(state.gradeTypeList).map((key, arrKey) => {
                                        return (
                                            <Radio
                                                style={{ flex: '0 0 40%' }}
                                                key={arrKey}
                                                value={parseInt(key)}
                                            >
                                                {formatMessage({ id: state.gradeTypeList[key] })}
                                            </Radio>
                                        )
                                    })
                                }
                            </Radio.Group>
                            {
                                state.gradeType > 0 &&
                                <div style={{ color: 'red' }}>
                                    {formatMessage({ id: state.gradeTypeList[state.gradeType] })} {state.scoreValue} {formatMessage({ id: 'score' })} / {formatMessage({ id: 'cgpa' })}
                                </div>
                            }
                        </React.Fragment>

                    }
                </React.Fragment>
            }

        </Modal>
    )
}