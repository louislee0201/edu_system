import React, { Component } from 'react'
import { connect } from 'dva';
import { Form, Input, Button, Checkbox, Avatar, message } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { formatMessage } from 'umi/locale';
import { SYSTEM_LANGUAGE } from '@/constants/default'
import './scss/login.scss';

@connect(({ loading, user }) => ({ loading, user }))
class Login extends Component {
    state = {
        loading: false
    }
    componentDidMount() {
        // To disable submit button at the beginning.
        this.props.form.validateFields();
    }
    submit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.setState({
                    loading: !this.state.loading
                })
                this.props.dispatch({
                    type: 'user/e_login',
                    payload: values,
                }).then((result) => {
                    if (!result) {
                        this.setState({
                            loading: !this.state.loading
                        })
                    }
                })
            }
            else {
                message.error(formatMessage({ id: 'error login' }))
            }
        });
    }

    changeLanguage = (language) => {
        this.props.dispatch({
            type: 'user/e_changeLanguage',
            payload: language,
        })
    }



    render() {
        function hasErrors(fieldsError) {
            return Object.keys(fieldsError).some(field => fieldsError[field]);
        }

        const { getFieldDecorator, getFieldsError, isFieldTouched } = this.props.form

        return (
            <div className="login_body">
                <div className="login_form">
                    <Form
                        name="login_form"
                    >
                        <Avatar className="login_avatarIcon" shape="square" size={80} icon={<UserOutlined />} />
                        {
                            getFieldDecorator('username', {
                                rules: [{
                                    required: true,
                                    message: formatMessage({ id: 'username_warning' })
                                }],
                            })
                                (
                                    <Input style={{ textAlign: 'center' }} className="login_username" placeholder={formatMessage({ id: 'username' })}
                                        onPressEnter={this.submit.bind(this)} />
                                )
                        }
                        {
                            getFieldDecorator('password', {
                                rules: [{
                                    required: true,
                                    message: formatMessage({ id: 'password_warning' })
                                }],
                            })
                                (
                                    <Input style={{ textAlign: 'center' }} type="password" placeholder={formatMessage({ id: 'password' })}
                                        onPressEnter={this.submit.bind(this)} />
                                )
                        }

                        <Form.Item
                            name="remember"
                        >
                            {
                                getFieldDecorator('rememberMe', {
                                    valuePropName: 'checked',
                                    initialValue: true
                                })
                                    (
                                        <Checkbox>{formatMessage({ id: 'remember_me' })}</Checkbox>
                                    )
                            }

                        </Form.Item>

                        <Form.Item>
                            <Button
                                loading={this.state.loading}
                                type="primary"
                                htmlType="submit"
                                onClick={this.submit.bind(this)}
                                disabled={hasErrors(getFieldsError())}
                            >
                                {formatMessage({ id: 'login' })}
                            </Button>
                        </Form.Item>

                        <div className="login_changeLanguage">
                            <Button
                                className="login_languagebtn"
                                disabled={this.props.user.language == SYSTEM_LANGUAGE.zh_CN}
                                type="link"
                                onClick={this.changeLanguage.bind(this, SYSTEM_LANGUAGE.zh_CN)}
                            >
                                中文
                            </Button>
                            <div className="login_languagebtn">
                                |
                            </div>
                            <Button
                                className="login_languagebtn"
                                disabled={this.props.user.language == SYSTEM_LANGUAGE.en_US}
                                type="link"
                                onClick={this.changeLanguage.bind(this, SYSTEM_LANGUAGE.en_US)}
                            >
                                英文
                            </Button>
                        </div>
                    </Form>
                </div>
            </div>
        );
    }
}

const WrappedLoginForm = Form.create()(Login);
export default WrappedLoginForm