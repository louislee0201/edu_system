import React, { useEffect, useState } from "react";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import DataView from '@/components/dataView'
import { Upload } from "antd";
import { getBase64 } from '@/tools'
import { Radio, Tabs, Checkbox, Input } from "antd";
import DescriptionTable from '@/components/descriptionTable'
import CourseArrangement from './courseArrangement'
export default function ViewSchool(props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        classMode: 'school'
    })

    useEffect(() => {
        const school_id = props.location.query.school_id;
        if (school_id) {
            dispatch({
                type: "school/e_getSchoolList",
                payload: {
                    isList: false,
                    school_id
                }
            }).then(result => {
                if (result) {
                    setState({
                        ...result,
                        school_id
                    })
                }
            });
        }
    }, []);

    function loadTitle() {
        return (
            [
                {
                    label: "email",
                    key: "email",
                    disabled: true
                },
                {
                    label: "school_name",
                    key: "school_name"
                },
                {
                    label: "school_name_cn",
                    key: "school_name_cn"
                },
                {
                    label: "official_website",
                    key: "official_website"
                },
                {
                    label: "address",
                    key: "address",
                    edit_element: <Input.TextArea onChange={(e) => setState({ address: e.target.value })} value={state.address} />
                },
                {
                    label: "country",
                    key: "country",
                    default_element: state.countryList &&
                        Object.values(state.countryList).map((value) => {
                            if (value.id == state.country) {
                                return value.country
                            }
                        })
                    ,
                    edit_element: state.countryList &&
                        <Radio.Group onChange={(e) => setState({ country: e.target.value })} value={state.country}>
                            {

                                Object.values(state.countryList).map((value, key) => {
                                    return (
                                        <Radio key={key} value={value.id}>{value.country}</Radio>
                                    )
                                })
                            }
                        </Radio.Group>
                },
                {
                    label: "region",
                    key: "region",
                    default_element: state.regionList &&
                        Object.values(state.regionList).map((value) => {
                            if (value.id == state.region) {
                                return value.region
                            }
                        }),
                    edit_element: state.regionList &&
                        <Radio.Group onChange={(e) => setState({ region: e.target.value })} value={state.region}>
                            {

                                Object.values(state.regionList).map((value, key) => {
                                    return (
                                        <Radio key={key} value={value.id}>{value.region}</Radio>
                                    )
                                })
                            }
                        </Radio.Group>
                },
                {
                    label: "school_tel",
                    key: "school_tel"
                },
                {
                    label: "picture",
                    key: "picture",
                    default_element: state.picture != '' ? <img src={state.picture} style={{ width: '120px', height: '120px', border: '1px solid' }} /> : '',
                    edit_element: <Upload
                        accept='.png,.jpg'
                        name="avatar"
                        listType="picture-card"
                        className="avatar-uploader"
                        showUploadList={false}
                        onChange={(info) => {
                            if (info.file.status === 'done') {
                                getBase64(info.file.originFileObj, imageUrl => setState({ picture: imageUrl })
                                );
                            }
                        }}
                    >
                        {
                            state.picture ?
                                <img src={state.picture} alt="avatar" style={{ width: '100%' }} />
                                :
                                <div>
                                    <div className="ant-upload-text">{formatMessage({ id: 'upload image' })}</div>
                                </div>
                        }
                    </Upload>
                },
                {
                    label: "logo",
                    key: "logo",
                    default_element: state.logo != '' ? <img src={state.logo} style={{ width: '120px', height: '120px', border: '1px solid' }} /> : '',
                    edit_element: <Upload
                        accept='.png,.jpg'
                        name="avatar"
                        listType="picture-card"
                        className="avatar-uploader"
                        showUploadList={false}
                        onChange={(info) => {
                            if (info.file.status === 'done') {
                                getBase64(info.file.originFileObj, imageUrl => setState({ logo: imageUrl })
                                );
                            }
                        }}
                    >
                        {
                            state.logo ?
                                <img src={state.logo} alt="avatar" style={{ width: '100%' }} />
                                :
                                <div>
                                    <div className="ant-upload-text">{formatMessage({ id: 'upload image' })}</div>
                                </div>
                        }
                    </Upload>
                },
                {
                    label: "contact_me",
                    key: "contact_me",
                    default_element: formatMessage({ id: state.contact_me ? 'yes' : 'no' }),
                    edit_element: <div>
                        {formatMessage({ id: state.contact_me ? 'yes' : 'no' })} <Checkbox checked={state.contact_me} onChange={(e) => setState({ contact_me: e.target.checked })} />
                    </div>
                },
                {
                    label: "school_type",
                    key: "school_type",
                    default_element: state.schoolTypeList &&
                        Object.keys(state.schoolTypeList).map((keys, arrkey) => {
                            if (keys == state.school_type) {
                                return formatMessage({ id: state.schoolTypeList[keys] })
                            }
                        }),
                    edit_element: state.schoolTypeList &&
                        <Radio.Group onChange={(e) => setState({ school_type: e.target.value })} value={state.school_type}>
                            {
                                Object.keys(state.schoolTypeList).map((keys, arrkey) => {
                                    return (
                                        <Radio key={arrkey} value={parseInt(keys)}>{formatMessage({ id: state.schoolTypeList[keys] })}</Radio>
                                    )
                                })
                            }
                        </Radio.Group>
                },
            ]
        )
    }

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }

    function updateData(callback = () => { }) {
        dispatch({
            type: 'school/e_updateSchool',
            payload: {
                isList: false,
                ...state,
                action: 'edit'
            }
        }).then((result) => {
            if (result) {
                setState({
                    ...result
                })
                callback()
            }
        })
    }

    return (
        <Tabs type="card">
            <Tabs.TabPane tab={formatMessage({ id: 'data' })} key="attr">
                <DataView {...state} title={loadTitle()} setState={setState} updateData={updateData} />
            </Tabs.TabPane>

            <Tabs.TabPane tab={formatMessage({ id: 'description' })} key="description">
                <DescriptionTable
                    {...({
                        ...state,
                        related_id: state.school_id
                    })}
                />
            </Tabs.TabPane>

            <Tabs.TabPane tab={formatMessage({ id: 'course arrangement' })} key="course_arrangement">
                <CourseArrangement
                    {...({
                        ...state,
                        related_id: state.school_id
                    })}
                />
            </Tabs.TabPane>
        </Tabs>
    )
}