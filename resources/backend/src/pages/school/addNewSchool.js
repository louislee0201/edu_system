import React, { Component } from 'react'
import { connect } from 'dva';
import router from 'umi/router'
import { Form, Input, Button, Divider, InputNumber, Upload, Radio } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { formatMessage } from 'umi/locale';
import CountryModal from '@/components/countryModal'
import RegionModal from '@/components/regionModal'


@connect(({ }) => ({}))
class addNewSchool extends Component {
    state = {
        loading: false,
        iconLoading: false,
        countryVisible: false,
        regionVisible: false,
        countryList: [],
        regionList: [],
        schoolTypeList:[],
        allowSetRegion: false,
        country_id: 0
    };

    getBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    submitWithPic = async (values) => {
        if (values.picture) {
            await this.getBase64(values.picture.file.originFileObj).then((result) => {
                values.picture = result
                return result
            });
        }

        if (values.logo) {
            await this.getBase64(values.logo.file.originFileObj).then((result) => {
                values.logo = result
                return result
            });
        }
        this.request(values)
    };

    request(values) {
        this.props.dispatch({
            type: 'school/e_updateSchool',
            payload: {
                ...values,
                action: 'add'
            }
        }).then((school_id) => {
            if (school_id) {
                router.push('/school/viewSchool?school_id=' + school_id)
            }
            this.setState({
                loading: !this.state.loading,
                iconLoading: !this.state.iconLoading
            })
        })
    }

    submit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {

            if (!err) {
                this.setState({
                    loading: !this.state.loading,
                    iconLoading: !this.state.iconLoading
                })
                if (typeof values.picture !== 'undefined' || typeof values.logo !== 'undefined') {
                    this.submitWithPic(values)
                }
                else {
                    this.request(values)
                }
            }
        });
    }

    checkPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback(formatMessage({ id: 'Two passwords that you enter is inconsistent!' }));
        } else {
            callback();
        }
    }

    loadForm() {
        return [
            {
                label: 'email',
                key: 'email',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'email_warning' })
                }],
                element: <Input />
            },
            {
                label: 'password',
                key: 'password',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'password_warning' })
                }],
                element: <Input type={'password'} />
            },
            {
                label: 'confirm_password',
                key: 'confirm_password',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'confirm_password_warning' })
                },
                {
                    validator: this.checkPassword,
                }],
                element: <Input type={'password'} />
            },
            {
                label: 'school_name',
                key: 'school_name',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'school_name_warning' })
                }],
                element: <Input />
            },
            {
                label: 'school_name_cn',
                key: 'school_name_cn',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'school_name_cn_warning' })
                }],
                element: <Input />
            },
            {
                label: 'official_website',
                key: 'official_website',
                element: <Input />
            },
            {
                label: 'school_type',
                key: 'school_type',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'school_type_warning' })
                }],
                element: <Radio.Group>
                    {
                        Object.keys(this.state.schoolTypeList).map((keys, arrkey) => {
                            return (
                                <Radio key={arrkey} value={parseInt(keys)}>{formatMessage({ id: this.state.schoolTypeList[keys] })}</Radio>
                            )
                        })
                    }
                </Radio.Group>
            },
            {
                label: 'country',
                key: 'country',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'country_warning' })
                }],
                element: <Radio.Group onChange={(e) => {
                    this.props.dispatch({
                        type: 'config/e_getRegionList',
                        payload: {
                            country_id: e.target.value,
                        }
                    }).then((result) => {
                        this.setState({ ...result, allowSetRegion: true, country_id: e.target.value })
                    })
                }}>
                    {
                        Object.values(this.state.countryList).map((value, key) => {
                            if (value.active) {
                                return (
                                    <Radio key={key} value={value.id}>{value.country}</Radio>
                                )
                            }
                        })
                    }
                </Radio.Group>,
                rightElement: <React.Fragment>
                    <Button style={{ float: 'right' }} type="link" onClick={() => this.setState({ countryVisible: true })}>
                        {formatMessage({ id: 'setting country' })}
                    </Button>

                    <CountryModal
                        onClose={({ visible }) => this.setState({ countryVisible: visible })}
                        {...this.state}
                        setState={this.setState.bind(this)}
                        visible={this.state.countryVisible}
                    />
                </React.Fragment>

            },
            {
                label: 'region',
                key: 'region',
                element: <Radio.Group>
                    {
                        Object.values(this.state.regionList).map((value, key) => {
                            if (value.active) {
                                return (
                                    <Radio key={key} value={value.id}>{value.region}</Radio>
                                )
                            }
                        })
                    }
                </Radio.Group>,
                rightElement: this.state.country_id > 0 && <React.Fragment>
                    <Button style={{ float: 'right' }} type="link" onClick={() => this.setState({ regionVisible: true })}>
                        {formatMessage({ id: 'setting region' })}
                    </Button>

                    <RegionModal
                        onClose={({ visible }) => this.setState({ regionVisible: visible })}
                        {...this.state}
                        visible={this.state.regionVisible}
                        setState={this.setState.bind(this)}
                    />
                </React.Fragment>
            },
            {
                label: 'contact_me',
                key: 'contact_me',
                element: <Radio.Group defaultValue={1}>
                    <Radio value={1}>{formatMessage({ id: 'yes' })}</Radio>
                    <Radio value={0}>{formatMessage({ id: 'no' })}</Radio>
                </Radio.Group>
            },
            {
                label: 'picture',
                key: 'picture',
                element: <Upload
                    accept=".png,.jpg"
                    showUploadList={false}
                >
                    <Button>
                        <UploadOutlined /> {formatMessage({ id: 'upload image' })} <span style={{ color: 'red' }}> ( {formatMessage({ id: 'only accept jpg or png' })} )</span>
                    </Button>
                </Upload>,
            },
            {
                label: 'logo',
                key: 'logo',
                element: <Upload
                    accept=".png,.jpg"
                    showUploadList={false}
                >
                    <Button>
                        <UploadOutlined /> {formatMessage({ id: 'upload image' })} <span style={{ color: 'red' }}> ( {formatMessage({ id: 'only accept jpg or png' })} )</span>
                    </Button>
                </Upload>,
            },
            {
                label: 'address',
                key: 'address',
                element: <Input.TextArea />
            },
            {
                label: 'school_tel',
                key: 'school_tel',
                element: <Input />
            }
        ]
    }

    render() {
        const { getFieldDecorator } = this.props.form
        return (
            <div>
                <Form className="" labelCol={{ span: 8 }} wrapperCol={{ span: 10 }}>
                    <Divider orientation="left">{formatMessage({ id: 'add new item' })}</Divider>

                    {
                        Object.values(this.loadForm()).map((value, key) => {
                            return (
                                <Form.Item key={key} label={formatMessage({ id: value.label })}>
                                    {
                                        value.rules
                                            ?
                                            getFieldDecorator(value.key, {
                                                rules: value.rules
                                            })
                                                (
                                                    value.element
                                                )
                                            :
                                            getFieldDecorator(value.key)
                                                (
                                                    value.element
                                                )
                                    }

                                    {
                                        value.rightElement &&
                                        value.rightElement
                                    }
                                </Form.Item>
                            )
                        })
                    }

                </Form>

                <div style={{ textAlign: 'center' }}>
                    <Button type="primary" loading={this.state.loading} onClick={this.submit.bind(this)}>
                        {formatMessage({ id: 'submit' })}
                    </Button>
                </div>
            </div>
        );
    }
}

const WrappedaddNewSchool = Form.create()(addNewSchool);
export default WrappedaddNewSchool