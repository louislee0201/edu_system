import React, { useEffect, useState } from "react";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import DataView from '@/components/dataView'
import { Upload } from "antd";
import { getBase64 } from '@/tools'
import { Spin, Modal, Button, Input, InputNumber, Popconfirm } from "antd";
import { QuestionCircleOutlined } from '@ant-design/icons';
import './index.less';
import { parse } from "path-to-regexp";

export default (props) => {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        loading: true,
        mode: 'add',
        modalVisible: false,
        title: '',
        schoolCourseArrangementList: {},
        schoolArrangementList: [],
        targetInfo: {},

        editTitleKey: 0,

        editTitleArrangement: false,

        editCourseArrangement: false,
        ckey: 0,

        recordCourseRangeKey: {},
        recordTitleRangeKey: {},

        editedRangeKeyCourse: {},
        editedRangeKeyTitle: {}
    })

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }

    const dragStart = (e) => {
        if (state.editTitleArrangement || state.editCourseArrangement) {
            e.preventDefault()
        }
        // 开始拖拽保存 拖拽元素的数据
        setState({
            targetInfo: JSON.parse(e.target.getAttribute('course-info'))
        })
    };

    const onDragEnter = (e) => {
        if (state.editTitleArrangement || state.editCourseArrangement) {
            e.preventDefault()
        }
        // 拖入目标元素 显示border
        if (e.target.className.indexOf('target') > 0 && state.targetInfo.school_course_arrangement_id != e.target.id) {
            e.target.style.border = '1px dashed red';
        }
    };

    const onDragLeave = (e) => {
        if (state.editTitleArrangement || state.editCourseArrangement) {
            e.preventDefault()
        }
        // 拖出目标元素 隐藏border
        if (e.target.className.indexOf('target') > 0) {
            e.target.style.border = 'none';
        }
    };

    const onDrop = (e) => {
        e.preventDefault(); // 避免浏览器对数据的默认处理（默认行为是以链接形式打开）
        if (e.target.className.indexOf('target') > 0 && state.targetInfo.school_course_arrangement_id != e.target.id) {
            e.target.style.border = 'none'
            setState({ loading: true })
            dispatch({
                type: 'school/e_editSchoolCourseArrangementTitle',
                payload: {
                    school_id: props.related_id,
                    changeTo: e.target.id,
                    course_id: state.targetInfo.id,
                    action: 'editCourse'
                }
            }).then((result) => {
                if (result) {
                    setState({
                        ...result,
                        loading: false
                    })
                }
            })
        }
    };

    useEffect(() => {
        let schoolArrangementList = state.schoolArrangementList
        let schoolCourseArrangementList = state.schoolCourseArrangementList
        let allow = true
        let recordCourseRangeKey = {}
        let recordTitleRangeKey = {}
        if (props.schoolArrangementList) {
            schoolArrangementList = props.schoolArrangementList

            schoolArrangementList.map((c) => {
                recordTitleRangeKey[c.id] = c.range_key
            })
        }
        if (props.schoolCourseArrangementList) {
            schoolCourseArrangementList = props.schoolCourseArrangementList
            Object.keys(schoolCourseArrangementList).map((titleKey) => {
                schoolCourseArrangementList[titleKey].map((c) => {
                    recordCourseRangeKey[c.id] = c.range_key
                })
            })
        }

        console.log(recordTitleRangeKey, recordCourseRangeKey)

        setState({
            schoolCourseArrangementList,
            schoolArrangementList,
            loading: allow,
            recordTitleRangeKey,
            recordCourseRangeKey
        })
    }, [props.schoolArrangementList, props.schoolCourseArrangementList]);


    state.loading && <Spin />
    return <div>
        <Modal
            title={state.mode == 'edit' ? formatMessage({ id: 'setting arrangement title' }) : formatMessage({ id: 'add arrangement title' })}
            visible={state.modalVisible}
            onCancel={() => {
                setState({ modalVisible: false })
            }}
            footer={[
                <Button
                    disabled={false}
                    key="submit"
                    type="primary"
                    onClick={() => {
                        setState({ loading: true })
                        dispatch({
                            type: 'school/e_editSchoolCourseArrangementTitle',
                            payload: {
                                school_id: props.related_id,
                                title: state.title,
                                action: state.mode,
                                editTitleKey: state.editTitleKey
                            }
                        }).then((result) => {
                            if (result) {
                                setState({
                                    ...result,
                                    loading: false,
                                    modalVisible: false,
                                    title: '',
                                    editTitleKey: 0,

                                })
                            }
                        })
                    }}
                >
                    {formatMessage({ id: 'submit' })}
                </Button>,
            ]}
        >
            <Input value={state.title} onChange={(e) => setState({ title: e.target.value })} />
        </Modal>
        <Button
            style={{ marginRight: '5px' }}
            key="add"
            type="primary"
            onClick={() => setState({
                modalVisible: true,
                action: 'add'
            })}
        >
            {formatMessage({ id: 'add arrangement title' })}
        </Button>

        <Button
            disabled={state.editTitleArrangement}
            type="primary"
            onClick={() => setState({ editTitleArrangement: true })}
        >
            {formatMessage({ id: 'edit title arrangement' })}
        </Button>

        {
            state.editTitleArrangement &&
            <Button
                key="edit title"
                type="danger"
                onClick={() => {
                    dispatch({
                        type: 'school/e_editSchoolCourseArrangementTitle',
                        payload: {
                            editedRangeKeyTitle: state.editedRangeKeyTitle,
                            school_id: props.related_id,
                            action: 'editTitleRangeKey'
                        }
                    }).then((result) => {
                        if (result) {
                            setState({
                                ...result,
                                loading: false,
                                editTitleArrangement: false,
                                editedRangeKeyTitle: {}
                            })
                        }
                    })
                }}
            >
                {formatMessage({ id: 'save' })}
            </Button>
        }

        {
            state.schoolArrangementList && state.schoolArrangementList.length > 0 &&
            <div className='arrangementList'>
                <div
                    className='arrangementList-child target'
                    key={0}
                    id={0}
                    onDrop={onDrop}
                    onDragEnter={onDragEnter}
                    onDragLeave={onDragLeave}
                    onDragOver={(e) => e.preventDefault()}
                >
                    {formatMessage({ id: 'unsign list' })} ({
                        state.schoolCourseArrangementList[0]
                            ? state.schoolCourseArrangementList[0].length
                            : 0
                    })
                </div>
                {
                    Object.values(state.schoolArrangementList).map((value, key) => {
                        return <div
                            className='arrangementList-child target'
                            key={value.id}
                            id={value.id}
                            onDrop={onDrop}
                            onDragEnter={onDragEnter}
                            onDragLeave={onDragLeave}
                            onDragOver={(e) => e.preventDefault()}
                        >
                            {value.title} ({state.schoolCourseArrangementList[value.id]
                                ? state.schoolCourseArrangementList[value.id].length
                                : 0
                            })
                        </div>
                    })
                }
            </div>
        }
        {

            <div className="classify-list-page" >
                <div
                    key={0}
                    className="classify-list"
                    onDragOver={(e) => e.preventDefault()}
                >
                    <div className="name" classify-index={-1}>
                        <span>
                            {formatMessage({ id: 'unsign list' })} ({
                                state.schoolCourseArrangementList[0]
                                    ? state.schoolCourseArrangementList[0].length
                                    : 0
                            })
                    </span>
                    </div>
                    <div
                        key={0}
                        id={0}
                        className="target"
                        onDrop={onDrop}
                        onDragEnter={onDragEnter}
                        onDragLeave={onDragLeave}
                        onDragOver={(e) => e.preventDefault()}
                    >
                        {
                            state.schoolCourseArrangementList[0] &&
                            Object.values(state.schoolCourseArrangementList[0]).map((course, key) => {
                                return (
                                    <div
                                        className="person-card"
                                        key={course.id}
                                        id={course.id}
                                        draggable="true"
                                        onDragStart={dragStart}
                                        course-info={JSON.stringify({ ...course })}
                                    >
                                        <span>{course.course_name}</span>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
                {
                    Object.values(state.schoolArrangementList).map((list, index) => {
                        return (
                            <div
                                key={list.id}
                                className="classify-list"
                                onDragOver={(e) => e.preventDefault()}
                            >
                                <div className="name" classify-index={index}>
                                    <span>
                                        {list.title} ({
                                            state.schoolCourseArrangementList[list.id]
                                                ? state.schoolCourseArrangementList[list.id].length
                                                : 0
                                        }) {
                                            state.editTitleArrangement &&
                                            <InputNumber
                                                onChange={(e) => {
                                                    let recordTitleRangeKey = state.recordTitleRangeKey
                                                    let editedRangeKeyTitle = state.editedRangeKeyTitle
                                                    recordTitleRangeKey[list.id] = parseFloat(e).toFixed(1)
                                                    editedRangeKeyTitle[list.id] = recordTitleRangeKey[list.id]
                                                    setState({ recordTitleRangeKey, editedRangeKeyTitle })
                                                }}
                                                value={state.recordTitleRangeKey[list.id]}
                                            />
                                        }
                                    </span>

                                    <div className="classify-list-btnList">
                                        <Button
                                            className="classify-list-btnList-btn"
                                            type="primary"
                                            onClick={() => setState({
                                                modalVisible: true,
                                                mode: 'edit',
                                                editTitleKey: list.id,
                                                title: list.title
                                            })}
                                        >
                                            {formatMessage({ id: 'setting arrangement title' })}
                                        </Button>

                                        <Popconfirm
                                            key={'delete'}
                                            title={formatMessage({ id: 'Are you sure?' })}
                                            onConfirm={() => {
                                                dispatch({
                                                    type: 'school/e_editSchoolCourseArrangementTitle',
                                                    payload: {
                                                        arrangement_id: list.id,
                                                        school_id: props.related_id,
                                                        action: 'deleteTitle'
                                                    }
                                                }).then((result) => {
                                                    if (result) {
                                                        setState({
                                                            ...result,
                                                            loading: false
                                                        })
                                                    }
                                                })
                                            }}
                                            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                        >
                                            <Button
                                                type="danger"
                                            >
                                                {formatMessage({ id: 'delete' })}
                                            </Button>
                                        </Popconfirm>

                                        <Button
                                            disabled={state.editCourseArrangement}
                                            className="classify-list-btnList-btn"
                                            type="primary"
                                            onClick={() => setState({ editCourseArrangement: true, ckey: list.id })}
                                        >
                                            {formatMessage({ id: 'edit course arrangement' })}
                                        </Button>

                                        {
                                            state.editCourseArrangement && state.ckey == list.id &&
                                            <Button
                                                className="classify-list-btnList-btn"
                                                type="danger"
                                                onClick={() => {
                                                    dispatch({
                                                        type: 'school/e_editSchoolCourseArrangementTitle',
                                                        payload: {
                                                            editedRangeKeyCourse: state.editedRangeKeyCourse,
                                                            school_id: props.related_id,
                                                            action: 'editCourseRangeKey'
                                                        }
                                                    }).then((result) => {
                                                        if (result) {
                                                            setState({
                                                                ...result,
                                                                loading: false,
                                                                editCourseArrangement: false,
                                                                editedRangeKeyCourse: {},
                                                                ckey: 0
                                                            })
                                                        }
                                                    })

                                                }}
                                            >
                                                {formatMessage({ id: 'save' })}
                                            </Button>
                                        }
                                    </div>
                                </div>
                                <div
                                    key={list.id}
                                    id={list.id}
                                    className="target"
                                    onDrop={onDrop}
                                    onDragEnter={onDragEnter}
                                    onDragLeave={onDragLeave}
                                    onDragOver={(e) => e.preventDefault()}
                                >
                                    {
                                        state.schoolCourseArrangementList[list.id] &&
                                        Object.values(state.schoolCourseArrangementList[list.id]).map((course, key) => {
                                            return (
                                                <div
                                                    className="person-card"
                                                    key={course.id}
                                                    id={course.id}
                                                    draggable="true"
                                                    onDragStart={dragStart}
                                                    course-info={JSON.stringify({ ...course })}
                                                >
                                                    <span>{course.course_name}</span> {
                                                        state.ckey == list.id &&
                                                        <InputNumber
                                                            onChange={(e) => {
                                                                let recordCourseRangeKey = state.recordCourseRangeKey
                                                                let editedRangeKeyCourse = state.editedRangeKeyCourse
                                                                recordCourseRangeKey[course.id] = parseFloat(e).toFixed(1)
                                                                editedRangeKeyCourse[course.id] = recordCourseRangeKey[course.id]
                                                                setState({ recordCourseRangeKey, editedRangeKeyCourse })
                                                            }}
                                                            value={state.recordCourseRangeKey[course.id]}
                                                        />
                                                    }

                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        )
                    })
                }
            </div>

        }
    </div>
}