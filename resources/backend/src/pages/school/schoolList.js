import React, { useState, useEffect } from 'react';
import DataTable from '@/components/dataTable'
import { Button, Tag, Popconfirm } from 'antd';
import router from 'umi/router'
import Authorized from '@/components/authorized'
import { formatMessage } from 'umi/locale';
import { useDispatch } from 'react-redux'
import { SearchOutlined, QuestionCircleOutlined } from '@ant-design/icons';

export default function SchoolList(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        totalItem: 0,
        currentPage: 0,
        loading: true,
        dataList: [],
        filterArr: {},
        columnsList: [{
            title: 'school_name',
            dataIndex: 'school_name', //follow by this key show data
            key: 'school_name',
            search: true,

        },
        {
            title: 'school_name_cn',
            dataIndex: 'school_name_cn', //follow by this key show data
            key: 'school_name_cn',
            search: true,

        },
        {
            title: 'address',
            dataIndex: 'address', //follow by this key show data
            key: 'address',
            search: true,
        },
        {
            title: 'actions',
            dataIndex: 'actions',
            fixed: 'right',
            render: (tags, listValue) => (
                <span >
                    <Authorized
                        authority={
                            ['admin', 'superb']
                        }
                    >
                        <Popconfirm key={'delete'}
                            title={formatMessage({ id: 'Are you sure?' })}
                            onConfirm={() => handleUpdate({ school_id: listValue.school_id, action: 'delete' })}
                            icon={< QuestionCircleOutlined style={
                                { color: 'red' }}
                            />} >
                            <Tag color={'red'} > {formatMessage({ id: 'delete' }).toUpperCase()}
                            </Tag> </Popconfirm>

                        <Popconfirm
                            key={'school_status'}
                            title={formatMessage({ id: 'Are you sure?' })}
                            onConfirm={() => handleUpdate({ school_id: listValue.school_id, action: 'activation', type: 'school' })}
                            icon={< QuestionCircleOutlined style={{ color: 'red' }} />}
                        >
                            {
                                listValue.active ?
                                    <Tag color={'orange'} > {formatMessage({ id: 'hide info' }).toUpperCase()} </Tag> :
                                    <Tag color={'orange'} > {formatMessage({ id: 'show info' }).toUpperCase()} </Tag>
                            } </Popconfirm>

                        <Popconfirm
                            key={'acc_status'}
                            title={formatMessage({ id: 'Are you sure?' })}
                            onConfirm={
                                () => handleUpdate({ school_id: listValue.school_id, action: 'activation', type: 'user' })}
                            icon={< QuestionCircleOutlined style={
                                { color: 'red' }}
                            />} >
                            {
                                listValue.acc_status ?
                                    <
                                        Tag color={'red'} > {formatMessage({ id: 'freeze account' }).toUpperCase()} </Tag> :
                                    <Tag color={'red'} > {formatMessage({ id: 'active account' }).toUpperCase()} </Tag>
                            }

                        </Popconfirm>

                        {
                            !listValue.generated_account &&
                            <Popconfirm
                                key={'generated_account'}
                                title={formatMessage({ id: 'generate frontend login auth' })}
                                onConfirm={() => {
                                    if (listValue.sEmail) {
                                        let r = confirm(
                                            formatMessage({ id: 'The email of this account already has an account in the frontend. Are you sure to overwrite?' })+
                                            listValue.sEmail
                                        )
                                        if (r) {
                                            handleUpdate({ school_id: listValue.school_id, action: 'generateAccount' })
                                        }
                                    }
                                    else {
                                        handleUpdate({ school_id: listValue.school_id, action: 'generateAccount' })
                                    }

                                }}
                                icon={< QuestionCircleOutlined style={{ color: 'red' }} />}
                            >
                                <Tag color={'pink'} > {formatMessage({ id: 'generate frontend login auth' }).toUpperCase()}</Tag>
                            </Popconfirm>
                        }

                    </Authorized>


                    <Tag
                        key={'view'}
                        color={'geekblue'}
                        onClick={
                            () => goTo('/school/viewSchool?school_id=' + listValue.school_id)} >
                        {formatMessage({ id: 'view' }).toUpperCase()}
                    </Tag>
                </span>
            ),
        }
        ]
    });

    useEffect(() => {
        requestData()
    }, []);

    function setState(value, callback = '') {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
        if (callback != '') {
            callback()
        }
    }

    function requestData(clickPage = 1) {
        setState({
            loading: true
        })
        dispatch({
            type: 'school/e_getSchoolList',
            payload: {
                isList: true,
                clickPage,
                filterArr: state.filterArr
            }
        }).then((value) => {
            setState({
                ...value,
                loading: false,
                currentPage: value.totalItem > 0 ? clickPage : 0
            })
        })
    }

    function goTo(url) {
        router.push(url)
    }

    function handleUpdate(data) {
        setState({
            loading: true
        })

        dispatch({
            type: 'school/e_updateSchool',
            payload: {
                ...data,
                clickPage: state.clickPage,
                isList: true,
                filterArr: state.filterArr
            }
        }).then((value) => {
            setState({
                ...value,
                loading: false
            })
        })
    }

    return (
        <div >
            <Button style={{ marginRight: '5px', marginBottom: '10px' }}
                type="primary"
                onClick={
                    () => goTo('/school/addNewSchool')} >
                {
                    formatMessage({ id: 'add new school' })
                }
            </Button>

            <DataTable goTo={goTo}
                requestData={requestData} {...state}
                key={'school_id'}
                setState={setState}
            />
        </div>
    );
}