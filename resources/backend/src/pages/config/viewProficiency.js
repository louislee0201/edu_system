import React, { useEffect, useState } from "react";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import DataView from '@/components/dataView'
import { Tabs, Button, Popconfirm, Tag } from "antd";
import { Radio } from "antd";
import DataTable from '@/components/dataTable'
import Authorized from '@/components/authorized'
import { SearchOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import QualificationSubjectModal from './qualificationSubjectModal'
import GradeModal from '@/components/gradeModal'

export default function ViewCourse(props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        GradeAddModalVisible: false,
        editData: {},
        modalMode: '',
        loading: true
    })

    useEffect(() => {
        const proficiency_id = props.location.query.proficiency_id;
        if (proficiency_id) {
            dispatch({
                type: "config/e_getProficiencyList",
                payload: {
                    isList: false,
                    proficiency_id
                }
            }).then(result => {
                if (result) {
                    setState({
                        ...result,
                        loading: false
                    })
                }
            });
        }
    }, []);

    function loadColumnsList() {
        return (
            [
                {
                    title: 'grade',
                    dataIndex: 'grade',//follow by this key show data
                    key: 'grade'
                },
                {
                    title: 'score',
                    dataIndex: 'score',//follow by this key show data
                    key: 'score'
                },
                {
                    title: 'cgpa',
                    dataIndex: 'cgpa',//follow by this key show data
                    key: 'cgpa'
                },
                {
                    title: 'actions',
                    dataIndex: 'actions',
                    fixed: 'right',
                    render: (tags, listValue) => (
                        <span>
                            <Authorized authority={['admin', 'superb']}>
                                <Popconfirm
                                    key={'delete'}
                                    title={formatMessage({ id: 'Are you sure?' })}
                                    onConfirm={() => handleUpdate({ proficiency_id: listValue.proficiency_id, action: 'delete', grade_id: listValue.id })}
                                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                >
                                    <Tag color={'red'}>
                                        {formatMessage({ id: 'delete' }).toUpperCase()}
                                    </Tag>
                                </Popconfirm>

                                <Popconfirm
                                    key={'school_status'}
                                    title={formatMessage({ id: 'Are you sure?' })}
                                    onConfirm={() => handleUpdate({ proficiency_id: listValue.proficiency_id, action: 'activation', grade_id: listValue.id })}
                                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                >
                                    {
                                        listValue.active
                                            ?
                                            <Tag color={'orange'}>
                                                {formatMessage({ id: 'hide info' }).toUpperCase()}
                                            </Tag>
                                            :
                                            <Tag color={'orange'}>
                                                {formatMessage({ id: 'show info' }).toUpperCase()}
                                            </Tag>
                                    }
                                </Popconfirm>

                            </Authorized>

                            <Tag
                                key={'view'}
                                color={'geekblue'}
                                onClick={() =>
                                    setState({
                                        GradeAddModalVisible: true,
                                        modalMode: 'edit',
                                        editData: {
                                            score: listValue.score,
                                            cgpa: listValue.cgpa,
                                            grade: listValue.grade,
                                            grade_id: listValue.id
                                        }
                                    })
                                }
                            >
                                {formatMessage({ id: 'edit' }).toUpperCase()}
                            </Tag>
                        </span>
                    ),
                }
            ]
        )
    }

    function loadTitle() {
        return (
            [
                {
                    label: "proficiency_name",
                    key: "proficiency_name"
                },
                {
                    label: "country",
                    key: "country",
                    default_element:
                        state.countryList &&
                        Object.values(state.countryList).map((value) => {
                            if (value.id == state.country) {
                                return value.country
                            }
                        })
                    ,
                    edit_element:
                        state.countryList &&
                        <Radio.Group onChange={(e) => setState({ country: e.target.value })} value={state.country}>
                            {

                                Object.values(state.countryList).map((value, key) => {
                                    return (
                                        <Radio key={key} value={value.id}>{value.country}</Radio>
                                    )
                                })
                            }
                        </Radio.Group>
                }
            ]
        )
    }

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }


    function updateData(callback = () => { }) {
        dispatch({
            type: 'config/e_updateProficiency',
            payload: {
                isList: false,
                ...state,
                action: 'edit'
            }
        }).then((result) => {
            if (result) {
                setState({
                    ...result
                })
                callback()
            }
        })
    }

    function goTo(url) {
        router.push(url)
    }

    function handleUpdate(data) {
        setState({
            loading: true
        })

        dispatch({
            type: 'config/e_updateProficiencyGrade',
            payload: {
                qualification_id: state.qualification_id,
                ...data,
                clickPage: state.clickPage,
                isList: true,
                filterArr: state.filterArr
            }
        }).then((value) => {
            setState({
                ...value,
                loading: false
            })
        })
    }

    return (
        <Tabs type="card">
            <Tabs.TabPane tab={formatMessage({ id: 'data' })} key="attr">
                <DataView {...state} title={loadTitle()} setState={setState} updateData={updateData} />
            </Tabs.TabPane>

            <Tabs.TabPane tab={formatMessage({ id: 'level list' })} key="Grade">
                <Button
                    style={{ marginRight: '5px', marginBottom: '10px' }}
                    type="primary"
                    onClick={() =>
                        setState({
                            GradeAddModalVisible: true,
                            modalMode: 'add'
                        })
                    }
                >
                    {
                        formatMessage({ id: 'add new grade' })
                    }
                </Button>

                <DataTable
                    goTo={goTo}
                    columnsList={loadColumnsList()}
                    {...({ ...state, dataList: state.proficiencyGradeList })}
                    key={'id'}
                    setState={setState}
                />

                <GradeModal
                    onClose={(result) => {
                        setState({
                            editData: {},
                            modalMode: '',
                            GradeAddModalVisible: result.visible
                        })

                        if (result.proficiencyGradeList) {
                            setState({ proficiencyGradeList: result.proficiencyGradeList })
                        }
                    }}
                    dispatchType={'config/e_updateProficiencyGrade'}
                    visible={state.GradeAddModalVisible}
                    {...({ ...state, dataList: state.proficiencyGradeList })}
                />
            </Tabs.TabPane>
        </Tabs >
    )
}