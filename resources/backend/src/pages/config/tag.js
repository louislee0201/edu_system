import React, { useState, useEffect } from 'react';
import DataTable from '@/components/dataTable'
import { Button, Tag, Popconfirm, Input, Modal } from 'antd';
import router from 'umi/router'
import Authorized from '@/components/authorized'
import { formatMessage } from 'umi/locale';
import { useDispatch } from 'react-redux'
import { SearchOutlined, QuestionCircleOutlined } from '@ant-design/icons';

export default (props) => {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        totalItem: 0,
        currentPage: 0,
        loading: true,
        dataList: [],
        filterArr: {},
        columnsList: [],
        addModalVisible: false,
        mainTagList: [],
        main_tag: ''
    });

    useEffect(() => {
        requestData()
    }, []);

    function setState(value, callback = '') {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
        if (callback != '') {
            callback()
        }
    }

    function requestData(clickPage = 1) {
        setState({
            loading: true
        })
        dispatch({
            type: 'config/e_getTag',
            payload: {}
        }).then((value) => {
            setState({
                ...value,
                loading: false
            })
        })
    }

    function goTo(url) {
        router.push(url)
    }

    function handleUpdate(data) {
        setState({
            loading: true
        })

        dispatch({
            type: 'config/e_updateMainTagList',
            payload: {
                ...data,
            }
        }).then((value) => {
            setState({
                ...value,
                loading: false
            })
        })
    }

    return (
        <div>
            <Button
                style={{ marginRight: '5px', marginBottom: '10px' }}
                type="primary"
                onClick={() => setState({ addModalVisible: true })}
            >
                {
                    formatMessage({ id: 'add new main tag' })
                }
            </Button>

            <Modal
                okButtonProps={{ disabled: state.main_tag == '' }}
                title={formatMessage({ id: 'add new main tag' })}
                visible={state.addModalVisible}
                onOk={() => {
                    setState({
                        loading: true
                    })
                    dispatch({
                        type: 'config/e_updateMainTagList',
                        payload: {
                            action: 'add',
                            main_tag: state.main_tag
                        }
                    }).then((value) => {
                        setState({
                            ...value,
                            loading: false,
                            addModalVisible: false,
                            main_tag: ''
                        })
                    })
                }}
                onCancel={() => setState({ addModalVisible: false, main_tag: '' })}
            >
                <Input value={state.main_tag} onChange={(e) => setState({ main_tag: e.target.value })}></Input>
            </Modal>

            <DataTable
                goTo={goTo}
                requestData={requestData}
                {...state}
                dataList={state.mainTagList}
                columnsList={[
                    {
                        title: 'main_tag',
                        dataIndex: 'main_tag',//follow by this key show data
                        key: 'main_tag',
                        // search: true,

                    },
                    {
                        title: 'actions',
                        dataIndex: 'actions',
                        fixed: 'right',
                        render: (tags, listValue) => (
                            <span>
                                <Authorized authority={['admin', 'superb']}>
                                    <Popconfirm
                                        key={'delete'}
                                        title={formatMessage({ id: 'Are you sure?' })}
                                        onConfirm={() => handleUpdate({ id: listValue.id, action: 'delete' })}
                                        icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                    >
                                        <Tag color={'red'}>
                                            {formatMessage({ id: 'delete' }).toUpperCase()}
                                        </Tag>
                                    </Popconfirm>

                                    <Popconfirm
                                        key={'school_status'}
                                        title={formatMessage({ id: 'Are you sure?' })}
                                        onConfirm={() => handleUpdate({ id: listValue.id, action: 'activation' })}
                                        icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                    >
                                        {
                                            listValue.active
                                                ?
                                                <Tag color={'orange'}>
                                                    {formatMessage({ id: 'hide info' }).toUpperCase()}
                                                </Tag>
                                                :
                                                <Tag color={'orange'}>
                                                    {formatMessage({ id: 'show info' }).toUpperCase()}
                                                </Tag>
                                        }
                                    </Popconfirm>


                                </Authorized>


                                <Tag
                                    key={'view'}
                                    color={'geekblue'}
                                    onClick={() => goTo('/config/viewTag?main_tag_id=' + listValue.id)}
                                >
                                    {formatMessage({ id: 'view' }).toUpperCase()}
                                </Tag>
                            </span>
                        ),
                    }
                ]}
                key={'id'}
                setState={setState}
            />
        </div>
    );
}