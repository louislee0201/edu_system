import React, { useEffect, useState } from "react";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import DataView from '@/components/dataView'
import { Tabs, Button, Popconfirm, Tag } from "antd";
import { Radio } from "antd";
import DataTable from '@/components/dataTable'
import Authorized from '@/components/authorized'
import { SearchOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import QualificationSubjectModal from './qualificationSubjectModal'
import GradeModal from '@/components/gradeModal'

export default function ViewCourse(props) {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        tabMode: 'attr',
        SubjectData: {
            totalItem: 1,
            currentPage: 1,
            loading: true,
            dataList: [],
            filterArr: {}
        },
        GradeData: {
            totalItem: 1,
            currentPage: 1,
            loading: true,
            dataList: [],
            filterArr: {}
        },

        modalMode: '',
        GradeAddModalVisible: false,
        SubjectAddModalVisible: false,

        editData: {}
    })

    useEffect(() => {
        const qualification_id = props.location.query.qualification_id;
        if (qualification_id) {
            dispatch({
                type: "config/e_getQualificationList",
                payload: {
                    isList: false,
                    qualification_id
                }
            }).then(result => {
                if (result) {
                    setState({
                        ...result,
                        SubjectData: {
                            ...state.SubjectData,
                            dataList: result.qualification_subject,
                            loading: false
                        },
                        GradeData: {
                            ...state.GradeData,
                            dataList: result.qualification_grade,
                            loading: false
                        },
                    })
                }
            });
        }
    }, []);

    function loadColumnsList() {
        if (state.tabMode == 'Subject') {
            return (
                [
                    {
                        title: 'subject_name',
                        dataIndex: 'subject_name',//follow by this key show data
                        key: 'subject_name'
                    },
                    {
                        title: 'code',
                        dataIndex: 'code',//follow by this key show data
                        key: 'code'
                    },
                    {
                        title: 'proficiency subject',
                        dataIndex: 'is_proficiency',//follow by this key show data
                        key: 'is_proficiency',
                        render: (tags, listValue) => (
                            <span>
                                {
                                    tags
                                        ? <span style={{ color: 'red' }}>{formatMessage({ id: 'yes' })}</span>
                                        : formatMessage({ id: 'no' })
                                }
                            </span>
                        )
                    },
                    {
                        title: 'actions',
                        dataIndex: 'actions',
                        fixed: 'right',
                        render: (tags, listValue) => (
                            <span>
                                <Authorized authority={['admin', 'superb']}>
                                    <Popconfirm
                                        key={'delete'}
                                        title={formatMessage({ id: 'Are you sure?' })}
                                        onConfirm={() => handleUpdate({ qualification_subject_id: listValue.id, action: 'delete' })}
                                        icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                    >
                                        <Tag color={'red'}>
                                            {formatMessage({ id: 'delete' }).toUpperCase()}
                                        </Tag>
                                    </Popconfirm>

                                    <Popconfirm
                                        key={'school_status'}
                                        title={formatMessage({ id: 'Are you sure?' })}
                                        onConfirm={() => handleUpdate({ qualification_subject_id: listValue.id, action: 'activation' })}
                                        icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                    >
                                        {
                                            listValue.active
                                                ?
                                                <Tag color={'orange'}>
                                                    {formatMessage({ id: 'hide info' }).toUpperCase()}
                                                </Tag>
                                                :
                                                <Tag color={'orange'}>
                                                    {formatMessage({ id: 'show info' }).toUpperCase()}
                                                </Tag>
                                        }
                                    </Popconfirm>

                                </Authorized>

                                <Tag
                                    key={'view'}
                                    color={'geekblue'}
                                    onClick={() =>
                                        setState({
                                            SubjectAddModalVisible: true,
                                            modalMode: 'edit',
                                            editData: {
                                                code: listValue.code,
                                                is_proficiency: listValue.is_proficiency,
                                                language: listValue.language,
                                                qualification_subject_txt: listValue.subject_name,
                                                qualification_subject_id: listValue.id
                                            }
                                        })
                                    }
                                >
                                    {formatMessage({ id: 'edit' }).toUpperCase()}
                                </Tag>
                            </span>
                        ),
                    }
                ]
            )
        }
        else if (state.tabMode == 'Grade') {
            return (
                [
                    {
                        title: 'grade',
                        dataIndex: 'grade',//follow by this key show data
                        key: 'grade'
                    },
                    {
                        title: 'score',
                        dataIndex: 'score',//follow by this key show data
                        key: 'score'
                    },
                    {
                        title: 'cgpa',
                        dataIndex: 'cgpa',//follow by this key show data
                        key: 'cgpa'
                    },
                    {
                        title: 'actions',
                        dataIndex: 'actions',
                        fixed: 'right',
                        render: (tags, listValue) => (
                            <span>
                                <Authorized authority={['admin', 'superb']}>
                                    <Popconfirm
                                        key={'delete'}
                                        title={formatMessage({ id: 'Are you sure?' })}
                                        onConfirm={() => handleUpdate({ grade_id: listValue.id, action: 'delete' })}
                                        icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                    >
                                        <Tag color={'red'}>
                                            {formatMessage({ id: 'delete' }).toUpperCase()}
                                        </Tag>
                                    </Popconfirm>

                                    <Popconfirm
                                        key={'school_status'}
                                        title={formatMessage({ id: 'Are you sure?' })}
                                        onConfirm={() => handleUpdate({ grade_id: listValue.id, action: 'activation' })}
                                        icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                    >
                                        {
                                            listValue.active
                                                ?
                                                <Tag color={'orange'}>
                                                    {formatMessage({ id: 'hide info' }).toUpperCase()}
                                                </Tag>
                                                :
                                                <Tag color={'orange'}>
                                                    {formatMessage({ id: 'show info' }).toUpperCase()}
                                                </Tag>
                                        }
                                    </Popconfirm>

                                </Authorized>

                                <Tag
                                    key={'view'}
                                    color={'geekblue'}
                                    onClick={() =>
                                        setState({
                                            GradeAddModalVisible: true,
                                            modalMode: 'edit',
                                            editData: {
                                                grade_id: listValue.id,
                                                score: listValue.score,
                                                cgpa: listValue.cgpa,
                                                grade: listValue.grade
                                            }
                                        })
                                    }
                                >
                                    {formatMessage({ id: 'edit' }).toUpperCase()}
                                </Tag>
                            </span>
                        ),
                    }
                ]
            )
        }
        return []

    }

    function loadTitle() {
        return (
            [
                {
                    label: "qualification_name",
                    key: "qualification_name"
                },
                {
                    label: "country",
                    key: "country",
                    default_element:
                        state.countryList &&
                        Object.values(state.countryList).map((value) => {
                            if (value.id == state.country) {
                                return value.country
                            }
                        })
                    ,
                    edit_element:
                        state.countryList &&
                        <Radio.Group onChange={(e) => setState({ country: e.target.value })} value={state.country}>
                            {

                                Object.values(state.countryList).map((value, key) => {
                                    return (
                                        <Radio key={key} value={value.id}>{value.country}</Radio>
                                    )
                                })
                            }
                        </Radio.Group>
                }
            ]
        )
    }

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }


    function updateData(callback = () => { }) {
        dispatch({
            type: 'config/e_updateQualificationList',
            payload: {
                isList: false,
                ...state,
                action: 'edit'
            }
        }).then((result) => {
            if (result) {
                setState({
                    ...result
                })
                callback()
            }
        })
    }

    function requestData(clickPage = 1) {
        setState({
            loading: true
        })
        dispatch({
            type: 'config/e_getQualificationList',
            payload: {
                isList: true,
                clickPage,
                filterArr: state.filterArr
            }
        }).then((value) => {
            setState({
                ...value,
                loading: false,
                currentPage: value.totalItem > 0 ? clickPage : 0,
                pageLoad: !state.pageLoad ? true : false
            })
        })
    }

    function goTo(url) {
        router.push(url)
    }

    function handleUpdate(data) {
        setTableState({
            loading: true
        })

        dispatch({
            type: 'config/e_updateQualification' + state.tabMode,
            payload: {
                qualification_id: state.qualification_id,
                ...data,
                clickPage: state.clickPage,
                isList: true,
                filterArr: state.filterArr
            }
        }).then((value) => {
            setTableState({
                ...value,
                loading: false
            })
        })
    }

    function setTableState(values) {
        setState({
            [state.tabMode + 'Data']: {
                ...state[state.tabMode + 'Data'],
                ...values
            }
        })
    }

    return (
        <Tabs type="card" onChange={(e) => setState({ tabMode: e })}>
            <Tabs.TabPane tab={formatMessage({ id: 'data' })} key="attr">
                <DataView {...state} title={loadTitle()} setState={setState} updateData={updateData} />
            </Tabs.TabPane>

            <Tabs.TabPane tab={formatMessage({ id: 'subject list' })} key="Subject">

                <Button
                    style={{ marginRight: '5px', marginBottom: '10px' }}
                    type="primary"
                    onClick={() =>
                        setState({
                            SubjectAddModalVisible: true,
                            modalMode: 'add'
                        })
                    }
                >
                    {
                        formatMessage({ id: 'add new subject' })
                    }
                </Button>

                <QualificationSubjectModal
                    onClose={(result) => setState({ SubjectAddModalVisible: result })}
                    visible={state.SubjectAddModalVisible}
                    setPropsState={setState}
                    {...state}
                />

                <DataTable
                    goTo={goTo}
                    requestData={requestData}
                    columnsList={loadColumnsList()}
                    {...state.SubjectData}
                    key={'id'}
                    setState={setTableState}
                />
            </Tabs.TabPane>

            <Tabs.TabPane tab={formatMessage({ id: 'level list' })} key="Grade">
                <Button
                    style={{ marginRight: '5px', marginBottom: '10px' }}
                    type="primary"
                    onClick={() =>
                        setState({
                            GradeAddModalVisible: true,
                            modalMode: 'add'
                        })
                    }
                >
                    {
                        formatMessage({ id: 'add new grade' })
                    }
                </Button>

                <DataTable
                    goTo={goTo}
                    requestData={requestData}
                    columnsList={loadColumnsList()}
                    {...state.GradeData}
                    key={'id'}
                    setState={setTableState}
                />

                <GradeModal
                    onClose={(result) => {
                        setState({
                            editData: {},
                            modalMode: '',
                            GradeAddModalVisible: result.visible
                        })

                        if (result.dataList) {
                            setTableState({ dataList: result.dataList })
                        }
                    }}
                    dispatchType={'config/e_updateQualificationGrade'}
                    visible={state.GradeAddModalVisible}
                    {...({ ...state, dataList: state.GradeData.dataList })}
                />
            </Tabs.TabPane>
        </Tabs >
    )
}