import React, { useEffect, useState } from "react";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import DataView from '@/components/dataView'
import { Tabs, Button, Popconfirm, Tag, Modal, Input, Radio } from "antd";
import DataTable from '@/components/dataTable'
import Authorized from '@/components/authorized'
import { SearchOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import QualificationSubjectModal from './qualificationSubjectModal'
import GradeModal from '@/components/gradeModal'

export default (props) => {
    const dispatch = useDispatch()
    const [state, setOriState] = useState({
        loading: true,
        subTagList: [],
        addSubTagModalVisible: false,
        sub_tag: '',
        modalMode: 'add',
        sub_tag_id: 0,
        sub_country_id: 0
    })

    useEffect(() => {
        const main_tag_id = props.location.query.main_tag_id;
        if (main_tag_id) {
            dispatch({
                type: "config/e_updateMainTagList",
                payload: {
                    isList: false,
                    main_tag_id
                }
            }).then(result => {
                if (result) {
                    setState({
                        ...result,
                        main_tag_id
                    })
                }
            });
        }
    }, []);

    function loadColumnsList() {
        return (
            [
                {
                    title: 'sub_tag',
                    dataIndex: 'sub_tag',//follow by this key show data
                    key: 'sub_tag'
                },
                {
                    title: 'country',
                    dataIndex: 'country_id',//follow by this key show data
                    key: 'country_id',
                    render: (tags, listValue) => (
                        <span>
                            {
                                state.countryList &&
                                Object.values(state.countryList).map((value) => {
                                    if (value.id == listValue.country_id) {
                                        return value.country
                                    }
                                })
                            }
                        </span>
                    ),
                },
                {
                    title: 'actions',
                    dataIndex: 'actions',
                    fixed: 'right',
                    render: (tags, listValue) => (
                        <span>
                            <Authorized authority={['admin', 'superb']}>
                                <Popconfirm
                                    key={'delete'}
                                    title={formatMessage({ id: 'Are you sure?' })}
                                    onConfirm={() => handleUpdate({ sub_tag_id: listValue.id, action: 'delete' })}
                                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                >
                                    <Tag color={'red'}>
                                        {formatMessage({ id: 'delete' }).toUpperCase()}
                                    </Tag>
                                </Popconfirm>

                                <Popconfirm
                                    key={'school_status'}
                                    title={formatMessage({ id: 'Are you sure?' })}
                                    onConfirm={() => handleUpdate({ sub_tag_id: listValue.id, action: 'activation' })}
                                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                >
                                    {
                                        listValue.active
                                            ?
                                            <Tag color={'orange'}>
                                                {formatMessage({ id: 'hide info' }).toUpperCase()}
                                            </Tag>
                                            :
                                            <Tag color={'orange'}>
                                                {formatMessage({ id: 'show info' }).toUpperCase()}
                                            </Tag>
                                    }
                                </Popconfirm>

                            </Authorized>

                            <Tag
                                key={'view'}
                                color={'geekblue'}
                                onClick={() =>
                                    setState({
                                        addSubTagModalVisible: true,
                                        modalMode: 'edit',
                                        sub_tag: listValue.sub_tag,
                                        sub_country_id: listValue.country_id,
                                        sub_tag_id: listValue.id
                                    })
                                }
                            >
                                {formatMessage({ id: 'edit' }).toUpperCase()}
                            </Tag>
                        </span>
                    ),
                }
            ]
        )
    }

    function loadTitle() {
        return (
            [
                {
                    label: "main_tag",
                    key: "main_tag"
                },
                {
                    label: "country",
                    key: "country_id",
                    default_element: state.countryList &&
                        Object.values(state.countryList).map((value) => {
                            if (value.id == state.country_id) {
                                return value.country
                            }
                        }),
                    edit_element: state.countryList &&
                        <Radio.Group value={state.country_id} onChange={(e) => setState({ country_id: e.target.value })}>
                            {
                                Object.values(state.countryList).map((value, key) => {
                                    return (
                                        <Radio key={key} value={value.id}>{value.country}</Radio>
                                    )
                                })
                            }
                        </Radio.Group>
                },
            ]
        )
    }

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }


    function updateData(callback = () => { }) {
        setState({
            loading: true
        })
        dispatch({
            type: 'config/e_updateMainTagList',
            payload: {
                ...state,
                action: 'edit'
            }
        }).then((result) => {
            if (result) {
                setState({
                    loading: false,
                    ...result
                })
                callback()
            }
        })
    }

    function requestData(clickPage = 1) {
        setState({
            loading: true
        })
        dispatch({
            type: 'config/e_updateSubTagList',
            payload: {
                isList: true
            }
        }).then((value) => {
            setState({
                ...value,
                loading: false
            })
        })
    }

    function goTo(url) {
        router.push(url)
    }

    function handleUpdate(data) {
        setState({
            loading: true
        })

        dispatch({
            type: 'config/e_updateSubTagList',
            payload: {
                ...data,
            }
        }).then((value) => {
            setState({
                ...value,
                loading: false
            })
        })
    }

    function setTableState(values) {
        setState({
            [state.tabMode + 'Data']: {
                ...state[state.tabMode + 'Data'],
                ...values
            }
        })
    }

    return (
        <Tabs type="card">
            <Tabs.TabPane tab={formatMessage({ id: 'data' })} key="attr">
                <DataView {...state} title={loadTitle()} setState={setState} updateData={updateData} />
            </Tabs.TabPane>

            <Tabs.TabPane tab={formatMessage({ id: 'sub tag list' })} key="subTagList">

                <Button
                    style={{ marginRight: '5px', marginBottom: '10px' }}
                    type="primary"
                    onClick={() =>
                        setState({
                            addSubTagModalVisible: true,
                            modalMode: 'add'
                        })
                    }
                >
                    {
                        formatMessage({ id: 'add new sub tag' })
                    }
                </Button>

                <Modal
                    okButtonProps={{ disabled: state.sub_tag == '' }}
                    title={formatMessage({ id: state.modalMode == 'add' ? 'add new sub tag' : 'setting sub tag' })}
                    visible={state.addSubTagModalVisible}
                    onOk={() => {
                        setState({
                            loading: true
                        })
                        dispatch({
                            type: 'config/e_updateSubTagList',
                            payload: {
                                action: state.modalMode,
                                sub_tag: state.sub_tag,
                                main_tag_id: state.main_tag_id,
                                sub_tag_id: state.sub_tag_id,
                                sub_country_id: state.sub_country_id
                            }
                        }).then((value) => {
                            setState({
                                ...value,
                                loading: false,
                                addSubTagModalVisible: false,
                                sub_tag: ''
                            })
                        })
                    }}
                    onCancel={() => setState({ addSubTagModalVisible: false, sub_tag: '' })}
                >
                    <Input value={state.sub_tag} onChange={(e) => setState({ sub_tag: e.target.value })}></Input>
                    <Radio.Group value={state.sub_country_id} onChange={(e) => setState({ sub_country_id: e.target.value })}>
                        {
                            state.countryList &&
                            Object.values(state.countryList).map((value, key) => {
                                return (
                                    <Radio key={key} value={value.id}>{value.country}</Radio>
                                )
                            })
                        }
                    </Radio.Group>
                </Modal>

                <DataTable
                    goTo={goTo}
                    requestData={requestData}
                    columnsList={loadColumnsList()}
                    dataList={state.subTagList}
                    {...state.SubjectData}
                    key={'id'}
                    setState={setTableState}
                />
            </Tabs.TabPane>
        </Tabs >
    )
}