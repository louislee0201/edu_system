import React, { useState, useEffect } from 'react';
import DataTable from '@/components/dataTable'
import { Button, Tag, Popconfirm, Tabs } from 'antd';
import router from 'umi/router'
import Authorized from '@/components/authorized'
import { formatMessage } from 'umi/locale';
import { useDispatch } from 'react-redux'
import { SearchOutlined, QuestionCircleOutlined } from '@ant-design/icons';

export default function QualificationList(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        totalItem: 0,
        currentPage: 0,
        loading: true,
        dataList: [],
        filterArr: {},
        columnsList: [],
        pageLoad: false,


        qualificationSubject: []
    });

    useEffect(() => {
        requestData()
        dispatch({
            type: 'config/e_getProficiencyQualificationSubject',
            payload: {
                isList: true,
                clickPage: 1
            }
        }).then((value) => {
            if (value) {
                setState({
                    qualificationSubject: value.dataList
                })
            }
        })
    }, []);

    useEffect(() => {
        if (state.pageLoad) {
            setState({
                columnsList: [
                    {
                        title: 'proficiency_name',
                        dataIndex: 'proficiency_name',//follow by this key show data
                        key: 'proficiency_name',
                        search: true,
                    },
                    {
                        title: 'language',
                        dataIndex: 'language',//follow by this key show data
                        key: 'language',
                        render: (tags, listValue) => (
                            <span>
                                {
                                    Object.values(state.languageList).map((value) => {
                                        if (value.id == tags) {
                                            return value.language
                                        }
                                    })
                                }
                            </span>
                        ),
                    },
                    {
                        title: 'country',
                        dataIndex: 'country',//follow by this key show data
                        key: 'country',
                        render: (tags, listValue) => (
                            <span>
                                {
                                    Object.values(state.countryList).map((value) => {
                                        if (value.id == listValue.country) {
                                            return value.country
                                        }
                                    })
                                }
                            </span>
                        ),
                    },
                    {
                        title: 'actions',
                        dataIndex: 'actions',
                        fixed: 'right',
                        render: (tags, listValue) => (
                            <span>
                                <Authorized authority={['admin', 'superb']}>
                                    <Popconfirm
                                        key={'delete'}
                                        title={formatMessage({ id: 'Are you sure?' })}
                                        onConfirm={() => handleUpdate({ proficiency_id: listValue.proficiency_id, action: 'delete' })}
                                        icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                    >
                                        <Tag color={'red'}>
                                            {formatMessage({ id: 'delete' }).toUpperCase()}
                                        </Tag>
                                    </Popconfirm>

                                    <Popconfirm
                                        key={'school_status'}
                                        title={formatMessage({ id: 'Are you sure?' })}
                                        onConfirm={() => handleUpdate({ proficiency_id: listValue.proficiency_id, action: 'activation' })}
                                        icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                    >
                                        {
                                            listValue.active
                                                ?
                                                <Tag color={'orange'}>
                                                    {formatMessage({ id: 'hide info' }).toUpperCase()}
                                                </Tag>
                                                :
                                                <Tag color={'orange'}>
                                                    {formatMessage({ id: 'show info' }).toUpperCase()}
                                                </Tag>
                                        }
                                    </Popconfirm>


                                </Authorized>


                                <Tag
                                    key={'view'}
                                    color={'geekblue'}
                                    onClick={() => goTo('/config/viewProficiency?proficiency_id=' + listValue.proficiency_id)}
                                >
                                    {formatMessage({ id: 'view' }).toUpperCase()}
                                </Tag>
                            </span>
                        ),
                    }
                ]
            })
        }
    }, [state.pageLoad])

    function setState(value, callback = '') {
        setOriState((prevState) => ({
            ...prevState,
            ...value
        }))
        if (callback != '') {
            callback()
        }
    }

    function requestData(clickPage = 1) {
        setState({
            loading: true
        })
        dispatch({
            type: 'config/e_getProficiencyList',
            payload: {
                isList: true,
                clickPage,
                filterArr: state.filterArr
            }
        }).then((value) => {
            setState({
                ...value,
                loading: false,
                currentPage: value.totalItem > 0 ? clickPage : 0,
                pageLoad: !state.pageLoad ? true : false
            })
        })
    }

    function goTo(url) {
        router.push(url)
    }

    function handleUpdate(data) {
        setState({
            loading: true
        })

        dispatch({
            type: 'config/e_updateProficiency',
            payload: {
                ...data,
                clickPage: state.clickPage,
                isList: true,
                filterArr: state.filterArr
            }
        }).then((value) => {
            setState({
                ...value,
                loading: false
            })
        })
    }

    return (

        <Tabs type="card">
            <Tabs.TabPane key={'proficiency list'} tab={formatMessage({ id: 'proficiency list' })}>
                <Button
                    style={{ marginRight: '5px', marginBottom: '10px' }}
                    type="primary"
                    onClick={() => goTo('/config/addProficiency')}
                >
                    {
                        formatMessage({ id: 'add new proficiency' })
                    }
                </Button>
                <DataTable goTo={goTo} requestData={requestData} {...state} key={'proficiency_id'} setState={setState} />
            </Tabs.TabPane>

            <Tabs.TabPane key={'qualification proficiency list'} tab={formatMessage({ id: 'qualification proficiency list' })}>
                <DataTable
                    goTo={goTo}
                    {...({
                        ...state,
                        columnsList: [
                            {
                                title: 'subject_name',
                                dataIndex: 'subject_name',//follow by this key show data
                                key: 'subject_name'
                            },
                            {
                                title: 'language',
                                dataIndex: 'language',//follow by this key show data
                                key: 'language',
                                render: (tags, listValue) => (
                                    <span>
                                        {
                                            Object.values(state.languageList).map((value) => {
                                                if (value.id == tags) {
                                                    return value.language
                                                }
                                            })
                                        }
                                    </span>
                                ),
                            },
                            {
                                title: 'qualification_name',
                                dataIndex: 'qualification_name',//follow by this key show data
                                key: 'qualification_name',
                            },
                            {
                                title: 'actions',
                                dataIndex: 'actions',
                                fixed: 'right',
                                render: (tags, listValue) => (
                                    <span>
                                        <Tag
                                            key={'view'}
                                            color={'geekblue'}
                                            onClick={() => goTo('/config/viewQualification?qualification_id=' + listValue.qualification_id)}
                                        >
                                            {formatMessage({ id: 'view' }).toUpperCase()}
                                        </Tag>
                                    </span>
                                ),
                            }
                        ],
                        dataList: state.qualificationSubject
                    })}
                    key={'subject_id'}
                />
            </Tabs.TabPane>
        </Tabs>
    );
}