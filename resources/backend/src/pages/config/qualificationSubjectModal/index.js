import React, { useEffect, useState } from "react";
import {
    Modal,
    Input,
    Button,
    Popconfirm,
    Collapse,
    message,
    Descriptions,
    Radio,
    Tag
} from "antd";
import { formatMessage } from "umi/locale";
import { useDispatch } from 'react-redux'
import { QuestionCircleOutlined } from '@ant-design/icons';
import LanguageModal from '@/components/languageModal'

export default function QualificationSubjectModal(props) {
    const dispatch = useDispatch();
    const [state, setOriState] = useState({
        buttonLoading: false,
        qualification_subject_id: 0,
        qualification_subject_txt: '',
        code: '',
        is_proficiency: 0,
        language: 0,
        languageList: [],
        languageModalVisible: false
    })

    function setState(values) {
        setOriState(prevState => ({
            ...prevState,
            ...values
        }))
    }
    useEffect(() => {
        if (props.modalMode == 'edit') {
            setState({
                ...props.editData
            })
        }
        // if (state.languageList <= 0) {
        //     dispatch({
        //         type: 'config/e_getLanguageList',
        //         payload: {}
        //     }).then((result) => {
        //         setState({
        //             ...result
        //         })
        //     })
        // }

    }, [props.visible]);

    function updateQualificationSubject(payload) {
        setState({
            buttonLoading: true
        })
        dispatch({
            type: 'config/e_updateQualificationSubject',
            payload: {
                ...payload,
                language: state.language,
                qualification_subject_id: state.qualification_subject_id,
                qualification_subject: state.qualification_subject_txt,
                code: state.code,
                is_proficiency: state.is_proficiency,
                qualification_id: props.qualification_id

            }
        }).then((value) => {

            props.setPropsState({
                mode: '',
                editData: {},
                SubjectAddModalVisible: false,
                SubjectData: {
                    ...props.SubjectData,
                    ...value
                }
            })

            setState({
                qualification_subject_id: 0,
                qualification_subject_txt: '',
                code: '',
                is_proficiency: 0,
                buttonLoading: false
            })

            message.success(formatMessage({ id: 'success update' }), 1)
        })
    }

    return (
        <Modal
            title={props.modalMode == 'add' ? formatMessage({ id: 'add new subject' }) : formatMessage({ id: 'setting qualification subject' })}
            visible={props.visible}
            footer={[
                <Button
                    key="submit"
                    loading={state.buttonLoading}
                    type="primary"
                    onClick={() => {
                        updateQualificationSubject({
                            action: props.modalMode
                        })
                    }}
                >
                    OK
                </Button>
            ]}
            onCancel={() => {
                props.setPropsState({
                    mode: '',
                    SubjectAddModalVisible: false,
                    editData: {}
                })
                setState({
                    qualification_subject_id: 0,
                    qualification_subject_txt: '',
                    code: '',
                    is_proficiency: 0,
                })
            }}
        >
            <Input style={{ marginBottom: '5px' }} placeholder={formatMessage({ id: 'subject_name' })} onChange={e => setState({ qualification_subject_txt: e.target.value })} value={state.qualification_subject_txt} />
            <Input placeholder={formatMessage({ id: 'code' })} onChange={e => setState({ code: e.target.value })} value={state.code} />

            <div style={{ display: 'flex', alignItems: 'center' }}>
                <Radio.Group
                    onChange={(e) => { setState({ is_proficiency: e.target.value }) }}
                    value={state.is_proficiency}
                >
                    <Radio value={1}>{formatMessage({ id: 'yes' })}</Radio>
                    <Radio value={0}>{formatMessage({ id: 'no' })}</Radio>
                </Radio.Group>
                <span style={{ color: 'red' }}>
                    {
                        formatMessage({ id: 'is_proficiency' }) + ' ?'
                    }
                </span>
            </div>

            {
                state.is_proficiency == 1 &&
                <div>
                    <span style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        {
                            formatMessage({ id: 'please select language' })
                        }

                        <Button
                            type="primary"
                            onClick={() =>
                                setState({
                                    languageModalVisible: true
                                })
                            }
                        >
                            {
                                formatMessage({ id: 'edit' })
                            }
                        </Button>
                    </span>

                    <Radio.Group onChange={(e) => setState({ language: e.target.value })} value={state.language}>
                        {

                            Object.values(state.languageList).map((value, key) => {
                                if (value.active) {
                                    return (
                                        <Radio key={key} value={value.id}>{value.language}</Radio>
                                    )
                                }
                            })
                        }
                    </Radio.Group>

                    <LanguageModal
                        onClose={(result) => setState({ languageModalVisible: result })}
                        visible={state.languageModalVisible}
                        {...state}
                        setState={setState}
                    />

                </div>
            }

        </Modal>
    )
}