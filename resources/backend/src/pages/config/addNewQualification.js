import React, { Component } from 'react'
import { connect } from 'dva';
import router from 'umi/router'
import { Form, Input, Button, Divider, InputNumber, message, Upload, Radio } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { formatMessage } from 'umi/locale';
import CountryModal from '@/components/countryModal'

@connect(({ }) => ({}))
class addNewSchool extends Component {
    state = {
        loading: false,
        iconLoading: false,
        visible: false,
        countryList: [],
        countryVisible: false
    };


    request(values) {
        this.props.dispatch({
            type: 'config/e_updateQualificationList',
            payload: {
                ...values,
                action: 'add',
                isList: false
            }
        }).then((qualification_id) => {
            if (qualification_id) {
                router.push('/config/viewQualification?qualification_id=' + qualification_id)
            }
            this.setState({
                loading: !this.state.loading,
                iconLoading: !this.state.iconLoading
            })
        })
    }

    submit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {

            if (!err) {
                this.setState({
                    loading: !this.state.loading,
                    iconLoading: !this.state.iconLoading
                })
                this.request(values)
            }
        });
    }

    loadForm() {
        return [
            {
                label: 'qualification_name',
                key: 'qualification_name',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'qualification_name_warning' })
                }],
                element: <Input />
            },
            {
                label: 'country',
                key: 'country',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'country_warning' })
                }],
                element: <Radio.Group>
                    {
                        Object.values(this.state.countryList).map((value, key) => {
                            if (value.active) {
                                return (
                                    <Radio key={key} value={value.id}>{value.country}</Radio>
                                )
                            }
                        })
                    }
                </Radio.Group>,
                rightElement: <React.Fragment>
                    <Button style={{ float: 'right' }} type="link" onClick={() => this.setState({ countryVisible: true })}>
                        {formatMessage({ id: 'setting country' })}
                    </Button>

                    <CountryModal
                        onClose={({ visible }) => this.setState({ countryVisible: visible })}
                        {...this.state}
                        visible={this.state.countryVisible}
                        setState={this.setState.bind(this)}
                    />
                </React.Fragment>

            }
        ]
    }

    render() {
        const { getFieldDecorator } = this.props.form
        return (
            <div>
                <Form className="" labelCol={{ span: 9 }} wrapperCol={{ span: 8 }}>
                    <Divider orientation="left">{formatMessage({ id: 'add new item' })}</Divider>

                    {
                        Object.values(this.loadForm()).map((value, key) => {
                            return (
                                <Form.Item key={key} label={formatMessage({ id: value.label })}>
                                    {
                                        value.rules
                                            ?
                                            getFieldDecorator(value.key, {
                                                rules: value.rules
                                            })
                                                (
                                                    value.element
                                                )
                                            :
                                            getFieldDecorator(value.key)
                                                (
                                                    value.element
                                                )
                                    }

                                    {
                                        value.rightElement &&
                                        value.rightElement
                                    }
                                </Form.Item>
                            )
                        })
                    }

                </Form>

                <div style={{ textAlign: 'center' }}>
                    <Button type="primary" loading={this.state.loading} onClick={this.submit.bind(this)}>
                        {formatMessage({ id: 'submit' })}
                    </Button>
                </div>
            </div>
        );
    }
}

const WrappedaddNewSchool = Form.create()(addNewSchool);
export default WrappedaddNewSchool