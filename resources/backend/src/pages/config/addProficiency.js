import React, { Component } from 'react'
import { connect } from 'dva';
import router from 'umi/router'
import { Form, Input, Button, Divider, InputNumber, message, Upload, Radio } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { formatMessage } from 'umi/locale';
import CountryModal from '@/components/countryModal'
import LanguageModal from '@/components/languageModal'

@connect(({ }) => ({}))
class addNewSchool extends Component {
    state = {
        loading: false,
        iconLoading: false,
        visible: false,
        countryList: [],
        languageList: [],
        languageModalVisible: false,
        countryVisible: false
    };

    request(values) {
        this.props.dispatch({
            type: 'config/e_updateProficiency',
            payload: {
                ...values,
                action: 'add',
                isList: false
            }
        }).then((proficiency_id) => {
            if (proficiency_id) {
                router.push('/config/viewProficiency?proficiency_id=' + proficiency_id)
            }
            this.setState({
                loading: !this.state.loading,
                iconLoading: !this.state.iconLoading
            })
        })
    }

    submit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                if (
                    values.reading_score_start >= values.reading_score_end && values.reading_score_start != 0 && values.reading_score_end != 0 ||
                    values.writing_score_start >= values.writing_score_end && values.writing_score_start != 0 && values.writing_score_end != 0 ||
                    values.speaking_score_start >= values.speaking_score_end && values.speaking_score_start != 0 && values.speaking_score_end != 0 ||
                    values.listening_score_start >= values.listening_score_end && values.listening_score_start != 0 && values.listening_score_end != 0
                ) {
                    message.error(formatMessage({ id: 'left score cannot more than right score' }), 1)
                    return
                }
                this.setState({
                    loading: !this.state.loading,
                    iconLoading: !this.state.iconLoading
                })
                this.request(values)
            }
        });
    }

    loadForm() {
        return [
            {
                label: 'proficiency_name',
                key: 'proficiency_name',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'proficiency_name_warning' })
                }],
                element: <Input />
            },
            {
                label: 'country',
                key: 'country',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'country_warning' })
                }],
                element: <Radio.Group>
                    {
                        Object.values(this.state.countryList).map((value, key) => {
                            if (value.active) {
                                return (
                                    <Radio key={key} value={value.id}>{value.country}</Radio>
                                )
                            }
                        })
                    }
                </Radio.Group>,
                rightElement: <React.Fragment>
                    <Button style={{ float: 'right' }} type="link" onClick={() => this.setState({ countryVisible: true })}>
                        {formatMessage({ id: 'setting country' })}
                    </Button>

                    <CountryModal
                        onClose={({ visible }) => this.setState({ countryVisible: visible })}
                        {...this.state}
                        setState={this.setState.bind(this)}
                        visible={this.state.countryVisible}
                    />
                </React.Fragment>

            },
            {
                label: 'language',
                key: 'language',
                rules: [{
                    required: true,
                    message: formatMessage({ id: 'language_warning' })
                }],
                element: <Radio.Group>
                    {
                        Object.values(this.state.languageList).map((value, key) => {
                            if (value.active) {
                                return (
                                    <Radio key={key} value={value.id}>{value.language}</Radio>
                                )
                            }
                        })
                    }
                </Radio.Group>,
                rightElement: <React.Fragment>
                    <Button style={{ float: 'right' }} type="link" onClick={() => this.setState({ languageModalVisible: true })}>
                        {formatMessage({ id: 'setting language' })}
                    </Button>

                    <LanguageModal
                        onClose={(result) => {
                            this.setState({
                                languageModalVisible: result
                            })
                        }}
                        {...({ ...this.state, visible: this.state.languageModalVisible })}
                        setState={this.setState.bind(this)}
                    />
                </React.Fragment>

            }
        ]
    }

    render() {
        const { getFieldDecorator } = this.props.form
        return (
            <div>
                <Form className="" labelCol={{ span: 9 }} wrapperCol={{ span: 8 }}>
                    <Divider orientation="left">{formatMessage({ id: 'add new item' })}</Divider>

                    {
                        Object.values(this.loadForm()).map((value, key) => {
                            return (
                                <Form.Item key={key} label={formatMessage({ id: value.label })}>
                                    {
                                        value.rules
                                            ?
                                            getFieldDecorator(value.key, {
                                                rules: value.rules
                                            })
                                                (
                                                    value.element
                                                )
                                            :
                                            getFieldDecorator(value.key)
                                                (
                                                    value.element
                                                )
                                    }

                                    {
                                        value.rightElement &&
                                        value.rightElement
                                    }
                                </Form.Item>
                            )
                        })
                    }

                    <Form.Item label={formatMessage({ id: 'Reading Score Limit' })}>
                        {
                            getFieldDecorator('reading_score_start')
                                (
                                    <InputNumber min={0} />

                                )
                        }
                        <span> ~ </span>
                        {
                            getFieldDecorator('reading_score_end')
                                (
                                    <InputNumber min={0} />

                                )
                        }
                    </Form.Item>

                    <Form.Item label={formatMessage({ id: 'Speaking Score Limit' })}>
                        {
                            getFieldDecorator('speaking_score_start')
                                (
                                    <InputNumber min={0} />

                                )
                        }
                        <span> ~ </span>
                        {
                            getFieldDecorator('speaking_score_end')
                                (
                                    <InputNumber min={0} />

                                )
                        }
                    </Form.Item>

                    <Form.Item label={formatMessage({ id: 'Writing Score Limit' })}>
                        {
                            getFieldDecorator('writing_score_start')
                                (
                                    <InputNumber min={0} />

                                )
                        }
                        <span> ~ </span>
                        {
                            getFieldDecorator('writing_score_end')
                                (
                                    <InputNumber min={0} />

                                )
                        }
                    </Form.Item>

                    <Form.Item label={formatMessage({ id: 'Listening Score Limit' })}>
                        {
                            getFieldDecorator('listening_score_start')
                                (
                                    <InputNumber min={0} />

                                )
                        }
                        <span> ~ </span>
                        {
                            getFieldDecorator('listening_score_end')
                                (
                                    <InputNumber min={0} />

                                )
                        }
                    </Form.Item>

                </Form>

                <div style={{ textAlign: 'center' }}>
                    <Button type="primary" loading={this.state.loading} onClick={this.submit.bind(this)}>
                        {formatMessage({ id: 'submit' })}
                    </Button>
                </div>
            </div>
        );
    }
}

const WrappedaddNewSchool = Form.create()(addNewSchool);
export default WrappedaddNewSchool