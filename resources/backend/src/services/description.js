import {
    API_UPDATE_DESCRIPTION, API_GET_DESCRIPTION_TYPE, API_GET_DESCRIPTION_LIST, API_GET_DESCRIPTION_DATA
} from '@/constants/api'

import request from '@/utils/request'

export function api_updateDescription(payload, method = 'POST') {
    return request(API_UPDATE_DESCRIPTION, method, payload);
}

export function api_getDescriptionType(payload, method = 'POST') {
    return request(API_GET_DESCRIPTION_TYPE, method, payload);
}

export function api_getDescriptionList(payload, method = 'POST') {
    return request(API_GET_DESCRIPTION_LIST, method, payload);
}
export function api_getDescriptionData(payload, method = 'POST') {
    return request(API_GET_DESCRIPTION_DATA, method, payload);
}
