import {
    API_UPDATE_COUNTRY, API_GET_COUNTRY, API_GET_COURSE_LEVEL, API_UPDATE_COURSE_LEVEL, API_SEARCH_SCHOOL, API_GET_QUALIFICATION, API_UPDATE_QUALIFICATION, API_UPDATE_QUALIFICATION_SUBJECT, API_UPDATE_QUALIFICATION_GRADE,
    API_GET_LANGUAGE_LIST, API_UPDATE_LANGUAGE, API_GET_PROFICIENCY_LIST, API_UPDATE_PROFICIENCY, API_GET_PROFICIENCY_QUALIFICATION_SUBJECT, API_GET_REGION_LIST,
    API_ADD_PROFICIENCY_GRADE, API_UPDATE_PROFICIENCY_GRADE, API_UPDATE_REGION, API_GET_TAG, API_UPDATE_MAIN_TAG_LIST, API_UPDATE_SUB_TAG_LIST, API_EDIT_COURSE_SUB_TAG_LIST
} from '@/constants/api'

import request from '@/utils/request'

export function api_editCourseSubTagList(payload, method = 'POST') {
    return request(API_EDIT_COURSE_SUB_TAG_LIST, method, payload);
}

export function api_updateSubTagList(payload, method = 'POST') {
    return request(API_UPDATE_SUB_TAG_LIST, method, payload);
}
export function api_updateMainTagList(payload, method = 'POST') {
    return request(API_UPDATE_MAIN_TAG_LIST, method, payload);
}

export function api_getTag(payload, method = 'POST') {
    return request(API_GET_TAG, method, payload);
}

export function api_getCountry(payload, method = 'POST') {
    return request(API_GET_COUNTRY, method, payload);
}

export function api_updateCountry(payload, method = 'POST') {
    return request(API_UPDATE_COUNTRY, method, payload);
}

export function api_getCourseLevel(payload, method = 'POST') {
    return request(API_GET_COURSE_LEVEL, method, payload);
}

export function api_updateCourseLevel(payload, method = 'POST') {
    return request(API_UPDATE_COURSE_LEVEL, method, payload);
}

export function api_searchSchool(payload, method = 'POST') {
    return request(API_SEARCH_SCHOOL, method, payload);
}

export function api_getQualificationList(payload, method = 'POST') {
    return request(API_GET_QUALIFICATION, method, payload);
}

export function api_updateQualificationList(payload, method = 'POST') {
    return request(API_UPDATE_QUALIFICATION, method, payload);
}

export function api_updateQualificationSubject(payload, method = 'POST') {
    return request(API_UPDATE_QUALIFICATION_SUBJECT, method, payload);
}

export function api_updateQualificationGrade(payload, method = 'POST') {
    return request(API_UPDATE_QUALIFICATION_GRADE, method, payload);
}

export function api_getLanguageList(payload, method = 'POST') {
    return request(API_GET_LANGUAGE_LIST, method, payload);
}

export function api_updateLanguage(payload, method = 'POST') {
    return request(API_UPDATE_LANGUAGE, method, payload);
}

export function api_updateProficiency(payload, method = 'POST') {
    return request(API_UPDATE_PROFICIENCY, method, payload);
}

export function api_getProficiencyList(payload, method = 'POST') {
    return request(API_GET_PROFICIENCY_LIST, method, payload);
}

export function api_getProficiencyQualificationSubject(payload, method = 'POST') {
    return request(API_GET_PROFICIENCY_QUALIFICATION_SUBJECT, method, payload);
}

export function api_addProfiencyGrade(payload, method = 'POST') {
    return request(API_ADD_PROFICIENCY_GRADE, method, payload);
}

export function api_updateProficiencyGrade(payload, method = 'POST') {
    return request(API_UPDATE_PROFICIENCY_GRADE, method, payload);
}

export function api_getRegionList(payload, method = 'POST') {
    return request(API_GET_REGION_LIST, method, payload);
}

export function api_updateRegion(payload, method = 'POST') {
    return request(API_UPDATE_REGION, method, payload);
}
