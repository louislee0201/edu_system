import {
    API_USER_LOGIN, API_USER_LOGOUT, API_CHECK_LOGIN
} from '@/constants/api'

import request from '@/utils/request'

export function api_login(payload, method) {
    return request(API_USER_LOGIN, method, payload);
}

export function api_logout(payload, method) {
    return request(API_USER_LOGOUT, method, payload);
}

export function api_checkLogin(payload, method) {
    return request(API_CHECK_LOGIN, method, payload);
}