import {
    API_GET_COURSE_LIST, API_UPDATE_COURSE, API_GET_REQUIREMENT_ATTR, API_SUBMIT_REQUIREMENT, API_UPDATE_REQUIREMENT, API_UPDATE_DESCRIPTION
} from '@/constants/api'

import request from '@/utils/request'

export function api_getCourseList(payload, method = 'POST') {
    return request(API_GET_COURSE_LIST, method, payload);
}

export function api_updateCourse(payload, method = 'POST') {
    return request(API_UPDATE_COURSE, method, payload);
}

export function api_getRequirementAttr(payload, method = 'POST') {
    return request(API_GET_REQUIREMENT_ATTR, method, payload);
}

export function api_submitRequirement(payload, method = 'POST') {
    return request(API_SUBMIT_REQUIREMENT, method, payload);
}

export function api_updateRequirement(payload, method = 'POST') {
    return request(API_UPDATE_REQUIREMENT, method, payload);
}

export function api_updateDescription(payload, method = 'POST') {
    return request(API_UPDATE_DESCRIPTION, method, payload);
}
