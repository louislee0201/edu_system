import {
    API_GET_SCHOOL_LIST, API_UPDATE_SCHOOL, API_EDIT_SCHOOL_COURSE_ARRANGEMENT_TITLE
} from '@/constants/api'

import request from '@/utils/request'

export function api_getSchoolList(payload, method = 'POST') {
    return request(API_GET_SCHOOL_LIST, method, payload);
}

export function api_updateSchool(payload, method = 'POST') {
    return request(API_UPDATE_SCHOOL, method, payload);
}

export function api_editSchoolCourseArrangementTitle(payload, method = 'POST') {
    return request(API_EDIT_SCHOOL_COURSE_ARRANGEMENT_TITLE, method, payload);
}
